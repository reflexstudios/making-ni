module.exports = {
  mode: 'jit',
  purge: [ 
  './resources/**/*.antlers.html',
  './resources/**/*.js',
  './resources/**/*.vue',
],

  darkMode: false, // or 'media' or 'class'

  
  theme: {
    extend: {

      screens: {
        '3xl': '1600px',
        'mobile': '712px',
      },     
       
      fontSize: {
          '11xl': '11.25rem',       //  180px   - h1 Homepage
          '9xl': '8.75rem',         //  140px   - Titles
          '7xl': '5rem',            //  80px    - Menu / Titles
          '5xl': '3rem',            //  
          '4xl': '2.5rem',          //  40px    - Title links
          '3xl': '1.875rem',        //  30px    - Title links
          '2xxl': '1.5rem',         //  24px    - Script content
          '2xl': '1.375rem',        //  22px    - Script content
          'bookTitle': '1.2rem',        //  22px    - Book Titles 
          '1xs': '0.8rem',       
          'xxs': '0.6875rem',       //  11px    - Small text
          'xxxs': '0.5625rem',      //  9px     - Extra Small text

          
      },        
      
        scale: {
          '38': '.38',
          '40': '.4',
          
        },
    
      lineHeight: {   
        '36': '9rem',                 // 144px
        '28': '7rem',                 // 112px 
        '19': '4.875rem',             // 78px 
        '16': '4rem',                 // 64px 
        '13': '3.25rem',              // 52px 
      },

      letterSpacing: {                
        widest: '.2em',
       },

      backgroundImage: {
        'star-lg': "url('/assets/images/stars/mapgb.png')",
        'star-sm': "url('/assets/images/stars/bck_stars_mobile.png')",
        'book-bck': "url('/assets/images/placeholders/booksbg.png')",
       },
  
      colors: {
        primary:'#000000',            // Black

        turquoise: '#46B4BA',         // turquoise 
        darkturquoise: '#006673',     // turquoise dark shade

        pink: '#F16B9B',              // Pink
        mediumpink: '#DB5687',              // Pink
        darkpink: '#7B2E4A',          // Pink dark shade

        purple: '#9B6CAD',            // Purple
        mediumpurple: '#87569D',      // Medium Purple
        darkpurple: '#623971',        // Purple dark shade

        shelf: '#b6958d',  // bookshelf
        shelfdark: '#b47d52',  // bookshelf
      },        
     
        spacing: {
          '1.25': '0.3125rem',      //5px          
          '2.5': '0.625rem',        //10px 
          '2.75': '0.6875rem',    	//11px 
          '3.75': '0.9375rem',     	//15px
          '5.25': '1.3125rem',    	//21px           
          '7.5': '1.875rem',        //30px       
          '12.5': '3.125rem',       //50px
          '13.5': '3.375rem',       //54px
          '15': '3.75rem',          //60px 
          '15.5': '3.875rem',       //62px           
          '17.5': '4.375rem',       //70px 
          '22.5': '5.625rem',       //90px 
          '25': '6.25rem',          //100px  
          '33.75': '8.4375rem',     //135px 
          '30': '7.5rem',           //120px
          '50': '12.5rem',          //200px 
          '78': '19.5rem',          //312px 
          '95': '23.75rem',         //380px 
          '200': '50rem',           //800px    

          
        },
        zIndex: {
          '100': '100',
        },
     
        maxWidth: {
            
         }
      },

      
      
     
      fontFamily: {             
        'Metropolis': ['Metropolis', 'sans-serif'],
        'Brixton_lead': ['Brixton_Lead Vector', 'sans-serif'],
        'Caveat': ['Caveat', 'cursive'],
        
      },    
      
  },
  variants: {
    extend: {
      scale: ['active', 'group-hover'],
      textColor: ['group-hover'],
      borderRadius: ['hover', 'focus'],
      mixBlendMode: ['hover', 'focus'],
      translate: ['hover', 'group-hover'],
      opacity: ['hover', 'group-hover'],
      transform: ['hover', 'group-hover'],
      
    },
  },
  plugins: [],
}
