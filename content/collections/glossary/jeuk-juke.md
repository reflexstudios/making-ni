---
id: cf1b1bce-afa4-4da5-9206-55f7b8e982b5
blueprint: glossary
title: 'Jeuk (also, juke)'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654014854
---
dodge, avoid, e.g. by moving quickly out of the way of a blow