---
id: 0e01236e-97c1-43de-9c38-7714b2cf7aa0
blueprint: glossary
title: 'Square up'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654278402
---
settling an outstanding problem (often involving payment of money)