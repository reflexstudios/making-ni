---
id: 50dbc4cf-652c-475b-b3f7-80af85402fd0
blueprint: glossary
title: Canny
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1653556117
---
careful, cautious, comfortable; "ca' canny" is an expression advising caution