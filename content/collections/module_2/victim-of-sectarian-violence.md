---
id: a72e16f0-8185-4481-9279-28aeddb051e8
blueprint: module_2
title: 'Victim of Sectarian Violence'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1653406884
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The narrative of the Ulster Scot as victim went beyond the systematic discrimination of a hostile Government and Established Church. An important part of the narrative has to do with sectarian violence.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'These incidents of sectarian violence were retold so often at the fireside and at street corners that they left indelible traces in the collective imagination. An obvious example of this concerns the events of 1641 which saw a major outbreak of sectarian violence in Ulster. Some of the more extreme accounts do not hesitate to go into elaborate detail about the appalling events of the period. However many, clearly conscious of the sensitivity of the issue, take a more restrained position.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Some, like Rev. Woodburn, writing in The Ulster Scot in 1914, when feelings were running particularly high around the Home Rule issue, seek to explain to their readers what he sees as the reasons why Irish Catholics had chosen to rebel against the authorities. He focuses on two fundamental issues: an existential threat to their religion – “the fear of the Catholics that their religion was to be extirpated” - and the resentment of the dispossessed – “the desire of the dispossessed to get back the land that had been confiscated in the Plantation of Ulster.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Woodburn also tries to defuse some of the tension around the discussion of 1641 by underlining, for example, how the numbers of Protestant victims had been grossly exaggerated from the start. He refers to the work of Ferdinand Warner, author of The History of the Rebellion and Civil War in Ireland, 1768, who “examined [the depositions] with great care and found them contradictory and incredible.” The fact that they had been “sworn ten or twelve years after the event” renders them, says Woodburn, suspect. This does not take away from the fact that “the crimes which disgraced the rebellion were atrocious and horrible.” It is simply that he is trying to place as neutral a frame as possible on what is particularly explosive material, especially given the tensions of the period in which his study is being published.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“It is certain that the two main causes of the rebellion were the fear of the Catholics that their religion was to be extirpated, and the desire of the dispossessed to get back the land that had been confiscated in the Plantation of Ulster. They felt that their only hope was to assume the offensive, and they thought the time opportune when the English Parliament and the English King were embroiled with one another.” (Woodburn, p. 102)."
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He goes on to explain that the Scots in Ulster were initially given special treatment, being left “entirely unmolested.” He suggests possible reasons for this: fear based on the conviction that the Scots were better organised and better armed than the English settlers, and hope that, because the Scots Presbyterians had also suffered from the Anglican authorities - cf. the tensions surrounding the “Black Oath” in 1639 - they might rise against the English authorities. However, the Scots quickly become targets.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The rising took place in the Northern province in the end of October [1641]. The planters were unprepared for it, and in the central parts of Ulster they were driven out of their homes, and their property seized. The leader in the North was Sir Phelim O’Neill… a weakling who could not keep his men under control. During the first fortnight the chief object of the rebels seems to have been plunder, few persons being murdered, and hostilities were directed against the English settlers only, while the Scots were left entirely unmolested. There are probably two reasons why in the beginning the Scots were thus favoured, first, the rebels feared to attack them, and, second, they hoped that those settlers who had suffered religious persecution under the Government might be induced to take part with them against the English […] But very soon no difference was made between Scots and English. After the first fourteen days the rebellion was disgraced by murders and great cruelties.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'No one can read the list of depositions that were taken down from 1652 to 1654 regarding the loss of life and property in connection with this rebellion without coming to the conclusion that the cruelties practised by the rebels were savage and brutal. We read constantly of stripping, drowning, hanging, and worse barbarities. Too much reliance must not be placed, however, on these depositions which were sworn ten or twelve years after the event; [Ferdinand] Warner examined them with great care and found them contradictory and incredible. Yet there must be an element of truth in them, and even allowing for great exaggeration, we are forced to the conclusion that the crimes which disgraced the rebellion were atrocious and horrible. (Woodburn, pp. 103-105).'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Sir Phelim O''Neill by unknown Artist of the English School - National Portrait Gallery, Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=39253013.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=39253013.'
    uuid: 104727f3-f65a-434c-83d4-7f2498cbbc32
    type: content
    enabled: true
    image: Sir_Phelim_ONeill.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Those writing from an Ulster-Scots standpoint often tie the events of 1641 in to the outbreak of anti-Protestant violence in Wexford during the rising in 1798.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The third volume of Reid’s, History of the Presbyterian Church in Ireland, published in 1853, at a period before the issue of Home Rule had exacerbated feelings on the issue, remains relatively allusive on the subject of the sectarian violence that broke out in the Wexford area after the defeat of the rebel forces at New Ross, in June 1798.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“When the northern rebels heard of the cruelties perpetrated on their Protestant brethren, in other parts of Ireland, by the Roman Catholic insurgents, they threw down their arms in disgust and indignation.” (Reid, Vol. III, p. 426)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Other authors, especially those writing in the context of the heightened tensions due to the Home Rule crisis, are much more direct in the narrative they give of the events in the south. Some, like the Rev. Hamilton (History of the Irish Presbyterian Church), do not hesitate to go into detail about the violence directed at the Protestant victims in incidents like Scullabogue or on Wexford Bridge. Although a small number of Catholic loyalists were among the victims at Scullabogue, authors did not always refer to their fate. The terms in which they describe this violence recall the most vivid accounts of the violence associated with 1641. Such parallels were indeed quickly drawn. As Latimer says in A History of the Irish Presbyterians:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“But even in Protestant districts, there was soon a reaction against the principles of the United Irishmen. Four generations had come and gone since the massacre of 1641, which was then almost forgotten; but fresh massacres in the South again roused the fears and excited the anger of the sturdy Ulster Protestants.” (Latimer, p.182).'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Given the graphic nature of much of the writing on the subject, the following passage from what is sometimes described as a “classic” of Ulster-Scots historiography… Rev. Woodburn’s, The Ulster Scot: His History and Religion (1914), presents the problem in a blunt, but comparatively restrained manner:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“The reaction in the North was also caused by the indiscriminate massacres of the Protestants by the Catholics in County Wexford, where the struggle had gradually assumed the character of a religious war. The scenes on Vinegar Hill and at Scullabogue impressed the Northern mind, and the Ulster Scots began to ask themselves the question whether, if the rebellion succeeded, it would not be for them one of the greatest of calamities.” (Woodburn, p. 309).'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Many obviously considered that this lesson could be applied directly to the Home Rule period. The more outspoken unionists did not hesitate to encourage fears of renewed sectarian violence in the event of Home Rule. In this context, it is perhaps no coincidence that Lord Ernest Hamilton, unionist MP for North Tyrone 1885-1892, author of The Soul of Ulster (1917), and one of the most outspoken advocates of the unionist cause in Ulster, should publish The Irish Rebellion of 1641, in 1920, the year the legislation creating Northern Ireland was passed.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The trauma of the events around the 1798 Rising, and the parallels drawn with 1641, contributed directly to the rejection of republican ideology that was to characterise Ulster-Scots thinking over the course of the 19th century. From that time on, there was never to be a collective commitment to the republican or the nationalist cause. Although individuals with an Ulster-Scots background might have adopted a nationalist stance, that choice was personal and never again extended to any sizeable proportion of the Ulster-Scots community.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Massacre at Scullabogue by George Cruikshank - Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=36547366.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=36547366.'
      -
        type: paragraph
    uuid: 75cfaad6-0da0-4d77-8777-9cf6808cf4bd
    type: content
    enabled: true
    image: MAXWELL(1845)_p162_Massacre_at_Scullabogue-min.jpg
    allow_zoom: true
---
