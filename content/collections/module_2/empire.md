---
id: c772540e-5895-4901-b084-c790639d41f4
blueprint: module_2
title: Empire
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654871878
audio: MOD2-EP13-SCR3.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'One of the main targets of the unionist campaign against Home Rule was the idea that Ireland had a separate identity, one that, according to nationalists, set it apart from the rest of the United Kingdom. Unionists argued that, on the contrary, Ireland was an essential constituent part of the United Kingdom and that Irish identity was one facet of a broader “British” identity.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Belonging to the Empire is central to this conception of Britishness. This theme becomes increasingly important in the course of the Home Rule debate. It does so in different ways.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'One of the ways in which Ulster Scots approached this theme was by drawing attention to the contribution of individuals from the Ulster-Scots community in the construction of the Empire. This became part of a wider demonstration of unionist loyalty.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Rev. Woodburn in, The Ulster Scot, His History and Religion (1914), includes a chapter entitled, “Famous Ulster Scots,” which features the following short passage on Lord Dufferin, who was a descendant of James Hamilton, one of the key figures in the early settlement of the Ards Peninsula. '
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bts_span
                    attrs:
                      class: null
                text: "There were many other distinguished Ulstermen in India in those and later days […] The Northern province can point with pride to Lord Dufferin, Viceroy of India and Governor-General of Canada […] He was a tactful and sagacious diplomatist, and was British Ambassador first at St Petersburg, and afterwards at Constantinople, Rome, and Paris […] “At Rome it was said of him, ‘He would have made a frozen bear good-tempered.’ […] It is undeniable that his far-sightedness in Canada, Egypt, and India immeasurably strengthened the roots of empire in those distant regions\_[…]” He was “one of the ablest, most versatile most successful, and most fascinating of our public men in the latter half of the reign of Queen Victoria.” (Woodburn, pp. 388-389)"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Dufferin had held key posts in the administration in various parts of the Empire, in Canada, Egypt and India. The adjectives Woodburn chooses – “tactful and sagacious,” “far-sighted,” “versatile,” “fascinating” – are uniformly positive and reflect his obvious admiration for Dufferin’s contribution to the defence of British interests in different parts of the world. Indeed, as far as Woodburn is concerned, Dufferin’s main merit is clearly that he used his talents to “strengthen the roots of empire in those distant regions.” These comments reflect the positive view of Empire held by many in the Ulster-Scots élite who saw it as a powerful source of support for Ulster unionism in its resistance to Home Rule.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Closer to home, Dufferin was also responsible for building Helen’s Tower on the family estate in the Ards. Significantly, as we shall see in Module 3, this tower was to be the model for the Ulster Tower, built on the Somme to commemorate the Ulstermen killed during the First World War.'
      -
        type: set
        attrs:
          values:
            type: notes
            bard:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'PHOTO:'
                  -
                    type: text
                    text: ' Lord Dufferin By The Notman studio -  Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=10470890.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=10470890.'
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Lord Dufferin By The Notman studio -  Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=10470890.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=10470890.'
    uuid: 69f6021c-ed47-4879-8ed2-c9283bdb9571
    type: content
    image: Lord_Dufferin.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Another angle that emerged over the period was the strength of the link between Ulster and people of Ulster-Scots descent whose ancestors had settled in various parts of the Empire.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Thus, during the Second Congress of the Scotch-Irish Society of America (1891), Rev. Stuart Acheson, who was a Presbyterian minister in Toronto, Canada, and whose Scots ancestors had settled in Counties Down and Derry before emigrating to North America, delivers a speech entitled, “The Scotch-Irish in Canada.”'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Mr. President, Ladies, and Gentlemen: There lies to the North of the great republic [the USA] my fair Canada. She is even now great in her infancy; and for me to write the achievements of the Scotch-Irish race […] is certainly no uninviting task. I speak of Canada as being, in infancy, a nation, and yet no mother ever held in her loving arms a child of greater promise. The idea of a mother will at once call up our British connection […] Our lot, in the providence of God, has been cast as forming an integral part of the British Empire. […] Her genius of constitutional government is at work in Canada, and under her genius we are developing a stability and a freedom unequaled among the free nations of the earth […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We have heard with pride the heroic deeds of valor recounted of the Scotch-Irish race in this struggle for freedom and independence. Whether in Congress at Philadelphia, in Parliament at Westminster, under every flag, in every land, and in every Church, the Scotch-Irish race has stood up to its conviction and planted itself upon the side of freedom and constitutional government.” (Acheson, pp. 195-197)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The fact that Rev. Acheson is based in Canada offers him a perspective on “Scotch-Irishness” that is clearly different from the dominant view among the American delegates. In the speech, he imagines Britain as the “motherland,” and refers at two points to the idea of Canada being “in her infancy.” He does not hesitate to celebrate what he calls “our British connection.” Indeed, he attributes Canada’s place within the Empire to “the providence of God.” Most significantly, he “stretches” the idea of Scotch-Irishness by suggesting that the fact of being Scotch-Irish does not prevent people from feeling they belong to the Empire. He is eager to find what we might call a common denominator capable of covering the entire Ulster-Scot/Scotch-Irish experience. He identifies this as being their commitment to “freedom and constitutional government,” no matter what form of government – republic or monarchy – they may choose to identify with. What he is saying is that there is a variety of Ulster-Scot/Scotch-Irish experience that can accommodate a range of allegiances.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The passage gives us an insight into the mind-set that gave cohesion to the Empire and that was such a potent source of support for the unionist cause at the Home Rule period.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is important to remember that the memories of Scotch-Irish resistance to the British Empire will be looked at from a different perspective when America enters the First World War alongside Britain.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: " \_The Relief of Ladysmith. Sir George White greets Lord Douglas Hamilton on 28 February 1900. By John Henry Frederick Bacon - Art UK, Public Domain, "
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=427066'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=427066'
      -
        type: paragraph
    uuid: fde3ca7b-2f28-4896-897b-23ac2f438dcc
    type: content
    image: The_Relief_of_Ladysmith_by_John_Henry_Frederick_Bacon.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Links with the Empire can also be given prominence within a much more local frame. This is the case with the poem entitled, “Defender o’ Ladysmith,” by Thomas Given.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Given, who lived in the village of Cullybackey in Co. Antrim, published a collection of poetry in 1900 entitled, Poems from College and Country by Three Brothers. Many of his poems are in Ulster-Scots. Given’s work is of considerable interest. Like that of Adam Lynn, another Ulster-Scots poet who was also from Cullybackey, his poetry tells us a lot about the preoccupations and mind-set of people living in a largely Ulster-Scots rural community at the beginning of the twentieth century.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The poem below is one of a number Given wrote about a “local hero,” General Sir George White. White had been born in Portstewart, but the family home was in the village of Broughshane, a few miles from Cullybackey. He had won the Victoria Cross for bravery in India and was knighted in 1886 for services in Burma (Myanmar). Here, Given celebrates his successful resistance at Ladysmith in South Africa, the site of a four-month siege by the Boers during the Second Boer War (1899-1902).'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'While the other poems addressed to White are in English, this one, “Defender o’ Ladysmith,” is in Ulster-Scots. As is usual in his work in Ulster-Scots, Given finds his way into the subject of the poem through references to nature, in particular the birds that inhabit the hedgerows and that he imagines singing to each other the news that White has returned from South Africa.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The poem shows us the sometimes surprising connections that existed between the hyper-local and the global. Although, the poem is designed first and foremost to celebrate the exploits of the “local hero,” it allows Given to demonstrate his support for the imperial project and underline his loyalty to Britain. He does not hesitate to introduce the idea of “nae surrender,” applying the rallying call of the besieged in Londonderry to White’s stand in South Africa.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Defender o’ Ladysmith'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Say is there truth in the souch that I hear,'
              -
                type: hard_break
              -
                type: text
                text: 'Blawn by the storm in the youth o’ the year,'
              -
                type: hard_break
              -
                type: text
                text: 'Sung tae by wee throats o’ mony a name,'
              -
                type: hard_break
              -
                type: text
                text: "Defender o’ Ladysmith welcome tae hame?\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '[…]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The slae thorn noo burstin’ in blossoms sae braw,'
              -
                type: hard_break
              -
                type: text
                text: 'Cries oot tae the mavis, the blackbird, an a’,'
              -
                type: hard_break
              -
                type: text
                text: 'Tae spare na their whustle, but cock up their “came”'
              -
                type: hard_break
              -
                type: text
                text: 'An’ chant a braw welcome tae him comin’ hame.'
          -
            type: paragraph
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Stan’ oot, ye wun blossoms, ’mid spin-drift and hail,'
              -
                type: hard_break
              -
                type: text
                text: 'An’ tell tae the mountains yer heart heesin’ tale,'
              -
                type: hard_break
              -
                type: text
                text: 'That Boyd’s Hill an’ Slemish, wi’ Trostan the same,'
              -
                type: hard_break
              -
                type: text
                text: 'May a’ clap their han’s whun oor leader comes hame.'
          -
            type: paragraph
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Ye burnies wha launch in the licht o’ the morn,'
              -
                type: hard_break
              -
                type: text
                text: 'Or juke intae eddies ’ neath elder or thorn,'
              -
                type: hard_break
              -
                type: text
                text: 'Tell the wag tail an’ swallow tae loudly proclaim'
              -
                type: hard_break
              -
                type: text
                text: 'The joy we a’ join in at worth comin’ hame.'
          -
            type: paragraph
          -
            type: paragraph
            content:
              -
                type: text
                text: 'How he upheld the flag o’ his fathers, an yours –'
              -
                type: hard_break
              -
                type: text
                text: 'Nae surrender wi’ him, tho’ the hale worl’ wur Boers;'
              -
                type: hard_break
              -
                type: text
                text: 'Nae wunner we’re proud o’ oor countryman’s fame,'
              -
                type: hard_break
              -
                type: text
                text: 'An thankfu’ tae Heaven for his comin hame. (Given, p. 259)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The poem also serves to remind us of the depth of difference between opinion in Ulster-Scots circles and opinion among Irish nationalists, many of whom supported the Boers’ struggle against the British Empire. A final irony lies in the fact that White’s son, Captain Jack White, was to become the military advisor of the republican, Irish Citizen Army, founded by James Connolly in 1913.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Sir George William White By H. H. S. Pearse. Photo credit Window & Grove -  Four Months Besieged by H. H. S. Pearse - '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'http://www.gutenberg.org/etext/16466'
                          rel: null
                          target: null
                          title: null
                    text: 'http://www.gutenberg.org/etext/16466'
                  -
                    type: text
                    text: ', Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=7355295.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=7355295.'
    uuid: 1fd0e5d5-86bc-4190-8a0d-d98c696cf97b
    type: content
    image: George_Stewart_Whitejpg.jpg
    audio_file:
      - audio/MOD2-EP13-SCR3.mp3
    enabled: true
    allow_zoom: true
---
