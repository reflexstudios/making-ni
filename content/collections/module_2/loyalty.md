---
id: 8155344a-0cf3-4137-bd5b-5483c0d643b7
blueprint: module_2
title: Loyalty
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654871485
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'During the campaign organised against Home Rule a lot was heard on the unionist side about their loyalty to the Crown and the Empire. However, as far as the Ulster-Scots component of unionism was concerned, the issue of loyalty had, historically, been anything but simple.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Throughout the course of their history, the Ulster Scots had been confronted with accusations that they were indeed disloyal. From the 17th century, they had been repeatedly forced to answer these accusations addressed against them by some of the most prominent writers and thinkers of the age. Thus, for example, John Milton, the author of Paradise Lost, in his “Answer to the Address of the Belfast Presbytery” (1649), accuses what he calls “the Scottish inhabitants of [Ulster],” and more particularly “these blockish Presbyters of Clandeboye” of “sedition.” '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
          -
            type: text
            text: 'Similarly, the virulently anti-Presbyterian Jonathan Swift, author of Gulliver’s Travels, in his Presbyterians’ Plea of Merit impartially explained (1733), refers to the “Petulancy and Scurrility” of what he calls “these Sectaries” and claims: “that a Scottish, or Northern Presbyterian, hates our Episcopal Establish''d Church, more than Popery itself.” '
          -
            type: text
            marks:
              -
                type: bold
            text: (2)
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Thus the Scots community in Ulster was repeatedly suspected of disloyalty. The democratic functioning of their Church and their refusal to recognise the monarch as Head of the Church laid them open to the charge of having a republican agenda which, needless to say, was seen as a real threat to the established order.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'These accusations were vigorously denied. The synods of the Presbyterian Church regularly produced lengthy statements proclaiming their loyalty. Individual ministers wrote detailed analyses that attempted to prove “Presbyterian loyalty.” However, we can guess something of the sensitivity of the question from the following comments by Rev. Lecky, Minister of Ballylennon in Donegal, in his, In the Days of the Laggan Presbytery (1908). Lecky says that when a certain Rev. Kirkpatrick published his book, Presbyterian Loyalty, in 1713, in the aftermath of the controversy around the Test Act, he was well advised to do so anonymously:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“The printing press was still but in its infancy, and printing was then a costly process; besides, it was a dangerous one. If a Presbyterian ventured on printing any production of his pen, his safest course was not to append his name to it, especially if it cast any reflections on the Government or on the Established Church. Thus when Kirkpatrick''s “Presbyterian Loyalty” was published in 1713, it was issued from the press without either name of author or printer appended. Printers feared that if they produced any work that gave offence to the authorities, either of Church or State, a troop of soldiers would be sent to smash up their type and press, or else steps would be taken in some other way to injure their business.” (Lecky, p. 37)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' “John Milton’s, Answer to the Address of the Belfast Presbytery” [1649], The Belfast Monthly Magazine, Vol. 10, No. 56 (Mar. 31, 1813), pp. 207-215, 213 & 215.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (2)
                  -
                    type: text
                    text: ' See “The Presbyterian''s plea of merit; in order to take off the test, (in Ireland,) impartially examined,” London 1733.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': : [Rev. James Kirkpatrick], An Historical Essay Upon the Loyalty of Presbyterians in Great Britain and Ireland from the Reformation to this Present Year 1713'
      -
        type: paragraph
    uuid: 1169a37c-3591-4993-a6ce-a590119d9efb
    type: content
    enabled: true
    image: Picture-3.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, apart from these sporadic denials of their disloyalty, much of the history of the Ulster Scots is presented as the best evidence to prove they have always been loyal. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Thus, the Plantation is to be read as the story of the Ulster Scot opening up and defending a territory in the interests of the British Crown.'
      -
        type: paragraph
        content:
          -
            type: text
            text: '1641 is to be read as the Ulster Scot resisting rebellion and standing firm against the enemies of the State. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Ulster-Scots participation in the siege of Derry is presented in the same light.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In each case, Ulster-Scots history underlines the key role played by their community in times of crisis. It emerges as a stabilising force and one on which the authorities in London could count in times of difficulty to act as a garrison defending British interests in Ireland.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'There is, of course, the exception of Ulster Scots involvement in the Rising of 1798 when many in the Ulster-Scots community took up arms against the British government. However, even here, the Presbyterian Church went out of its way to distance itself from the actions of a section of its people. Thus, the General Synod of Ulster, which met in Lurgan in August 1798, a few weeks after the Rising had collapsed in Ulster, sent the following “Humble Address” to the King. '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Most Gracious Sovereign'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We your Majesty’s Dutiful & Loyal Subjects the Ministers & Elders of the General Synod of Ulster, though prevented by a Daring & Flagitious [i.e. wicked, vicious] Rebellion raging in two Counties of this Province, from assembling at [the intended time], eagerly embrace the first Opportunity afforded by the return of Tranquility to lay our Sentiments of Loyalty to your Majesty, and of sorrow & shame for the Calamities & Crimes of our Country, at the foot of the Throne.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We cheerfully renew those Declarations of Fidelity to the Crown & Attachment to the Constitution, from which we have never swerved. We […] appeal to History to attest the Inviolable Attachment of the Presbyterians of Ireland to Monarchy, Counselled by an Hereditary Nobility, & supported & limited by an Elective Representative of the Commons.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'But while we dwell with Exultation on those Periods of our Annals in which our Ancestors […] vigorously supported the Exertions of King William, & strenuously defended your Majesty''s Illustrious Progenitors […] we are constrained to lament, with the deepest Humiliation, that the most Stable & Sacred Principles of many of our People & of some of our members, have been shaken by the Convulsions of this Sceptical & Revolutionary aera.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Though we cannot presume to suggest the general Infatuation of a great part of Europe, nor the incessant & deceitful artifices employed in this country to seduce our People, as Apologies for crimes which we ourselves deem inexcusable, […] we venture to entreat your Majesty not to impute to the Whole the Transgressions of a part […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Let the madness of the Multitude be hidden from your eyes, by the courage, & sufferings of those of our Communion who have fought & died in defence of their King & Country, their Liberty, & Religion […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Signed in our name & by our appointment,'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'THOMAS CUMING, Mod[erato]r. (Synod of Ulster, pp. 208-209)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Presbyterian Church in Ireland. Synod of Ulster. Records of the General Synod of Ulster: From 1691 to 1820, Vol. III, 1778-1820, Belfast, 1898.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Portrait of King William III, 1690, by Godfrey Kneller - '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://www.rct.uk/collection/405675/william-iii-1650-1702-0'
                          rel: null
                          target: null
                          title: null
                    text: 'https://www.rct.uk/collection/405675/william-iii-1650-1702-0'
                  -
                    type: text
                    text: ', Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=111000201.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=111000201.'
      -
        type: paragraph
    uuid: 852b1280-39e4-47f0-92d2-6e93ca976a30
    type: content
    enabled: true
    image: King_William_III_of_England.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Synod bends over backwards to placate the authorities. The vocabulary chosen to describe the Rebellion – “flagitious [i.e. wicked, vicious],” “raging” – leaves the reader in no doubt as to its position. The Synod maintains that it has remained constant in its loyalty to the Crown and the Constitution – “from which we have never swerved.” It reinforces this statement by appealing to “History,” which, it claims, proves its attachment to the established order of the Constitution – Crown, Lords and Commons. “History” is, no doubt, a reference to the crises, such as the Rising in 1641 and the Williamite Wars, during which the Presbyterians defended the interests of Protestantism in Ireland.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Nevertheless, the Synod is forced to recognise that some of its people and, more worryingly, some of its ministers, have been seduced by revolutionary theories. It puts this down to “the Convulsions of this Sceptical & Revolutionary aera” and “the general Infatuation of a great part of Europe.” The Synod also denounced what they saw as the strategies of deceit employed by the revolutionaries to seduce the people into rebellion. But, although they recognise that "many of our People" and "some of our members" were misled by the revolutionary project, others remained loyal to the Crown. Basically, they implore the King, “not to impute to the Whole the Transgressions of a part.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Minimising the Presbyterian role in the Rising, they end by claiming that the Presbyterians have overwhelmingly been a source of stability and one that has defended the interests of the Protestant religion in Ireland.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Ulster-Scots’ involvement in 1798 was gradually re-written over the course of the 19th century, with people like Rev. Reid claiming that Presbyterian involvement in the rebellion had been greatly exaggerated and that the bulk of the Presbyterian population and especially the Church authorities had in fact remained loyal to the Government.'
    uuid: 173bc6f1-d4f4-4ca0-8420-846b5ff09285
    type: content
    enabled: true
    image: MOD2-FRA8-SCR4-1639668971.jpg
    allow_zoom: true
---
