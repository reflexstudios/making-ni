---
id: b86b009a-9dd3-4d43-8136-1f47e8cd6728
blueprint: module_2
title: 'Victim of Discrimination'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654870804
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is probably not surprising that the histories of the Presbyterian Church - or of the Reformed Presbyterian Church (cf. the Covenanters), for that matter - should devote so much attention to the “grievances” suffered by the Presbyterian community in Ireland. All of the authors from Reid, whose three-volume History of the Presbyterian Church in Ireland was published between the 1830s and the 1850s, to Woodburn, who was writing around the time of the First World War, insist on how, historically, the Established Church (the Church of Ireland) had used its power to target nonconforming Protestants. Because the Ulster Presbyterians formed the largest, most concentrated and most influential group of nonconformists in Ireland, they were to come in for particular attention.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'One of the most important examples of this religious discrimination directed at Presbyterians was, curiously, linked to an "Act to prevent the further growth of Popery," passed in 1704 (See opposite). This "Test Act," so called because it imposed conditions (a Test) on certain categories of the Queen''s subjects, was initially directed solely against Roman Catholics. However, the English authorities extended some of its provisions to nonconformists, including therefore the Scots Presbyterians in Ulster. Because Presbyterians did not “conform [themselves] to the church of Ireland as by law established,” they were excluded from “any office, civil or military” and could not “receiv[e] any pay or salary from the crown.” Overnight, Presbyterians found themselves excluded from all types of public office.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As J. W. Kernohan explains in the following extract from, The County of Londonderry in Three Centuries, published in 1921, this treatment was seen as particularly unjust especially as it came so soon after Presbyterian involvement in the defence of the Williamite cause at Londonderry in 1689. In order to underline the depth of this injustice, Kernohan does not hesitate to quote estimates that suggest that the Presbyterian contingent within the walls of the besieged city was “twenty to one.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'His reference to “defections” is an allusion to people leaving the Presbyterian Church and joining the Church of Ireland for social advancement. He says that the Church of Ireland opened up an entire range of career possibilities which were henceforth closed to members of the Presbyterian Church. He says that the only areas left to the Presbyterians, “trade and farming,” did not offer sufficient opportunities for the most ambitious in the community. As a result, many preferred to try their luck in America. It is interesting that he should say that emigration, which began in earnest in 1718, in the aftermath of the Test Act, continued “at an appalling rate” throughout the 18th century. Kernohan is of course reflecting the point of view of many in Ireland who watched with growing alarm the continuous drain on the Presbyterian population through mass emigration.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“The Presbyterians were twenty to one within the walls of Derry, and the support they gave to the Revolution settlement should have secured them full political privileges. King William was up against the jealousy of the [Anglican] Church party at every point. They saw their own parishioners growing fewer while the influx of Scotchmen made the Nonconformists so numerous that one of their bishops calculated they were as forty to one of the Episcopalians […] A Test Act was introduced and passed 1704 (after the death of William) which expelled them from all public offices and employments […] Even in Derry, the city which they had defended by their heroism, they were excluded from offices of honour unless they violated their conscience. The disabilities inflicted need not be specified further than to say that office in the army and navy, in excise or customs, and in the courts of law, was denied to a Presbyterian. Episcopacy [here, membership of the Church of Ireland] being thus the way to public life, as well as being the religion of a “gentleman,” it is not surprising that there were for social reasons many defections from the officially ostracised body. On the other hand, there were still more who loyally adhered to their faith and friends. The attractions of trade and farming were not such as to counterbalance the denial of social and political freedom. News was being brought of better chances in the Western continent. Encouragement would be given for settling in regions which […] gave that measure of freedom which was refused at home. Then began the stream of emigration which continued at an appalling rate during the eighteenth century.” (Kernohan, pp. 46-47).'
    uuid: 3109416b-0ce8-4467-a18b-085162751b0c
    type: content
    enabled: true
    image: Picture-1-1646817161.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Such discrimination on religious grounds affected the life of the Ulster-Scots community well beyond the sphere of public office. It was the basis of a broader system of economic and social discrimination that made them into what today we would call “second-class citizens.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following speech, “The Making of the Ulsterman,” by Rev. John Samuel MacIntosh, was delivered at the second congress of the Scotch-Irish Society of America in Pittsburg in 1890.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'MacIntosh’s career is a perfect illustration of the complex networks that tied the Ulster-Scots community in Ulster in to the Scots-Irish diaspora. Although he had been born in Philadelphia in 1839, he “returned” with his mother to Ulster where he was ordained on 5th November 1862. He was chosen as minister of Connor Presbyterian Church in County Antrim, and went on (1868) to take over from the famous Rev. Henry Cooke, in May Street, Belfast, one of the most well-known Presbyterian Churches in the province. During a visit to America in 1880 to attend the Council of the Presbyterian Alliance, he received a call as minister to the Second Church in Philadelphia, which he accepted. He was President of the Scotch-Irish Society of Pennsylvania and Vice President General of the Scotch-Irish Society of America.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The speech is designed to explain to an American audience why the Ulster Scots chose to leave Ireland in such numbers at the beginning of the 18th century and why they became the most determined supporters of American independence later in the century.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "“The dark and wicked forces change the Ulsterman from the contented colonist to the exasperated emigrant […]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Landlords and bishops made common cause to spoil the Ulster [tenant farmers]. As the thrifty and toiling farmer improved his funds he was taxed on his invested capital by the ever-swelling rent till he was rackrented; and then if he would not pay the legalised robbery he was mercilessly evicted. His father and he had made a waste a garden while the proprietor idled. Then by law the idler claimed the fruits of hard toil […] Added to these agrarian wrongs, were the denial of education, the shutting of schools [and] the barring of college by [religious] tests […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The right of free and independent voting was refused […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The baptism of his children was made a laughing-stock, and the legality of marriage by non-episcopal clergy [for example, Presbyterian ministers] officially denied. I have seen calm men, not many years back, grind their teeth as they spoke of this […] Do you wonder at this intense, burning exasperation?'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'And the Ulsterman who endured all this shame and wrong and open robbery, was the very man who had made and who had kept the land. He had made it. When he came ‘twas a war-wasted desert; when he was driven to our shores from it, he left behind him homesteads and fertile fields.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'He had kept it, and Derry is the proof.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Derry, whose salvation belongs not to Walker [a Church of Ireland minister], but to the Rev. James Gordon [a Presbyterian minister] and his Presbyterian “boys;” for Gordon led to the closing of the gates, and Gordon led the ships to the breaking of “the boom” and the relief of the garrison.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Yet, after that very siege and that very defence […] the men and the party that were the real saviours of the country and the keepers of the pass, were wronged and wronged, till their hearts blazed with justest anger.” (MacIntosh, pp. 99-103)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Title page of: The Parallel: or, Persecution of Protestants: The Shortest Way to prevent the Growth of Popery in Ireland, a pamphlet published anonymously in 1705 by Daniel Defoe, author of Robinson Crusoe (1719). In this pamphlet, Defoe defended the loyal behaviour of the Ulster Presbyterians and argued that extending the application of the Test Act to include them represented a direct threat to nonconformists in England.'
    uuid: a42d1518-5d0e-45ff-a168-4a4f70977a5a
    type: content
    enabled: true
    image: IMG_3410.JPG
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In this highly polemical text, the style of the oratory is very emotive, clearly seeking to stir up indignation in his listeners. MacIntosh identifies a list of grievances perpetrated against “the Ulsterman,” a term he uses as a synonym for the Ulster Scot.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'These grievances allegedly resulted from the actions of the English State, which he sees as being associated with “dark and wicked forces.“'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The laws against the nonconformists created disabilities in a number of areas, resulting in the deprivation of many of their civic, commercial and economic freedoms. However, he sees the (supposedly Anglican) landlords as the willing accomplices of the Church and State. His use of the term “rackrented” and his denunciation of “legalised robbery” in association with evictions recall similar attacks by Irish nationalists at the same period.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'MacIntosh links this attack on the landlords in to a familiar theme – the argument that this treatment is particularly unjust because it had been the Ulster Scot who had created Ulster’s prosperity in the first place. It was, he says, the Ulster Scot who settled in “a war-wasted desert” and made “a waste a garden while the proprietor idled.” He says that once that prosperity has been achieved, the “idler”, i.e. the Church of Ireland landlord, was ready to take control of it.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He goes on to list a series of disabilities in the areas of education and voting rights but focuses particular attention on the sensitive issue of the Established Church’s position on the validity of Presbyterian marriages. Indeed, it was not until 1782 that marriages between Presbyterians performed by Presbyterian ministers were recognised as legal, and not until the Marriage Act, 1844, that marriages between a member of the Church of Ireland and a Presbyterian were given legal recognition.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He goes on to refer to the restrictions on Irish trade imposed by commercial interests in England. His attack is therefore infinitely more detailed than that of Kernohan. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'When he talks about the Ulster Scots as being “the keepers of the pass,” MacIntosh may be comparing their resistance at Derry to the famous battle at the narrow pass of Thermopylae in 480 B.C. when a small Greek army, although vastly outnumbered, managed to block the advance of the invading Persian army for several days.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As Kernohan had pointed out in the earlier text, frustration at perceived injustice led directly to the mass emigration of Ulster Scots from 1717 on.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'At the end of the century, a similar sense of frustration at what is seen as an unjust political system ends up pushing many Ulster Scots into participating in the republican Rising that takes place in 1798.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: "Advertisements in the Londonderry Sentinel, Saturday 3 April 1830, showing the large range of possibilities open to people hoping to emigrate from Ulster at this time.\_"
      -
        type: paragraph
    uuid: 22c24c44-2f5c-4d83-b344-7e53cbbf2112
    type: content
    enabled: true
    image: Screenshot-2022-05-16-at-16.14.10.png
    allow_zoom: false
---
