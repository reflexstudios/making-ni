---
id: 0ec0c184-36db-43c3-a59a-dfdff20a96a6
blueprint: module_2
title: Rebel
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655629275
audio: MOD2-EP8-SCR1.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'One of the key representations of the Ulster Scot is as a rebel.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The obvious example of this fundamental characteristic is Ulster-Scots involvement in the republican Rising – the “Turn-oot” - of 1798.  At this period, many members of the Presbyterian community, especially in Counties Down and Antrim, including a certain number of prominent ministers, were sympathetic to the democratic ideas of the United Irishmen. The United Irishmen were formed in Belfast in 1791 to campaign for radical parliamentary reform. The Society of United Irishmen identified sectarian tensions in Ireland as the fundamental source of the country’s problems. Its members accused England of stirring up these tensions in order to control Ireland more easily. The United Irishmen, many of whose leaders in Ulster were of Ulster-Scots descent and sympathetic to Catholic Emancipation, sought to end sectarian tensions by uniting “Protestant, Catholic and Dissenter” under the “common name” of Irishman. From 1795, the movement went underground and adopted an explicitly revolutionary position, working for the creation of an Irish Republic based on the model of the French Republic which had come into existence in 1789 at the time of the Revolution.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The grass-roots sympathy for the movement that existed in Scots Presbyterian areas, especially in Antrim and Down, comes out strongly in literature by Ulster-Scots authors of the period. They write with great passion about their personal involvement and about the disillusion they feel after the collapse of the movement. We think for example of the weaver poet, James Orr, from Ballycarry (1770–1816), who took part in the Rising, and who managed to escape the repression after the collapse of the movement by going into exile in America.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Donegore Hill was the site of the camp where the local United men gathered prior to the Battle of Antrim where they were to suffer their first major defeat. In the poem, “Donegore Hill,” '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
          -
            type: text
            text: ' Orr describes the disarray within his community when it is faced with the life and death realities of a revolutionary enterprise. We see them as they prepare for the battle. Some are totally committed and ready for a fight with pikes or guns or whatever they can get their hands on. Others are much less enthusiastic and stay “like hens in byre-neuks.” Others need encouragement from their women-folk, some of whom are clearly very worried about what is going to happen to their loved ones. This is first rate writing that challenges simplistic readings of historical events.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'While close-leagu’d crappies '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (2)
              -
                type: text
                text: ' rais’d the hoards'
              -
                type: hard_break
              -
                type: text
                text: 'O’ pikes, pike-shafts, forks, firelocks,'
              -
                type: hard_break
              -
                type: text
                text: 'Some melted lead - some saw’d deal-boards —'
              -
                type: hard_break
              -
                type: text
                text: 'Some hade, like hens in byre-neuks:'
              -
                type: hard_break
              -
                type: text
                text: 'Wives baket bonnocks '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (3)
              -
                type: text
                text: ' for their men,'
              -
                type: hard_break
              -
                type: text
                text: 'Wi’ tears instead o’ water;'
              -
                type: hard_break
              -
                type: text
                text: 'An’ lasses made cockades o’ green'
              -
                type: hard_break
              -
                type: text
                text: 'For chaps wha us’d to flatter'
              -
                type: hard_break
              -
                type: text
                text: '          Their pride ilk day […]'
              -
                type: hard_break
              -
                type: hard_break
              -
                type: text
                text: 'The truly brave, as journeyin’ on'
              -
                type: hard_break
              -
                type: text
                text: 'They pass by weans an’ mithers,'
              -
                type: hard_break
              -
                type: text
                text: 'Think on red fiel’s, whare soon may groan,'
              -
                type: hard_break
              -
                type: text
                text: 'The husbands, an’ the fathers:'
              -
                type: hard_break
              -
                type: text
                text: 'They think how soon thae bonie things'
              -
                type: hard_break
              -
                type: text
                text: 'May lose the youths they’re true to;'
              -
                type: hard_break
              -
                type: text
                text: 'An’ see the rabble, strife ay brings,'
              -
                type: hard_break
              -
                type: text
                text: 'Ravage their mansions, new to'
              -
                type: hard_break
              -
                type: text
                text: '          Sic scenes, that day.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'After a series of defeats, notably at Antrim and Ballynahinch, the Rising was put down with extreme brutality by the authorities. Ominously, one of the final stanzas accurately foresees the fate that awaits the defeated rebels. But even here, Orr’s cynicism comes through strongly. He says, for example, that although the entire population was going to suffer government reprisals – “But wives an’ weans stript, cattle hought, / An’ cots, an’ claughin’s burnin’” – very few of them were at risk of execution or flogging (“The ninetail’d cat”), because that was a fate reserved for “hero[es]”:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Come back, ye dastards! — Can ye ought'
              -
                type: hard_break
              -
                type: text
                text: 'Expect at your returnin’,'
              -
                type: hard_break
              -
                type: text
                text: 'But wives an’ weans stript, cattle hought, '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (4)
              -
                type: hard_break
                marks:
                  -
                    type: bold
              -
                type: text
                text: 'An’ cots, an’ claughin’s burnin’?'
              -
                type: hard_break
              -
                type: text
                text: 'Na, haste ye hame; ye ken ye’ll ’scape,'
              -
                type: hard_break
              -
                type: text
                text: 'Cause martial worth ye’re clear o’;'
              -
                type: hard_break
              -
                type: text
                text: 'The nine-tail’d cat, or choakin’ rape,'
              -
                type: hard_break
              -
                type: text
                text: 'Is maistly for some hero,'
              -
                type: hard_break
              -
                type: text
                text: "          On sic a day. (Orr, pp. 13-16)\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' James Orr, Poems on Various Subjects, Belfast, 1804, pp. 13-17.'
                  -
                    type: hard_break
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (2)
                  -
                    type: text
                    text: ' Ulster-Scots variant of “croppies.” The term, meaning republicans, referred to the fashion      among the French revolutionaries of wearing their hair cropped short.'
                  -
                    type: hard_break
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (3)
                  -
                    type: text
                    text: ' Round cakes made of oatmeal or flour.'
                  -
                    type: hard_break
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (4)
                  -
                    type: text
                    text: ' To “hough” is to mutilate cattle by cutting the hamstrings.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Orr''s Grave, Ballycarry. Photo by Wesley Hutchinson, February 2022.'
    uuid: 4aa010ac-fd47-4681-924d-986690679953
    type: content
    enabled: true
    image: IMG_3051.jpg
    allow_zoom: true
    audio_file:
      - audio/MOD2-EP8-SCR1-1655628770.mp3
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the third volume of Reid’s History of the Presbyterian Church in Ireland (1853), the Rising is presented as a period of generalised chaos. In what was to become in many ways an “official” history of the Presbyterian Church, Reid’s representation of the Rising is designed as a grim warning to the members of the Ulster-Scots community regarding the dangers of allowing themselves to be “seduced by the visionary theories of speculative men.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Reid makes it clear that no-one emerged innocent from the chaos of the Rising. The Government, but also private individuals of every shade of opinion, exploited the widespread confusion for private revenge. He considers that the Government is as guilty as the rebels in encouraging and sanctioning violence. In particular, he denounces the operation of a corrupt justice system that employed informers to obtain whatever result the Government wished while the same courts protected Government supporters from prosecution.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“The year 1798 presents one of the darkest passages in the ecclesiastical, as well as in the civil history of Ireland. When the insurrection actually took place, many seized on the occasion as an opportunity for indulging their private malice; and distrust, treachery, and falsehood pervaded almost all the departments of society. The rebels, particularly in the South, exhibited, in the hour of victory, the ferocity of savages ; and the yeomanry [locally-recruited pro-Government troops] in their turn, in many instances, perpetrated the most disgraceful acts of rapine and violence. Means utterly unwarrantable were adopted to secure the condemnation of parties suspected of disloyalty. The vilest informers were openly encouraged, the minions of government, even when guilty of horrid crimes, were screened from punishment, and the conduct of the state officials was often better fitted to [stir up] than to quiet a rebellion.” (Reid, Vol. III, pp. 424-425).'
    uuid: 12a050d2-8e96-42aa-9477-e19637d4b9fc
    type: content
    enabled: true
    image: MOD2-FRA8-SCR2.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'We have a very different version of the Rising, and especially of Ulster-Scots involvement in it, in nationalist and republican writing of the period. On the nationalist side, the active role the Ulster Scots played in “the ’98” is hailed as evidence that, historically, the Ulster Scot saw himself, not as a Scot, or a “Briton,” but as an Irishman, ready to fight alongside his Catholic neighbour to defend the idea of an independent Irish Republic.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Throughout the Home Rule period, nationalist and republican commentators go out of their way to underline Ulster-Scots involvement in the Rising. This is particularly the case in the build-up to 1898, the hundredth anniversary of the Rising, when the actions of people like Henry Joy McCracken or Betsy Gray were celebrated in the nationalist press. It seemed particularly important to recall northern Presbyterian involvement in the Rising because that involvement stood in such sharp contrast to the pro-British, anti-separatist stance that their descendants were taking during the Home Rule crisis.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As the following extract illustrates, this theme comes to the fore again at the time of the third Home Rule Bill. In this text, published in The Irish Review in 1914 at the height of the third Home Rule crisis,'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1)'
          -
            type: text
            text: " the author – although he calls himself “An Ulster Scot,” he takes a clear nationalist line - tries to convince the Ulster-Scots population to remember and re-assess what he sees as a key aspect of their history. His blunt statement that, “History […] is a matter of which the ‘Ulster Scot’ is profoundly and amazingly ignorant,” is unlikely to make him many friends in the Ulster-Scots community. As we shall see below, the problem did not lie in “ignorance” of history, but rather in its interpretation.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“History, by the way, is a matter of which the “Ulster Scot” is profoundly and amazingly ignorant. I will wager that there is no class of people in the civilised world so ignorant of their country''s history as they are. If they did read and study the history of their country, there would be more, as in ''98, on the Nationalist side. McCracken, Hope, Munro, Neilson, the names are Scotch, the men were patriotic Ulstermen, who died for the ideal of Ireland a Nation, and not against, as men of their blood threaten to do to-day […] Time is a great healer, so let us hope that after Home Rule is an actual fact the bitterness will pass, and the “Ulster-Scots” be found in their proper place - the van of the Irish Nation – and emulate the patriotic efforts of their forefathers who died for Ireland in the “bad times.” (The Irish Review, pp. 233-234) '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The main body of opinion within the Ulster-Scots community clearly did not agree with this analysis. Although many of their ancestors had decided to support a republican agenda, opinion in the community had shifted radically. In the aftermath of the rising, they very quickly came to see the choices of the period as an aberration. It was simply not true to say that these people were “ignorant of their country’s history,” as the author of the above passage states. On the contrary, over much of the 19th century, Ulster-Scots authors make frequent allusion to the choices made at this period.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although we get a number of nostalgic references in the poetry, such as in James Campbell’s, “Willie Wark’s Song,”'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (2) '
          -
            type: text
            text: 'where he says:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“In Ninety-eight we arm’d again,'
              -
                type: hard_break
              -
                type: text
                text: 'To right some things that we thought wrang.'
              -
                type: hard_break
              -
                type: text
                text: 'We gat sae little for our pains,'
              -
                type: hard_break
              -
                type: text
                text: 'It’s no worth mindin’ in a sang,”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'much of the focus on ’98 was concentrated on exploring how these choices had been profoundly mistaken and on warning contemporaries never to repeat the same mistakes.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' An Ulster Scot, “The denial of North-East Ulster,” The Irish Review (Dublin), Vol. 2, No. 17 (July 1912), pp. 228-234. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(2) '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: 'James Campbell, The Poems and Songs of James Campbell of Ballynure, Ballyclare, 1870, p. 100.'
    uuid: 69a4fda4-8c95-4ba5-b730-e14112e557b1
    type: content
    enabled: true
    image: MOD2-FRA8-SCR1-1638613254.jpg
    audio_file:
      - audio/MOD2-EP8-SCR3-Rebel.mp3
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'A typical example of this reading of the Rising is Rev. Thomas Hamilton’s, History of the Irish Presbyterian Church. This text, published in Edinburgh in 1886, at the height of the first phase of the Home Rule crisis, was used as a handbook for young people who were following Bible classes. Given the target audience, the contents were clearly designed to play a role in shaping the vision of history of a generation of young Irish Presbyterians. '
          -
            type: hard_break
          -
            type: hard_break
          -
            type: text
            text: 'Hamilton says that he is amazed – “the marvel is” - that the community did not realise the hopelessness of their revolutionary enterprise. He points out their lack of training, their lack of equipment, and their shaky morale – “their confidence in their own cause so tremulous” – and concludes that “the British power in Ireland” had little to fear from such “a motley crowd of untrained ploughmen.” His conclusion dismisses the entire enterprise as “a miserable fiasco.” The message is crystal clear: the Ulster-Scots Presbyterians were fools to get involved. The suggestion is that they now know better.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“In Ulster [the rebellion] was confined to the three counties of Antrim, Down, and Derry [i.e. the counties with the most concentrated Ulster-Scots communities], and even there it was foredoomed to failure from the first. The marvel is that the crowds of farmers and labourers who assembled at Roughfort, and on Donegore Hill, and at Saintfield, and Ballynahinch, did not themselves see the ludicrous hopelessness of an enterprise whose supporters were a motley crowd of untrained ploughmen, many of them with reaping-hooks or scythes tied to poles as their only weapons, a bag of oaten cakes their [only supplies], and their confidence in their own cause so tremulous, that when in the act of assembling, the distant sound of horses feet, as a few farmers trotted home from a fair, was sufficient to send a whole regiment of them to the right-about [i.e. to retreat], in full belief that the dragoons were upon them. The British power in Ireland was not very likely to be overthrown by such foes. In the North, therefore, the whole movement ended in a miserable fiasco.” (Hamilton, p. 14)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'One of the key factors explaining why the Presbyterian community rejected the revolutionary choices of their ancestors was the extreme violence before, during and after the rising, in particular in the Ulster-Scots areas. The brutality surrounding the events – many accounts suggest that the people were literally beaten into submission – left an indelible mark in the memory of the community.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'But it was the anti-Protestant violence that occurred during the Rising in “the South” that undoubtedly had a more lasting negative impact on the imagination of the Ulster-Scots Presbyterian.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'So, whereas popular nationalist history was celebrating the republican courage of the Ulster-Scots heroes, its Presbyterian equivalent was going out of its way to show that, although their idealism might have been admirable, their forefathers had been profoundly mistaken in their political analysis.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The increasing conservatism of the Presbyterian Church over the course of the 19th century, under the influence of ministers like Rev. Henry Cooke, was perfectly in tune with the political shift that was taking place within the Ulster-Scots community.'
    uuid: 253b10ca-7a1b-40bd-8755-6bced241a5bb
    type: content
    enabled: true
    image: MOD2-FRA8-SCR3-copy-min.jpg
    allow_zoom: true
---
