---
id: 9c8a1f00-16bb-42a0-903c-ccc49e8bf9d3
blueprint: module_2
title: 'Kith & Kin'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1652456170
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The notion of “kith and kin” is one of the most important frames through which Ulster-Scots authors try to generate sympathy for their position during the Home Rule crisis. Throughout their writings, they underline how individuals and communities in Ulster form part of close family and religious networks that have their roots in Scotland and England. Authors frequently insist on how these links have existed for centuries. These networks are particularly strong in relation to Scotland.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The earliest schemes to settle Scots in Co. Down at the beginning of the 17th century show how the two Scotsmen, Hugh Montgomery and James Hamilton, who had recently acquired large tracts of the lands that had belonged to Con O’Neill, called on their Scottish networks to plant the areas in question.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Three short extracts, taken from accounts by John Stevenson, Charles A. Hanna and Rev. George Hill, focus on this issue.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The first extract comes from John Stevenson’s book, Two Centuries of Life in Down, published in 1920. Stevenson was the co-owner of one of Belfast’s largest printing concerns, McCaw, Stevenson & Orr. He was deeply interested in the history of the Ulster-Scots community. He wrote a number of books about counties Antrim and Down, including a collection of poetry in Ulster-Scots.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Hamilton established himself at Bangor, and took his younger brother William into his service. Montgomery made his headquarters at Newtownards, where he roughly repaired portions of an old Castle to provide shelter for himself and friends. He brought with him the Shaws, uncle and brother of his wife, Patrick Montgomery, brother-in-law, and Hugh Montgomery, a cadet of the planter’s family. With him also, or following quickly, came members of allied families, Boyd, Nevin, Moore, Neil, Catherwood, and many others. To these were given large tracts of land […], laying the foundation for the status of minor gentry of the county in the centuries to come. (Stevenson, p. 45)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Stevenson explains how, when Montgomery and Hamilton came over to Ulster to take up their new possessions in County Down, they came with members of their own families. Hamilton comes with his younger brother, whereas Montgomery brought over members of his wife’s family. Interestingly, he states that they complete these family connections with what he calls “allied families,” suggesting the existence of complex networks of influence and loyalty.'
    uuid: 78d15579-3528-40d4-b447-65b518169176
    type: content
    enabled: true
    image: DownMapExtended.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The American, Charles A. Hanna, writing a few years earlier in his influential, two volume study, The Scotch-Irish (1902), goes into greater detail about the key role played by family networks in the early stages of the Scottish settlement of Ulster:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Hamilton seems to have received the hearty support of his own family, for four of his five brothers aided his enterprise and shared his prosperity. From them are descended numerous families in Ulster, and at least two Irish noble families. Hamilton founded the towns of Bangor and Killyleagh, in county Down, and there is no doubt that he did “plant” the land which he had acquired with Scottish tenants, the most of them evidently from the same counties in Scotland—Ayr, Renfrew, Wigtown, Dumfries, and Kirkcudbright—as the men who followed Montgomery […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: "Montgomery belonged to a family having numerous connections throughout North Ayrshire and Renfrewshire, and to them he turned for assistance. His principal supporters were his kinsman, Thomas Montgomery\_[…]; his brother-in-law, John Shaw \_[…]; and Colonel David Boyd \_[…] With their help, Montgomery seems to have persuaded many others of high and low degree to try their fortunes with him in Ireland. The names of the emigrants are intensely Scottish. They began to cross [from Scotland to Ulster] in 1606. (Hanna, Vol. I, pp. 487-490)"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Hanna explains how both Hamilton and Montgomery – “having numerous connections”– needed to mobilise their networks of family and friends in order to respect their obligations under the royal grant. He states that Hamilton focused on his immediate family – “four of his five brothers aided his enterprise” - while Montgomery called first on his immediate relatives - “his kinsman, […] his brother-in-law” – before widening the circle to include his friends and acquaintances. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The fact that he should try and entice “many others of high and low degree to try their fortunes with him in Ireland” gives us an inkling of the risks involved in this plantation scheme. Although the conditions were attractive, and the opportunities for making money considerable – Hanna talks about “prosperity” and notes, “from them are descended […] at least two Irish noble families” - the undertaking was not without its dangers to life and limb. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Hanna underlines how many of these initial settlers came “from the same counties in Scotland—Ayr, Renfrew, Wigtown, Dumfries, and Kirkcudbright.” This is important as it suggests that the settlement involved a transposition of large sections of Lowland society into the north of the Ards Peninsula. The collective nature of the migration means that Lowland Scotland is to a certain extent producing a copy of itself in this part of Ulster.'
    uuid: 4c0d7afe-1beb-4cd4-abaf-f644bce96e69
    type: content
    enabled: true
    image: MOD2-FRA1-SCR2-copy.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'These links with Scotland did not suddenly stop once the initial settlers had been installed. On the contrary, the authors working on the history of the Ulster Scot insist on how waves of Scots kept crossing the Channel to take up land or jobs in Ulster. The following passage is from The Montgomery Manuscripts, an edition of which was published by Rev. George A. Hill in Belfast in 1869. It underlines the ease with which travel and commerce took place between Co. Down and the west of Scotland, and how there was a “constant flux of passengers” between the two areas. The North Channel is seen not as an obstacle to communication, but rather as facilitating contact and circulation of people and goods.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'As likewise in the fair summer season (twice, sometimes thrice every week) they were supplied from Scotland, as Donaghadee was oftener, because but three hours sail from Portpatrick, where they bespoke provisions and necessaries [...], to be brought over by their own or that town’s boats whenever wind and weather served them, for there was a constant flux of passengers coming daily over.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I have heard honest old men say that in June, July, and August, 1607, people came from Stranraer, four miles, and left their horses at the port, hired horses at Donaghadee, came with their wares and provisions to Newton, and sold them, dined there, staid two or three hours, and returned to their houses the same day by bed-time, their land journey but 20 miles. Such was their encouragement from a ready market, and their kind desires to see and supply their friends and kindred, which commerce took quite away the evil report of wolves and woodkerns, which [those who envied the] planters'' industry had raised and brought upon our plantations […] (Hill (ed.), p. 60)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It would, of course, also be a mistake to think that this was a one-way street, i.e. that people only moved from Scotland to Ulster. On the contrary, it was very much of a two-way street! As the previous passage makes clear, people were constantly moving backwards and forwards, to see members of the family (“supply their friends and kindred”), to buy and sell livestock and goods (“came with their wares and provisions”), or, depending on the political situation of the moment, to lie low on one side of the North Channel or the other.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The author underlines that this intense exchange was taking place despite the rumours of “wolves and woodkerns” that were being circulated at the time. Indeed, there were still wolves in Ireland at the time of the plantations. The last wolf in Ireland was not killed until the very end of the 18th century. As for the “woodkern,” the term refers to the Irish inhabitants who had been pushed off the land at the time of the Plantation, but who remained in the area, living as outlaws in the woods. Together, the wolf and the woodkern embodied the threat that the settlers faced on this new frontier.'
    uuid: 3efe48e9-b1c3-4985-bc70-365f6a17c711
    type: content
    enabled: true
    image: MOD2-FRA1-SCR3-copy.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'John Harrison, the author of a very important text, The Scot in Ulster, published in 1888, in the midst of the on-going Home Rule debate, highlights some of the many ways in which these family connections between Ulster and Scotland manifest themselves in the landscape of Ulster. In the following passage, he takes the example of the graveyard in Templepatrick, near Donaghadee.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Two miles south from Donaghadee, on the shore road into the Upper Ards, that narrow peninsula between Strangford Lough and the Irish Sea, there lies a little enclosure which must arrest the stranger’s attention. It is a graveyard, and is called Templepatrick. It is surrounded by low stone walls; no church or temple is now within its confines; no trees or flowers give grateful shade, or lend colour and tender interest; it is thickly covered with green mounds, and with monumental slabs of grey slaty stone, — the graves are packed close together. Read the simple “headstones,” and you discover no trace of sentiment, few fond and loving words; no request for the prayers of the passer-by for the souls of those who sleep below; nothing more akin to sentiment than “Sacred to the memory of.” [...]  Read the names on the stones \_[… ] The names are very Scottish — such as Andrew Byers, John Shaw, Thomas M’Millan, Robert Angus; it is a burying-place of the simple peasants of County Down, who are still, in the end of the nineteenth century, as Scottish as they were when they landed here nearly three centuries ago. (Harrison, pp. 13-14)\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He says that the people buried here, “the simple peasants of County Down, […] are still, in the end of the nineteenth century, as Scottish as they were when they landed here nearly three centuries ago.” In other words, despite the fact that their families had lived in Ireland for generations, they were still as Scottish as they were at the time of the Plantation. Indeed, in his opinion, even though they are in Ireland, these are in fact Scottish graves.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Harrison is suggesting that the Scottish connection is so strong that it is almost as if Scotland has extended itself into this part of Ireland. The fact that these headstones were “packed close together” underlines the intensity of the Scottish presence in the area over this time.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Harrison suggests that the plainness of the headstones tells us something about the culture that produced them. Their style illustrates how these “simple” people – the word is repeated several times in the text – are wary of elaborate declarations, preferring not to express their emotions in public – no “loving words,” “no trace of sentiment.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He also makes it clear that the people buried here are “peasants.” This is an important detail. He published this text initially in The Scotsman before publishing it in book form. The text was therefore addressed to a public in Britain. It was important to show them that these Ulster Scots were just ordinary people and did not, as many Home Rulers alleged, belong to the privileged classes'
    uuid: ca971f78-ad52-4045-a73f-706c21eb1519
    type: content
    enabled: true
    image: MOD2-FRA1-SCR4-copy.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Harrison pushes the idea one step further and points to how the family networks that link Ulster in to Scotland are reproduced on the other side of the Atlantic and even further afield. Again, the central focus is the Scottish family name that features on the headstone and which at once “locates” individuals in a given country and “connects” them to all the others in the network. Again, as with north Down, Scotland is seen as – literally - reproducing copies of itself wherever the Scots settle.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'These graveyards of the Scots are now on every shore, — among the great forests of Canada, as well as here by the side of the Irish Sea; where the new Dunedin [South Island, New Zealand] rises out of the Southern Ocean, as well as in the old Dunedin [Dùn Èideann, Edinburgh in Scots Gaelic], under the shadow of its Castle rock. Thus enter into a common rest those who shared a common toil, whether in the old motherland, or far away scattered over the wide world. (Harrison, p.14)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'What these texts are showing us is the importance of family networks in the construction of the Ulster-Scots diaspora. The attitudes and reflexes of the first Scottish settlers in Ulster were reproduced when the Ulster Scots migrated to North America and to so many other parts of the world in later years. The texts underline how the link back to Scotland –what Harrison refers to as “the old motherland” - represents a central part of their identity and how that sense of identity is kept alive through family memory.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'These networks – with Scotland and the diaspora - will be reactivated during the Home Rule crisis.'
    uuid: f2c699fd-5087-4cf7-9065-79bb50c906e4
    type: content
    enabled: true
    image: MOD2-FRA1-SCR5-copy.jpg
    allow_zoom: true
---
