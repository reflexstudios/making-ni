---
id: c0990a91-448e-41d3-a02f-144bcf7ca670
blueprint: module_2
title: 'Social Class'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654870390
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Ulster-Scots authors of the Home Rule period often seek to underline the idea of a tightly organised, “organic” society that brings together the various social classes in a unified whole. They suggest that the community shares a common base, a common purpose and a common destiny.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'One of the key aspects of this supposed unity is the way in which writing on the Ulster Scot often insists on the relative absence of class conflict. This comes out strongly in the fiction of the period written by Ulster-Scots authors for an Ulster-Scots audience.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "This is the case, for example, in the novels of people like W. G. Lyttle or Archibald McIlroy. Both of these authors produce a picture of a particularly harmonious society, one in which, when conflict does arise, it is resolved through the joint efforts of people belonging to different social classes. Ultimately, rather than focusing on their own selfish interests, the characters in the novels are shown to have the interests of the community at heart.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Here again many of the writers suggest that one of the reasons for this cohesion is that a shared Presbyterianism breaks down some of the social barriers that would otherwise have existed between the different classes.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'We find this idea, for example, in John Stevenson’s novel, Bab of the Percivals (1926), set in an Ulster-Scots community in the Ards Peninsula:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“In the bad old days of the 17th and 18th centuries, when to be Presbyterian was to be out of favour with the powers, the branch of the family to which the squire belonged remained faithful to the despised body, and the Squire, like his forefathers, worshipped with the farmers, tenants of the Percival estate.” (Stevenson, pp. 14-15)"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The supposed solidarity that existed between the various classes that make up Ulster-Scots society comes up in the writing time and time again. Thus, Stevenson, in his autobiographical text, A Boy in the Country, published in 1912, insists on how the local farmers and their wives live exactly the same lives as their labourers and servants, doing the same jobs, working side by side:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“At dawn the farmer is up and out. He ploughs, harrows, carts, spreads lime or manure, cuts hay, does everything, indeed, that his men do, and in all weathers. Inside his wife bakes griddle bread for household and servants, makes butter, helps a woman servant to milk, and prepare food for fowls and pigs. The pair eat hurried, comfortless meals of tea and bread or bacon and potatoes.” (Stevenson, pp. 186-187)."
    uuid: b2c4364f-4a55-4b7b-abb2-970fff14470d
    type: content
    enabled: true
    image: MOD2-FRA4-SCR1-1638797410.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This image of a largely classless working environment is confirmed by Lynn Doyle in An Ulster Childhood (1921), when he contrasts working and social practices in “the Black North” (i.e. Presbyterian parts of Ulster) with what happens in Leinster and Munster:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“In Leinster and Munster, farms are generally large; and there is a distinct gap between master and man. Farm servants there will call their employer “sir,” and even touch their hats to him; a thing we do not hold with in the Black North, unless we are working for “the gentry.” But farms in Ulster are much smaller. In many cases farmer and hands sit at the same table, go afield together, and pick potatoes side by side in the same outhouse. In their working hours there is no social distinction between them. They will sit down amicably in the same ditch side to smoke a pipe together – literally “a pipe,” for in the deficiency of tobacco I have seen one pipe do duty in alternate mouths […] - and the servant’s address to his master is simply “Robert,” or “John,” or “Thomas,” as the case may be.” (Doyle, pp. 8-9)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Similarly, in W.G. Lyttle’s novel, Sons of the Sod (1886), his representation of the “Big Hoose” in the Presbyterian area of North Down provides a surprising contrast with images associated with the “Big House” elsewhere in Irish literature. Lyttle is suggesting that, far from living a life of ease, cut off from their surroundings, the local Ulster-Scots landowners share the living conditions of the people among whom they live.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“In a northern portion of the broad, pleasant and prosperous Ards there stood a large, unshapely, old-fashioned structure, the residence of a certain country squire, and designated by the people of the country for many miles around “The Big Hoose.” The building stood fully a mile from the highway and was approached by an old cart road, which [was] mangled and torn […] by the heavily laden carts continually passing over it […]. A narrow avenue […] branched off this cart-road and led to the squire’s. It was a dull cheerless-looking house, with small windows and narrow entrance […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The squire owned a considerable stretch of land. The greater part of this land had been let off in small farms of from seven to twenty acres. It had been so when he came into possession, and it so remained. Times without number had he been importuned by his agent to turn out of doors some unlucky or improvident tenant whose rent was in arrear, but the squire […] turned a deaf ear to Black Ben [the nickname of the agent], or put him off with the remark, “Give them a little longer.” The squire’s property could boast of but few comfortable farmhouses. They were chiefly of the cottier class, bearing, oddly enough, a striking resemblance to the “Big Hoose” so far as cheerlessness and lack of comfort were concerned.” (Lyttle, pp. 16-17).'
    uuid: 24b4c2fe-5a94-4b1d-915b-81c092996ba4
    type: content
    enabled: true
    image: MOD2-FRA4-SCR2-copy-min-1638613080.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Everything about the squire’s house is “small,” “narrow” and “cheerless.” The description goes out of its way to underline the similarities between the living conditions of the landlord and those of his tenants. Decidedly, there is nothing extravagant about his life style!\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'More importantly, however, is his reluctance to evict his tenants if they fall behind in their rent. It is by no means a coincidence that Lyttle should provide this detail. The book, published in 1886, the year draft Home Rule legislation is introduced, is set against the background of the movement for tenant right earlier in the century.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although the demand for tenant right was largely driven by people with nationalist sympathies, it had considerable support in rural Presbyterian areas. The fact that the Ulster-Scots “squire” was so reluctant to evict tenants who were behind in their rent therefore shows a much more benevolent image of “the landlord” than what was being produced in nationalist literature of the period. Rather than confrontation, Lyttle clearly wants to project the idea that Ulster-Scots society can come to a compromise on issues such as these.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The subtext would appear to suggest that, unlike the Anglican landed classes in Ulster and elsewhere  - what Doyle refers to as “the gentry” -  Presbyterian society has a more democratic, egalitarian streak. In any case, the representation we are being given is that the distance in terms of living conditions between gentry and peasantry is less striking in the Scots areas of Ulster than elsewhere.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As in so many other domains, social relations in Ulster-Scots areas are being “imagined” as being radically different from the rest of Ireland.'
    uuid: 884e9be3-b51d-42e7-a469-1021aeca4986
    type: content
    enabled: true
    image: MOD2-FRA4-SCR3.jpg
    allow_zoom: true
---
