---
id: 3f44a27e-4430-43cb-91d8-4ac34121931b
blueprint: module_2
title: Fighter
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655629195
audio: MOD2-EP6-SCR2-1638558064.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Authors writing from an Ulster-Scots perspective frequently underline the role of the Ulster Scot as a fighter. Accounts of the exploits of the young apprentices in Derry/Londonderry are typical of this representation.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Indeed, the Ulster Scot’s capacity to take determined action, even against apparently overwhelming odds, is something that comes up repeatedly throughout the history writing and the literature.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'An interesting example is to be found in a poem by Sir Samuel Ferguson, one of the most famous poets of the 19th century. Ferguson, who is best known for his work centred on ancient Irish myth and history, was very conscious of his Scottish roots. This comes out strongly in the poem he wrote about one of his ancestors, a certain Willy Gilliland, “Willy Gilliland. An Ulster Ballad,” '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ' published in The Dublin University Magazine in 1836 and later reprinted in his famous collection, Lays of the Western Gael (1865). One of the key aspects of the story is Gilliland’s ability to look after himself by single-handedly taking on an entire group of soldiers to retrieve the horse they had stolen from him on the moors.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "In order to escape religious repression in Scotland, he had crossed over to Ulster, taking refuge in the Scots Presbyterian areas in Antrim. Gilliland went into hiding round Collon in mid Antrim, quite close to Glenwherry.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although he is an outlaw, he is looked on as a hero by the local Presbyterian population, many of whom were, quite naturally, sympathetic to the position of their co-religionists in Scotland.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'But, the story also underlines his physical courage, his capacity to live off the land and to improvise a deadly weapon when he decides to go for what is in fact a one-man, guerrilla-type attack on the troops in Carrickfergus. Against all the odds, he manages to take his horse back, before disappearing once again into the hills.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: 'The poem “Willie Gilliland. An Ulster Ballad,” which was published originally in 1836, appeared in, Lays of the Western Gael And Other Poems, London, Bell & Daldy, 1865, pp. 110-118.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Portrait of Sir Samuel Ferguson, as a young man, as published in Mary Catherine Ferguson''s, Samuel Ferguson in the Ireland of His Day, Vol. II. Source: '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://books.google.com/books?id=9aEzAAAAMAAJ'
                          rel: null
                          target: null
                          title: null
                    text: 'https://books.google.com/books?id=9aEzAAAAMAAJ'
                  -
                    type: text
                    text: ', Public Domain'
    uuid: b40b6aa4-ef76-4a96-ac32-c2a7474d394f
    type: content
    image: Samuel.Ferguson.in.his.youth.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The story is not only a chance to recall the Covenanting tradition, its association with freedom of conscience, and the rebellious defiance of authority which many in the Presbyterian community saw as some of its own characteristic traits. Equally importantly, the focus on the Covenanting past underlines how, historically, Ulster has formed part of a “cultural continuum” stretching into Scotland. The ease with which Gilliland travels to Ulster and the sympathy his situation arouses illustrate the closeness of the ties that exist between the two areas.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Most importantly perhaps, the story the poem tells suggests that, despite being on his own, Gilliland is in some way placed under divine protection. The idea that there is a force that protects the faithful few against apparently overwhelming odds is something that would come out repeatedly in representations of the Ulster unionists during the Home Rule crisis.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'In vain to fly his enemies he fled his native land;'
              -
                type: hard_break
              -
                type: text
                text: 'Hot persecution waited him upon the Carrick strand;'
              -
                type: hard_break
              -
                type: text
                text: 'His name was on the Carrick cross, a price was on his head,'
              -
                type: hard_break
              -
                type: text
                text: 'A fortune to the man that brings him in alive or dead!'
              -
                type: hard_break
              -
                type: text
                text: 'And so on moor and mountain, from the Lagan to the Bann,'
              -
                type: hard_break
              -
                type: text
                text: 'From house to house, and hill to hill, he lurk’d an outlaw’d man.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The opening line of Ferguson’s poem identifies Gilliland as someone who, because of religious repression against the Presbyterian Covenanters in Scotland - “hot persecution” - had been forced to “worship [...] God upon the hill [...] in the mountain solitudes.” Indeed the poet shows him continuing to worship, alone on the mountain, “his hand upon the Book.” The image of the outlaw “fill[ing] the lonely valley with the gladsome word of God,” in defence of “a persecuted kirk, and […] her martyrs dear,” recalls the highly emotive folk images of the persecuted Covenanters on the Scottish moors. '
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'And on the river''s grassy bank, even from the morning grey,'
              -
                type: hard_break
              -
                type: text
                text: 'He at the angler''s pleasant sport had spent the summer day; […]'
              -
                type: hard_break
              -
                type: hard_break
              -
                type: text
                text: 'His blithe work done, upon a bank the outlaw rested now,'
              -
                type: hard_break
              -
                type: text
                text: 'And laid the basket from his back, the bonnet from his brow;'
              -
                type: hard_break
              -
                type: text
                text: 'And there, his hand upon the Book, his knee upon the sod,'
              -
                type: hard_break
              -
                type: text
                text: 'He fill''d the lonely valley with the gladsome word of God;'
              -
                type: hard_break
              -
                type: text
                text: 'And for a persecuted kirk, and for her martyrs dear,'
              -
                type: hard_break
              -
                type: text
                text: "And against a godless church and king he spoke up loud and clear.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is while he is off fishing that the troopers from Carrickfergus discover his hide-out which they set on fire. They also kill his faithful greyhound and steal his horse and his broadsword, the only means he had to defend himself.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'They’ve slain my dog, the Philistines! they’ve ta’en my bonny mare!” —'
              -
                type: hard_break
              -
                type: text
                text: 'He plung’d into the smoky hole; no bonny beast was there —'
              -
                type: hard_break
              -
                type: text
                text: 'He groped beneath his burning bed, (it burn’d him to the bone,)'
              -
                type: hard_break
              -
                type: text
                text: 'Where his good weapon used to be, but broadsword there was none;'
              -
                type: hard_break
              -
                type: text
                text: 'He reel’d out of the stifling den, and sat down on a stone,'
              -
                type: hard_break
              -
                type: text
                text: 'And in the shadows of the night ’twas thus he made his moan —'
              -
                type: hard_break
              -
                type: text
                text: '“I am a houseless outcast; I have neither bed nor board,'
              -
                type: hard_break
              -
                type: text
                text: 'Nor living thing to look upon, nor comfort save the Lord:'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Furious, Gilliland swears vengeance, and prepares himself for what is sure to be an uneven fight, improvising a weapon from his fishing rod:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'His fishing-rod with both hands he gripped it as he spoke,'
              -
                type: hard_break
              -
                type: text
                text: 'And, where the butt and top were spliced, in pieces twain he broke; [… ]'
              -
                type: hard_break
              -
                type: hard_break
              -
                type: text
                text: 'He ground the sharp spear to a point; then pull’d his bonnet down,'
              -
                type: hard_break
              -
                type: text
                text: 'And, meditating black revenge, set forth for Carrick town.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Gilliland lies in wait at the gates of Carrickfergus Castle. When the troopers leave the castle to go on patrol, he spots the one who is riding his mare, runs him through with his home-made weapon and manages to escape back on to the moor:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'And now the gates are open’d, and forth in gallant show'
              -
                type: hard_break
              -
                type: text
                text: '[…] jeering grooms and burghers blythe, and troopers in a row;'
              -
                type: hard_break
              -
                type: text
                text: 'But one has little care for jest, so hard bested is he,'
              -
                type: hard_break
              -
                type: text
                text: 'To ride the outlaw’s bonny mare, for this at last is she!'
              -
                type: hard_break
              -
                type: text
                text: 'Down comes her master [Gilliland] with a roar, her rider with a groan,'
              -
                type: hard_break
              -
                type: text
                text: 'The iron and the [wooden shaft] are through and through him gone!'
              -
                type: hard_break
              -
                type: text
                text: 'He lies a corpse; and where he sat, the outlaw sits again,'
              -
                type: hard_break
              -
                type: text
                text: 'And once more to his bonny mare he gives the spur and rein;'
              -
                type: hard_break
              -
                type: text
                text: 'Then some with sword, and some with gun, they ride and run amain;'
              -
                type: hard_break
              -
                type: text
                text: 'But sword and gun, and whip and spur, that day they plied in vain! '
              -
                type: hard_break
              -
                type: text
                text: '(Ferguson, pp.110-118)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The ballad clearly sees this violence as quite justified. It is presented as being in reaction to the systematic violence against the Covenanters by the forces of the State. Ferguson ends the ballad by recalling how Gilliland survived this period of repression to become one of the foremost figures in the local Ulster-Scots community.'
    uuid: 8fecdde5-e684-45f9-b944-2c4f2c07f767
    type: content
    image: MOD2-FRA6-SCR2.jpg
    audio_file:
      - audio/4.-Charlie.wav
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The theme of the Ulster Scot as courageous fighter takes on a particular importance in writing about the Ulster Scot in America. Here, the authors underline the physical courage of the frontiersmen and frontierswomen who are shown as pushing fearlessly deeper and deeper into the wilderness.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'But it is especially in the context of the American War of Independence (1775-1783) that we find the often idealised narratives insisting on the fighting skills of the Ulster- Scots settlers.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This is the case, for example, for John Walker Dinsmore, whose, The Scotch-Irish in America (1906),'
          -
            type: text
            marks:
              -
                type: bold
            text: ' '
          -
            type: text
            text: 'identifies the Scotch-Irish as “incomparably the most effective element” in the American army. Like many other commentators, he mentions the Battle of King’s Mountain. In his account, he underlines how the people of Scotch-Irish descent played a key role in defeating the British troops. They are shown as determined, indeed merciless fighters who allow none of their enemies to escape – “every man of the enemy was either killed or captured.”'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“During the war the Scotch-Irish were incomparably the most effective element in Washington''s army. They were exceedingly influential in the Continental Congress, and in the various colonial assemblies. […] In the darkest hours, […] when multitudes were ready to give up, they stood stalwart and resolute. Their ministers preached and prayed, and in not a few instances, organized companies and regiments and led them to battle. The battle of King''s Mountain for instance, which drove Cornwallis and the British forces from the entire southern country, was fought almost exclusively by these Scotch-Irish, nearly every regiment being commanded by a Presbyterian elder.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'That battle was peculiar in this, that every man of the enemy was either killed or captured. Not a single man got away. Undoubtedly in preparing for the great struggle and during its continuance, the men and women of this blood had a share far out of proportion to their numbers. [...] Here may be quoted the words of Col. A. K. M’Clure, the famous Philadelphia editor:'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“It was the Scotch-Irish people of the colonies that made the declaration of 1776 […] [T]he Scotch-Irish had declared that these colonies are and of right ought to be free and independent. They had taught this not only in their public speeches, but at their altars, in their pulpits, at their firesides, and it was from these that came that outburst of rugged and determined people that made the declaration of 1776 possible.” (Dinsmore, pp. 30-33)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Once again, religion is a factor that is underlined in the narrative. Thus, we are told how “nearly every regiment [was] commanded by a Presbyterian elder,” and how “[t]heir ministers preached and prayed, and in not a few instances, organised companies and regiments and led them to battle.” '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The narrative also underlines how the military prowess of the Scotch-Irish was only one aspect of the role they played in obtaining the independence of the United States. The passage lists their involvement at every level “in the Continental Congress, and in the various colonial assemblies,” and states categorically that it was they who “made the declaration of 1776.” But, beyond particular examples of their contribution to the revolutionary cause, Dinsmore suggests that it was specifically within Scotch-Irish society – “in their public speeches, at their altars, in their pulpits, at their firesides” – that the revolutionary spirit of the colonies was born. It is thus that Dinsmore claims that they played a role in the American revolution “far out of proportion to their numbers.”'
      -
        type: paragraph
    uuid: 12893f84-b337-489d-a395-01838e81a7cc
    type: content
    image: MOD2-FRA6-SCR3-copy-min.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Ulster-Scots history writing often focuses on individual life stories. These stories always trace the person’s family connections back to their origins in Ulster and Scotland. Rev. Woodburn, in his The Ulster Scot: His History and Religion, published in 1914, includes a chapter entitled, “Famous Ulster Scots.” Among the people referred to in the chapter, he includes a number of famous Scots-Irishmen, especially those who had been involved in the War of Independence or who had been elected as Presidents of the American Republic.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the following passage, Woodburn, refers to Ulster-Scots connections with the American Presidency, and provides a brief sketch of one of the most prominent figures, Andrew Jackson (1767-1845). '
          -
            type: text
            marks:
              -
                type: bts_span
                attrs:
                  class: null
            text: "Although Woodburn goes into some biographical detail, he reveals the dominant attitudes of the period in which he is writing by not mentioning the fact that Jackson was a slave owner.\_"
          -
            type: text
            text: 'The long quote is, interestingly, from The Ulster Land War of 1770, by the nationalist and Celtic revivalist, Francis Joseph Bigger.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“The forefathers of Andrew Jackson, Arthur, and M`Kinley came from County Antrim, of Polk from County Derry, of Buchanan from County Donegal, and of Woodrow Wilson from County Tyrone. “I was born somewhere,” said President Jackson, “between Carrickfergus and the United States.” As a matter of fact, he was born in North Carolina two years after his parents had been evicted from their farm, and forced to leave Ulster in 1765. He was a distinguished soldier of the American Republic. In the last war waged between England and America one hundred years ago, an English force of veteran soldiers landed at New Orleans with the purpose of bringing the United States under British rule. Andrew Jackson was chosen to lead the American troops against them. He was a great favourite with his men, who called him “Old Hickory.” “It was a great fight, this battle of New Orleans; it was sudden and certain. The English troops were hunted from the field, and hurled back on their ships, leaving 700 dead on the ground, with 1,400 wounded and 500 prisoners. Jackson had exclaimed of the English at the beginning of the fight: ‘By the Eternal, they must have no rest on our soil,’ and literally he carried out his desire.” (Woodburn, p. 382)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Woodburn is reminding his readers - especially his American readers – of the key role played by people of Ulster Presbyterian extraction in the struggle for American independence. He underlines the fact that the English troops that Jackson defeated at New Orleans in 1815 were “veteran soldiers,” but that, nonetheless, under Jackson’s leadership, the fledgling American republic was able to “hunt [them] from the field.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The first thing to note is that Woodburn is obviously not writing this account out of anti-British sentiment. Indeed, he goes out of his way to remind his readers that “the last war waged between England and America” took place “one hundred years ago.” The relationship between the two nations has changed radically in the meantime.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The real purpose of the story is clearly elsewhere.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The book was published in 1914, at the very height of the Home Rule crisis. American opinion was, as always, an important element in the political equation. For unionists, it was vital to make sure people in America should have a clear idea of the distinction between Irish America and Scotch-Irish America. Irish-Americans were often actively involved in lobbying to promote the aspirations of nationalist, indeed republican Ireland. The Ulster Scots were firmly opposed to both. People had to understand that distinction. Reminding people of how those of Ulster-Scots extraction contributed militarily to the creation of America was a way to place the contemporary political and cultural aspirations of the Ulster-Scots community in Ulster in a more favourable light and to ensure that this narrative should not be claimed by “Irish America.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As we will see in Module 3, such structures as the Pennsylvania Scotch-Irish Society had been preparing the ground for such a message through their focus on how the Ulster Scots and the Scotch-Irish had contributed to the construction of America.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This narrative would take on even greater importance when America joins the war as Britain’s ally in 1917.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Portrait of Andrew Jackson by Ralph Eleaser Whiteside Earl - '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://www.whitehousehistory.org/galleries/presidential-portraits'
                          rel: null
                          target: null
                          title: null
                    text: 'https://www.whitehousehistory.org/galleries/presidential-portraits'
                  -
                    type: text
                    text: ', Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=12731789.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=12731789.'
    uuid: a9b82be7-e32b-4fb7-ab33-3397ec4be18b
    type: content
    image: Andrew_jackson_head-min.jpg
    enabled: true
    allow_zoom: true
---
