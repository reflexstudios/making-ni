---
id: b2f6ef85-1c4a-4c26-951c-cef97157fafb
blueprint: module_2
title: Londonderry
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654871232
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Siege of Derry/Londonderry is one of the founding episodes of Ulster unionist identity. The siege is often seen as a straightforward clash between two opposing – Catholic and Protestant – agendas.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, from the very moment the city was relieved, there was a keen rivalry between Presbyterians and Anglicans as to the contributions of their respective communities to this iconic victory. The Church of Ireland Governor of the city, Rev. George Walker, published A True Account of the Siege of London-Derry in 1689, just a short time after the end of the siege. In it, he appeared to minimize the Presbyterian contribution, while underlining his own role and that of the Anglican community. This did not go down well with the Presbyterians. One of their number, John Mackenzie, who had been “Chaplain to a regiment during the Siege,” responded immediately with an attack against Walker’s version of events, pointedly entitled, A Narrative of the Siege of London-Derry or, The late memorable transactions of that city faithfully represented to rectifie the Mistakes, and supply the Omissions, of Mr Walker’s Account (1690).'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The debate on the role that people of Ulster-Scots Presbyterian origin played in the defence of the city was still rumbling on almost two hundred years later when the question of Home Rule was coming to the fore. In 1873, for example, Thomas Witherow, who was professor of Church History at Magee College in Londonderry, published Derry and Enniskillen in the Year 1689, a history of the events of the revolutionary period “from an entirely Protestant standpoint.” In it, he is keen to underline how “the two sections of the Protestant garrison, in presence of a common danger, merged their religious differences, and united as one man, for the protection of their lives and the support of King William.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although he insists on the unity among Protestants during the siege, Witherow nevertheless reminds his readers of the specific contribution of the “Scots Presbyterians,” suggesting that if the city managed to hold out so effectively for the Williamite cause, it was essentially thanks to them. The tone of Witherow’s comments reflects the tensions that had existed historically between the two groups as to which party could claim to have contributed the most to the preservation of the Protestant interest in Ireland.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Witherow states that, in terms of numbers involved, the fighters in Derry were overwhelmingly Presbyterian. This is the case especially “among the ordinary soldiers.” It is interesting to see that the further he goes down the social ladder, the greater the proportion of Presbyterians becomes. This suggests a keen awareness of the links between class and religion within the broad “Protestant” camp.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The hostile remark to the effect that the besieged were just “a pack of Scots Presbyterians” is interesting if for nothing else than that it shows clearly how the Ulster Scots were perceived as constituting a distinct group.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“From the district around Derry, as well as from the Counties of Down, Antrim, and Tyrone, came the great bulk of the men who manned the walls, and, therefore, it was only natural the defenders should be a pretty fair representation of the general Protestant population. Among the superior officers of the garrison, the Episcopalians [i.e. members of the Church of Ireland] and the Presbyterians were nearly equal; among the inferior officers, the latter were in the majority; but among the ordinary soldiers, they were more than ten to one. During the progress of the siege, this was very well known in England, and […] this fact made some people there indifferent as to whether the city was relieved or not. A[n ecclesiastical] gentleman of great sense, [… when he heard] of so many dying in Derry of famine, spoke plainly […]: “’Twas no matter how many of them died; for they were but a pack of Scots Presbyterians.” No true Englishman, we are sure, would feel any sympathy with the red-hot hate of this clerical partisan, but his words shew that he knew something of the religious profession of the majority of the defenders of Derry.” (Witherow, pp. 254-256)'
    uuid: 89327015-2292-45ad-b91e-28b0390a0821
    type: content
    enabled: true
    image: MOD2-FRA7-SCR1.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The decisive action of Scots Presbyterians at key moments in the siege is another recurring theme in the Presbyterian literature of the period.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Thus, Rev. W.D. Killen, in an edition of Mackenzie’s, Memorials of the Siege of Derry, '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ' published a few years before Witherow’s book, underlined how it was supposedly a young Scots Presbyterian apprentice who took the key decision to close the gates to prevent James II’s troops – ironically a regiment of Catholic Highlanders – from entering the city.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“It would appear that almost all, if not all, the Apprentice Boys who shut the gates were Presbyterians. Hence they disregarded the advice of the [Church of Ireland] Bishop, and acted on the recommendation of the [Presbyterian] Rev. James Gordon […] In a pamphlet, published in 1685, the shutting of the gates is ascribed to “one Irwin, a young Presbyterian Scot, with some others of his fellows.” (Killen (ed.), p.10)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'A later History of the Irish Presbyterian Church, by Rev. Thomas Hamilton, reiterated the supposedly disproportionate contribution of the Scots Presbyterians to the defence of the city. This text, published in Edinburgh in 1886, at the height of the first phase of the Home Rule crisis, was used as a handbook for young Presbyterians following Bible classes. It was so popular that a second, and indeed a third edition, described as “a cheap issue of this work, which might be circulated by the thousand in Ireland,” was published the following year. The contents were clearly designed to play a role in shaping the “official” vision of history of a new generation of young Irish Presbyterians.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the following passage, Hamilton picks up this theme of the overwhelming role played by the Scots Presbyterians during the famous siege. Once again, his account focuses particular attention on the iconic moment when the gates of the city are closed. As always, the focus is on the young apprentices who take matters into their own hands and arm themselves to protect the city from a perceived threat. This celebration of the decisive nature of the action by the younger generation and the mockery of members of the ageing élite are essential elements in the fundamental message of the narrative of the siege. Hamilton’s account suggests total unanimity among the Derry Presbyterians, whereas in reality many would have been sensitive to Hopkins’ argument about the dangers of disobeying “the anointed of the Lord.”'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“Alderman Tomkins, the Mayor’s deputy, was in a quandary. He consulted the Rev. James Gordon, Presbyterian minister of Glendennot […] ‘Shut the gates and keep them out,’ was the short and bold advice which he received. But the Episcopalian [Church of Ireland] bishop, Ezekiel Hopkins by name, strongly disapproved of such a step. They were King James’s troops, and King James ought to be obeyed. He was ‘the anointed of the Lord,’ and subjects should under all circumstances submit to their sovereign […] The Alderman was [baffled by] this contradictory advice. But there are times when aldermen and bishops, and all such importances, if they choose to oppose themselves to common sense, must be quietly left standing in their dignity, while others, who, though they may have no titles to their names, have got the gift of mother wit, act. Five minutes will settle the matter now one way or other, for the Redshanks [James’ regiment] are by this time within sixty yards of the Ferryquay Gate. Fortunately for Derry, for us all, the wise advice of the Presbyterian minister was taken. Eight or nine young men, apprentices of the city, ran to the gate […] In a trice they had raised the drawbridge, shut the gate, and locked it, leaving the astonished Redshanks to stand outside in wonderment at their boldness […]"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'No wonder [the] memory [of the Apprentice Boys] is cherished in the Maiden City, and far beyond it. It should be specially cherished by all Presbyterians. For these young men who saved the city that day were almost all, perhaps all, Presbyterians, and more than one Presbyterian minister has been proud to be able to trace his ancestry to a Derry Prentice Boy.'
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_[ …] The gates are kept closed. The Apprentice Boys mount guard upon them. They arm themselves from the magazine. Next day Bishop Ezekiel finds he has been long enough in Derry. Remarkable to relate, the city was able to get on without him.” (Hamilton, pp. 85-86)"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The tensions between the Churches were however well on the way to being resolved by the beginning of the Home Rule period. This helped pave the way for the formation of a broad Protestant front around opposition to Home Rule. Very rapidly, the hostility evident in the barbed remarks on both sides, would be a thing of the past, and Presbyterians and Anglicans would be found working together on a common, “Protestant” agenda under the banner of unionism.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Mackenzie’s Memorials of the siege of Derry: including his narrative and its vindication; with an introduction and notes by W. D. Killen, Belfast, C. Aitchison, London, Hamilton, Adams & Co., 1861.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: " Advertisements in the Londonderry Sentinel, Saturday 03 April 1830, showing the large range of possibilities open to people hoping to emigrate from Ulster at this time.\_"
    uuid: 92c59eec-f292-4f3c-9e70-82e5c97276e5
    type: content
    enabled: true
    image: MOD2-FRA7-SCR2.jpg
    allow_zoom: true
---
