---
id: 45c17a20-b789-4106-b2d9-050cdfe948ed
blueprint: module_2
title: 'Material Prosperity'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654870644
unit_content:
  -
    image: MOD2-FRA2-SCR1-1638612951.jpg
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Many of those writing from an Ulster-Scots perspective in the final decades of the 19th century are keen to portray their community as a civilising and stabilising force in society. This representation keeps coming up in the accounts of the early settlers in counties Down and Antrim.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the following passage, Rev. Hamilton, in his History of the Irish Presbyterian Church, first published in 1886, right at the start of the Home Rule crisis, presents what is, in many ways, a standard version of the narrative of the impact of the Ulster Plantation, especially with regard to its Scottish components.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Soon the sound of the axes of many settlers rang through the forests of Ulster, and of their crowbars and hammers in the quarries, as they felled trees and hewed out stone for the erection of homesteads. Castles, houses, and bawns rose quickly all over the country. The fields presented scenes of busy labour such as they had long been strangers to. Cattle browsed peacefully on the rich pasture lands, herds of swine fattened in the woods, while plough and harrow and spade transformed what had been a waste into such fields as the new inhabitants had left behind them across the Channel.'
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_They needed to be men of stout heart and watchful eye, as well as skilful and industrious hand. In the long nights of winter the howling of packs of hungry wolves around his dwelling admonished the settler of the need of bawn"
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' (1)'
              -
                type: text
                text: ' and well-barred gate, while the saffron-dyed garment of a wood-kerne '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (2)
              -
                type: text
                text: ', gleaming in the distance among the trees, or an arrow flying from his bow of tough native yew, often told the newcomers that they were in an enemy''s country. But they bravely held their ground.'
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_Clusters of houses, destined to be the germs of important towns, rose quickly under the hands of mason and carpenter, each with its earthen rampart or its stone wall for protection against the enemy. Londonderry grew up on the Foyle, and Coleraine at the mouth of the Bann. A few thatched houses, with a humble Presbyterian Church amongst them, formed themselves into Ballymena in the heart of the County Antrim."
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_Carrickfergus had already crouched for four hundred years behind the shelter of its castle, and a church and castle, with a few fishermen's cabins, also marked the site where Belfast, the town of the Lagan ford, was destined to develop into the chief commercial town of Ireland, with a busy population of nearly a quarter of a million of inhabitants. But both towns received a great accession of strength and influence from the Plantation, while in County Down such places as Newtownards, Bangor, Donaghadee, and Killyleagh owe their existence to the same potent factor."
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Plantation altered the whole history of the north of Ireland. To it may largely be attributed the fact that Ulster, which has fewer natural advantages than either Munster, Leinster, or Connaught, is the most prosperous, the most industrious, the most law-abiding, and the most loyal part of all Ireland. (Hamilton, pp. 30-31)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' A “bawn” is a fortified dwelling built by the principal undertakers in the Ulster Plantation. They were intended as places of refuge in case of an uprising. The word comes from the Irish word, bádhún, meaning a walled enclosure.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (2)
                  -
                    type: text
                    text: "\_ “Wood-kerne” here means an Irish outlaw."
    uuid: 54de248a-7109-45c1-b913-1c4785570c50
    type: content
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Hamilton uses a series of verbs to suggest the energy of the settlers: thus, they “fell trees,” they “hew out stone.” The result is immediately evident in the landscape.  “Castles, houses, and bawns'
          -
            type: text
            marks:
              -
                type: bold
            text: ' '
          -
            type: text
            text: 'rose quickly.” Signs of prosperity were to be seen everywhere: “Cattle browsed peacefully” and “herds of swine fattened in the woods.” Through their work they are seen as replicating Scotland in Ireland: “plough and harrow and spade transformed what had been a waste into such fields as the new inhabitants had left behind them across the Channel.” All this is done “with a skilful and industrious hand.” The idea of the painstaking transformation of a “waste land” into what is presented as the most prosperous region of Ireland will become a standard version of the Plantation in  Ulster unionist writing.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'There is, nonetheless, a certain sense of nervousness, due to “the howling of packs of hungry wolves” in the long winter nights and, more ominously, the allusive reference to “the saffron-dyed garment of a wood-kerne,” telling us that the Irish inhabitants who had been pushed off the land by the Plantation were still there, living as outlaws in the woods. Hence, the idea of a community under constant threat of attack.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, at the same time, the passage insists that this community is one which does everything in its power to ensure its own security, as witness the references to “castle,” “bawn,”'
          -
            type: text
            marks:
              -
                type: bold
            text: ' '
          -
            type: text
            text: 'and “well-barred gate,” not to mention the “earthen rampart or […] stone wall” that surrounded the growing towns. The author therefore shows that the settlers are able – and determined – to look after themselves.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The passage outlines the growth of the towns, holding its readers’ attention by listing the names  of familiar places such as Londonderry, Coleraine, Ballymena, Carrickfergus, Newtownards, Bangor, Donaghadee, and Killyleagh, many of which are in Ulster-Scots areas. The author therefore suggests that they are behind the urbanisation of the province.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although Hamilton is supposedly talking about history, it is clear that he wants his reader to see connections between that history and the present day (1886). Hence the reference to the fact that “Belfast […] was destined to develop into the chief commercial town of Ireland, with a busy population of nearly a quarter of a million of inhabitants.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'More importantly though, when he tells his readers how “[t]he Plantation altered the whole history of the north of Ireland,” and how, as a result, Ulster “is the most prosperous, the most industrious, the most law-abiding, and the most loyal part of all Ireland,” he is clearly saying that the prosperity and stability that Ulster Scots attribute to the Plantation period have continued to characterise these areas to this day. Authors will frequently underline continuities of this sort. '
    uuid: e92c8982-8608-4b41-904d-72b4e2c563cd
    type: content
    enabled: true
    image: MOD2-FRA2-SCR2.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This version of the Plantation comes through strongly in the work of Ulster-Scots authors, even those who, unlike Rev. Hamilton, are not writing within the frame of Presbyterian Church history. We think for example of a text published more than thirty years later by John Stevenson, Two Centuries of Life in Down. This book, which appeared in 1920, the year the Government of Ireland Act laid down the conditions for the creation of Northern Ireland, contains the following account of the impact of the initial Plantation scheme in County Down. Quoting from The Montgomery Manuscripts 1603-1706, edited by Rev. George Hill and published in 1869, Stevenson, who was himself one of the most important figures in the printing business, insists on the “industry” of the early settlers.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'A contemporary reader (1920) would immediately pick up the references to flax growing, and the fact that Elizabeth Shaw “encouraged linen and woollen manufactory,” since the textile industry was one of the most important sources of income in Ulster at the Home Rule period. The other area that Shaw was interested in - agriculture and food production – is, of course, another key activity in the area. '
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_Montgomery had, in Elizabeth Shaw [his first wife], a helper to whose wisdom and energy much of the success of his settlement was due. [...] Of her labours the Montgomery historian has to say:"
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“Her ladyship had also her farms at Greyabbey and Coiner (Comber) as well as at Newtown, both to supply new-comers and her house; and she easily got men for plough and barn, for many came over [from Scotland] who had not stocks to plant and take leases of land, but had brought a cow or two and a few sheep, for which she gave them grass and so much grain per annum, and an house and garden-plot to live on, and some land for flax and potatoes, as they agreed on for doing their work […]; and this was but part of her good management, for she set up and encouraged linen and woollen manufactory, which soon brought down the prices of […] cloth […].”"
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_Looking back three-quarters of a century to the Colony's early days, the writer casts an admiring glance at the capable lady, ordering well her household, and the larger household of the settlement [...} (Stevenson, pp. 66-68)"
    uuid: 94488adc-b80d-4aae-b300-228bfab42d05
    type: content
    enabled: true
    image: MOD2-FRA2-SCR3.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This emphasis on the material prosperity associated with the Plantation period is seen as a model for a prosperity that continues in Ulster into the 19th, and indeed the 20th century. The following extracts from an article entitled, “The settlement of Ulster,” published in The Edinburgh Review in April 1869, underline how those sympathetic to the Ulster-Scots cause are keen to underline continuities of this sort between the 17th century and the period in which they are writing.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The article quotes a passage from Johann Georg Kohl, a German traveller to the region. An English translation of his Reisen in Irland (1843) was published in 1844 (Travels in Ireland), the year before the Great Famine. You might wish to compare what he says with similar passages from other visitors to Ulster that we looked at in Module 1. The quotation from Kohl is useful because it confirms the author’s underlying thesis – that Ulster is radically different from the rest of Ireland, and that that difference is immediately visible in the landscape. What is more, Kohl attributes this specifically to what he calls “a totally different people, namely […] the Scottish settlers — the active and industrious Presbyterians.” These comments are all the more valuable to the argument the article is trying to make, in that, since Kohl is a foreigner, readers will likely assume that he has less of an axe to grind than an Irish or British author.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The author of the Edinburgh Review article sees the contemporary Ulster-Scots community as reflecting the energy and enterprise of the early settlers. This continuity is evident not only in their advanced farming methods, but also in the industrial base that, the article claims, sets a more urbanised Ulster apart from the other provinces of Ireland. The greater industrialisation of Ulster is going to be an important element in the arguments unionists will put forward against Home Rule. They will argue that the Ulster economy is intimately built into the broader British and imperial economy and needs those connections if it is to maintain its sources of raw materials and export markets. They will claim that a future Home Rule Ireland would favour agricultural production over industrial production and, as a result, the northern economic base would suffer irreparable damage.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '[T]he crowning benefit of the Plantation was that it laid the foundation of Ulster prosperity, and eventually formed a middle-class, hardly anywhere else to be found in Ireland. […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The traveller is always struck by [Ulster’s] superiority over the other provinces.The German tourist Kohl, who traversed the greater part of Ireland in 1842, says: “Not far from Newry, the province of Leinster ends and that of Ulster begins. The coach rattled over the boundary line, and all at once we seem to have entered a new world. I am not in the slightest degree exaggerating when I say that everything was as suddenly changed as if struck by a magician’s wand […] Regular plantations, well-cultivated fields, pleasant little cottage gardens, and shady lines of trees, met the eye on every side. The improvement lasted all the way to Newry, and from Newry to Belfast everything continued to show me that I had entered the country of a totally different people, namely the district of the Scottish settlers — the active and industrious Presbyterians.” But the Plantation effected more than the mere creation of a rural population, industrious, moral, and peaceful. It founded the manufacturing superiority of the northern province. Manufactures have never taken root in the south, though many attempts have been made to plant them there; […] while the great linen trade of the north has created one of the finest commercial capitals in the United Kingdom, besides a number of smaller centres that are growing rapidly in population and wealth. (Edinburgh Review, CCLXIV, pp. 439-441).'
    uuid: 87720700-b274-4c09-857a-5ad7c9cac853
    type: content
    enabled: true
    image: MOD2-FRA2-SCR4.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "In America, we find a similar story. When the Ulster Scot arrives on the east coast, he is pushed out on to the frontier and then on into “the wilderness.” The often idealised histories that are written at this period show how, through their energy and courage, this supposedly “empty” space is transformed into prosperous farming land. Like many other groups of settlers, the Scotch-Irish never question their “right” to these territories.\_ The following passage is taken from an address delivered to the first Scotch-Irish Congress held in Columbia, Tennessee, in May 1889 by Ex-Governor Proctor Knott of Kentucky"
          -
            type: text
            marks:
              -
                type: bold
            text: '. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The passage is interesting because Knott writes in almost identical terms about the two migrations – that of the Scots to Ulster at the time of the Plantation, and the second, from Ulster to America at the beginning of the 18th century. The image Knott is trying to project in this highly idealised representation of Ulster-Scots and Scotch-Irish society is that of a self-reliant community (“they organized,” “they set up”) whose energy has a positive influence throughout the surrounding society (“set an example of industry, economy and morality.”) In both cases, he focuses on the institutions developed by the community – “their churches, […] their presbyteries, […] their schools.” In both cases, Knott insists on the idea of “thrifty industry” and on the capacity of the community to “transform” the area for the better. He does not hesitate to flatter his Scotch-Irish audience, remarking that the influence of these early settlers “is still visible in the intelligence, thrift, refinement and orderly deportment which distinguish the communities in which they settled.”'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Soon after his accession to the English crown, King James I […] commenced the plantation of a Scotch and English colony in the year 1609. The fertility of the soil, the favorable terms upon which it was proposed to be let to immigrants, and the advantages offered to them by a variety of other circumstances, soon lured a number of his countrymen [i.e. Scots] to this promising plantation, whither they were followed from time to time by others of their kindred until they became eventually the predominant element throughout the province […] They carried with them to their new homes not only the personal traits peculiar to their race, but its political and religious prejudices as well as its ecclesiastical polity. They built their churches, organized their presbyteries, established their schools, and pursued their respective callings with a thrifty industry which soon transformed the province of Ulster from the wildest and most disorderly to the best cultivated and most prosperous portion of Ireland\_[…]"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Later in the address, Knott goes on to talk about the subsequent migration of this Ulster-Scots community to America – in exactly the same terms:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“They came over in groups of families, neighbors, perhaps in the homes they had left in Ulster, and located themselves, generally, in the colonies of Pennsylvania, Virginia, and the Carolinas, where, as their fathers had done in Ireland, they organized their congregations, set up their neighbourhood schools, and by a sedulous attention to their own affairs, set an example of industry, economy and morality, the influence of which is still visible in the intelligence, thrift, refinement and orderly deportment which distinguish the communities in which they settled. (Knott, pp. 84-88)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The moral is clear: whether it is in Ulster or in America, the Ulster Scot, or his American cousin, the Scotch-Irishman, ensures economic growth and surrounds himself with a network of institutions – churches, presbyteries, schools, the professions – that produce a solid, “orderly” society. The Ulster Scot is therefore presented as being a force for prosperity and stability wherever he chooses to settle.'
      -
        type: paragraph
    uuid: 5c00ffb5-07b2-4d21-abaa-21fb14703788
    type: content
    enabled: true
    image: MOD2-FRA2-SCR5.jpg
    allow_zoom: true
---
