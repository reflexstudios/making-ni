---
id: 2873ab3d-8e6e-4290-b16f-b473717a8c6b
blueprint: module_2
title: 'The Frontier'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655807681
audio: INDIAN-Jaimie-1638568492.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Ulster Scots are often associated with the Frontier.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "This is the case, for example, when they arrive in Ulster at the time of the initial plantations, when they are faced with the threat of “the wolf and wood-kern.” \_It is also the case when they emigrate to America and are pushed away from the coast towards the frontier zone that separates the settler population from the territories of the American Indians."
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following poem, entitled “Jamie Cochran, the Indian captive,” was written by the New Hampshire poet, Robert Dinsmoor, in 1833. '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: " Dinsmoor was the descendant of Ulster-Scots settlers who emigrated to America in the 1720s. An edited collection of his poems was published in Boston in 1898, at a period when there was growing interest in the role of the Scotch-Irish in the construction of the United States. Dinsmoor’s poems are of considerable interest. Some contain a number of Ulster-Scots words and expressions, showing how the emigrants’ language use continued to be an influence several generations after the initial emigration.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The poem centres on Jamie Cochran, whose father had come over to Maine from Co. Londonderry in the first major Ulster-Scots migration of 1718. Cochran senior and his four brothers, all of whom migrated to America, had all been present in Londonderry during the siege.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'A note to the 1898 collection explains that the events described in the poem date from 1728, i.e. exactly ten years after the arrival of the first five migrant ships from Ulster. Dinsmoor, who wrote the poem in 1833, used the story to recall the orgins of the Cochran family, now long established in America. He tells his readers that the family is originally from Argyleshire in Scotland. They “found a shelter” in “Ulster Province, Erin’s northern strand” before emigrating to America in 1718. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Interestingly, Dinsmoor goes out of his way to underline that, although they had remained almost a century in Ireland, “[t]hose emigrants were not Hibernian blood, / But sturdy Scotsmen true.” This suggests that, already in the 1830s, Dinsmoor thought it necessary to make a clear distinction between the people of “Scotch-Irish” descent and those of “Hibernian” origin. This will become increasingly important in the years after the Famine due to the mass migration of people of Irish Catholic origin to America.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Dinsmoor hints at the piety of these early settlers by slipping in the fact that when they left Ulster, “[t]hey had their ministers to pray and preach.” This remark is clearly linked to his reference to the story of David and Goliath, one of the best-known incidents in the Old Testament (1 Samuel, 17). Dinsmoor can be certain that his public will make all the necessary connections: like David, Jamie had gained his experience with weapons hunting in the wilderness, and, like David, Jamie seems faced with impossible odds. However, the poem suggests that there is another similarity that is key to the outcome: just as David places his trust in what the Old Testament text calls “the living God,” the “Lord of hosts,” so Jamie calls on his “father’s God” for protection before winning the day, single-handedly. When Dinsmoor compares Jamie to “that young Hebrew stripling,” he is illustrating a fundamental reflex within Ulster-Scots culture that constantly draws parallels between the experience of the Ulster Scot and those of Old Testament Israel.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: 'Robert Dinsmoor, “Jamie Cochran: the Indian Captive” [1833], in The Rustic Bard, Boston, Mass., Damrell & Upham, 1898, pp. 43-53.'
    uuid: 47e1de93-3c45-4533-abbf-19dd0503de9e
    type: content
    enabled: true
    allow_zoom: true
    image: MOD2-FRA11-SCR5-copy-min.jpg
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "A surprising aspect of this “captivity narrative” is that, despite his obvious admiration for his young “hero,” Dinsmoor nevertheless builds in to the poem a statement of a fundamental American Indian grievance, voicing their anger at\_what they see as the lies and double standards of the settlers\_and at the way they are threatening their way of life by hunting game on their territory without seeking permission or offering compensation."
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The hero of my tale is Indian Jamie,'
              -
                type: hard_break
              -
                type: text
                text: 'His history I''ll give lest you should blame me.'
              -
                type: hard_break
              -
                type: text
                text: 'In Ulster Province, Erin''s northern strand,'
              -
                type: hard_break
              -
                type: text
                text: 'Five shiploads joined to leave that far off land.'
              -
                type: hard_break
              -
                type: text
                text: 'They had their ministers to pray and preach,'
              -
                type: hard_break
              -
                type: text
                text: 'These twenty families embarked in each.'
              -
                type: hard_break
              -
                type: text
                text: 'Here I would note and have it understood,'
              -
                type: hard_break
              -
                type: text
                text: 'Those emigrants were not Hibernian blood,'
              -
                type: hard_break
              -
                type: text
                text: 'But sturdy Scotsmen true, whose fathers fled'
              -
                type: hard_break
              -
                type: text
                text: 'From Argyleshire […]'
              -
                type: hard_break
              -
                type: text
                text: 'They found a shelter on the Irish shore'
              -
                type: hard_break
              -
                type: text
                text: 'In Ulster, not a century before.'
              -
                type: hard_break
              -
                type: text
                text: 'Four of those ships at Boston harbor landed;'
              -
                type: hard_break
              -
                type: text
                text: 'The fifth, by chance, at Casco Bay was stranded.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '[…]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'When Jamie''s blood had felt the heat'
              -
                type: hard_break
              -
                type: text
                text: 'Of sixteen summers, high his pulses beat.'
              -
                type: hard_break
              -
                type: text
                text: 'He then from bears could guard his father''s corn,'
              -
                type: hard_break
              -
                type: text
                text: 'Armed with his gun, shot-bag, and powder horn.'
              -
                type: hard_break
              -
                type: text
                text: 'The howling wolf that he was wont to hear'
              -
                type: hard_break
              -
                type: text
                text: 'And catamount [a wild cat] made music to his ear.'
          -
            type: paragraph
            content:
              -
                type: text
                text: …
          -
            type: paragraph
            content:
              -
                type: text
                text: 'As deeds of valor add to courage strength,'
              -
                type: hard_break
              -
                type: text
                text: 'So this young hero proved it out at length.'
              -
                type: hard_break
              -
                type: text
                text: 'Like that young Hebrew stripling, when he slew'
              -
                type: hard_break
              -
                type: text
                text: 'A bear and lion — more courageous grew'
              -
                type: hard_break
              -
                type: text
                text: 'And fearless, fought and killed Goliah too.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Jamie goes on a hunting expedition and is captured by a band of “Indian warriors.”'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'A flock of ducks, through the thick air above,'
              -
                type: hard_break
              -
                type: text
                text: 'With whistling wings, all lighted in a cove'
              -
                type: hard_break
              -
                type: text
                text: 'Within short distance ; Jamie cocked his gun'
              -
                type: hard_break
              -
                type: text
                text: 'And made towards his game upon the run.'
              -
                type: hard_break
              -
                type: text
                text: 'His fire was true, and plainly he could tell'
              -
                type: hard_break
              -
                type: text
                text: 'Some lay dead, and others wounded fell.'
              -
                type: hard_break
              -
                type: text
                text: 'He left his musket on the shore,'
              -
                type: hard_break
              -
                type: text
                text: 'His powder horn, shot-bag, and all his store;'
              -
                type: hard_break
              -
                type: text
                text: 'With ducks and drakes his knapsack soon was filled,'
              -
                type: hard_break
              -
                type: text
                text: 'No matter then, how many he had killed.'
              -
                type: hard_break
              -
                type: text
                text: 'With success flushed he turned toward the shore'
              -
                type: hard_break
              -
                type: text
                text: 'And lo ! he saw an armed Sagamore [a chief]'
              -
                type: hard_break
              -
                type: text
                text: 'Take up his gun, powder horn, and shot;'
              -
                type: hard_break
              -
                type: text
                text: 'Ten Indian warriors stood there on the spot.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The leader of the group explains that Jamie is their captive and that they intend to sell him to the French in Canada with whom the British had been at war.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'You shoot our bears, the Indians want their grease,'
              -
                type: hard_break
              -
                type: text
                text: 'You shoot our ducks, and carry off our geese;'
              -
                type: hard_break
              -
                type: text
                text: 'You kill our moose and deer, no heed our speeches,'
              -
                type: hard_break
              -
                type: text
                text: 'Eat up their flesh and wear their skins for breeches;'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although they intend to take him to Canada to sell him to the French, they do not hesitate to tell him bluntly that, if he resists, “your scalp may do as well.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Jamie goes out of his way to obey their orders and gradually wins their confidence, all the time planning to escape.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Now Jamie tried his masters to obey,'
              -
                type: hard_break
              -
                type: text
                text: 'Nor made the least attempt to run away.'
              -
                type: hard_break
              -
                type: text
                text: 'He seemed to place his life in their protection,'
              -
                type: hard_break
              -
                type: text
                text: 'And by hypocrisy gained their affection.'
              -
                type: hard_break
              -
                type: text
                text: 'As they grew intimate, he seemed contented;'
              -
                type: hard_break
              -
                type: text
                text: 'They lived like brothers when they got acquainted.'
    uuid: 0758fe72-81a3-4245-a004-64170a0d562f
    type: content
    image: MOD2-FRA12-SCR2.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although he is clearly horrified by the idea, he decides that the only way he can escape is to kill the two young men who are guarding him.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'And Jamie''s mind absorbed in deep reflection,'
              -
                type: hard_break
              -
                type: text
                text: 'Besought his father''s God for his protection. […]'
              -
                type: hard_break
              -
                type: text
                text: 'But Jamie''s mind on his escape was bent,'
              -
                type: hard_break
              -
                type: text
                text: 'That to accomplish was his whole intent. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He waits his chance, and, having carefully prepared his attack, cold-bloodedly murders both of his captors.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The poet, who clearly admires the survival instinct of the young captive, ends describing how what he calls “this poor Scottish boy” manages to make it back home, where he is received with open arms.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'His pack supplied him to his father''s home,'
              -
                type: hard_break
              -
                type: text
                text: 'Where parents mourned as dead their favorite boy.'
              -
                type: hard_break
              -
                type: text
                text: 'There they embraced him with ecstatic joy.'
              -
                type: hard_break
              -
                type: text
                text: 'Gladly they saw the trophies he had won,'
              -
                type: hard_break
              -
                type: text
                text: 'While he returned the knapsack and the gun.'
              -
                type: hard_break
              -
                type: text
                text: 'The Indian scalps proved Jamie''s victory grand,'
              -
                type: hard_break
              -
                type: text
                text: 'As did Goliah''s head in David''s hand. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The poem – a real curiosity – is of interest in that it gives us an idea of the complex nature of the contacts that were taking place along the American frontier in the early days of the initial Ulster-Scots migration. Despite the naivety of its form and style, Dinsmoor’s text shows something of the breadth of emotion generated by these - often extremely violent - contacts with the unknown “Other” and the mixture of curiosity and fear evident on both sides.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He is clearly not interested in exploring the many complex moral issues behind the colonial experience that would occur to readers of today.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, despite these silences, the narrative tells us about another important aspect of Ulster-Scots culture – how the descendants of these early settlers were reading their everyday experience through the lens of the Bible. There is clearly a readiness to confuse the space of the frontier and the wilderness of the Old Testament. Such constant “translation” of events in terms of a biblical model is a recurring cultural reflex.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' The Dinsmoor homestead. The 1718 Project Discover Ulster Scots '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://discoverulsterscots.com/emigration-influence/america/1718-migration'
                          rel: null
                          target: null
                          title: null
                    text: 'https://discoverulsterscots.com/emigration-influence/america/1718-migration'
    uuid: 81da1f1a-2678-400a-92aa-53800be7b0da
    type: content
    image: dismoor-homestead.jpeg
    enabled: true
    allow_zoom: true
---
