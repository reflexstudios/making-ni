---
id: b33827df-bc14-4d38-a2e7-f50d5be425d8
blueprint: module_2
title: Presbyterianism
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654018244
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Ulster-Scots literature of the Home Rule period constantly underlines the central importance of the Presbyterian Church in the construction and consolidation of the Ulster-Scots community.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This is true whether the history is produced in Ulster or by members of the Ulster-Scots diaspora in America. Thus, the American Charles A. Hanna, author of the two-volume, The Scotch-Irish or The Scot in North Britain, North Ireland and North America, published in New York in 1902, states categorically that the history of the Ulster Scots cannot be separated from the history of Presbyterianism in Ulster. Indeed, it is the religious factor – what he refers to as “the stern Calvinism” of the people - that he identifies as the key factor in allowing them to maintain a separate sense of identity since the beginning of the 17th century.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Hanna does not hesitate to tie his history in to the contemporary political situation on the ground in Ireland. We see this when he refers to how the descendants of the initial Scottish and English planters still wish, i.e. at the beginning of the 20th century, “to remain united with the countries from which their people sprang.” This is an obvious allusion to the Home Rule crisis. The picture he is giving is therefore one in which the past continues to influence, indeed, to determine the present.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“The north of Ireland is now very much what the first half of the seventeenth century made it. North Down and Antrim, with the great town of Belfast, are English and Scottish now as they then became, and desire to remain united with the countries from which their people sprang […] In many parts of Ireland which were at one time and another colonized with English, the colonists became absorbed in the native population; but in Ulster, where the Scottish blood is strong, this union has not taken place […] It is perhaps the stern Calvinism of these Scots, which still survives, that has prevented the colony from mixing with the surrounding people, and being absorbed by them […] The history of the Presbyterian Church is therefore an important part of the story of the Scot in Ulster; in fact, for many years the history of Ulster, as far as it has a separate history, is chiefly ecclesiastical. It must be so; for this is a story of Scotsmen and of the first half of the seventeenth century, and at that time the history of Scotland is the history of the Scottish Church.” (Hanna, Vol. I, pp. 504-505)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Throughout the literature, authors underline the idea of a special bond uniting the Ulster-Scots community to the Presbyterian Church. This may not be that surprising, given how much of the history of the Ulster Scot was produced by Presbyterian ministers. However, the importance of the link is recognised well beyond the circles of the church historians.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Referring to a period in the 1630s when Presbyterian ministers in Ulster were being persecuted by the Anglican bishops in Dublin who were trying to make them conform to the rites of the Church of Ireland, John Stevenson, the author of Two Centuries of Life in County Down 1600-1800 (1920), reminds his readers of the lengths to which members of the congregations were prepared to go in order to maintain their ties to their chosen ministers:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“A bishop might silence their ministers, but could not turn the hearts of the people from their principles, nor quench the love they held for their persecuted guides.The ejected ministers remained for a time in their parishes, and by private conversation and meetings of small numbers in convenient places, endeavoured unobtrusively to carry on their work."
          -
            type: paragraph
            content:
              -
                type: text
                text: 'How true the people, in the main, were to their deposed [ministers], is evidenced by the fact that, when Livingstone [a well-know Presbyterian minister] accepted a call to Stranraer, some of his hearers followed and settled there; and twice a year a large company in one case five hundred persons travelled to Donaghadee, and braved the risks and annoyances of a stormy passage in the small boats of the time, in order that they might attend the Communion service presided over by their loved minister. On one day he baptised twenty-eight children brought from the scene of his labours in Down.” (Stevenson, pp. 133-134).'
    uuid: 29e51f1a-383d-4431-afd7-04ecb0b3caa4
    type: content
    enabled: true
    image: MOD2-FRA3-SCR1.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although they have no problem recognising the enormous contribution of the early Ulster-Scots settlers to the prosperity of the areas in which they settle, some of the historians, especially those whose focus is the Presbyterian Church in Ireland, refer to what they see as the immorality of some of the first settlers and their distance from the Church. Rev. Reid, for example, in the first volume of his History of the Presbyterian Church in Ireland, quotes an earlier manuscript source from Rev. Stewart that paints a surprisingly negative picture of some of the early settlers.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Stewart suggests that a “great number” of the settlers who came to Ulster at the time of the Plantation were trying to get away from an awkward situation, such as debt or legal proceedings, at home. They were drawn to Ulster specifically “to be without fear of man''s justice.” Given the risks involved in settling in such a hostile environment, it is hardly surprising that the Plantation project should have attracted a broad range of people.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'According to Reid, Stewart’s depiction of a “godless” frontier society may be slightly exaggerated - “a little overcharged.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, just as the material success of the early Plantation was outstanding – Reid says the Plantation, “flourished amazingly” – the histories tell us how a similar transformation was witnessed in the area of the community’s spirituality. This is the theme of the following passage, which appears a few pages later in Reid’s history:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“It was not long before the zealous labours of the ministers […] began to be visibly blessed. A remarkable improvement in the habits and [manners] of the people was speedily effected. The thoughtless were roused to serious inquiry on the subject of religion… and the immoral reclaimed […] The revival of religion which occurred at this period subsequently attracted considerable attention both in Scotland and in England. The fame of it extended even to America, and it has been repeatedly referred to by religious writers of the last century, as one of those sudden and extensive manifestations of the power of divine grace upon a careless people, with which the church has been occasionally favoured.” (Reid, Vol. 1, p. 101)."
    uuid: 64b6ef3e-11ab-4143-9580-8aeadbf1c319
    type: content
    enabled: true
    image: MOD2-FRA3-SCR2.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Reid goes on to explain the origins of the religious revival that came to be known as the Six Mile Water Revival'
          -
            type: text
            marks:
              -
                type: bold
            text: ' '
          -
            type: text
            text: '(1625), from the name of the river in South Antrim where the events were concentrated. He refers to a certain Scottish minister, Rev. Glendinning, who preaches “the terrors of God” to a mesmerised audience, and who is considered to be behind what he sees as “the first important incident occurring in the history of the Presbyterian Church in Ulster.” Once again, he quotes from the manuscript history of Rev. Stewart, the man who had earlier denounced the immorality of the same settlers.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Here Stewart describes what would appear to be an almost miraculous transformation of the population. It is obvious that the reader is meant to see the Presbyterian Church as the chosen vehicle through which a divinely-inspired regeneration of the people is brought about. The transformation is all the more remarkable given the allegations concerning the moral condition of some of the initial settlers. In other words, the Presbyterian Church, through the agency of its preaching, is represented as a profoundly stabilising force within society, capable of taking sometimes dubious raw material and transforming it into “mighty Christians” almost overnight. This new-found morality is presented as having spread through the whole of Ulster-Scots society. Indeed, it is represented as a form of collective purification.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'What is at stake here is projecting an image that makes it clear that the Presbyterian Church is the key institution within the community, that it ensures the “godliness” of that community and that, by extension, it contributes to the stability of Ulster – and Irish - society as a whole.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“But behold the success! For the hearers […] fell into such anxiety and terror of conscience that they looked on themselves as altogether lost and damned; and this work appeared not in one single person or two, but multitudes were brought to understand their way, and to cry out, […] what shall we do to be saved? I have seen them myself stricken into a swoon with the word; yea, a dozen in one day carried out of doors as dead; so marvellous was the power of God smiting their hearts for sin, condemning and killing. And of these were […] indeed some of the boldest spirits who formerly feared not with their swords to put a whole market-town in a fray... I have heard one of them, then a mighty strong man, now a mighty Christian, say that his [purpose] in coming to church was to consult with his companions how to work some mischief. And yet at one of those sermons was he so catched, that he was fully subdued.” (Reid, Vol. I, pp. 102-103)"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Many authors do not hesitate to tie the events of 1625 in to the “1859 Revival” which saw similar manifestations of spiritual awakening in certain, primarily Ulster-Scots areas in County Antrim. The events of 1859 are linked to similar expressions of evangelical activity in the American Awakening of 1857-8. This again underlines the extent to which local Ulster-Scots communities were tuned in to the international networks of evangelical Protestantism.'
    uuid: 566ea371-70e7-45c3-a38c-ba4d0839fa8b
    type: content
    enabled: true
    image: MOD2-FRA3-SCR3.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Another essential component of the links between the Ulster Scot and a Presbyterian heritage is through the influence of the Covenanters. The name “Covenanter” is linked to two Presbyterian covenants drawn up in Scotland in the first half of the 17th century: the National Covenant and the Solemn League and Covenant.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The National Covenant was read out in 1638 in Edinburgh. By signing the Covenant, people were making a public demonstration of resistance to the policy of the king, Charles I, who was trying to impose his authority on the way the Church of Scotland was run. The king had sought to change the form of church services and to impose himself as Head of the Church through consolidating the power of the bishops. This policy was seen as a direct attack on the way the Presbyterian system, with its elected elders and assemblies, worked. A similar attempt to impose conformity had been made in Ulster, where many of the Presbyterian ministers had been thrown out of their churches because they had refused to respect the new rules the State was trying to impose. Copies of the National Covenant were sent to the Presbyterian communities in Ulster where there was considerable sympathy for the Covenanter cause.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the wake of the rising in Ulster in 1641, a Scottish Presbyterian army arrived in Ulster to defend the Ulster Scots. This army, accompanied by a number of Scots Presbyterian ministers, set up the first Presbytery in Ulster, in Carrickfergus in June 1642. The presence of this army facilitated the signing of a second covenant, called the Solemn League and Covenant, which was drawn up by the Scots in 1643 in an attempt to have Presbyterianism recognised as the established form of religion throughout the three kingdoms of Scotland, England and Ireland.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Covenanters are closely associated with the repression of which they were the victims. From the Restoration of the monarchy in 1660, and up until the Glorious Revolution in 1688, the Covenanters were the targets of increasing repression. Ministers were driven from their pulpits; congregations were forced to meet illegally “in solitary places.” (Cf. Gamble’s account of a conventicle in Module 1) As repression increased in Scotland, with troops being used to impose fines on people refusing to attend approved parish services, the situation quickly deteriorated, ending in sporadic risings followed by merciless government repression. The 1680s, known as the “Killing Times,” which saw the active persecution of the Covenanter population across Scotland, came to an end after William III became king in 1688 at the time of the Glorious Revolution. During this period, Covenanters from Scotland frequently crossed over into Ulster seeking refuge from the authorities in the Ulster-Scots communities.'
    uuid: fd85d6ec-68f7-407c-b15c-78ebe66435c3
    type: content
    enabled: true
    image: MOD2-FRA3-SCR4.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The church histories are full of stories that are designed to underline the sufferings of the Covenanters and the dignity of their resistance to state oppression.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following passage from Rev. Hamilton’s, History of the Irish Presbyterian Church, deals with one of the most notorious incidents connected with the signing of the National Covenant in Ulster. In 1639, the Lord Lieutenant of Ireland, Thomas Wentworth, determined to intimidate the Ulster Scots to prevent them from helping the Scots Presbyterians who were gearing up for armed confrontation with the Crown. The result was what came to be known in Presbyterian circles as the “Black Oath.” Hamilton explains that although the people are quite prepared to swear allegiance to the Crown, they are not prepared to renounce the Covenant, which is seen as guaranteeing “the maintenance of true religion.” Similarly, they are not prepared to promise that “no matter what [the king] might command, they would never oppose him.” The Covenant, and the Presbyterian Church had taught them that there may be limits to loyalty and that obedience to the king may be conditional on his respecting his duties towards his people. In any case, they were clearly not prepared to sign a blank cheque. Expressions such as, “One’s blood begins to boil,” leaves the reader in no doubt that the author feels directly involved in the tale he is telling. There is no question of distance or objectivity. The author takes a clear line that he does not hesitate to push with the greatest force.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Presbyterians – seen as dangerous sectaries by the authorities – are represented as victims of a tyrannous State. The representation of the female Presbyterian as victim is, of course, meant to be particularly powerful. Thus we are told of “[r]espectable women, far advanced in pregnancy, [being] forced to travel long distances to appear before the Commissioners,” and of “crowds of defenceless females” having to seek “refuge in mountain caves.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although Hamilton describes the events as “harrowing,” indeed “sickening,” this does not prevent him from going into the story in some considerable detail. We must remember that he is writing this history for a primarily Presbyterian audience. The highly emotional tone he uses throughout this passage is therefore designed to ensure that his - Presbyterian - readers will feel nothing but outrage at the account of such barbaric treatment. In its turn, this contributes to making people feel sympathy for the Covenanter cause in the broader Presbyterian community.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“In their fury the dominant party [King Charles'' advisors] now resolved upon the adoption of a new weapon [...] This was an oath, popularly called the Black Oath, which all Presbyterians were to be obliged to take, or suffer the ‘uttermost and most severe punishments […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_Great numbers of the people refused to take such an oath. They were quite prepared to swear allegiance to the King, notwithstanding all that they had suffered under him. But to promise that, no matter what he might command, they would never oppose him, and to renounce the covenants which had proved such safeguards and helps to the maintenance of true religion, they felt they dared not, and they determined to refuse, and abide the consequences. Those consequences Wentworth and […] the bishops were not slow to visit on them to the very utmost. The details of the cruelties which they inflicted are not merely harrowing they are sickening. […] Respectable men were, on their refusal to perjure themselves by taking the Black Oath, bound together with chains and thrown into dismal dungeons. [Officers] from Dublin travelled up and down Ulster, arresting and imprisoning. The most exorbitant penalties were inflicted. […] Respectable women, far advanced in pregnancy, were forced to travel long distances to appear before the Commissioners. If they hesitated to attend, or refused to swear, they were treated in the most cruel manner, notwithstanding their condition. The woods in some parts of the country were thronged with crowds of defenceless females who had fled [there] to escape the persecutors who had no mercy on either sex or age. […]” (Hamilton, pp. 52-54)"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Beyond showing the Presbyterian as the victim of cruel State repression, the Covenanter story is a perfect illustration of a theme that becomes increasingly important during the Home Rule period – the idea that Scotland extends into Ireland through the culture and mind-set of the Ulster-Scots community. A shared Presbyterianism is the key factor in this equation. This is made obvious by the constant to-ing and fro-ing of people between Lowland Scotland and Ulster during periods of religious repression. This is taken as concrete evidence that both belong to a single cultural space.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Perhaps the most striking symbolic expression of these links is the choice of the Solemn League and Covenant as the model for the Ulster Covenant in 1912. We will look at this in more detail in Module 3. In choosing this historical reference, the unionists are consciously tapping in to this shared history, with all its associated emotion.'
    uuid: 916cdfc8-4ccb-41d6-8c9d-fb1afd27d3da
    type: content
    enabled: true
    image: MOD2-FRA3-SCR5.jpg
    allow_zoom: true
---
