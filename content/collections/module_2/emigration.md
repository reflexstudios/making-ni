---
id: f07994d5-8139-4320-aee4-6b6ece0d22be
blueprint: module_2
title: Emigration
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655726650
audio: how-hideous-the-hold-1638557968.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'When the Scots settlers came over to Ulster at the beginning of the 17th century, they often came over in family groups.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'We find exactly the same reflexes when the descendants of these initial Ulster Scots decide to leave Ulster and migrate to America.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following extracts are taken from Charles Knowles Bolton’s, Scotch Irish Pioneers in Ulster and America, published in Boston in 1910. The author underlines that the migration from Ulster to America that began in 1718 involved the movement, not so much of individuals, but rather of entire communities. Several families therefore would move across the Atlantic as a group, often accompanied by their ministers. Again, what is important is that the family is the central social unit. Furthermore, it is the family connection that will facilitate the migration of other members in the years to come through “chain migration.” The notion of “kith and kin” is at the heart of this particular form of solidarity.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The passage gives us some idea about how people in Ulster picked up their information about what life was like in America. As soon as the migration got under way, and letters from family members started coming back from the other side of the Atlantic, people’s sources of information would soon become more personal – and, probably, more accurate. In any case, what they heard clearly attracted them, because, when Bolton describes the level of interest in emigration, he uses the expression: “like a fever.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The issue of the testimonial vouching for the moral credentials of the emigrants and their families underlines the moral reach of the Church on both sides of the Atlantic. It also demonstrates the power of the individual minister within the community. This testimonial would be presented to the minister of the new congregation on arrival at their destination in America and would admit them “to Church priviledges,” and, by extension into the church network on the other side of the Atlantic. The same practice of course applied when people moved anywhere outside their area, for example, to Scotland.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "“It would not be difficult to picture to ourselves the excitement produced by the preparations of those who contemplated removing to America. Families were closely allied in Ulster, and the affairs of each one interested a wide circle. The itinerant weaver brought from Dublin tales of the New World, more or less accurate accounts of the life across the Atlantic, derived from ship captains, or even from American students at the University there […] [T]he desire for emigration had gone through Ulster like a fever […]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'There was much to be done by a family before removal. A supply of food, clothing and bedding was necessary; and the house-hold goods had to be packed for the long voyage. The land, the farm animals and the heavier tools must be sold. These were busy days, and the partings must have been hard for all, unless friends hoped to follow soon. In leaving their Churches the emigrants did not fail to procure testimonials of good standing to be used in forming fresh religious ties in New England. We find mention of these testimonials at Rutland, at Needham, Middleboro and elsewhere, but rarely the actual text. That brought over by William Caldwell, one of the defenders of Londonderry, was lost only a few years ago. It was written on parchment the size of a half sheet of note paper:'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“The bearer, William Caldwell, his wife Sarah Morrison, with his children, being designed to go to New England in America. These are therefore to testifie they leave us without scandal, lived with us soberly and inoffensively, and may be admitted to Church priviledges. Given at Dunboe Aprile 9, 1718, by Jas. Woodside, Jr. Minister.” (Bolton, pp. 130-131)'
    uuid: 242839b0-4dec-43d7-97ed-3f8d8f5a2062
    image: MOD2-FRA11-SCR1.jpg
    type: content
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the following passage, Bolton is discussing the arrival of some of the earliest Ulster settlers on the “common field” about the mouth of West-running Brook [New Hampshire]. He lists the names of the sixteen men and whatever details he can find about their lives and those of their family members. The sixteen in question are: James McKeen, James Gregg, John Barnett, Archibald Clendenin, John Mitchell, James Sterrett, James Anderson, Allen Anderson, Randal Alexander, James Clark, James Nesmith, Robert Weir or Wear, John Morison, Samuel Allison, Thomas Steele and John Stuart.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The passage underlines how these families all came from the same areas in Ulster. They are frequently interconnected through marriage or because of shared business interests. One of the businessmen behind the venture, John McKeen, dies just before the group is due to leave. Interestingly, his widow decides to continue with her emigration plans. The fact that she should decide to emigrate with her four children suggests a high level of solidarity among the families. The other key component in this system of solidarity within the community is again the minister who clearly acts as the leader and spokesperson of the group.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“With the sixteen settlers should be associated the Rev. James McGregor who married Marion Cargill, the sister of Mrs. McKeen and Mrs. James Gregg. These people were all from the banks of the Bann River, or the Bann Water, as it was called, and had ties of blood or social intercourse to hold them together. James McKeen and his brother John were in business together at Ballymoney, County Antrim, in 1718, and had prospered. They determined to emigrate to America, influenced perhaps by James’s brother-in-law McGregor who felt keenly the effects of commercial depression and religious strife in Ireland. John McKeen died a short time before the ship was to sail; but his widow with her four children continued with the party, which was evidently composed of families allied by marriage or closely associated with the McKeen business interests in Ballymoney, or with the Rev. Mr. McGregor''s religious life across the Bann at Aghadowey and Macosquin. We are not surprised therefore to hear that McKeen’s daughter said to her granddaughter one day that “James McKeen, having disposed of his property embarked with his preacher, Rev. James McGregor and sixteen others, who had bound themselves to him for a certain time to pay for their passage to America.” He no doubt engaged the ship and became responsible for most of the expense of the enterprise.” (Bolton, pp. 252-257)'
      -
        type: paragraph
        content:
          -
            type: text
            text: "\_"
    uuid: 089e943d-a0e0-4e09-8d94-81b057d8ee9a
    type: content
    image: MOD2-FRA11-SCR2.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: 'Emigration in Ulster-Scots literature'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Emigration is also a theme in Ulster-Scots literature. One of the most famous “weaver poets,” James Orr, went into exile in America after the failure of the 1798 Rising in which he had been involved as an active member of the United Irishmen. \_He wrote two poems about his own emigration experience, one in English, “Song, Composed on the banks of Newfoundland,” and the other, “The passengers,”"
          -
            type: text
            marks:
              -
                type: bold
            text: ' '
          -
            type: text
            text: 'in Ulster-Scots (James'
          -
            type: text
            marks:
              -
                type: bold
            text: ' '
          -
            type: text
            text: 'Orr, Poems on Various Subjects, pp. 147-149 & pp. 114-120). In both cases, he paints a pretty grim picture of this experience.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In “Song,” he complains bitterly about the promiscuity of the emigrant ship, and the constant risks the emigrants are exposed to during the crossing – not only the danger of shipwreck, but also the risk of contagion or violence from their fellow travellers or the crew. Orr gives us a series of flashes that give a vivid idea of the range of activities going on at once on board – couples courting, people arguing, people telling boring stories. The extract also suggests the tensions between the passengers and the sailors, whose behaviour does not always seem particularly reassuring. The whole scene is a picture of “sickness and sorrow.” He compares the ship to a leper hospital, which he places “far, far at sea” – a particularly dismal image.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'How hideous the hold is! - Here, children are screaming,'
              -
                type: hard_break
              -
                type: text
                text: 'There dames faint, thro’ thirst, with their babes on their knee;'
              -
                type: hard_break
              -
                type: text
                text: 'Here, down ev’ry hatch the big breakers are streaming,'
              -
                type: hard_break
              -
                type: text
                text: 'And, there, with a crash, half the fixtures break free:'
              -
                type: hard_break
              -
                type: text
                text: 'Some court - some contend - some sit dull stories telling,'
              -
                type: hard_break
              -
                type: text
                text: 'The mate’s mad and drunk, and tars task’d and yelling:'
              -
                type: hard_break
              -
                type: text
                text: 'What sickness and sorrow pervade my rude dwelling! -'
              -
                type: hard_break
              -
                type: text
                text: 'A huge floating lazar-house, far, far at sea. (Orr, p. 148)'
    uuid: 058114be-d82f-4ef5-a5d1-9ca81df42714
    type: content
    image: MOD2-FRA11-SCR3-copy-min.jpg
    audio_file:
      - audio/Emigration-1.mp3
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The poem in Ulster-Scots, “The Passengers,” is much longer. It takes the reader from the preparations for their departure right through to their arrival in Newcastle, Pennsylvania. Once again, the poem presents a very negative image of the emigrant experience. After saying goodbye sadly to all his friends and neighbours, Orr heads for the ship, clearly worried about his new surroundings – this time described as “owre like Bedlam” – and about the long journey ahead. As with the other poem, he gives us a vivid picture of life inside the emigrant ship. He tells us of the despair of his fellow travellers whom he shows squabbling among themselves and blaming each other for the decision to emigrate. One of them even says she’d go begging through Ireland on her own if only the Lord would allow her to go back home:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Some mak'' their beds; some haud their heads,'
              -
                type: hard_break
              -
                type: text
                text: 'An’ cry wi’ spite, a’ pantin’! -'
              -
                type: hard_break
              -
                type: text
                text: '“Ye brought us here ye luckless cauf!'
              -
                type: hard_break
              -
                type: text
                text: '(“Aye did he; whisht my darlin’!)'
              -
                type: hard_break
              -
                type: text
                text: 'L--d sen’ me hame! wi’ poke an’ staff,'
              -
                type: hard_break
              -
                type: text
                text: '“I’d beg my bread thro’ AirIan’,'
              -
                type: hard_break
              -
                type: text
                marks:
                  -
                    type: bts_span
                    attrs:
                      class: pl-10
                text: 'My lane, that day.” (Orr, p. 116)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Once again, Orr insists on the violence of the storms and describes the horrors that people had to put up with on board:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'But now a gale besets our bark,'
              -
                type: hard_break
              -
                type: text
                text: 'Frae gulph to gulph we’re tumble’t;'
              -
                type: hard_break
              -
                type: text
                text: 'Kists, kits, an’ fam’lies, i’ the dark,'
              -
                type: hard_break
              -
                type: text
                text: 'Wi’ ae side-jerk are jumble’t:'
              -
                type: hard_break
              -
                type: text
                text: 'Some stauchrin’ thro’ a pitch lays laigh -'
              -
                type: hard_break
              -
                type: text
                text: 'Some, drouket, ban the breaker;'
              -
                type: hard_break
              -
                type: text
                text: 'While surge, on surge, sae skelps her - Hegh!'
              -
                type: hard_break
              -
                type: text
                text: 'Twa three like that will wreck her '
              -
                type: hard_break
              -
                type: text
                marks:
                  -
                    type: bts_span
                    attrs:
                      class: pl-10
                text: 'A while ere day. (Orr, p. 118)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The poem tells of the relief of the emigrants once they finally land in Pennsylvania:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Sae, wi’ our best rags on our back,'
              -
                type: hard_break
              -
                type: text
                text: 'We mixt amang the Yankies, '
              -
                type: hard_break
              -
                type: text
                marks:
                  -
                    type: bts_span
                    attrs:
                      class: pl-10
                text: 'An skail’t that day. (Orr, p. 120)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, Orr clearly does not feel at home in the States and decides to put an end to what he calls his “sad refuge.”'
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ' He leaves after only a few months, returning to his native Ulster, taking advantage of the terms of a government amnesty.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Orr, Posthumous Works, Belfast, 1817, p. vii.'
    uuid: e4455ac7-e7a0-445e-9024-d6ce3043e3b0
    type: content
    image: MOD2-FRA11-SCR4-copy-min.jpg
    enabled: true
    allow_zoom: true
    audio_file:
      - audio/Emigration-2.mp3
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although Orr did not decide to stay, tens of thousands of Ulster Scots did, forming one of the most sizeable and influential groups in the new republic.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'By the end of the 19th century, the crossing had become much less dangerous. Archibald McIlroy’s novel, The Auld Meetin’-Hoose Green (1898), shows how common the journey to the States had become for so many in the Ulster-Scots community who were benefitting from the improved travelling conditions that made emigration and installation in the new world so much easier than for people of Orr’s generation. In the following humorous passage, the author describes how a visitor from Ulster is unimpressed by the grandeurs of the American landscape:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“One of the chief beauties of rivers—the St. Lawrence, for example — is the profusion of islands which dot their surface, varying the monotony of the waste of waters. And that reminds us that it was young Rab Dinsmore (nephew to Felix) who, during the first years he spent in America, was treated by an extra patriotic Yankee cousin to numerous excursions through the country, and rather riled his friend by the matter-of-fact and unenthusiastic way he had of looking at things in general. On visiting Niagara, and Rab viewing even it without much emotion, the cousin became exasperated. “Where, man, could you see such another sight in this ’ere planet? Look, will you, at the millions of tons of water surging over the rocks and tumbling down in a mighty, whirling avalanche!” “Weel, an’ what’s tae hinner’t?” was all Rab answered. (We were not in the habit of going into ecstasies over trifles.)” (M’Ilroy, pp. 7-8)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Needless to say, emigration took on a particular political significance during the Home Rule crisis. Just as the Ulster Scots will be reminded of their American connections, so the Scotch-Irish will be reminded of their Ulster-Scots heritage. Amongst other things, the Ulster Scots will try and make the Scotch-Irish sensitive to their position on Home Rule. This will put them at loggerheads with “Irish America,” overwhelmingly Catholic, and often overtly in favour of the Irish nationalist position.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Extract from an article entitled, "Emigration," in The Belfast Commercial Chronicle, March 22, 1828, in which the author offers advice to prospective emigrants.'
    uuid: 645a53c1-b958-4370-a50a-55a868c60938
    type: content
    image: Screenshot-2022-05-16-at-16.30.28.png
    enabled: true
    allow_zoom: true
    audio_file:
      - audio/Emigration-3.mp3
---
