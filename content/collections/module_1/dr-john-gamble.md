---
id: c9008e3f-d6dd-4142-bb3e-49c755fd8c89
blueprint: module_1
title: 'Dr John Gamble'
intro_text: 'Sketches of History, Politics and Manners, Taken in Dublin, and the North of Ireland, in the Autumn of 1810, 1811, and A View of Society and Manners, in the North of Ireland, in the Summer and Autumn of 1812, 1813.'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654016830
audio: MOD1-EP2-SCR2.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Dr. John Gamble was born in Strabane around 1770. A Presbyterian, he studied at the University of Edinburgh, later serving as an officer in the British Army. He lived for several years in London where he was clearly interested in the literary and cultural life of the metropolis. He returned home to Ireland on a number of occasions between 1810 and 1819. On each visit, he publishes lively, well-written accounts of his travels which are concentrated in “the North of Ireland.” The books were aimed primarily at an English audience'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1).'
          -
            type: text
            text: ' Gamble describes in often colourful detail the places he visits and the people he meets along the way, commenting frankly, and with an insider’s knowledge, on the political and religious background. He has an excellent ear, recording dialogue, sometimes word for word. This is of particular interest when he gives his reader a remarkably clear idea of the influence of Scots on the English spoken by some of the people he meets.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the following passage, Gamble describes arriving in the town of Monaghan after a harrowing night spent in the Londonderry mail coach that had arrived in Drogheda from Dublin at one o’clock in the morning.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Monaghan is a neat little place; it has a thriving trade in linen and other articles; — the inhabitants are mostly presbyterians; their meeting-house is a large and unornamented building. I was forcibly struck with the contrast between this town, and the one I quitted the night before [Drogheda], — it was as if one had fallen asleep in London, and awoke in Edinburgh: the accent, looks, and manners of the people were so different. Monaghan may be considered the boundary of the north in this direction, and here its peculiarities, and strongly-marked Scottish character, begin to be distinguished. I, who am acquainted with the Northern Irish accent, know it the instant I hear it — an Englishman almost always takes it for Scotch; but he is deceived, it is neither Scotch nor Irish, but a mixture of both, as are the people. — A great proportion of the inhabitants of this part of the kingdom, are the descendants of Scotchmen, settled here after the accession of James the First, to the throne of England.— It would appear incredible, how pertinaciously they retain the customs and usages of their ancestors… (Sketches, p. 153)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Anon. [John Gamble], Sketches of History, Politics and Manners, Taken in Dublin, and the North of Ireland, in the Autumn of 1810, London, C. Cradock and W. Joy, 1811; J. Gamble, A View of Society and Manners, in the North of Ireland, in the Summer and Autumn of 1812, London, C. Cradock and W. Joy; Edinburgh, Doig and Sterling; Dublin, Martin Keene, 1813; John Gamble, Views of Society and Manners in the North of Ireland, in a Series of Letters written in the Year 1818, London, Longman, Hurst, Rees, Orme and Brown, 1819.'
    uuid: 7adad7a8-0f2b-4358-9d42-410814482549
    type: content
    image: Mask-group.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Some time later, John Gamble finds himself near the village of Emma-Vale, “formerly called Scarnageragh, an Irish word, of which [he did not] know the meaning.”\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I was curious, however, to learn the etymology of Scarnageragh; I overtook a middle-aged man, decently dressed, and asked him if he could inform me. “I dinna ken,” said he; “I canna spake Erish — I would never fash myself with it”; “[A]re you not an Irishman yourself?” “In troth, and I’m nane; I, and aw my generation, ha gone to meeting this fowr hundred years.” — “They must have been a clever generation indeed,” said I “to have gone to meeting a hundred years before there was any. '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (1)
              -
                type: text
                text: ' — Where was you born?” “in yon wee hoose,” said he, “on the tap o’ the brae with the auld tree our it; — gin ye hae time to step up, the auld wife will be able, to gie us a bunnock, and a drap of buttermilk.” … [H]is ancestors probably were settled a century among [the Erish - by the Erish he meant the native Irish, or the catholics]; yet he spoke and thought of them, exactly as a Scotchman would have done. (Sketches, pp. 206-207)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: Dr
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' '
                  -
                    type: text
                    text: 'Gamble, who is himself a Presbyterian, is gently poking fun at the man. “Four hundred years” would take them back well beyond the Reformation!'
    uuid: 994c962a-7206-4a8f-a9d6-9bcde749696c
    type: content
    image: MOD-1-EP2-Screen_2.jpg
    audio_file:
      - audio/MOD1-EP2-SCR2-(John-Gamble)-Liam-as-narrator.mp3
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'John Gamble recounts a conversation he has with “an old Covenanter” as he is walking along the road somewhere in the vicinity of Violet Bank near Toome. This reminds him of how he had attended a Covenanter “conventicle” – an open-air religious meeting - as a child. Although he does not agree with their theology, considering them to be “the most rigid of Presbyterians,” this event had clearly left a strong impression on him. It is interesting to see how the shared memory of persecution in Scotland still affects the behaviour of their descendants in Ulster.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The number of their [the Covenanters] congregations in this country is about twenty. They have now public worship pretty generally in houses – formerly it was almost universally [always] performed in the open fields. Their ancestors were driven by persecution to wilds and glens, where only they could worship their Maker by stealth and in secrecy; … they retained the custom long after the original cause was removed. I recollect being at one of those meetings when I was a very little boy, it is present to my recollection as fresh as if it were yesterday. I see it now as if it were before my eyes; the bright sun and clear sky – the wild glen, and dark woods, and foaming torrent – the thin dapper figure – the sharp face, and keen visage of the preacher, as he projected his head from the little pulpit covered with canvas, placed on the verge of the hill; the immense multitude of all ages and sexes, in scarlet cloaks and grey mantles, and blue and russet-coloured, and heath-dyed coats – in hoods and bonnets, … and old-fashioned hats, standing, sitting, and lying around.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The sermon lasted upwards of three hours… (A View, pp. 214-215)'
    uuid: a36ca622-6e8c-4962-9e62-580f5af9462a
    type: content
    image: MOD-1-EP2-Screen_3jpg.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "John Gamble often interrupts the account of his journey with digressions on different subjects, such as the quality of Irish inns of which he often approves, or “the late unfortunate rebellion”\_ (i.e. 1798) of which he strongly disapproves. The following extracts are of interest as they discuss the issue of the Ulster Scots' attachment to place and their attitude towards emigration.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Presbyterian, like the Scotchman, wanders wherever he thinks he can best earn a livelihood… His attachment to the country is not half so strong as the Catholic’s… Oppressed by his landlord, whose exactions hardly allow him the necessities of life, he seeks, most commonly in America, what Ireland denies him; where his perseverance and industry soon give him independence and affluence. The departure of these men is of infinite disadvantage to their country— Active and enterprising, sober and reflecting, reading and reasoning— estimable even in their prejudices, for they are all on the side of morality and religion, they are the best friends of a good government, as they are the bitterest enemies of a bad one — Their loss, I fear, will every year be more and more sensibly felt. (Sketches, p. 251)'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '[A] Presbyterian of the North of Ireland […] has little sentiment of locality, and, therefore, emigrates with an indifference only inferior to that of an American planter, who, having created a beautiful spot in the wilderness, disposes of it, and removes some hundreds of miles to create and abandon in the same manner, another. He is, of consequence, liberal-minded, and perfectly free from national prejudice. It is an undue fondness for country which makes us so often short-sighted, ungenerous, and unjust. (A View, pp. 273-274)'
      -
        type: paragraph
    uuid: ed019c4e-de1d-432d-8033-72bb1ca6bd59
    type: content
    image: MOD-1-EP2-Screen_4.jpg
    enabled: true
    allow_zoom: true
---
