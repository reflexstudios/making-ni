---
id: 1bcfd862-11e3-4f7b-8ed2-b9df547c39e4
blueprint: module_1
title: 'John Harrison'
intro_text: 'The Scot in Ulster, 1888.'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654870302
unit_content:
  -
    image: De-Latcocayne-Screen_1-2-(14).png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Scot in Ulster: Sketch of the History of the Scottish Population of Ulster was first published in 1888 '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: " as a series of articles in The Scotsman, one of the leading Scottish newspapers. A re-worked version of these sketches was published in Edinburgh and London later in 1888, two years after Gladstone had introduced his first Home Rule Bill (April 1886; defeated in the Commons the following June). The fact that the articles and the subsequent book were published outside Ireland indicates the growing levels of interest in “the Irish question” in Great Britain.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "John Harrison, the author, himself a Scot, makes no bones about the fact that this publication was produced with a specific political intention. The Preface explicitly places it within the frame of resistance to the Irish nationalist demand for Home Rule, which he calls “the policy of Separation from Great Britain.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The main objective behind the book is to establish the fact that the Ulster Scot is culturally separate from the Irish Celt and could not therefore be incorporated into a Home Rule Ireland.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: "\_John Harrison, The\_Scot in Ulster, Edinburgh & London, William Blackwood and Sons, 1888."
    uuid: 480cffee-d5aa-4798-8209-059e5f4b376f
    type: content
    enabled: true
    allow_zoom: true
  -
    image: Screenshot-2022-05-09-at-18.31.39.png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Harrison constantly reiterates the strength of the ties that bind Ulster to Britain and to Scotland in particular. Much of the following passages has to do with geography. Harrison sees the river Bann as a natural frontier at the western edge of what is presented as a distinctly Scots cultural territory. Indeed, according to him, this isn’t really Ireland at all, but rather an extension of Scotland into the neighbouring island.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'It is strange for any man who is accustomed to walk through the southern districts of Scotland, and to meet the country people going about their daily work in their everyday clothes and everyday manner, to cross into Ireland and wander through the country roads of Down or Antrim. He is in a country which is supposed to be passionately anxious to set up a separate nationality [he is referring here to the nationalist demand for Home Rule], and yet he cannot feel as if he were away from his own kith and kin. The men who are driving the carts are like the men at home; … the signs of the little shops in the villages bear well-known names — Paterson, perhaps, or Johnstone, or Sloan ; the boy sitting on the “dyke” with nothing to do, is whistling “A man''s a man for a’ that”... '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (1)
              -
                type: text
                text: '  The want of strangeness in the men and women is what strikes him as so strange.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Then he crosses the Bann, and gets into a different region… he gets within sight of the South Derry hills, and the actors in the scene partly change. Some are very familiar; the smart maid at his inn is very like the housemaid at home, and the principal grocer of the little village is the “very image” of the elder who taught him at the Sunday-school; but he meets a donkey-cart, and neither the donkey nor its driver seem somehow or other to be kin to him; and the “Father” passes him, and looks at him as at a stranger who is visiting his town, —then the Scotsman knows that he is out of Scotland and into Ireland. (pp. 97-98)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " \_A well-known poem/song by Robert Burns."
    uuid: 2c9fe4b7-a635-4004-89bd-fdd0b0d86419
    type: content
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Harrison is clearly very sensitive to issues of space and the intimate link between space and the dominant culture of the local population. The leisurely nature of his mode of travel – he says he is going through Antrim and Down on foot - allows him to sense the changes in population as he moves from place to place. Harrison insists heavily on the fact that the sense of distinctiveness is particularly intense in the country districts and suggests the city environment ends up blurring the borders between the Scottish and Irish “types.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "It is of course no coincidence that he should identify country towns in Antrim and Down as being particularly Scottish. Here again the relationship between the Scottish visitor and the surrounding population is represented in terms of family and blood relations, “kith and kin,” the notion of one’s “own people” being a central aspect of his argument, and one that was to be picked up by those producing material for the anti-Home Rule campaign.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'It is not in Belfast that [the Scot] feels the likeness to home so much, for everybody is walking fast just as they are in Glasgow, so he cannot notice them particularly, and, of course, the “loafers” at the public-house doors, who are certainly not moving smartly, do not count for anything in either town; but it is in the country districts – at Newtownards, or Antrim, where life is leisurely, that he recognises that he is among his own people. While it is in a town which is in the border-land between Scottish and Irish, say at Coleraine, on a Saturday market-day, that he has the difference of the two types in face and figure brought strongly before him. Some seem foreign to him, others remind him of his “ain countrie,” and make him feel that the district he is in, is in reality the land of the Scot. (p. 99)'
    uuid: 7fc87f42-be3f-44f2-982e-8ddaed190ccb
    type: content
    enabled: true
    image: Screenshot-2022-05-09-at-18.31.46.png
    allow_zoom: true
---
