---
id: 1448dee1-9bec-462a-9491-681fbbbcf30a
blueprint: module_1
title: 'Rev. Alexander G. Lecky'
intro_text: 'The Laggan and its Presbyterianism, 1905, and, In the Days of the Laggan Presbytery, 1908.'
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1653339622
unit_content:
  -
    image: MOD-1-EP9-Screen_1-corrected.jpg
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Rev. Alexander G. Lecky was installed as minister at Ballylennon Presbyterian Church, between Raphoe and Bready, on 5th December 1878. He is best known for two books dealing with the history of Presbyterianism in the Laggan, The Laggan and its Presbyterianism, 1905, and, In the Days of the Laggan Presbytery, 1908.\_"
          -
            type: text
            marks:
              -
                type: bold
            text: "\_(1)"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The first Presbytery – or church court - in Ireland was set up at Carrickfergus in June 1642. Lecky explains that it consisted of five ministers and four elders. The ministers were the chaplains of the regiments of the Scots army that had been sent over to Ireland in response to the 1641 Rising. The elders were officers in the same regiments. The Presbytery oversaw the activities of all the congregations in Ulster. “In 1654,” he tells us,\_ “‘The Presbytery’ divided into three sub-Presbyteries, or ‘meetings’, as they were called, viz., Antrim, Down and Laggan…” (In the Days, p. 4)"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In the following extracts, he explains how people at the time looked at the way the Scots planters had left a permanent mark on the area.\_ \_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'It should be remembered, however, that there are two Donegals —an outer and an inner. The former, which is almost wholly Roman Catholic, and from which the County to a large extent takes its character and complexion in the eye of the public, consists of the extensive mountainous districts that lie along the western seaboard, and at some points run far inland. The latter consists of the more flat and fertile country that lies between the mountains and the river Foyle—the eastern boundary of the County. It is largely Protestant, and from a very early period in history has been known as the Laggan, i.e., the low or level country. In the days of the Ulster Plantation, from 1607 onwards, this district, on account of its fertility and also from the fact that the undertakers or persons who obtained the grant of estates in it were chiefly Scotchmen, was largely peopled by immigrants from Scotland, whose descendants, unto this day, till the fields their forefathers then acquired, and keep to the Presbyterian principles they brought with them from their native land. (In the Days, p. 1)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "(1) \_"
                  -
                    type: text
                    text: 'These two books were published in a collected edition with a Foreword by Rev.William Hanna, The Laggan Presbytery Books, Roots of Presbyterianism in Donegal, n.p. 1978, and reprinted by the Ulster-Scots Agency (2017).'
    uuid: dffd1676-e0f2-4880-8766-48f597d1b088
    type: content
    enabled: true
    allow_zoom: true
  -
    image: MOD-1-EP9-Screen_2-corrected.jpg
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In a passage in the earlier text, The Laggan and its Presbyterianism, Lecky discusses the circumstances behind this Plantation as it affected the Laggan district of Donegal:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'From this day onward [September 1611] the English and Scotch settlers began to arrive in the Laggan and prepare new homes for themselves there, and as so many of the Laggan undertakers '
              -
                type: text
                marks:
                  -
                    type: bold
                text: '(1) '
              -
                type: text
                text: "were Scotchmen, and as Scotland was of easy access through the port of Derry, the great majority of the new settlers hailed from\_"
          -
            type: paragraph
          -
            type: paragraph
            content:
              -
                type: text
                text: '“The land of mountain and of flood,'
              -
                type: hard_break
              -
                type: text
                text: 'Of brown heath and shaggy wood.” '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (2)
          -
            type: paragraph
          -
            type: paragraph
            content:
              -
                type: text
                text: "A fact that is testified down to the present day both by their speech and by their names. A Laggan youth in the matter of “braid Scots” could almost hold his own with “Wee MacGreegor,” whilst the family names of the Protestant inhabitants have all a strong Scottish flavour. (The Laggan, p. 7)\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Lecky is clearly interested in the question of language use in the Laggan. What he says should be read alongside what Micí Mac Gabhann had to say from the perspective of one of the Irish-speaking boys Lecky refers to who were sent to the Laggan to work for the local farmers.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The name Laggan has, to a large extent, fallen out of use amongst the inhabitants of the district. The people who most commonly call it by this name nowadays are the inhabitants of the mountainous parts of Donegal, lying between Letterkenny and the western seaboard, whose forefathers were at the time of the Plantation driven out of the Laggan to make room for the English and Scotch settlers… Large numbers of young men and girls from these districts are in the habit of coming to the Laggan to engage as servants with the farmers there, and actuated by a more common-sense principle than are the moving spirits of the Gaelic revival, of which we hear so much at present, these young people look upon this period of service as a not unimportant part of their education, and speak of it as “going up to the Laggan to lift the Scotch”— that is, to learn to speak English. (The Laggan, p. 2)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: "\_“Undertaker” is a technical term used to describe one of the groups of planters in the context of the Ulster Plantation. Undertakers “undertook” - i.e. promised - to do a certain number of things, such as bring over a certain number of Protestant tenants from Scotland or England, build fortified houses and villages and supply tenants with arms."
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(2) '
                  -
                    type: text
                    text: "\_This is a quotation from Walter Scott's poem, The Lay of the Last Minstrel, 1805. This passage refers to Scotland. "
    uuid: 4dee60e9-4282-4127-b4c7-f1644f111e28
    type: content
    enabled: true
    allow_zoom: true
---
