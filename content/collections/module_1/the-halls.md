---
id: 9be94df6-ec11-48a2-b084-6de298f0b437
blueprint: module_1
title: 'Mr & Mrs S.C. Hall'
intro_text: 'Ireland: Its Scenery, Character &c, Vol. III, 1843.'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655657709
audio: 2.-Module-1-Episode-4-Halls_edit.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Both of the authors were born in Ireland: Samuel Carter Hall was born in the Geneva Barracks in Waterford in 1800, while his wife, Anna Maria, née Fielding, was born in Dublin the same year. They met and married while living in London. Samuel Hall worked mostly as a journalist, editing several literary journals. Anna Hall was a prolific writer, producing a wide variety of material including novels, plays, and stories, as well as a range of writings on philanthropy and temperance. Starting in June 1840, the couple returned to Ireland on a series of tours that resulted in what is perhaps their best-known publication, Ireland: Its Scenery, Character, &c., which appeared in three volumes between 1841 and 1843 '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: '. The books, complete with engravings of Irish landscapes and illustrations of the economic and social activities described in the texts, met with considerable success and there were several revised editions and reprints over the course of the nineteenth century. The text offers the image of an Ireland that was to be swept away in the maelstrom of the Great Famine that devastated the country a few years later.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Halls are struck by the impact the Scottish Presbyterians have had on certain parts of Ulster.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'As Belfast is a sort of ecclesiastical metropolis for the Presbyterians, being the place where their synods meet, where the greatest amount of wealth and talent is to be found connected with their body, and from which their periodical and other publications generally issue, this seems the proper opportunity for giving an account of that important portion of the population of Ireland…'
          -
            type: paragraph
            content:
              -
                type: text
                text: "Towards the beginning of the seventeenth century…\_ a considerable part of six of the northern counties… was “planted,” under the patronage of King James I., with colonists from Scotland, by whom Presbyterianism was introduced into Ulster, and soon obtained a firm footing in the country. These Scotch settlers have changed the external as well as the religious aspect of the northern province. About two centuries ago, it was the most barbarous, uncivilized, and wretched portion of Ireland; it has become the most peaceable, enlightened, and prosperous… (Vol. III, pp. 72-73)"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " \_Mr and Mrs S.C. Hall, Ireland: Its Scenery, Character &c, Vol. III, London, Jeremiah How, 1843. This is the last of three volumes based on five tours of Ireland undertaken between 1825 and 1840."
    uuid: 589d7134-22dc-464d-b220-1bdc6fc6b9c4
    type: content
    image: De-Latcocayne-Screen_1-2-(5).png
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Similar themes emerge in their visit to Island Magee.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '[T]he long and narrow peninsula of “Island Magee”… extends about seven miles from north to south, along the coast opposite to Ayrshire, and is in few places more than two miles in breadth. The inhabitants are all of Scottish descent, and are still “thoroughly Scotch” in dialect, manners, and customs; they are a remarkably intelligent race; and it is worthy of remark, that out of a population of nearly three thousand, no person living can recollect an instance of a native of this place being imprisoned for or convicted of any criminal offence. (Vol. III, p. 123)'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The section on County Down contains some particularly interesting passages.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The people of the county Down, as a whole, are of Scotch origin. There are, of course, numerous exceptions; but so small a proportion do they bear to the whole, that the lowland or Ayrshire dialect was commonly spoken all over the county, till about the middle or towards the end of the last century. [...] “The nearness” of this county to the Mull of Galloway has made the districts, on the two sides, scarcely distinguishable; and the stream of Scottish population can be traced most distinctly from Donaghadee and Bangor, upwards to the interior. In the eastern part of the parish of Hillsborough, the Scottish dialect and religion are still preserved…'
          -
            type: paragraph
            content:
              -
                type: text
                text: "Soon after entering the county of Down, we began to feel we were in another country; in a district, at least, where the habits as well as the looks of the people were altogether different from those to which we had been accustomed… Both men and women wore neat and well-mended clothes. Tartan shawls, ribands, and even waistcoats, intimated our close approximation to the Scottish coast. We met a little rosy girl, and her replies to our questions proved that we had left behind us the soft, woolly brogue of the south, and should, for some time at all events, hear nothing but the hard, dry rasping of the Scottish accent…\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Where are you going, my dear?” we inquired.'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“I’m ganging to scule,” was the little maid’s reply.'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“And where do you live?”'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Is it whar I leive? —joost wi’ mee faither and mee mither.”'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“How old are you?”'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Joost sax'
              -
                type: text
                marks:
                  -
                    type: bold
                text: .”
              -
                type: text
                text: ' And off she trotted, apparently regretting that she had wasted so much time upon inquisitive travellers. (Vol. III, pp. 22-24)'
    uuid: 1e178c48-41c4-4f02-aed1-84cc7d3048b2
    type: content
    image: De-Latcocayne-Screen_1-2-(6).png
    enabled: true
    audio_file:
      - audio/Halls-Edit-Screen-2-(with-narration).mp3
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The Halls go into the cottage the child had just left. They find that the place, “though small, was neat and tidy.”\_ The father “was working at his loom; his wife was spinning, rocking the cradle with one foot, and turning her wheel with the other; while an elder girl was carding flax.” One of the rare things in the house was “a large Bible.” Interestingly, they notice that “the holy book was covered with a well-worn, dark green tartan.” When the Halls ask if a young boy they see in the garden is their son, the mother tells them, “not their born son. ‘He’s my husband’s brother’s child; and his fayther and mither are gane their ain gait to Canada; an’ if they find a’ prosperin’, why they’ll send hame word, and we’ll follow.’” When they are asked if they would not be sorry to leave their own country, this is the answer they give:"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Ay,” said the weaver, … “that I will; and there is a text against it. It is written— ‘Dwell in the land, and verily thou shalt be fed.’”'
          -
            type: paragraph
            content:
              -
                type: text
                text: "“Aweel,” answered the woman… “but the Israelites were commanded to depart out o’ the lan’ o’ Egypt. Now, you wad hae remained, suner than quit the lan’ o’ bondage you were born in.”\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Ah!” observed the poor fellow, shaking his head, and speaking to us of his wife; “she left her ain canny Aberdeen when she was a wee bairn, and came ower to Belfast, and that unsettled her airly; but I was born in yon bed, and I followed my gran’fayther an’ gran’mither, and my ain parents, out of that door to their graves, and I thought to lay beside them. I’ll no quit the auld place till I ken mair of the new, and sae I tauld the wife: but women,” he added, smiling, “are aye for gadding; we might gang farther and fare waur than in the county of Down, bad as times are.”'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'To this we most cordially assented; it was new to us to hear the words of Scripture quoted in an Irish cottage, by a mere peasant…'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Weel, weel,” she exclaimed, “Alick, ye’ll joost do as you like at the end, ga’ or stay. It wa’d break my heart to see you mourn the country when you’d be far frae it; and it would break my heart to bring up the children to meesery; but, come what may, there’s nae harm in savin’ a’ we can, though sometimes it’s nae mair nor a ha’penny a-week, again’ a saft day, either at hame or abroad.” (Vol. III, pp. 25-26)'
    uuid: fc1e6be4-2821-44e6-9be3-66e44bc13127
    type: content
    image: De-Latcocayne-Screen_1-2-(7).png
    enabled: true
    audio_file:
      - audio/Halls-----Edit-Screen-3-1655657705.mp3
    allow_zoom: true
---
