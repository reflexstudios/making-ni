---
id: 9930666e-4f49-4457-b04d-bd7d9df92eca
blueprint: module_1
title: 'The Scotch-Irish Congress'
intro_text: 'The Scotch-Irish in America: Proceedings of the Scotch-Irish Congress at Columbia, Tennessee, May 8-11, 1889.'
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1653339568
unit_content:
  -
    image: MOD-1-EP8-Screen_1-Corrected.jpg
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following extracts are from two of the addresses delivered at the first Scotch-Irish Congress in Columbia, Tennessee in May 1889 '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1). '
          -
            type: text
            text: "This congress was an important milestone in the campaign by the Scotch-Irish in the United States to encourage a deeper understanding of the specific contribution of their community to the construction of the United States of America. This awakening of interest in Scotch-Irish identity coincides exactly with the increased focus on the Ulster Scot in the context of the demand for Home Rule on the other side of the Atlantic. The content of these texts - “What the Scotch-Irish have done for education,” by Professor George Macloskie of Princeton College, and, “The Scotch-Irish of the South,” by Hon. William Wirt Henry, Virginia - is typical of the way in which the Ulster Scot was being imagined at the time from the other side of the Atlantic.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "It is interesting to note that the Scotch-Irishman as he is represented here shares many of the characteristics of the Celtic Irishman who is under construction at more or less the same time. In both cases, there is a determination to underline their distinctiveness and how these supposedly innate characteristics set them apart from those around them.\_ Above all, in both cases, the narrative involves a high degree of idealisation."
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: "\_The Scotch-Irish in America: Proceedings of the Scotch-Irish Congress at Columbia, Tennessee, May 8-11, 1889, Cincinnati, Robert Clarke & Co. 1889."
    uuid: 84dd69a2-b5b5-40d8-8b51-dcdfd0684231
    type: content
    enabled: true
    allow_zoom: true
  -
    image: De-Latcocayne-Screen_1-2-(17).png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The members of the Congress were interested in following the history of their community both in Ulster and in the States. This explains the constant to-ing and fro-ing between the two contexts.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "[The Flight of the Earls, 1607, represents the beginning of] a new era in Irish history. They left large tracts of land in north Ireland unoccupied and forfeited to the crown, and these were parceled out among a body of Scotch and English, brought over for the purpose. The far greater number of these plantations were from the lower part of Scotland, and became known as “Scotch-Irish.” Thus a new population was given to the north of Ireland, which has changed its history. The province of Ulster, with fewer natural advantages than either Munster, Leinster, or Connaught, became the most prosperous, industrious, and law-abiding of all Ireland. Indeed, the difference between Scotland and Spain is not greater than between Ulster and her sister counties, even to this day. \_ (Henry, p. 114)"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "There came over… along with them, the religious and educational methods of Scotland. John Knox had established a system of schools, so that every minister had a hand in teaching during some part of his career, and every boy, however poor, had before him the opportunity of gaining education up to his ability. The church and the school went together, as both of the people and for the people… No Irish colleges welcomed these boys, as the only Irish university, though at first it was started under Scottish teachers, was soon closed against their characteristic faith. The Scotch-Irish lads, after their school training was completed, had to go on foot to the seaside, whence they embarked on a packet for Scotland, and again went afoot in groups to Glasgow or Edinburgh University, whence they were sent back, in the course of a few years, with the university diploma.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "Non-conformists were forbidden to teach school. Moreover, the Scottish colonists of Ulster came to experience extortions by landlords, and to be denied the rights of freemen in the country for which they had done so much.\_ In Ireland, as in America, a three-fold struggle for liberty had to be carried on: (1) for liberty to be educated; (2) for religious liberty;\_ (3) for civil liberty.\_ (Macloskie, pp. 92-94)\_"
    uuid: 0e1be246-2c7c-4d72-9b2a-58083be2209a
    type: content
    enabled: true
    allow_zoom: true
  -
    image: De-Latcocayne-Screen_1-2-(18).png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Henry explains that political instability and the injustice suffered at the hands of the State, the Church and the landlords forced many Ulster Scots to consider emigration.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'But the Protestant population thus transplanted to the north of Ireland was destined to suffer many and bloody persecutions… [I]n the early part of the eighteenth century the great movement began which transported so large a portion of the Scotch-Irish into the American colonies, and, through their influence, shaped in a great measure the destinies of America…'
          -
            type: paragraph
            content:
              -
                type: text
                text: "This bold stream of emigrants struck the American continent mainly on the eastern border of Pennylvania, and was, in great measure, turned southward through Maryland, Virginia, North Carolina, and South Carolina, reaching and crossing the Savannah river… Turning westward, the combined flood overflowed the\_ mountains and covered the rich valley of the Mississippi beyond. … [T]hey gave tone to its people and direction to its history. (Henry, pp. 114-116)"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the following passage, Makloskie brings the story of the Ulster Scots who remained in Ireland up to date (1889):'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We will not follow the struggle as it went on in Ulster. Suffice it to say, that the men who saved England by closing the gates of Derry, were robbed of the honor of their services, were afterward declared unfit to hold office in the city which they had defended, or anywhere under the British crown; and laws were passed to destroy their woolen trade, to make their marriages null…; nor were they relieved of their disabilities until the rebellion of the American colonies taught England to deal gently with the oppressed at home. Step by step, Ulster has fought its way to political equality, to protection for its tenant farmers, to religious freedom, and to high educational rank. Belfast, at present, holds the third place in Great Britain as a seaport, being surpassed in the tonnage of its shipping only by London and Liverpool. Ulster is remarkably free from crime, and has few police, as compared with the rest of Ireland, or even England. And men who have gone from Ulster, with the education and principles of the Scotch-Irish, occupy the highest positions as teachers or statesmen in England, India, China, Australia, and America. (Macloskie, p. 94)'
    uuid: da1de8b1-88ca-4834-bafb-b65e0f28aaa7
    type: content
    enabled: true
    allow_zoom: true
---
