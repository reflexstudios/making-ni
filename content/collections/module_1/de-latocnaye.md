---
id: d751edf0-405a-466e-b329-df8a5273927b
blueprint: module_1
title: 'De Latocnaye'
intro_text: 'Promenade d’un Français dans l’Irlande, 1797'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654870091
unit_content:
  -
    image: images/placeholders/De-LatcocayneScreen_1.png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'John Stevenson (1850-1931) was the co-owner of one of Belfast’s largest printing concerns, McCaw, Stevenson & Orr. He wrote several books with a clear focus on the Scots community in Ulster. In 1903 he published a collection of poems – many of them in Ulster-Scots – under the pseudonym, Pat M’Carty, entitled Pat M’Carty, Farmer of Antrim: His Rhymes, with a Setting. In 1912, he writes with great affection of the period of his childhood spent with an aunt in the rural Presbyterian community in County Antrim in the 1860s - A Boy in the Country. He later went on to publish a social history of County Down, Two Centuries of Life in Down (1920), looking at life in the area from the Plantation period up to the end of the 18th century.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Stevenson, who had attended Belfast ‘Inst,’ also produced a translation from French of a fascinating account of a trip through Ireland in the period immediately before the outbreak of the United Irishmen’s Rising – what the Ulster Scots called the Turn-oot - in 1798 '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ". The author was a certain De Latocnaye.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Originally from Brittany, he had been an officer in the French army. He was opposed to the Revolution which had toppled the French monarchy in 1789, and, like many other royalists, found himself in the French expatriate community that had taken refuge in London. After writing an account of his travels in England and Scotland – Promenade d’un Français dans la Grande Bretagne, published in 1797 – De Latocnaye decides to travel through Ireland on foot to collect “observations on the country and its people.”'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " De Latocnaye, A Frenchman’s Walk through Ireland, 1796-7, Belfast,\_McCaw, Stevenson & Orr, 1917. This is John Stevenson's translation from French of De Latocnaye, Promenade d’un Français dans l’Irlande, Dublin, 1797. "
    type: content
    enabled: true
    uuid: 0d45dbe6-98c7-452d-a930-78df5d31e2cb
    allow_zoom: true
  -
    image: 'images/placeholders/De-Latcocayne Screen_2.png'
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "De Latocnaye had spent quite a lot of time in Scotland and clearly understood – and used – a good deal of Scots in his dealings with the locals. Indeed, because of the fear of being taken for a French spy, he made himself out to be a Scot, telling people his name was MacTocnaye! In the following extracts, he gives some of his impressions of the customs he comes across in certain parts of Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'While on a stay at Florence Court '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ", belonging to Lord Enniskillen, he says:\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I met here, on the way, a funeral, and I noted that the women did not cry as in the south or west of Ireland, from which it would seem that the south and north of this island are inhabited by people who have not the same origin. As a matter of fact, those of the north of Ireland are much more of a mixed race, their ancestors for the most part having been Scotch. (p. 182)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Later, after a visit to Londonderry and the Giant''s Causeway, he goes to stay in Coleraine.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I was received with much kindness in this town [Coleraine] by Mr. Richardson, with whom I passed three or four days. The country women-folk at Coleraine are, on Sundays, very like the Scotch peasant women in the neighbourhood of Montrose. They are extremely well dressed, their shoulders usually covered by a red mantle. One can hardly believe that this is Ireland. (p. 204)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'On his way from Glenarm to stay at Shane’s Castle, near the town of Antrim, he passes through the village of Broughshane, before going on to visit Belfast. In both places, he remarks the strong Scottish influence. This is interesting because it suggests that Ulster’s “Scottishness” is also an urban phenomenon:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Quitting the coast, I had to cross the mountains to get to the interior, and I stopped at Brushin [the translator inserts “Broughshane” in a footnote], where most of the inhabitants are Presbyterians. One could hardly imagine that he is among the same people. The way of speaking, and even of dressing, is much more Scotch than Irish. (p. 218)'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I had heard so much about the troubles, the assassinations, and the conspiracies of which Belfast was the centre, that I felt considerable reluctance to proceed there. I was, however, agreeably surprised to find the town in perfect peace and quietness… Belfast has almost entirely the look of a Scotch town, and the character of the inhabitants has considerable resemblance to that of the people of Glasgow. (pp. 221-222)'
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bts_span
                attrs:
                  class: null
            text: 'De Latocnaye leaves Ireland on a cattle boat from Donaghadee to Portpatrick, a distance of “scarcely twenty miles.” He is impressed by the intensity of the cattle trade between Ulster and Scotland, saying that, “'
          -
            type: text
            text: 'On the day I crossed there were four hundred horned cattle taken over to Scotland, and in the six weeks previous there had been transported nearly thirty thousand.'
          -
            type: text
            marks:
              -
                type: bts_span
                attrs:
                  class: null
            text: '” (p. 225)'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bts_span
                    attrs:
                      class: null
                text: “
              -
                type: text
                text: 'In two hours and a half I was carried to the opposite side of the water, saluted anew the coast of Scotland…'
              -
                type: text
                marks:
                  -
                    type: bts_span
                    attrs:
                      class: null
                text: "\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I had left Dublin on May 25, and I landed at Portpatrick on December 21, so I had spent more than six months on this walk, travelling without provisions, without cares, and without any baggage beyond what my pockets were able to contain.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'My time, I must say, had been well employed; I had never spent time, indeed, with greater pleasure…'
              -
                type: text
                marks:
                  -
                    type: bts_span
                    attrs:
                      class: null
                text: '” (p. 226)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '(1) Florence Court is a large house and estate near Enniskillen, County Fermanagh.'
    type: content
    enabled: true
    uuid: cb77115b-0431-45cf-beb8-bc00545f3bdb
    allow_zoom: true
---
