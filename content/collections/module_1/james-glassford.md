---
id: 9cd266a9-f073-435c-96bf-9a097b7d72d3
blueprint: module_1
title: 'James Glassford'
intro_text: 'Notes of Three Tours in Ireland in 1824 and 1826.'
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1653339242
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "James Glassford (1771-1845) of Dougalston, an estate in Scotland near Glasgow, was a Scottish advocate. In June 1824, he was appointed as one of the five members of a Royal Commission of Inquiry into the State of Education in Ireland. According to the First Report of the Commissioners on Education in Ireland, published in May 1825, the Commission had been appointed “to inquire into the Nature and Extent of the Instruction afforded by the several Institutions in Ireland, established for the purpose of Education, and maintained either in Whole or in Part from the Public Funds… and to report as to the measures which can be adopted for extending generally to all Classes of Our People of Ireland the benefits of Education.” Glassford’s work on the Commission involved him visiting schools and other educational establishments throughout the country between 1824 and 1826 (1). The commissioners left Dublin on September 1st 1824 and began their inquiry in Ulster.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "(1) \_"
                  -
                    type: text
                    text: 'James Glassford, Notes on Three Tours in Ireland in 1824 and 1826, Bristol, W. Strong and J. Chilcott, 1832.'
    uuid: 3837740b-940d-4de5-b330-c3d12db7bfd7
    type: content
    enabled: true
    image: MOD-1-EP3-Screen_1-min.jpg
    allow_zoom: true
  -
    image: James-Glassford-Screen-2-1.png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'James Glassford’s Notes of Three Tours in Ireland in 1824 and 1826 was printed privately in January 1831. However, “on account of the great, and by no means decreasing, interest which is taken by both the legislature and the public at large, in all the affairs of Ireland” (Notes, p. iii), it was decided to publish the text the following year.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Proceeded by the course of the Lagan, through a well-cultivated country, abounding with printing and bleaching establishments, and with all the marks of industry, to Belfast, within the county of Antrim, at the junction of the Lagan with Belfast Lough. This capital of the North bids fair to outstrip the towns as yet larger of the South; is more pleasing than Dublin, in the general neatness of the dwellings… The people of this part of Ireland, Down and Antrim, are chiefly descended from Scotch settlers, and much intercourse exists between the two countries. In the features and dress, and even language of the\_people, little difference is discerned, and the general aspect of the country is nearly the same with that of Scotland. Oatmeal much used in the food of the people…"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The town of Belfast increasing much, with great appearance of business and industry; cotton mills, breweries, a large distillery; the surrounding country covered with works of industry; a good deal of shipping in the harbour… The manufacture and trade of cotton has much increased, and is even superseding the linen. (pp. 25-28)'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'On a visit to the Coleraine area, Glassford calls in to the Free School in Drummachose. This short extract from his Notes shows how, during his time in Ulster, he is often struck by comparisons with his native Scotland.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Visited the parish Free School of Drummachose. Of 115 present at late inspection, the proportions were, 71 Presbyterian, 20 Church of England, 24 Roman Catholic… The master here is uncommonly intelligent, capable, and well instructed. The Roman Catholic priest is liberal, and favours the schools: no interference: Roman Catholic children attend the Protestant school, and the Protestant children attend the Roman Catholic school: children in general poor; come to school very young, and seldom remain more than 14 months. Somewhat of the feeling of the Scotch peasantry observable here; the farmers and cotters have no objection to the parish school, except that the instruction is altogether free; yet many are too poor to be able to pay, probably, if it were required. (p. 48)'
    uuid: 7a0ecc3f-f759-49d4-9764-d78ea64921cd
    type: content
    enabled: true
    allow_zoom: true
---
