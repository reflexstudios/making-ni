---
id: 038f6aae-7d92-4adc-8daa-909cb7883d2b
blueprint: module_1
title: T.C.
intro_text: '“Ulster and its people,” Fraser’s Magazine, August 1876.'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654788991
audio: 10.-Coorse-`Straw.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following passages appeared in an article entitled “Ulster and its people,” published in Fraser’s Magazine.'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1) '
          -
            type: text
            text: "The magazine, which ran from 1830 to 1882, was published in London and featured contributions from people of widely differing political positions such as the famous Scots writer and commentator, Thomas Carlyle, author of Heroes, Hero-Worship and the Heroic in History (1841), and John Stuart Mill, author of The Subjection of Women (1869). The article, “Ulster and its people,” signed T.C., is clearly written from a Scottish perspective; it underlines the multiple interconnections between Ulster and Scotland, looking at issues of character and behaviour, religion, intellectual achievements, language and the economy.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "It should be remembered that the Home Rule movement, launched by Isaac Butt in 1870, was already making headway when this article was written. After a number of by-election victories for people standing on a Home Rule ticket (Limerick, 1871; Galway and Kerry, 1872), the Home Rulers won 60 out of the 103 Irish seats at the general election of February 1874. These MPs formed themselves into a separate group in March and began using “obstruction” tactics in the House of Commons to draw attention to their demands.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'T.C.’s insistence on the deep-seated ‘Scotch’ characteristics of the Ulsterman is interesting in this context in that it is designed to underline how “entirely unlike” the Ulster Scot is from “the people who fill the rest of the island.” It is precisely this difference that will be used later on to argue that Ulster should be given special treatment if Home Rule were to become Law.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Ulstermen have been described as a mongrel community. This is true in a sense. They are neither Scotch, English, nor Irish, but a mixture of all three; and they are an ingredient in the Irish population distinguished by habits of thought, character, and utterance entirely unlike the people who fill the rest of the island. It is easy to see, however, at a single glance that the foundation of Ulster society is Scotch. This is the solid granite on which it rests. There are districts of country – especially along the eastern coast, running sixty or seventy miles, from the Ards of Down to the mouth of the Foyle – in which the granite crops out on the surface, as we readily observe by the Scottish dialect of the peasantry.'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' '
              -
                type: text
                text: 'Only twenty miles of sea separate Ulster from Scotland at one point; and just as the Grampians cross the Channel to rise again in the mountains of Donegal, there seems to be no break in the continuity of race between the two peoples that inhabit the two opposite coasts. Thus it comes to pass that much of the history of Ulster is a portion of Scottish history inserted into that of Ireland; a stone in the Irish mosaic of an entirely different colour and quality from the pieces that surround it. (p. 220)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " \_T.C., “Ulster and its people,” Fraser’s Magazine, August 1876, pp. 219-229."
    uuid: e3425183-ed1a-47b5-8411-712b0719c42e
    image: De-Latcocayne-Screen_1-2-(11).png
    type: content
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Language issues crop up frequently in this 10-page article. The following extracts do not hesitate to tie language use in to religious practice.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We must say a word of the Ulster dialect and pronunciation, which is very unlike anything to be heard in any other part of Ireland. The language of the Northern province is a curious mixture of English, Scotch, and Irish, but moulded into a particular patois that is more Scotch than anything else… The Ulsterman usually pronounces I ‘a’ or ‘aw,’ as ‘a will’ for ‘I will;’ he says ‘aye’ when he means ‘yes;’… while he uses a whole heap of words and expressions borrowed evidently from the Scotch, such as brash, wheen, speel, sleekit, sevendible, scringe, bing, skelly, farl, thraw, curnaptious, dotther, thole, boke, dunsh, oxther, coggle, sheugh, stour, foother, jeuk, floosther, sthroop, dwine, cowp, flype, thon (yon), corp (corpse), dixenary, girn, wumman, umberell, slither.”'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'A Presbyterian peasant was once boasting of the preaching ability of his minister, and especially of the length of his services, which extended to three hours. His neighbour listened to the eulogy patiently enough till he heard the boast about the long services. He then quietly replied, ‘Coorse straw taks lang to chow’ (chew)…(pp. 224-225)'
    uuid: 745eb74e-b3f3-4ad8-8d4d-595ca28de26a
    type: content
    audio_file:
      - audio/MOD1-EP6-SCR2-T.C.-Coorse-`Straw-.mp3
    image: IMG_0559.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'T.C. goes on to say that: “It is not generally known that there was a Janet Geddes in Ulster.” '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
          -
            type: text
            text: 'He explains that, at the Restoration in 1660, Jeremy Taylor, the newly-appointed Church of Ireland Bishop of Down and Connor expelled a well-known Presbyterian minister, Rev. James Gordon, from a church in Comber and replaced him with a minister who was prepared to accept his authority as Bishop. Clearly, this did not go down well in the local community, many of whom did not approve of the vestments (“the white sark”) the new minister was wearing.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The women of the parish collected, pulled the new clergyman out of the pulpit, and tore his white surplus to ribbons. They were brought to trial at Downpatrick and one of the female witnesses made the following declaration: “And maun a tell the truth, the haile truth, and naethin but the truth?’ ‘You must,’ was the answer. ‘Weel, then,’ was her fearless avowal, ‘these are the hands that poo’d the white sark ower his heed.’ It is Presbyterianism that has fixed the religious tone of the whole province… (pp. 222-225)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "(1) \_Janet Geddes, a street-seller, allegedly threw a stool at the head of the minister in St Giles Cathedral in Edinburgh the first time the Scottish Book of Common Prayer was read in public (July 1637). This protest was said to have been the cause of more widespread disturbances in the city. The introduction of a Book of Common Prayer was looked at with deep suspicion by many in Scotland. "
      -
        type: paragraph
    uuid: ccaa6ee6-f5f9-485e-9da0-1230fb629e19
    image: De-Latcocayne-Screen_1-2-(13).png
    type: content
    enabled: true
    allow_zoom: true
---
