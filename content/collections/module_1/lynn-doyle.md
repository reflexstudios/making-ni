---
id: 49f6ea27-ca1c-47ed-a9a2-a4b47f6c1730
blueprint: module_1
title: 'Lynn Doyle'
intro_text: 'An Ulster Childhood, 1921.'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655627377
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Lynn Doyle (1873-1961) was the pen-name of Leslie A. Montgomery who was born in Downpatrick, Co. Down. He wrote a number of plays for the Ulster Literary Theatre but is best known for his highly-successful humorous stories centring on Ballygullion, written over a period of some fifty years. '
          -
            type: hard_break
          -
            type: hard_break
          -
            type: text
            text: 'His “not an autobiography,” An Ulster Childhood, was published in 1921'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1)'
          -
            type: text
            text: ', the year the Northern Ireland Parliament opened. In it, he explains that, although he was not brought up a Presbyterian himself, he spent a lot of time with his Presbyterian relatives. It is on their farm that he has his first contact with the work of Scotland’s national poet, Robert (Rabbie) Burns. He explains that this takes place not through a member of his own family, but thanks to a certain Paddy Haggarty, “a servant on my aunt’s farm.” This detail is significant at a number of levels. First, it underlines how attachment to Burns’ poetry was by no means confined to an intellectual élite; as Paddy says, Burns’ poetry is “just like a labourin’ man’s talk.” Secondly, Doyle tells us that Paddy is a “Celtic Irishman.” This is important in that it shows how an interest in Burns, and by extension, understanding and appreciation of writing in Scots, extends well beyond the Scots Presbyterians and into the broader community in Ulster. However, even though Paddy is perfectly familiar with the Scots culture of his Presbyterian neighbours, there is no suggestion that he felt himself to be anything other than Irish. This clearly poses no particular problem to either side. What this illustrates – in Doyle’s typically casual manner – is the way in which the close, everyday contact between these rural communities ensured that what we would today call “cross-community exchange” was taking place quite effortlessly in the Ulster of the day.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "It is interesting that the poem mentioned in Doyle’s account, “The Twa Dogs, A Tale,” is the very first poem to feature in the famous collection of Burns’ poetry,\_Poems, Chiefly in the Scottish Dialect, published in Belfast in 1787. The fact that this – pirate - edition of Burns’ poetry appeared in Belfast so soon after the initial editions in Kilmarnock and Edinburgh shows that there was clearly a receptive public – and therefore a market - for poetry in Scots in Ulster. This volume of poetry, along with copies of\_“Fragments of Scotch poetry”"
          -
            type: text
            marks:
              -
                type: italic
            text: "\_"
          -
            type: text
            text: 'by Burns that had already appeared in the Belfast News Letter the year before, is now part of the huge Gibson collection held at the Linen Hall Library in Belfast. The collection, named after Ayrshire-born, Andrew Gibson, a Belfast shipping agent with strong unionist sympathies who was Governor of the Linen Hall Library for over thirty years, is one of the largest collections of Burns-related material outside Scotland.'
          -
            type: hard_break
          -
            type: hard_break
          -
            type: text
            text: "The range of the Gibson collection reflects the immense impact that Burns was to have on Ulster literature and society. The so-called “weaver poets,” people like James Orr (some of whose work we will see in Module 2), Samuel Thomson and Hugh Porter, were operating within the same social and literary tradition as Burns, often referred to as the “Ayrshire ploughman.” They used Scots in their poetry simply because it was the language used within the communities for which they were writing. Many in Ulster would have agreed with the character in Samuel S. McCurry’s,\_Stories of an Ulster Village\_(1931), when he said: “There are two books in my opinion that a man shud be aye readin’, an’ they are his Bible and Burns. I like Burns because he’s so easy to understan’.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "This interest in Burns was reflected in the fact that the centenaries of his birth – 1859, the same year as the great religious “Revival” – and death – 1896 - were widely celebrated throughout Ulster. The first of many Burns Clubs was officially launched in Belfast in 1872. Such clubs are evidence of the keen interest in connections between Ulster and Scotland that was to intensify over the Home Rule period.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " \_Lynn Doyle, An Ulster Childhood, Dublin and London, Maunsel & Roberts Ltd, 1921."
    uuid: 1f9a5cd6-13aa-462b-a604-c235bc23f0e6
    image: De-Latcocayne-Screen_1-2-(21).png
    type: content
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I was reared in the Lowland Scottish tradition of homely realism, and my Gamaliel'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' (1)'
              -
                type: text
                text: " was, strangely enough, a Celtic Irishman, one Paddy Haggarty, a servant on my aunt's farm… He slept in a small apartment off the stable… [T]he real glory of our friendship dates from the night when Paddy, after shuffling in silence on his stool for a long time, asked me suddenly “If I knew anything of Rabbie Burns at all?”\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "I answered that I knew nothing of him save the name; but that I had often intended to read his poetry only there was not a copy of it in our house. To this day I can remember the almost reverent expression with which Paddy drew the dumpy little… volume from beneath his pillow. That night the harness-room Burns club was inaugurated.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '… I remember that he began with “The Twa Dogs”… Till then my acquaintance with verse had been restricted to Pope’s Homer… this was a new kind of poetry that Paddy was reading me. The note of sincerity touched even my childish heart; the homely dialect words sounded kindly in my Ulster ears.'
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Twa dogs that were na thrang'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' '
              -
                type: text
                text: 'at hame.”'
              -
                type: text
                marks:
                  -
                    type: bold
                text: '(2) '
              -
                type: text
                text: "From that line onward I listened with all my soul; and when the poem was finished I had become with Paddy a devotee in the worship of Rabbie Burns.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'In general Paddy was sparing of commentary, and [any remarks he made were] apt to be coloured by his political opinions. When, for instance, he read in “The Twa Dogs” of the “poor tenant bodies, scant o’ cash, How they maun thole a factor''s snash,” '
              -
                type: text
                marks:
                  -
                    type: bold
                text: '(3) '
              -
                type: text
                text: 'he paused to explain that a factor was, with us, a land-agent. “And, God knows,” he added heartily, “the people of this counthry had plenty to thole from them too, before Billy Gladstone''s time.” But I was rapt in the discovery that “thole” and “snash” were real words, and that I might use them in the future without shamefacedness…'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " \_Gamaliel taught the apostle Paul (Acts 22\_:3). Here, it is being used to mean that Paddy was the person who introduced and explained Burns to the young Doyle.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (2)
                  -
                    type: text
                    text: ' Thrang means busy.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (3)
                  -
                    type: text
                    text: ' Snash means impertinence or abusive language.'
    uuid: f757cd91-1eb2-4e6b-96d2-c4c13adfa8e9
    image: Dogs_large.png
    type: content
    audio_file:
      - audio/MOD1-EP10-SCR2.mp3
    enabled: true
    allow_zoom: true
---
