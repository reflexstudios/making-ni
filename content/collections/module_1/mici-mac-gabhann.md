---
id: ef7a7fcf-ce59-4fd3-bb78-a67e322a85e8
blueprint: module_1
title: 'Micí Mac Gabhann'
intro_text: 'Rotha Mór an tSaoil, 1959.'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654017017
audio: MOD1-EP5-SCR1.mp3
unit_content:
  -
    image: De-Latcocayne-Screen_1-2-(8).png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Micí Mac Gabhann (1865-1948) was born in Cloughaneely, an Irish-speaking area in the west of Donegal. In his Irish language autobiography, Rotha Mór an tSaoil\_ (lit. “the big wheel of life”), he describes how, like many of the other boys in the area, he was brought to a hiring fair in Letterkenny when he was nine years of age to be taken on as a seasonal farm hand for the “Scots” farmers (“Albanaigh”) in the Laggan district. Here, he manages to learn some English, which would later allow him to go further afield to work in Scotland, before trying his luck in America, eventually heading off to the Klondike during the Gold Rush in 1898. Years later, his son-in-law recorded his memories; an edited version was published in 1959, (1) around a decade after his death."
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following passage, from the early part of the book, describes how he returns to the hiring fair for the second time at the age of 10. He feels much more confident than he did the previous year because, having spent six months on a farm, he had been able to pick up some basic English. However, he soon realises that the “English” spoken in the Laggan is really “Scotch.” This is what he says about it:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Indeed, [the] accent and idiom [of the Laggan people] was hard to follow. It was not the same as what the master who taught me at school had. When they’d be talking about boys such as myself, ‘bairns’ they call them. One man was telling me about a horse that took fright as she was being led to the fair that morning and what he said was that the animal was ‘copin’ curly.’'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' (2) '
              -
                type: text
                text: 'Another man averred that he was ‘sagged'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' '
              -
                type: text
                text: ' with the rheumatics’ '
              -
                type: text
                marks:
                  -
                    type: bold
                text: '(3) '
              -
                type: text
                text: "and that we were lucky to have ‘suchan a brave day.’ When I heard all this and more, I was of the opinion that it would be just as easy for them to understand my Irish as it was for me to make head or tail of their English.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Micí Mac Gabhann, Rotha Mór an tSaoil [1959], Indreabhán, Cló Iar-Chonnachta, 1996. The book was translated into English by Valentin Iremonger as The Hard Road to Klondike, London, Routledge & Kegan Paul, 1962. The extracts are taken from this translation.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (2)
                  -
                    type: text
                    text: ' “Coup” or “cowp” in Ulster-Scots means to overturn, to upset. The expression “copin’ curly” means to turn head over heals, to do somersaults. Here, it would mean something like “jumping around wildly.”'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(3) '
                  -
                    type: text
                    text: "\_“Sagged” means doubled in two (with pain)."
    uuid: 50e8e902-20b3-4289-a29e-e89578096cf9
    type: content
    audio_file:
      - audio/5c.-Charlie-(Mici-Mac-Gabhann).wav
    enabled: true
    allow_zoom: true
  -
    image: De-Latcocayne-Screen_1-2-(9).png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The young Micí is taken on by an elderly man called Sam Dubh (black-haired Sam), whom Mac Gabhann immediately identifies as belonging to the “Clann Liútair,” i.e. the Lutherans, clearly a catch-all term for “Protestants”!\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "When he arrives, he is struck by the slovenliness of Sam’s elderly sister, Jane, telling us how the pair of men’s shoes she was wearing hadn’t been cleaned since the day they were bought. The farmhouse they live in is equally untidy.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: '“There was an old log in the fireplace that stuck out into the middle of the floor; over it a pot hung in which there were potatoes being boiled for the pigs while another pot and a tub hung in the ingle-nook also. […]'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Despite this, they treat the boy relatively well. Above all, they make sure that he attends his “kirk” every Sunday.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "They were kind enough in their own way […] they had no ill will towards Catholics, for instance. Come Sunday morning, if I wasn’t up myself, they’d call me and tell me it was time to dress myself to go to the ‘kirk’ as they called the church. As a result, when I grew older and went out in the world, my respect for the old pair grew. The minister would come to the house now and again and I believe he would read them something called 'the lesson', but I'd be sent out while he'd be there.\_"
      -
        type: paragraph
    uuid: 13f00a0c-122a-4e72-bace-a1b05b982998
    type: content
    enabled: true
    allow_zoom: true
  -
    image: De-Latcocayne-Screen_1-2-(10).png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This section of the book gives us some fascinating insights into the everyday life of the Ulster-Scots community in the Laggan.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Amongst other things, the following extract shows that practices like the kailey and figures like the story-teller that we associate primarily with the Irish Gaelic tradition (cf. seanchaí) had their equivalents in the Scots communities in the north of Ireland. The section in the Irish-language original is entitled “seanchaí albanach,” i.e. “the Scottish story-teller.” In other words there was clearly a very high level of “cultural continuity” between the traditions in the rural areas, something that continued up until the 1950s at least.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "[N]ow and again there’d be great nights of story-telling and a wealth of tales would be told. There was one old man in particular named Billy Craig who came visiting regularly and I can’t tell you the number of stories he had. He’d have been nearing eighty at this time and he was one of those who spent the early part of his life going around the province of Ulster ploughing in the spring-time. He had a great reputation as a ploughman. I think it was because of all his wandering that he had the gift of story-telling. I often heard people say since then that the Scots had no lore or superstitions but I can tell you they’re wrong. Billy Craig had plenty of lore and stories just like some of our own people and he was as superstitious as any of the old people that you’d ever meet. Sam and Jane were just the same.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "… There was no kind of story-telling that he didn’t know something about but he specialised in stories about ghosts and ‘the little people.’ It’s likely enough that he believed in ghosts because if he was to stay till morning he wouldn’t leave the house without a companion although he lived only a short distance away.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '… There was no end to the stories that Billy had about [the little people] – stories about everything pertaining to them: people they helped and people they hurt; people they took away and who never returned; people who felled trees or bushes in some sacred place and who never did any good afterwards; people who built houses in forbidden places; … Many is the night I spent listening to these stories about fairies and ghosts…'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I said that Sam and Jane were superstitious and that’s nothing but the truth. I saw Sam tying a red tassle around the neck of a calf that he was letting out for the first time. I saw Jane take a piece of fresh butter out of the churn and stick it to the wall over the cattle. She did this to save the milk from evil charms; and of course, there was a horse-shoe on the door of the cow-house with the heel part upwards. I thought nobody paid any attention to these customs except at home but I was finding out that the people of the Laggan were, if anything, more superstitious than our own…'
    uuid: 09e5d603-8ce8-45e8-a2f6-390f711b1d7e
    type: content
    enabled: true
    allow_zoom: true
---
