---
id: 0f7c00c8-115e-497f-829b-8acb4576616c
blueprint: about
title: About
template: about
author: 73b51ea8-5c46-4529-be4c-3ec8796f6b5f
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1657388443
large_title: 'About The Project'
author_name: 'Wesley Hutchinson'
author_bio: 'Wesley Hutchinson, who is from Ballymena, spent his entire teaching career in France where he lived for almost 40 years. He is Professor Emeritus (Irish Studies) at the Université Sorbonne Nouvelle and a former President of the Société Française d’Études Irlandaises (SOFEIR). For the past ten years, he has been working almost exclusively on issues connected with Ulster-Scots history, literature and identity. His most recent book, Tracing the Ulster-Scots Imagination, published by Ulster University with financial support from the Department of Foreign Affairs and Trade Reconciliation Fund, and Pretani Associates, came out in December 2018.'
author_image:
  - IMG_3473.jpg
page_content:
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Overall Introduction to the Making Northern Ireland Resource'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The purpose of this resource is to show how references to Ulster-Scots history and culture and the links between Ulster and Scotland form an essential part of the Home Rule debate in the years leading up to the creation of Northern Ireland in 1921.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The site is divided into three, interconnected Modules, all of which are provided with Timelines and Bibliographies. There is also a Glossary of the Ulster-Scots words that appear in the documents.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The site has been designed as an educational resource across a broad range of levels. As an example of how the site can be used at specific levels, several Student Tasks have been provided specially aimed at Key Stage 3 and 4 students. These Tasks will allow the material to be explored creatively through the production of films, comic books and sound recordings.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'However, the site can also be used by those working at more advanced levels as well as by those with a general interest in Ulster-Scots history and culture.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Module 1 '
      -
        type: text
        text: 'sets the scene by showing how people visiting Ulster during the 19th and early 20th centuries are struck by how different it is from the rest of Ireland. Consistently, they say that if the region “feels” so different, it is because it is so Scottish. This evidence points to the existence of a distinct community, recognised as such by outsiders and conscious of its own identity in terms of language use and social practice.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Module 2 '
      -
        type: text
        text: 'identifies a series of “frames” that explain the way the Ulster Scots see themselves and their community over the course of the centuries. This module includes content that highlights the Ulster-Scots perspective on a number of key historical events. Exploring the cultural background in this way will help us understand the Ulster-Scots mind-set. It will also help explain the choices unionist politicians were making in their publications and speeches when appealing specifically to an Ulster-Scots audience at the Home Rule period.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Module 3 '
      -
        type: text
        text: 'charts the progress of the unionist campaign from the time Gladstone introduced the first Home Rule Bill in 1886 up to 1925 when the negotiations between London, Dublin and Belfast finally confirm the border of the recently-established Northern Ireland. Throughout this period, unionists consistently return to themes that underline the closeness of Ulster’s connection to Scotland over the centuries. The intensity of these references shows how central “Ulster-Scottishness” is to a large – and growing - cross-section of the population in Ulster at the time.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'It is over this period that the notion of the “Ulster Scot” becomes clearly established in the popular imagination.'
      -
        type: hard_break
      -
        type: hard_break
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Recordings of Ulster-Scots material are by Charlie Gillen, Liam Logan, Katie McCurdy and Ruth McNeill. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Commentaries are read by Wesley Hutchinson.'
      -
        type: hard_break
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: hard_break
      -
        type: text
        text: 'The author would like to thank Professor Graham Walker (Queen''s University, Belfast) for his comments and suggestions on the content of the site. '
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Except as where otherwise indicated (with Notes identifying the rights holders of content referenced within) this website and its content is copyright of the author Wesley Hutchinson - © Wesley Hutchinson 2022. All rights reserved.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Any redistribution or reproduction of part or all of the contents in any form is prohibited other than the following:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'you may print or download to a local hard disk extracts for your personal, educational and non-commercial use only'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'you may copy the content to individual third parties for their personal use, but only if you acknowledge the website as the source of the material'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'You may not, except with our express written permission, distribute or commercially exploit the content. Nor may you transmit it or store it in any other website or other form of electronic retrieval system.'
      -
        type: hard_break
  -
    type: paragraph
  -
    type: paragraph
---
