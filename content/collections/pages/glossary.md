---
id: 65280a34-9768-41ea-b6b4-0331341e18eb
blueprint: default
first_line_of_title: Glossary
second_line_of_title: 'of Ulster-Scots terms that appear as spelt in the extracts'
title: Glossary
template: glossary/index
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1651489797
---
