---
id: home
blueprint: home_page
title: Home
template: home
large_title: 'Making Northern Ireland'
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1655470339
large_title_line_1: Making
large_title_line_2: Northern
large_title_line_3: Ireland
intro:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bts_span
            attrs:
              class: null
        text: "Making Northern Ireland looks at Ulster-Scots history and culture from the Plantation period on to show how unionist politicians called on Ulster’s strong Scottish connections in their campaign against Home Rule that was to lead to the creation of Northern Ireland.\_"
video: 'https://youtu.be/0hriS3mGWRY'
---
