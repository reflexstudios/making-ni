---
id: 367fd442-4bbc-4b43-baab-c9908bf4eb31
blueprint: modules_index
template: module_3/index
title: 'Home rule - an Ulster-Scots Perspective'
author: 73b51ea8-5c46-4529-be4c-3ec8796f6b5f
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655475881
module_number: '03'
module_thumbnail: menu-mod-3.jpg
module_subtext: 'This Module will explore how the unionist campaign against Home Rule used themes that underlined the closeness of Ulster''s connections to Scotland over the centuries. The intensity of these references shows how central "Ulster-Scottishness" was to an increasingly large cross-section of the population in Ulster at the time.'
resources: docs/MakingNI_Learning_Resources.zip
module_intro_content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The main focus of this Unit is on the material that was being used in this campaign to further the anti-Home Rule cause using Scots and Ulster-Scots references.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We will see that, right from the outset, there was a decision to “stir up the feeling of Scotland in favour of this movement." This involved using the various Frames that we looked at in Module 2 in order to explain why many in the Ulster-Scots, and, by extension, the broader unionist community, were opposed to the proposed legislation.'
  -
    type: paragraph
    content:
      -
        type: text
        text: "Arguments were therefore put forward underlining the close historical ties, the family and commercial links, or the Presbyterian connections that existed between Ulster and Scotland.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The following Unit provides concrete examples of how these links to Scotland were exploited in the unionist campaign not only at home in Ulster but also in Scotland.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This material cannot be seen as having been conjured up out of nowhere. On the contrary, it corresponded to a lived reality for large sections of the population in certain parts of Ulster. The political use to which Ulster-Scots references were being put merely reflected this underlying social reality.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Thus, despite the fact that Ulster-Scots was primarily an oral culture, the 1880s saw the publication of a number of writings which reflected various aspects of life in the Ulster-Scots community. We think, for example, of novels such as Maria Crommelin’s, Orange Lily, published in London in 1880, or W.G. Lyttle’s, Sons of the Sod, published in Belfast in 1886, the year the Home Rule Bill came before Parliament. 1883 saw the publication of an important posthumous collection of poetry by David Herbison, the “Bard of Dunclug” - The Select Works of David Herbison, published in Belfast, Ballymena and Londonderry. And then in 1886, we have the publication in Edinburgh of History of the Irish Presbyterian Church, by Rev. Thomas Hamilton, one of a number of Presbyterian church histories to appear during the Home Rule period. All of this material was going to contribute – even if only indirectly - to the on-going “construction” of the “Ulster Scot” in the popular imagination by feeding in to the developing political campaign.'
unit_number: '00'
ulster-scots_perspective:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Hello World'
---
