---
id: 02b390a1-9f95-4982-b944-1f7cf5e69495
blueprint: default
title: 'Terms & Conditions'
author: 73b51ea8-5c46-4529-be4c-3ec8796f6b5f
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1639031004
page_content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Lorem ipsum dolor sit amet'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'consectetur adipiscing elit. Morbi vel imperdiet urna, quis mollis massa. Integer vitae congue eros. Mauris rutrum eleifend semper. '
      -
        type: text
        text: 'Sed '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: '#'
              rel: null
              target: null
              title: null
        text: 'sapien lorem'
      -
        type: text
        text: ', vestibulum sit amet lacus a, ultrices fringilla turpis. Quisque purus sem, venenatis vitae aliquet nec, suscipit sed sem. Cras vehicula libero ac risus fermentum pharetra. Donec vel nisl ac neque pretium tempor et vitae tellus. Suspendisse sit amet metus ut purus venenatis feugiat. Integer ornare ornare pharetra.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Curabitur id lorem tempor'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'vestibulum quam eget, tempor ligula. In hac habitasse platea dictumst. Donec mattis mollis ex, quis posuere diam mattis efficitur. '
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: italic
        text: 'Praesent porttitor magna ut metus pretium varius. Vestibulum quis lacus vulputate massa egestas porta. Mauris massa tellus, pulvinar in mi a, ullamcorper porttitor eros. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'In volutpat non erat eu pellentesque. Sed eros justo, vehicula vel mattis ac, eleifend lobortis sem. Nullam a rhoncus purus. Praesent nec mauris eget leo condimentum consectetur. '
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Nullam ac mattis tellus, vel tincidunt augue. '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Etiam egestas mattis urna nec interdum. '
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Ut vulputate leo id diam consectetur, in tincidunt augue fermentum. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Praesent vestibulum cursus odio, non placerat ipsum consequat ut. Cras nec metus sit amet libero tincidunt dictum a ut erat. Phasellus nisl purus, vulputate nec est et, semper fringilla nulla.'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::MOD-1-EP7-Screen_2-To-be-added-1638612577.jpg'
          alt: null
      -
        type: text
        text: 'mollis ac ultrices sed, laoreet in lorem. Mauris lectus metus, gravida non bibendum non, convallis ac sem. Phasellus maximus ultricies risus, '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: '#'
              rel: null
              target: null
              title: null
          -
            type: bold
        text: 'tincidunt pulvinar'
      -
        type: text
        text: ' nunc bibendum et. Morbi scelerisque tortor eget erat congue, id pretium risus placerat. Ut arcu justo, sodales non tincidunt eu, feugiat at odio. Curabitur imperdiet imperdiet fermentum. Pellentesque in sem quis augue suscipit volutpat sit amet nec dolor. Nam nec nisi vehicula, gravida nisl vitae, euismod ipsum.'
  -
    type: blockquote
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: '"Curabitur non consequat ipsum. Donec quam nisl, laoreet quis finibus vel, gravida eu libero. Sed sed sem sit amet dui aliquam tristique. Maecenas blandit dui vitae lacus condimentum sodales. In aliquam finibus mauris, vitae luctus lacus sollicitudin id. Proin tincidunt eget mi a scelerisque. Morbi at hendrerit erat. Sed vel dolor bibendum, aliquam ante ut, feugiat lacus. In risus orci"'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'mollis ac ultrices sed, laoreet in lorem. Mauris lectus metus, gravida non bibendum non, convallis ac sem. Phasellus maximus ultricies risus, '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: '#'
              rel: null
              target: null
              title: null
          -
            type: bold
        text: 'tincidunt pulvinar'
      -
        type: text
        text: ' nunc bibendum et. Morbi scelerisque tortor eget erat congue, id pretium risus placerat. Ut arcu justo, sodales non tincidunt eu, feugiat at odio. Curabitur imperdiet imperdiet fermentum. Pellentesque in sem quis augue suscipit volutpat sit amet nec dolor. Nam nec nisi vehicula, gravida nisl vitae, euismod ipsum.'
  -
    type: set
    attrs:
      values:
        type: video
        video: 'https://www.youtube.com/watch?v=DWcJFNfaw9c'
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        text: 'mollis ac ultrices sed, laoreet in lorem. Mauris lectus metus, gravida non bibendum non, convallis ac sem. Phasellus maximus ultricies risus, '
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: '#'
              rel: null
              target: null
              title: null
          -
            type: bold
        text: 'tincidunt pulvinar'
      -
        type: text
        text: ' nunc bibendum et. Morbi scelerisque tortor eget erat congue, id pretium risus placerat. Ut arcu justo, sodales non tincidunt eu, feugiat at odio. Curabitur imperdiet imperdiet fermentum. Pellentesque in sem quis augue suscipit volutpat sit amet nec dolor. Nam nec nisi vehicula, gravida nisl vitae, euismod ipsum.'
  -
    type: set
    attrs:
      values:
        type: related_enteries
        related_unites_module_title: 'Further Reading'
        related_units_modules:
          - d751edf0-405a-466e-b329-df8a5273927b
          - c9008e3f-d6dd-4142-bb3e-49c755fd8c89
          - f07994d5-8139-4320-aee4-6b6ece0d22be
          - b3bc48fa-27a8-467f-86cb-366253976041
          - 4c069e3a-2e33-4e16-9b49-0764790bc3ec
  -
    type: paragraph
first_line_of_title: 'Terms &'
second_line_of_title: Conditions
---
