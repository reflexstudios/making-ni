---
id: 26c15ae3-f454-4a94-8e9f-5f0635feb088
blueprint: modules_index
template: module_1/index
title: 'Why did Ulster feel so "Scotch"?'
author: 73b51ea8-5c46-4529-be4c-3ec8796f6b5f
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655629851
module_number: '01'
module_thumbnail: menu-mod-1.jpg
resources: docs/MakingNI_Learning_Resources.zip
module_intro_content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: 'Why did Ulster feel so "Scotch"?'
  -
    type: paragraph
    content:
      -
        type: text
        text: "People have often seen Ulster as “a place apart,” one that is strikingly different from other parts of Ireland.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "From the beginning of the 17th century, when large parts of Ulster were “planted” with English and Scottish settlers, one of the main differences people remarked was the Scottish influence on the region. This is interesting because, although Ulster had one of the strongest concentrations of English settlement in Ireland, it is often not the “Englishness” of the region that stands out, but its “Scottishness.”\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "It is almost as if people saw Ulster as being so different because it was so Scottish.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "This first section contains a number of documents that show how the Scots have left an indelible mark on the region.\_"
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: "Representing the Scots community in Ulster\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "Some of the texts are personal accounts written by people who simply travel through the area as visitors. These texts will give you precious glimpses of what everyday life in the Ulster-Scots communities was like at the time.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "Then, you’ll find other texts that are written from a specifically Scottish point of view. Here, the authors tell you how they are struck by the similarities between Ulster and Scotland.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "Or again, there are the extracts from the very first Scotch-Irish Congress in Columbia, Tennessee, in 1889. These texts are written by the descendants of Ulster Scots who had been settling in America since the earliest large-scale migrations in the early 18th century. These people, who called themselves “Scotch-Irish,” decided to start looking into their own identity at exactly the same time as the Ulster-Scots community in Ireland.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "You’ll probably not be surprised to see that many of the documents underline the strong link between Presbyterianism and the Scots community in Ulster.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "This doesn’t mean, however, that “Ulster-Scottishness” is only for Presbyterians. Individuals and communities have always interacted in Ulster. This is the case for the Irish and Scots traditions, for example, especially in the rural areas which formed the backbone of Ulster society up until very recently. This aspect comes out strongly in some of the texts.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: 'A lot of the authors make references to the way people use language. They frequently see the use of Scots as one of the most striking features of the community. They sometimes even quote the people they meet on their travels. This is really interesting because some of the texts therefore allow us to “hear” what ordinary people belonging to that community have to say “in their own words.”'
  -
    type: paragraph
    content:
      -
        type: text
        text: "If you pay attention to the place-names mentioned in the texts, it will give you a pretty good idea of where the Ulster-Scots communities lived in Ulster.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "Some of the authors do not try to hide the fact that they are trying to promote the “Ulster Scot” and make him look as good as possible. Others are more neutral and just describe what they see. This means that, even those with no particular political or cultural agenda are sufficiently impressed by the Scottish feel of this part of Ireland to comment on it in detail in their writing.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "In other words, it is not only the people of Scottish origin in Ulster who see themselves as making up a distinct community – outsiders see them as distinctly Scottish as well.\_"
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Towards the “Ulster Scot”'
  -
    type: paragraph
    content:
      -
        type: text
        text: "The characteristics highlighted in these texts will come into sharper focus when Ulster unionism begins to organise itself to resist Home Rule. One of the many facets of that resistance that was specific to Ulster was the “Ulster Scot” who emerges as a recognisable figure in the history and literature of the period.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: 'However, at this stage, we are only interested in showing how these authors identify the cultural distinctiveness of this community. The way that distinctiveness is used for a specific political purpose – opposition to Home Rule – will be looked at later.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bts_span
            attrs:
              class: null
        text: 'The well-known material in the Ordnance Survey (begun in 1824 and completed in 1846) provided evidence of the strong Scottish influence on parts of the North-East of Ulster at that particular period. The selection in this Module - which covers a much more extended period, over one hundred and twenty years from the end of the 18'
      -
        type: text
        text: th
      -
        type: text
        marks:
          -
            type: bts_span
            attrs:
              class: null
        text: "\_century up to 1920 - leaves us in no doubt as to how deeply rooted this distinct Ulster-Scots community was and continues to be in the province of Ulster.\_"
module_subtext: 'This Module shows how people visiting Ulster during the 19th and early 20th centuries are struck by how different it is from the rest of Ireland. Consistently, they say that if the region “feels” so different, it is because it is so Scottish.  Click on the Icons below to discover their story...'
next_module: 6f1cb0dd-042f-42ee-a375-a3610707f15e
timeline_items:
  -
    year: '1775'
    link_to_entry:
      - 0f7c00c8-115e-497f-829b-8acb4576616c
    type: new_item
    enabled: true
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Beginning of American War of Independence'
    link: 'entry::0f7c00c8-115e-497f-829b-8acb4576616c'
  -
    year: '1789'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'French Revolution'
    type: new_item
    enabled: true
  -
    year: '1791'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Foundation of the United Irishmen in Belfast'
    type: new_item
    enabled: true
  -
    year: '1797'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'De Latocnaye, Promenade d’un Français dans l’Irlande'
    type: new_item
    enabled: true
  -
    year: '1798'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'United Irishmen''s Rising'
    type: new_item
    enabled: true
  -
    year: '1800'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Act of Union abolishes the Irish Parliament and creates the United Kingdom of Great Britain and Ireland'
    type: new_item
    enabled: true
  -
    year: '1811'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'John Gamble, Sketches of History, Politics and Manners, Taken in Dublin, and the North of Ireland, in the Autumn of 1810'
    type: new_item
    enabled: true
  -
    year: '1813'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'John Gamble, A View of Society and Manners, in the North of Ireland, in the Summer and Autumn of 1812'
    type: new_item
    enabled: true
  -
    year: '1823'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Catholic Association founded by Daniel O’Connell'
    type: new_item
    enabled: true
  -
    year: '1829'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Catholic Emancipation Act allows Catholics to sit in parliament'
    type: new_item
    enabled: true
  -
    year: '1831'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Tithe War begins'
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Introduction of ‘national’ system of elementary education'
    type: new_item
    enabled: true
  -
    year: '1832'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'James Glassford, Notes of Three Tours in Ireland in 1824 and 1826'
    type: new_item
    enabled: true
  -
    year: '1834'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'The Presbyterian minister, Rev. Henry Cooke, addresses a meeting of Conservatives in Hillsborough, calling for the formation of a united pro-union front between Presbyterians and members of the Church of Ireland '
    type: new_item
    enabled: true
  -
    year: '1840'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Foundation of the Repeal Association'
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'General Assembly of the Presbyterian Church in Ireland formed'
    type: new_item
    enabled: true
  -
    year: '1843'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Mr and Mrs S.C. Hall, Ireland: Its Scenery, Character &c, Vol. III'
    type: new_item
    enabled: true
  -
    year: '1845'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Potato blight first noticed in September: beginning of Great Famine'
    type: new_item
    enabled: true
  -
    year: '1859'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Religious Revival in Ulster'
    type: new_item
    enabled: true
  -
    year: '1865'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Micí Mac Gabhann (1865-1948), author of Rotha Mór an tSaoil, born in Cloughaneely, Donegal'
    type: new_item
    enabled: true
  -
    year: '1867'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Fenian Rising'
    type: new_item
    enabled: true
  -
    year: '1869'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Disestablishment of the Church of Ireland'
    type: new_item
    enabled: true
  -
    year: '1870'
    additional_items:
      -
        year: August
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone’s first Land Act'
        type: additional_items
        enabled: true
      -
        year: September
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'foundation of Home Government Association by Isaac Butt'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1873'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Home Rule League founded'
    type: new_item
    enabled: true
  -
    year: '1876'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'T.C., “Ulster and its people,” Fraser’s Magazine'
    type: new_item
    enabled: true
  -
    year: '1879'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Irish National Land League founded'
    type: new_item
    enabled: true
  -
    year: '1884'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Gaelic Athletic Association founded'
    type: new_item
    enabled: true
  -
    year: '1885'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Irish Loyal and Patriotic Union founded'
    type: new_item
    enabled: true
  -
    year: '1886'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'First Home Rule Bill introduced; defeated in the Commons'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Loyalist Anti-Repeal Union founded'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1888'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'John Harrison, The Scot in Ulster'
    type: new_item
    enabled: true
  -
    year: '1889'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'The Scotch-Irish in America: Proceedings of the Scotch-Irish Congress at Columbia, Tennessee'
    type: new_item
    enabled: true
  -
    year: '1892'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Ulster Unionist Convention in Belfast'
    type: new_item
    enabled: true
  -
    year: '1893'
    content:
      -
        type: heading
        attrs:
          level: 3
    additional_items:
      -
        year: february
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Second Home Rule Bill introduced'
        type: additional_items
        enabled: true
      -
        year: july
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gaelic League Formed'
        type: additional_items
        enabled: true
      -
        year: September
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Second Home Rule Bill is defeated in the Lords'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1905'
    additional_items:
      -
        year: March
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Unionist Council created'
        type: additional_items
        enabled: true
      -
        year: November
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Sinn Féin policy launched'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Rev. Alexander G. Lecky, The Laggan and its Presbyterianism'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1907'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'The Presbyterian Historical Society of Ireland founded'
    type: new_item
    enabled: true
  -
    year: '1908'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Rev. Alexander G. Lecky, In the Days of the Laggan Presbytery'
    type: new_item
    enabled: true
  -
    year: '1911'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Parliament Act removes veto of the House of Lords'
    type: new_item
    enabled: true
  -
    year: '1912'
    additional_items:
      -
        year: April
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Third Home Rule Bill introduced'
        type: additional_items
        enabled: true
      -
        year: 'September 28'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Day: Signing of Ulster’s Solemn League and Covenant'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1913'
    additional_items:
      -
        year: January
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Volunteer Force founded'
        type: additional_items
        enabled: true
      -
        year: September
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Unionist Council approves the creation of an Ulster Provisional Government under Sir Edward Carson'
        type: additional_items
        enabled: true
      -
        year: November
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Citizen Army founded'
        type: additional_items
        enabled: true
      -
        year: November
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Volunteers founded'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1914'
    additional_items:
      -
        year: April
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'UVF gun-running'
        type: additional_items
        enabled: true
      -
        year: August
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First World War begins'
        type: additional_items
        enabled: true
      -
        year: September
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Government of Ireland act passed; implementation suspended during the war'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1916'
    additional_items:
      -
        year: April
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Easter rising in Dublin'
        type: additional_items
        enabled: true
      -
        year: July
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Battle of the Somme'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1918'
    content:
      -
        type: heading
        attrs:
          level: 3
    type: new_item
    enabled: true
    additional_items:
      -
        year: November
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'End of First World War'
        type: additional_items
        enabled: true
      -
        year: December
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'General election across the United Kingdom.'
        type: additional_items
        enabled: true
  -
    year: '1919'
    content:
      -
        type: heading
        attrs:
          level: 3
    type: new_item
    enabled: true
    additional_items:
      -
        year: January
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First meeting of Dáil Éireann'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Beginning of War of Independence'
        type: additional_items
        enabled: true
  -
    year: '1920'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'Government of Ireland Act partitions Ireland, creating Northern Ireland (six counties, with a Parliament in Belfast), Southern Ireland (26 counties, with a Parliament in Dublin) and a Council of Ireland '
    type: new_item
    enabled: true
  -
    year: '1921'
    additional_items:
      -
        year: June
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Opening of the Northern Ireland Parliament by King George V'
        type: additional_items
        enabled: true
      -
        year: December
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Anglo-Irish Treaty ends the War of Independence'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Lynn Doyle, An Ulster Childhood'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
module_introduction_audio: audio/1.-Unit-1-Intro_Why-Did-Ulster-Feel-So-Scotch_edit-(Paragraph-removed).mp3
---
