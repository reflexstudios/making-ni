---
id: 6f1cb0dd-042f-42ee-a375-a3610707f15e
blueprint: modules_index
template: module_2/index
title: 'Putting the Ulster Scot Centre Stage'
author: 73b51ea8-5c46-4529-be4c-3ec8796f6b5f
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655632293
module_number: '02'
module_thumbnail: menu-mod-2.jpg
module_intro_content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: “Frames”
  -
    type: paragraph
    content:
      -
        type: text
        text: 'As part of their campaign against Home Rule, many Ulster unionists chose to appeal to the sense of Scottishness that was such a fundamental part of Ulster identity.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'If they decided to focus attention on this “Scottish dimension,” they were doing so for one simple reason: they knew that they could tap into a cultural background that was shared by a large section of the Ulster population and that could be put to effective use to further their political cause.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Module 2 will explore that cultural background by identifying a number of events or themes that are of particular importance if we are to understand the fundamental reflexes of the Ulster-Scots community over the centuries and especially at the Home Rule period.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'It identifies a number of “frames” that will help de-code the choices the unionist politicians were making in their publications and speeches when they were trying to appeal to an Ulster-Scots audience.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Each frame focuses on an event or a theme across the culture, calling exclusively on work in circulation in the late 19th and early 20th century, and using sources from Ulster, Britain and North America. The idea is to try and get to what a particular event or theme meant to the Ulster Scot at this particular period. The reason for this is that historical events (like 1798) or certain concepts (like “loyalty”) could mean something to an Ulster-Scots audience that was sometimes quite different from what other sections of the Irish or British public might understand.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'In other words, rather than proposing a “History of the Ulster Scot from A to Z,” the idea is to go for a series of individual entries that highlight the particular “angle” the Ulster Scot takes on this or that historical event.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'By identifying and thinking about these frames, and by being sensitive to this imaginary, it will be easier to understand the effect of the “buttons” that the speakers and writers were pushing when they were putting forward their arguments during their campaign against Home Rule.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The idea, therefore, is to look into the mindset of the community at a particular point in time.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: '"Constellations and Pathways"'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Just as there is no set chronology to follow in this Module, so there is no prescribed “pathway” through the material on offer. Everyone is free to choose which route they want to take.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Each frame can be read on its own. So, you can start with any frame you choose. And then move on to any other one that takes your fancy.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'However, as you read through any given entry, the text will indicate links to other frames that you may wish to follow up.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Alternatively, you may choose to take a look at the “Constellations”. Here, the different frames have been arranged together in groups that bring together a number of frames that have obvious interconnections. Thus, for example, the theme of “Emigration” is tied in to “the Frontier” and also to “the Empire.”'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Whichever route you choose, one of the things to do at every stage is to imagine how a given event or theme could be “applied” to the Home Rule debate. When we come to Module 3, we will see how the writers and speakers take the material that we have identified here and use it as a sounding board for some of their arguments. In the meantime, Module 2 can be used to get people thinking about the various “angles” that allow history and culture to be mobilised for a specific political purpose.'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        text: '"Questions"'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Here you will find information about how the Ulster Scot appears so sharply at this period and why this happened when it did. Although you may choose to skip this section and go straight to the frames, it may be useful to understand something of the underlying cultural dynamics of the period.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'If you do decide to take a look, you will see that, again, you are free to move around the section in whichever order you choose.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Finally, it is important to remember that the people who were writing the books and articles or delivering the speeches were products of their age. The way they thought, the way they express their arguments, the attitudes that underpin some of their declarations are coloured by the prejudices of the period and do not always necessarily correspond to notions of what is “appropriate” today. Bear this in mind when you go through the following material.'
module_subtext: 'This Module will explore a number of historical events or themes that will help us understand the fundamental reflexes of the Ulster-Scots community at the Home Rule period. We have presented them in the form of "constellations". Click on any "frame" below to begin your journey. If you would like to look at the "questions" section first, scroll down below the constellations.'
key_questions:
  -
    question: 'When does the “Scot in Ulster” become the “Ulster Scot”?'
    answer:
      -
        type: set
        attrs:
          values:
            type: add_audio
            audio_file: audio/4.-Module-2-Question-1_edit.mp3
      -
        type: paragraph
        content:
          -
            type: text
            text: 'We saw in Module 1 that people visiting Ulster often thought that it was remarkably different from the rest of Ireland. The reason given for this difference was the strength of the Scottish influence that they felt in some areas.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'But, when they talked about the people, how did they refer to them?'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The following are examples from what the authors said: “the descendants of Scotchmen” (Gamble); “the Scotch settlers” (Glassford); “Albanaigh/Scots” (Mac Gabhann); “the Ulsterman” (T.C.); “the Scot in Ulster” (Harrison); “the Scotch-Irish” (Macloskie).\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "What is striking is that none of them uses the expression “Ulster Scots.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This is odd, because this term had existed since the 17th century. Sir George Radcliffe, a friend and advisor to the Lord Lieutenant, Thomas Wentworth, was the first to talk about “the Ulster Scots” as a group as early as 1640. '
      -
        type: paragraph
        content:
          -
            type: text
            text: "However, the term “Ulster Scot” did not come into general use until the final decades of the 19th century. Indeed, it is precisely during the Home Rule period that “Ulster Scot” becomes the standard way of referring to this community.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The reason for this is that it is at this time that the figure of the “Ulster Scot” emerges clearly in the popular imagination, with a particular set of characteristics and, more importantly, a distinct history.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'An indication of the popularity of the term at the Home Rule period is the way in which those hostile to the political choices of the Ulster Scots used the term in their attacks.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'One of the best examples of this is to be found in an article published in July 1912, at the very height of the crisis around the third Home Rule Bill. The article appeared in The Irish Review, a “monthly magazine of literature, art and science” published in Dublin. (1) The author of the article, although he signs himself “An Ulster Scot,” is clearly sympathetic to the nationalist cause. He states that “Ulster Scot” is now “the favourite [term] adopted by the Anti-Irish Ulsterman”:'
      -
        type: paragraph
        content:
          -
            type: text
            text: '"In my youth the Unionists of the North-east corner of Ulster were contented with the name “Ulstermen,” “North-of-Ireland” men, or even “Scotch-Irish,” but... “Ulster Scots” now appears to be the favourite term."'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The author goes on to say something of the particular significance that this term had taken on in the area of politics and identity:'
      -
        type: paragraph
        content:
          -
            type: text
            text: '"I had a discussion with a typical Ulster Unionist the other day, in which he refused to label himself as Irish, Scotch, English, or Welsh, or anything else but an “Ulster Scot.” His family has resided in Ireland for an unknown period, probably from the beginning of the 17th century, anyway so long that no record exists of from what part of Scotland, or in what epoch his ancestors migrated; but in his estimation he was still an alien in Ireland!" (p. 229)'
      -
        type: paragraph
        content:
          -
            type: text
            text: "So, it is clear that, if the term “Ulster Scot” had become the standard way of referring to this group it is because it denotes what Benedict Anderson has called an “imagined community.” That is to say that, by the Home Rule period, the community had begun to do just that – to imagine itself as a distinct community.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'They do that by piecing together what one author at the time called a “distinct and connected history of this people,” and using that to project a recognisable image of the “Ulster Scot” into the collective imagination.'
      -
        type: paragraph
        content:
          -
            type: text
            text: '(1) An Ulster Scot, “The denial of North-east Ulster,” The Irish Review, July 1912, Vol. 2 N° 17, p. 228.'
    type: new_set
    enabled: true
  -
    question: 'Why did the figure of the Ulster Scot appear so clearly  at the Home Rule period?'
    answer:
      -
        type: set
        attrs:
          values:
            type: add_audio
            audio_file: audio/4.-Module-2-Question-2_edit.mp3
      -
        type: paragraph
        content:
          -
            type: text
            text: 'During the fifty years Home Rule dominated politics in Ireland, issues of cultural identity took on greater and greater political importance.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Irish Revival was creating a clear image of “the Gael” who was seen as the embodiment of an independent Irish nation, and whose roots went back thousands of years.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, a majority of unionists saw themselves as “British” and considered this separatist Irish identity as exclusive and as celebrating what was to them an alien culture.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In reaction to this, certain sections of the unionist community began to focus their attention on the “Ulster Scot,” a figure who emerges at this time as an alternative to that of the Gael.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Ulster Scots, it was argued, had their own history, one that spanned Scotland, Ulster and America. An understanding of that history, which had not been attempted until this period, was to play an increasingly important role in the way people belonging to this Ulster-Scots community imagined themselves as a group.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Whereas the Irish revival underlined the separateness of Irish culture, the Ulster Scots reminded people of the many links that existed between Ireland and Great Britain. Their story focused particular attention on the historical links that had existed for centuries between Ulster and Lowland Scotland, in particular since the Plantation at the beginning of the 17th century.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Many areas of Counties Down, Antrim, Londonderry and Donegal had large populations whose origins lay in the Lowlands of Scotland. This shared Scottish culture continued to connect large parts of Ulster into the neighbouring island. It therefore challenged Irish nationalism’s insistence on demanding a separate, independent Ireland with a stand-alone Irish culture.'
    type: new_set
    enabled: true
  -
    question: 'Why was the “absorbing power” of the Gael both a threat and a stimulus to the Ulster Scot?'
    answer:
      -
        type: set
        attrs:
          values:
            type: add_audio
            audio_file: 'audio/4. Module 2 Question 3_edit.wav'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Irish culture often laid great emphasis on what D.P. Moran, writing in 1899, called its “absorbing power,” i.e. its capacity to assimilate foreigners of every origin: “The foundation of Ireland is the Gael, and the Gael must be the element that absorbs. On no other basis, can an Irish nation be reared.” The future Irish nation, centred on the Gael, was to be both politically and culturally independent of Great Britain.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Irish nationalism saw the people of Scots origin in Ulster simply as Irish. At best, the cultural tradition of the Scots in Ulster was seen as a sub-category of an all-embracing Irish culture that covered the island as a whole.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This vision clearly did not appeal to many in the Ulster-Scots community.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is as if it was only when they felt that their way of life might be under threat by an Irish identity that seemed to be trying to absorb them that they felt the need to give their own identity a sharper form.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'They needed an image that was instantly recognisable, one they could identify with and that could be used for political purposes, in the same way as the idea of “the Gael” was being used to promote Irish independence.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'That figure that was to emerge was the Ulster Scot.'
    type: new_set
    enabled: true
  -
    question: 'Why did Ulster-Scots geography and Ulster-Scots history seem so hard to explain?'
    answer:
      -
        type: set
        attrs:
          values:
            type: add_audio
            audio_file: audio/4.-Module-2-Question-4_edit.mp3
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Ulster Scot had a harder time explaining who he was and where he came from than the Gael.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'For those promoting the Celtic Irishman, things were relatively simple.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'For a start, geography was on their side. They were able to show a map of Ireland and say – “That’s where we’re from. This is Ireland. It’s an island out on its own, ‘surrounded by water,’ separate from the islands around it.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The idea that geography determined cultural and political identity was very attractive. It was easy to understand because it was easy to imagine.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The problem was, Irish unionists didn’t agree. They felt that Ireland could not be looked at in isolation because it was part of a larger group of islands, an archipelago that placed Ireland and Great Britain within a complex network of inter-connections including things like family ties, cultural bonds and political identity.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Of course, a part of them was Irish. But that was far from the whole story. Other parts of their identity were just as important and they were tied in to places outside Ireland.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Nonetheless, geography was a major problem for the Ulster Scot. What could he show when he held up a map?'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Bits of Ulster + bits of Lowland Scotland + bits of the United States.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'But he couldn’t draw a line round these and say: “That’s my space. It belongs to me.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The bottom line was that it was far easier to IMAGINE Ireland than it was to IMAGINE Ulster-Scots territory.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The same problem applied to history.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Gael could point to his map of the island of Ireland and say: “My history is the history of the Island of Ireland. Full stop.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'But the Ulster Scot had to say: “Well, my history starts on the Borders between England and Scotland. But it also involves whole areas of the Scottish Lowlands. Then, a bit later, my ancestors went over to settle in the province of Ulster. But, later still, some of them started moving again and sailed over to the east coast of North America. And, before you know it, they’re pushing the American frontier further and further west...”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'That was a great story. But, at the time of the Home Rule crisis was beginning, that story hadn’t been fully written. The reason for this was quite simple. Up until then, the history of the Ulster Scot had been written into several national histories – those of Scotland, Ireland and America.'
      -
        type: bullet_list
        content:
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'When the Ulster Scots appeared in Ireland, whatever they did there became a sub-plot of Irish history.'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'When they appeared in Scottish history, they were re-cast as Scots who had returned to the fold.'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'When the same Ulster Scots showed up in America, they were seen as just an extra in the bigger picture of United States history.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In other words, the Ulster Scots always seemed to end up playing second fiddle – wherever they went!'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Rather than being seen as a prime mover, the Ulster Scot was seen as following in somebody else’s footsteps, always condemned to operate on the margins of history.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'There had never been a focus on a specific ULSTER-SCOTS history.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'That’s what was to change during the Home Rule period.'
    type: new_set
    enabled: true
  -
    question: 'How was the Ulster Scot written into history...  and literature?'
    answer:
      -
        type: set
        attrs:
          values:
            type: add_audio
            audio_file: audio/4.-Module-2-Question-5_edit.mp3
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In nationalist Ireland the work of organisations such as the Gaelic Athletic Association and the Gaelic League helped to create a clear idea of what “Irishness” was taken to mean at the end of the 19th century. All of this activity in the various areas of sport, language learning and literature created a very powerful and easily recognisable image of what the “Gael” represented in the popular imagination.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The image of the Ulster Scot did not come about in the same way. Rather, the Ulster Scot emerged from a variety of disparate sources whose interaction was much more loosely conceived.'
      -
        type: bullet_list
        content:
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'One obvious source was the body of Presbyterian Church histories that were being written at this period. This included not only histories of the Presbyterian Church in Ireland by people like Rev. Hamilton or Rev. Latimer, for example, but also histories of given presbyteries such as Rev. A. G. Lecky’s history of the Laggan Presbytery in Donegal, or even histories of individual congregations, such as J.W. Kernohan’s History of the Parishes of Kilrea and Tamlaght O’Crilly. All of these contributed, sometimes indirectly, to the strengthening and embedding of the image of the Ulster Scot in the popular imagination.'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Then, there were the novels and the poetry written by people like Thomas Given, W.G. Lyttle or Archibald McIlroy. These people were producing material in Ulster-Scots about the Ulster-Scots community for an audience in Ulster and in the Ulster-Scots diaspora. Although much of their writing is not explicitly political, the fact that it was being produced during the Home Rule period proves that a demand for such material clearly existed.'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'On top of that, there were the more overtly political writings that emerged during the period by people like the Scot, John Harrison, or Rev. James Woodburn, focusing on the history and contemporary position of the Ulster Scot. This material was specifically designed to contribute to the on-going debate on Home Rule and to offer a radically different perspective to the growing body of material promoting an “Irish Ireland.”'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Outside the Ulster context, there was also a lot of history writing being produced by the Scotch-Irish community in America. Publishing in New York and Boston, authors such as Charles A. Hanna or Henry Jones Ford contributed a great deal to the construction of our image of the Ulster Scot by showing how these Ulster-Scots settlers had crossed the Atlantic to become the “Scotch-Irish.” This work was closely linked in to that of structures like the Scotch-Irish Society of America. The story of Ulster-Scot emigration to North America is vitally important because it allows the Ulster Scot to be imagined on a global scale.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Thanks to these various strands coming together at this period, the Ulster Scots found themselves centre stage for the very first time. At last, they could see coherence and pattern in a distinct and separate narrative about themselves. This newly-asserted sense of belonging to a distinct community was to be put to use by those organising resistance to Home Rule.'
    type: new_set
    enabled: true
module_introduction_audio: audio/3.-Module-2-Intro_edit-(1).wav
constellation_one:
  -
    page_link:
      -
        entries: 9c8a1f00-16bb-42a0-903c-ccc49e8bf9d3
        top: null
        left: null
      -
        entries: c0990a91-448e-41d3-a02f-144bcf7ca670
        top: null
        left: null
      -
        entries: 45c17a20-b789-4106-b2d9-050cdfe948ed
        top: null
        left: null
      -
        entries: b33827df-bc14-4d38-a2e7-f50d5be425d8
        top: null
        left: null
    entries: 9c8a1f00-16bb-42a0-903c-ccc49e8bf9d3
    top: 4
    left: 13
  -
    entries: c0990a91-448e-41d3-a02f-144bcf7ca670
    top: 39
    left: 21
  -
    entries: 45c17a20-b789-4106-b2d9-050cdfe948ed
    top: 16
    left: 41
  -
    entries: b33827df-bc14-4d38-a2e7-f50d5be425d8
    top: 76
    left: 9
constellation_two:
  -
    entries: a72e16f0-8185-4481-9279-28aeddb051e8
    top: 51
    left: 28
  -
    entries: b86b009a-9dd3-4d43-8136-1f47e8cd6728
    top: 13
    left: 48
constellation_three:
  -
    entries: 3f44a27e-4430-43cb-91d8-4ac34121931b
    top: 27
    left: 21
  -
    entries: 0ec0c184-36db-43c3-a59a-dfdff20a96a6
    top: 40
    left: 55
  -
    entries: b2f6ef85-1c4a-4c26-951c-cef97157fafb
    top: 56
    left: 22
  -
    entries: 8155344a-0cf3-4137-bd5b-5483c0d643b7
    top: 86
    left: 13
constellation_four:
  -
    entries: f07994d5-8139-4320-aee4-6b6ece0d22be
    top: 49
    left: 13
  -
    entries: 2873ab3d-8e6e-4290-b16f-b473717a8c6b
    top: 15
    left: 71
  -
    entries: c772540e-5895-4901-b084-c790639d41f4
    top: 57
    left: 65
next_module: 367fd442-4bbc-4b43-baab-c9908bf4eb31
resources: docs/MakingNI_Learning_Resources.zip
timeline_items:
  -
    year: '1594'
    type: new_item
    enabled: true
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Nine Years War breaks out in Ulster'
        type: additional_items
        enabled: true
  -
    year: '1603'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Elizabeth I dies and is succeeded by James VI of Scotland who becomes James I of England; the surrender of the Earl of Tyrone brings the Nine Years War to an end'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1606'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Scots brought over to settle land in County Down acquired by two Scottish lairds, Hugh Montgomery and James Hamilton'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1607'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Flight of the Earls; their lands are “escheated” to the Crown'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1609'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Ulster Plantation begins in six “escheated” counties in Ulster: Donegal, Coleraine (now Londonderry), Tyrone, Armagh, Fermanagh, Cavan'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1611'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'King James version of the Bible published'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1625'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Six Mile Water Revival'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1636'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Presbyterian ministers in Ulster removed from Church of Ireland parishes'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1638'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The National Covenant read out in Edinburgh'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1639'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The ‘Black Oath’ imposed on Presbyterians over 16 in Ulster'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1641'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Rebellion breaks out in Ulster'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1642'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Scottish army arrives in Ulster; first Presbytery meets in Carrickfergus'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1643'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Solemn League and Covenant signed by Presbyterians in Scotland and later in Ulster'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1660'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Restoration of the monarchy in England; Charles II proclaimed King'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1684'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Persecution of the Covenanters in Scotland'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1688'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Glorious Revolution; beginning of the “Williamite Wars”'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1689'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Siege of Londonderry'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1690'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Battle of the Boyne'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1704'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Test Act'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1717'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First wave of Ulster Presbyterian emigration to America'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1775'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Beginning of the American War of Independence'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1782'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Marriage Act recognises legality of marriages of Presbyterians performed by Presbyterian ministers'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1789'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'French Revolution'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1791'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Creation of the Society of United Irishmen in Belfast'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1795'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Battle of the Diamond and creation of the Orange Order'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1798'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'United Irishmen’s rising'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1800'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Act of Union abolishes the Irish Parliament and creates the United Kingdom of Great Britain and Ireland'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1823'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Catholic Association founded by Daniel O’Connell'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1829'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Catholic Emancipation Act permits Catholics to sit in Parliament'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1834'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Presbyterian minister, Rev. Henry Cooke, addresses a meeting of Conservatives in Hillsborough, calling for the formation of a united pro-union front between Presbyterians and members of the Church of Ireland '
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1840'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Foundation of the Repeal Association by O’Connell'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'General Assembly of the Presbyterian Church in Ireland formed'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1844'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Marriage Act recognises legality of marriages between Presbyterians and members of the Church of Ireland'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1845'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Potato blight; beginning of the Great Famine'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1850'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'General Assembly of the Presbyterian Church in Ireland declares in favour of Tenant Right'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1859'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'A religious Revival sweeps through Presbyterian areas in Ulster'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1869'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Disestablishment of the Church of Ireland'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1870'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Home Government Association founded by Isaac Butt'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone’s First Land Act'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1879'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Irish National Land League founded'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1881'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone’s Second Land Act'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1886'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone introduces the First Home Rule Bill in the Commons'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
---
