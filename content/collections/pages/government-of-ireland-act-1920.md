---
id: b3bc48fa-27a8-467f-86cb-366253976041
blueprint: modules_index
module_number: '03'
title: 'Government of Ireland Act 1920'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655633731
related_units:
  - e0355eb6-2adc-49fd-b3c0-26c0f5ac5291
  - c6762377-24ab-49e1-89ab-80770884a84b
  - 386c1c88-f2c7-43d5-a6ae-487a779df46b
  - 4c069e3a-2e33-4e16-9b49-0764790bc3ec
  - 64c90d38-7625-4341-bdd7-b35e6a288b24
  - ccc69764-e80e-4004-8167-e22478c22bf6
module_intro_content:
  -
    type: set
    attrs:
      values:
        type: accordian
        replicator:
          -
            title: 'The Government of Ireland Act, 1920'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Even though the third Home Rule Bill had been signed into law in September 1914, it did not come into force as intended at the end of the war. This was because the war and its aftermath had totally redefined the situation on the ground and the issues at stake regarding Home Rule.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In the nationalist camp, opinion had become more radical. The Sinn Féin victory at the 1918 elections showed clearly that nationalist opinion had moved beyond a request for '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'limited autonomy'
                  -
                    type: text
                    text: '. A majority of people in Ireland now demanded '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'total independence'
                  -
                    type: text
                    text: ".\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'This demand was expressed politically by the creation of '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Dáil Éireann'
                  -
                    type: text
                    text: ', a republican Parliament operating outside British control. It was also expressed militarily in the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'War of Independence'
                  -
                    type: text
                    text: ". This war was fought between the British army and the Royal Irish Constabulary (RIC) on the one hand and the Irish Republican Army (the term “IRA” emerges for the first time at this period) on the other. The war dragged on between January 1919 and July 1921 when a truce was signed.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'All through this period – and beyond - the cities of Belfast and Derry/Londonderry saw prolonged periods of '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'sectarian rioting'
                  -
                    type: text
                    text: " that left dozens of people dead and hundreds injured, fuelling further cycles of retaliation.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In the middle of the War of Independence (Feb. 1920), the British government put forward a fourth '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Government of Ireland Bill'
                  -
                    type: text
                    text: ' which became law in December 1920. This text finally gave Home Rule to Ireland. What was different this time round was that the text now included '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: partition
                  -
                    type: text
                    text: ".\_"
            type: add_set
            enabled: true
          -
            title: 'Four, six or nine?'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "The idea of partitioning Ireland had been in the air for a long time. As we saw in the previous Unit, the Liberal MP, Agar-Robartes, had proposed the “exclusion” of the four northern counties with Protestant (and by extension unionist) majorities, i.e. Antrim, Down, Londonderry and Armagh. Although the proposed amendment was rejected, the idea of “exclusion” did not go away.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "The central issues involved were – what territory should be excluded from the provisions of the bill and for how long?\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "The three configurations envisaged were known as the “four-county,” the “six-county” and the “nine-county” options.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The first “'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'four-county option'
                  -
                    type: text
                    text: "” (Antrim, Down, Londonderry and Armagh) was justified by the fact that there was a clear anti-Home Rule majority in each of the four counties involved. However, unionists felt that the area would be too small to have any chance of survival in the long term.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'nine-county option'
                  -
                    type: text
                    text: " corresponded to the Province of Ulster, an historical entity which also corresponded to the area covered by those who had been eligible to sign the Solemn League and Covenant in 1912. However, many unionists felt that the Protestant majority in the nine-county unit (some 53% according to the Census of 1911) was too fragile to last.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Their preferred option was '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'the six-county unit'
                  -
                    type: text
                    text: ' (i.e. the four-county option plus Fermanagh and Tyrone = present-day Northern Ireland). There, although there were Catholic (and by extension nationalist) majorities in two of them (Fermanagh & Tyrone), overall there was a 66% Protestant majority, again according to the 1911 Census. However, this option raised the question of the unionist minorities in the counties of Cavan, Monaghan and Donegal, many of whom had signed the Covenant in 1912.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Despite this, it was to be this last option, which gave the unionists a two-to-one majority over nationalists, that was to be adopted by those drawing up the Government of Ireland Act.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The text thus set up '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'two parliaments'
                  -
                    type: text
                    text: ", one in Dublin for Southern Ireland (26 counties) and one in Belfast for Northern Ireland (6 counties). The two parliaments were to have responsibility for local affairs. International affairs were to be dealt with in the imperial Parliament in London which retained sovereignty over Ireland as a whole.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'A third body, the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Council of Ireland'
                  -
                    type: text
                    text: ", bringing together representatives of the two Irish parliaments, would allow the administrations north and south of the border to deal with matters of mutual concern. Indeed, the Act foresaw that when sufficient common ground was established, this body could turn itself into a single Parliament for the whole of Ireland.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In other words, the text saw '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Partition as a temporary, transitional phase'
                  -
                    type: text
                    text: .
            type: add_set
            enabled: true
          -
            title: 'Just one problem: the Act pleased no-one'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'As we have seen, the overwhelming majority of nationalists now demanded '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'the creation of a Republic'
                  -
                    type: text
                    text: ", which involved a break with the Crown. The idea of partition was incompatible with the republican vision which insisted on the unity of the Irish people. The territorial unity of the island of Ireland was seen as the physical expression of this unity.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The unionists were no happier in that they had never asked for Home Rule. Indeed, they had spent the previous forty-odd years fighting against it! However, they were now ready to accept it because, from the unionist point of view, “'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Home Rule with partition'
                  -
                    type: text
                    text: "” was infinitely better than any form of government exercised from Dublin.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'After elections in May 1921, the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Northern Ireland Parliament'
                  -
                    type: text
                    text: " was opened by King George V the following month. The King’s presence was seen as clear evidence of the extraordinary constitutional significance of the event. The ceremony, which was an outstanding success, was a cause of great celebration in the unionist community.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "However, in a sign of problems to come, it was boycotted not only by Sinn Féin (republicans), but even by the (more moderate) nationalists.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Meanwhile, Sinn Féin had won an overwhelming majority in the elections to the Southern Ireland Parliament (124 out of 128 seats), with all its candidates being returned unopposed. However, in accordance with their policy of “abstention,” they refused to take their seats in it and met as the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Second Dáil'
                  -
                    type: text
                    text: ".\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "It should be remembered therefore that the Northern Ireland Parliament was the only one of the three institutions foreseen under the terms of the Government of Ireland Act that ever actually came into existence. Because the Southern Ireland Parliament was never operational, it could not send representatives to a Council of Ireland.\_"
            type: add_set
            enabled: true
          -
            title: 'Towards a solution?'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The period between July and December 1921 saw a series of negotiations between the British Government and the Sinn Féin leadership which were to end with the signature of an '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Anglo-Irish Treaty'
                  -
                    type: text
                    text: " on 6 December 1921.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The unionists refused to take part in these discussions because Craig thought he might be forced into making concessions, especially on the border. The Treaty foresaw the creation of an '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Irish Free State'
                  -
                    type: text
                    text: " which was to be given “dominion status.” This meant that Ireland was being offered a greater level of independence than under Home Rule. Concretely, it would have the same constitutional relationship to the United Kingdom as Canada. However, the Treaty also gave Northern Ireland one month to “opt out” of this Irish Free State. This would allow it to remain within the United Kingdom, but with a (Home Rule) Parliament in Belfast. The Treaty said that, if this happened - as everyone knew it would, - a Boundary Commission would be set up to make recommendations on possible changes to the border.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The Treaty caused a split within Sinn Féin, with many republicans seeing it as falling well short of the republic they wanted. The split in the party was mirrored by a split in the IRA. These divisions led to a '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Civil War'
                  -
                    type: text
                    text: ' (1922-1923) which was fought between the forces of the new Irish Free State and the “irregulars,” i.e. those members of the IRA opposed to the Treaty. The pro-Treaty faction, with material support from the British, succeeded in defeating the IRA and in consolidating the Irish Free State.'
            type: add_set
            enabled: true
          -
            title: 'Northern Ireland'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "All this time, Northern Ireland had been functioning as a separate political unit.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Northern Ireland, which continued to send 13 MPs to Westminster, had its own Parliament made up of the King (represented by the Governor of Northern Ireland), a Senate and a House of Commons of 52 members, elected by Proportional Representation. This Parliament had the power to make law for the “peace, order and good government of Northern Ireland.” The first Prime Minister was Sir James Craig, who had taken over from Edward Carson as the leader of the Ulster unionists in February 1921. Craig was assisted by a cabinet, with ministries in such areas as finance, agriculture, commerce, education and home affairs. However, Northern Ireland remained an integral part of the United Kingdom and laws passed by Westminster continued to apply there.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Although the Belfast Parliament immediately set to work, producing important legislation such as the Education Act (Northern Ireland), 1923, which tried –unsuccessfully - to set up a non-denominational school system under the control of local authorities, the country was far from stable.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Northern Ireland continued to be affected by forms of political violence linked to the underlying sectarian tensions that had led to so many clashes during the Home Rule period. This was the case, especially in the urban centres like Belfast and Londonderry, where politics and religion were often closely tied in to issues of employment.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Furthermore, Northern Ireland was the target of a concentrated campaign of violence from the IRA. This campaign consisted in attacks on private property and public installations, as well as murders of police and “Specials” (see below) and kidnappings of prominent unionists living in border areas.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Needless to say, the unionist authorities were quick to respond. A number of strong security measures were taken in quick succession.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'As early as November 1920, i.e. before the creation of Northern Ireland, an '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Special Constabulary'
                  -
                    type: text
                    text: " (USC) was created to assist the police in its fight against the IRA. Initially, it attracted many recruits from the former UVF. One section of this force, the part-time ‘B Specials,’ was to continue in existence until 1970 when it was finally disbanded.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Once the Northern Ireland Parliament came into existence, the Government continued to be preoccupied with security. Thus, the Northern Ireland Parliament voted the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Civil Authorities (Special Powers) Act'
                  -
                    type: text
                    text: ' in April 1922. This'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' '
                  -
                    type: text
                    text: 'gave the Government wide-ranging powers to ban what were considered to be seditious publications and organisations and to search and detain persons without trial. Similarly, in May 1922, when the Irish police (RIC) was dismantled as a result of the Treaty, the authorities in Belfast created their own police force, the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Royal Ulster Constabulary (RUC)'
                  -
                    type: text
                    text: ".\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Although unionists approved these measures which most saw as necessary to counter worrying levels of violence, many in the nationalist community saw them as oppressive and as having been conceived primarily against them.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Indeed, from the outset, Northern Ireland’s existence was challenged by the large Catholic and nationalist minority, many of whom felt they had been “trapped” inside a territory whose legitimacy they did not recognise. Their opposition was made clear by the decision to boycott the institutions of the new state. This campaign of passive resistance saw the nationalist parties refuse to sit in the Northern Ireland Parliament, while, at a local level, several local authorities, such as the County Council of Fermanagh, went as far as pledging allegiance to Dublin.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Many nationalists hoped that the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Boundary Commission'
                  -
                    type: text
                    text: ", once it finally started work, would propose such major transfers of territory that Northern Ireland would not be able to survive as a separate unit.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In this initial period, many on both sides of the border were convinced that Northern Ireland’s days were numbered.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "In other words, Craig’s government could not feel in any way secure until the issue of the border was decided once and for all.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "For various reasons, notably the on-going violence of the Civil War, and the northern Government's refusal to cooperate, the Boundary Commission did not begin its work until 1924. After much deliberation, it was decided that it should only have the power to recommend relatively small changes to the border as it had been drawn under the Government of Ireland Act, 1920. This was to come as a major relief to the unionists, who were determined that they would not give up “an inch” of Northern Ireland territory. It was also a major disappointment to nationalists who began to realise that changes to the territory of Northern Ireland would be less than they had hoped.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "In the end, after the Commission’s proposals were leaked prematurely to the press, causing the resignation of the Free State representative, tripartite negotiations held between London, Dublin and Belfast (December 1925) decided to leave the border exactly as it was.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Northern Ireland, set up in 1921 in the middle of a war, was finally placed on a solid foundation.\_ "
            type: add_set
            enabled: true
timeline_items:
  -
    year: '1919'
    additional_items:
      -
        year: 'January 18th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Formal opening of the Paris Peace Conference.'
        type: additional_items
        enabled: true
      -
        year: 'January 21st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First meeting of Dáil Éireann in Dublin.'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'IRA ambush in Soloheadbeg: beginning of the War of Independence.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1920'
    additional_items:
      -
        year: January
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Recruiting begins of ex-servicemen (later known as Black and Tans) to support police against the IRA.'
        type: additional_items
        enabled: true
      -
        year: February
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Judge W.C. Benet, “Scots and Ulster-Scots in the Southern States, Part II,” The Caledonian, Vol. XIX, N° X.'
        type: additional_items
        enabled: true
      -
        year: 'February 25th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Government of Ireland Bill introduced in the House of Commons.'
        type: additional_items
        enabled: true
      -
        year: June
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Riots in Derry/Londonderry.'
        type: additional_items
        enabled: true
      -
        year: July
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: "Riots in Belfast; violence flares up again in August.\_"
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Tensions remain high in the city during the period 1920-1924.'
        type: additional_items
        enabled: true
      -
        year: 'November 1st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Enrolment of the Ulster Special Constabulary begins.'
        type: additional_items
        enabled: true
      -
        year: 'November 21st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Bloody Sunday: IRA assassinates 14 British secret service agents in Dublin; Black and Tans open fire on a GAA match in Croke Park, Dublin, killing 12.'
          -
            type: paragraph
        type: additional_items
        enabled: true
      -
        year: 'December 23rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Government of Ireland Act partitions Ireland, creating Northern Ireland (six counties, with a Parliament in Belfast), Southern Ireland (26 counties, with a Parliament in Dublin) and a Council of Ireland. '
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'H.S. Morrison, Modern Ulster: Its Character, Customs, Politics and Industries; completed in January.'
  -
    year: '1921'
    additional_items:
      -
        year: 'February 4th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Carson resigns as leader of the Ulster unionists; he is replaced by Sir James Craig.'
        type: additional_items
        enabled: true
      -
        year: 'May 13th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'All candidates for the Southern Ireland Parliament returned unopposed; Sinn Féin takes 124 of the 128 seats; Sinn Féin members boycott the Southern Ireland Parliament and meet as the Second Dáil (August 1921).'
        type: additional_items
        enabled: true
      -
        year: 'May 24th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'In elections to Northern Ireland Parliament: unionists win 40 seats, Sinn Féin 6 and nationalists 6.'
        type: additional_items
        enabled: true
      -
        year: 'June 7th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Sir James Craig appointed Prime Minister of Northern Ireland.'
        type: additional_items
        enabled: true
      -
        year: 'June 8th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: "Leonard Raven-Hill cartoon, “Starting the settlement,” in Punch.\_"
        type: additional_items
        enabled: true
      -
        year: 'June 22nd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Opening of Northern Ireland Parliament by King George V.'
        type: additional_items
        enabled: true
      -
        year: 'July 11th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Truce begins in the War of Independence.'
        type: additional_items
        enabled: true
      -
        year: 'October 11th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Anglo-Irish Conference in London between the British Government and Dáil delegates; Craig does not attend.'
        type: additional_items
        enabled: true
      -
        year: 'November 19th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Opening of the Ulster Tower at Thiepval.'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: '“The Ulster War Memorial,” in The Northern Whig.'
        type: additional_items
        enabled: true
      -
        year: 'December 6th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Anglo-Irish Treaty signed in London; Irish Free State is given “dominion status”; Northern Ireland is free to opt out; a Boundary Commission to review the border between the two parts of Ireland.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1922'
    content:
      -
        type: heading
        attrs:
          level: 3
    additional_items:
      -
        year: January
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'On-going IRA campaign.'
        type: additional_items
        enabled: true
      -
        year: 'February - March'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Sectarian violence in Belfast and elsewhere.'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ronald McNeill’s, Ulster’s Stand for Union, completed in February.'
        type: additional_items
        enabled: true
      -
        year: 'April 7th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Civil Authorities (Special Powers) Act gives the Belfast Government wide-ranging powers, e.g. detention without trial.'
        type: additional_items
        enabled: true
      -
        year: 'May 31st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Legislation creating the Royal Ulster Constabulary (RUC) passed.'
        type: additional_items
        enabled: true
      -
        year: 'June 28th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Civil War begins between pro- and anti-Treaty republicans.'
          -
            type: paragraph
        type: additional_items
        enabled: true
      -
        year: 'September 11th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Local Government Act (Northern Ireland) abolishes proportional representation in local government elections.'
        type: additional_items
        enabled: true
      -
        year: 'October 23rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Andrew Bonar Law (Conservative) becomes Prime Minister of the United Kingdom'
        type: additional_items
        enabled: true
      -
        year: 'December 7th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Northern Ireland Parliament opts out of Irish Free State.'
        type: additional_items
        enabled: true
      -
        year: 'December 9th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The office of the Governor of Northern Ireland, established.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1923'
    additional_items:
      -
        year: 'May 24th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Civil War ends.'
        type: additional_items
        enabled: true
      -
        year: 'June 22nd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Education Act (Northern Ireland) establishes system of non-denominational schools.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1924'
    content:
      -
        type: heading
        attrs:
          level: 3
    type: new_item
    enabled: true
    additional_items:
      -
        year: 'November 4th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Stanley Baldwin (Conservative) becomes Prime Minister of the United Kingdom'
        type: additional_items
        enabled: true
      -
        year: 'November 5th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Boundary Commission begins work under the chairmanship of Mr Justice Richard Feetham. '
        type: additional_items
        enabled: true
  -
    year: '1925'
    additional_items:
      -
        year: 'April 3rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'General election in Northern Ireland; nationalists decide to end policy of abstention.'
        type: additional_items
        enabled: true
      -
        year: 'November 7th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Proposals of the Boundary Commission leaked in the Morning Post.'
        type: additional_items
        enabled: true
      -
        year: 'November 20th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Bab M’Keen, “The Boundary,” in The Ballymena Observer.'
        type: additional_items
        enabled: true
      -
        year: 'December 3rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Agreement between London, Belfast and Dublin; the existing border between the Free State and Northern Ireland confirmed; the idea of a Council of Ireland abandoned; additional financial terms agreed.'
        type: additional_items
        enabled: true
      -
        year: 'December 18th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Bab M’Keen, “The Boundary Buried,” in The Ballymena Observer.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1932'
    additional_items:
      -
        year: 'November 16th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Parliament Buildings, at Stormont, officially opened by the Prince of Wales.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1948'
    content:
      -
        type: heading
        attrs:
          level: 3
    type: new_item
    enabled: true
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Hugh Shearman, Anglo-Irish Relations.'
        type: additional_items
        enabled: true
unit_number: '04'
ulster-scots_perspective:
  -
    type: paragraph
    content:
      -
        type: text
        text: "As with previous Units, the main focus here is on material that illustrates how Ulster’s Scottish connections are used both to frame arguments against Home Rule and to give a solid grounding to the new Northern Ireland that is emerging out of the turmoil of the preceding fifty years.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "In this section we will be looking at texts by key figures in the unionist establishment of this new Northern Ireland, people like H.S. Morrison, future Ulster Unionist MP for Queen’s University, and Ronald McNeill, Conservative MP and author of a classic history of “the Ulster movement.” Both insist on the centrality of the Ulster-Scots tradition not only to the everyday life of the area but also in terms of the fundamental cultural reflexes of northern society.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: 'As in earlier units, we include passages in Ulster-Scots. It is important to feature this type of material as it shows that people like Bab M’Keen were continuing to respond to a demand for writing in Ulster-Scots in areas where it was an integral part of the community’s life. '
  -
    type: paragraph
    content:
      -
        type: text
        text: "The section also illustrates the on-going connections with America and how American involvement in the war effort alongside Britain was used by Ulster unionists to challenge the perception of “the Irish question” among those Americans with Scotch-Irish roots.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: 'But the Unit also looks at forms of expression other than written political material…'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We will see, for example, how a cartoonist like Leonard Raven-Hill, addressing a national audience in the pages of Punch, uses frames which are clearly tied in to the image of the Ulster Scot that was developing in the popular imagination.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Similarly, we will show how Craig’s choice of Helen’s Tower as his model for the Ulster Tower in the Somme can be seen as illustrating fundamental cultural connections between Ulster and Scotland. The fact that this monument, the first war memorial to be completed on the western front, should be in such a strongly Scottish style, said a lot about the image the unionist authorities wished to project to the outside world.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Once again, it is important to remember that while this material was being written, often urgently, in response to an evolving political situation, Ulster-Scots authors were continuing to produce work that explored different aspects of their community looked at from the inside. Thus, John Stevenson, the author of Pat M’Carty: His Rhymes, published an important social history in 1920 entitled, Two Centuries of Life in Down (1600-1800). The focus of the book is immediately clear in the titles he chooses for the chapters: “The Coming of the Scots,” “The Scots at Work,” “The Kirk in Down,” etc. Other books like Lynn Doyle’s, An Ulster Childhood, published in 1921, the year the Northern Ireland Parliament was opened, give us fascinating insights into the everyday life of Ulster-Scots families around the end of the 19th century. Material such as this contributes actively to what we called the “construction” of the Ulster Scot. By focusing so directly on the Ulster-Scots community, texts such as these will ensure that the “idea” of the Ulster Scot would continue to be embedded in the popular imagination.'
---
