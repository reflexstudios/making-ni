---
id: 2b84ddf4-6861-4fdd-a014-1a8e9aba79c4
blueprint: modules_index
module_number: '03'
title: 'Second Home Rule Bill'
module_intro_content:
  -
    type: set
    attrs:
      values:
        type: accordian
        replicator:
          -
            title: 'The second Home Rule Bill'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Most Ulster Scots would have agreed with Rev. Hamilton in his History of the Irish Presbyterian Church (1886), when he said: “The defeat of Mr. Gladstone''s Bill [in] June, 1886, was hailed with deep satisfaction.”'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'However, that satisfaction was to be short-lived.'
            type: add_set
            enabled: true
          -
            title: 'Home Rule – second try'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In August 1892, Gladstone returned to power, forming his fourth ministry at the age of 83. However, as in 1886, he did not have a majority and depended on the support of the Irish Nationalist MPs. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Many were convinced that Gladstone would make a second attempt to obtain Home Rule for Ireland. And indeed, a second Home Rule Bill was duly introduced in February 1893 and began its journey through the Commons. Despite organised resistance by unionist MPs every step of the way, the Bill went through the various stages thanks to the solidity of the Liberal/Nationalist alliance.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The bill had its third reading in the Commons on 2nd September and was accepted by a large majority (301 votes for/267 against). It then went on to the Lords where it was immediately rejected a few days later by a majority of more than ten to one (419 against/41 for). The unionists had an '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'overwhelming majority in the House of Lords'
                  -
                    type: text
                    text: ' and did not hesitate to use it as their last line of defence. As we will see in some of the examples in this part of the Module, those producing the anti-Home Rule material were perfectly confident all along that the Lords would “kill the bill.”'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The weight of unionist influence in the Lords was to become the target of increasing Liberal attention. This will explain the decision some twenty years on to force a confrontation on the issue. As we shall see, the resulting Parliament Act, 1911, removing the powers of the Lords to veto bills that had successfully gone through the Commons, was to force the unionists into a radical change of strategy.'
            type: add_set
            enabled: true
          -
            title: 'The unionist campaign in Ireland'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The unionists did not wait for Gladstone to introduce the second Home Rule Bill to renew their resistance. This explains why we have chosen to include material for the period between 1887 and 1893. You will see that this campaign outside Parliament was fought on several fronts, not only in Ireland, but also in England and Scotland and - even if to a much lesser extent - in North America.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "The Irish unionists were led by Colonel Edward James Saunderson, a prominent member of the Orange Order, who came from a landed background in Co. Cavan.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In the southern provinces, the Irish Loyal and Patriotic Union adopted a new constitution (8 April 1891) and renamed itself the Irish Unionist Alliance. It sought to improve unionist cooperation throughout Ireland and coordinate the anti-Home Rule campaign in England and Scotland. It did so with great efficiency, organising rallies in Dublin and London, sending deputations and speakers to England and Scotland, distributing leaflets and organising petitions.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In Ulster, the anti-Home Rule campaign saw numerous meetings and demonstrations organised at a local level in towns and villages throughout the province. It was, however, structured around a number of spectacular events designed to attract maximum publicity. One of these was the'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' Ulster Unionist Convention'
                  -
                    type: text
                    text: ' organised in the Botanic Gardens, Belfast, in June 1892, i.e. the month before the general election that was to see Gladstone’s Liberals return to power. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The Ulster Convention and other well stage-managed events such as the huge demonstration in Belfast at the beginning of April 1893 attended by the leading Conservative politician, James Balfour, were important to galvanise opinion in Ulster. But they were also designed as messages to demonstrate to the rest of the UK the depth of anti-Home Rule feeling in Ulster.'
            type: add_set
            enabled: true
          -
            title: 'Developments in the campaign'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Indeed, it was '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'vital to bring unionist arguments to the electorate in Britain'
                  -
                    type: text
                    text: '. It was there that things were going to be decided. In Ireland, although unionism could hope to win a constituency here or there - as we saw in Unit 1 in Russell’s victory in South Tyrone - it could never hope to make significant inroads into nationalist opinion.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Things were profoundly different elsewhere in the United Kingdom. Unionist organisations such as the southern-based '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Irish Unionist Alliance '
                  -
                    type: text
                    text: 'or the northern based '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Loyal Anti-Repeal Union'
                  -
                    type: text
                    text: ', therefore sent speakers to address anti-Home Rule meetings in England and Scotland. They also targeted by-elections, sending canvassers to support unionist candidates in marginal seats in the hope of getting people to vote Conservative rather than Liberal. Interestingly, the Ulster organisation had responsibility for running this campaign in Scotland. In both cases, they needed to explain the reasons for their opposition to Home Rule and to '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'tackle '
                  -
                    type: text
                    text: “
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: misrepresentations
                  -
                    type: text
                    text: '” by their nationalist and Liberal opponents. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'While this highly visible, constitutional agitation was going on across unionist networks, a small minority of unionists like Fred Crawford, founder of an armed group called Young Ulster (1892), and the future organiser of the UVF gun-running (see the following Unit in this Module), was convinced that “our resistance, to be successful, must eventually come to armed resistance.” To this end, they began to acquire small amounts of arms and train for the possibility that the Home Rule debate might end in armed confrontation.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In order to ensure that “hotheads” like Crawford were kept under control, it was quickly decided to set up a network of '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Unionist Clubs'
                  -
                    type: text
                    text: ' that would allow the unionist leadership to channel energies at grassroots level in more acceptable directions. The resulting network, tightly structured, particularly in Ulster, and linked in to unionist organisations in Scotland and England, did much to ensure the ever-increasing efficiency of the unionist campaign against the Second Home Rule Bill.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Although the Bill went through the Commons, it was, as predicted, rejected by the Lords. Despite this defeat, the Liberals remained in office until July 1895 when they lost the general elections. The Conservatives returned to power and the threat of Home Rule receded once again.'
            type: add_set
            enabled: true
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655658469
related_units:
  - b17a70a9-c081-4c9d-b39c-b8cfb71b3448
  - 0fb0ef24-dac4-4223-912a-be192041f609
  - e61ff15e-62dc-425e-8510-112ba4d25c77
  - 1da42e59-4f61-43dd-8177-86f0bc026b5a
  - 14844afd-88a2-4492-ae39-fb9244ed015c
  - 4c01053c-0a47-4346-ac09-4bd4d0ed79ae
timeline_items:
  -
    year: '1887'
    additional_items:
      -
        year: 'June 1st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Isabella M. S. Tod, “Myth and fact,” in The Liberal Unionist.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1889'
    additional_items:
      -
        year: May
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Scotch-Irish Society of America holds its first Congress in Columbia, Tennessee.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1890'
    additional_items:
      -
        year: 'May 29th - June 1st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Rev. John S. MacIntosh, “The Making of the Ulsterman,” in The Scotch-Irish in America, Proceedings and Addresses of the Second Congress, Pittsburg, Pennsylvania.'
        type: additional_items
        enabled: true
      -
        year: 'December 6th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Irish Parliamentary Party splits over the scandal involving its leader, Parnell, in a divorce case.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1891'
    additional_items:
      -
        year: April
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Loyal and Patriotic Union becomes the Irish Unionist Alliance.'
        type: additional_items
        enabled: true
      -
        year: 'October 6th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Death of Charles Stewart Parnell.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1892'
    additional_items:
      -
        year: 'June 17th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Unionist Convention: a pavilion was specially constructed in the Botanic Gardens in Belfast for the 12,000 delegates, representing tenant farmers, and business, professional and industrial interests from all over Ulster.'
        type: additional_items
        enabled: true
      -
        year: 'June 23rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Unionist Convention in Dublin brings together unionists from the provinces of Munster, Leinster and Connaught; it was attended by a delegation from the Ulster Unionist Convention.'
        type: additional_items
        enabled: true
      -
        year: July
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'UK General Election.'
        type: additional_items
        enabled: true
      -
        year: August
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'At 83, Gladstone becomes PM for the fourth time; he is again dependent on the support of the Irish Parliamentary Party (IPP).'
        type: additional_items
        enabled: true
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Near the end of the year, an armed secret society, Young Ulster, was set up by Fred Crawford, future organiser of the UVF gun-running (see 1914).'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1893'
    additional_items:
      -
        year: January
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Formation of earliest Unionist Clubs later placed under a central body, the Unionist Clubs Council.'
        type: additional_items
        enabled: true
      -
        year: 'February 13th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First reading of the Second Home Rule Bill.'
        type: additional_items
        enabled: true
      -
        year: March
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'An Ulster Defence Union formed; the Liberal Unionist, Thomas Sinclair, appointed chairman of its executive council; designed to coordinate resistance between the various strands of unionism.'
        type: additional_items
        enabled: true
      -
        year: 'April 4th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Mass demonstration of unionists in Belfast an estimated 100,000 men march; attended by the future Conservative Prime Minister, Arthur Balfour.'
        type: additional_items
        enabled: true
      -
        year: 'April 5th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Northern Whig report, “Mr Balfour in Belfast. The Grand Unionist demonstration”.'
        type: additional_items
        enabled: true
      -
        year: 'April 12th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: '“A Belfast Scotchman''s Opinion.” Belfast News-Letter.'
        type: additional_items
        enabled: true
      -
        year: 'May 24th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Former (and future) Conservative PM, Lord Salisbury addresses unionists at the Ulster Hall.'
        type: additional_items
        enabled: true
      -
        year: 'June 10th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Belfast News Letter, “Delegation from the Free Church”.'
        type: additional_items
        enabled: true
      -
        year: 'July 31st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Conradh na Gaeilge/Gaelic League founded to promote the Irish language.'
        type: additional_items
        enabled: true
      -
        year: 'September 2nd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone’s second Home Rule Bill passes its Third Reading in the Commons (301 for; 267 against).'
        type: additional_items
        enabled: true
      -
        year: 'September 9th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'HR bill rejected by the Lords; 419 against, 41 for.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1894'
    additional_items:
      -
        year: 'March 3rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone resigns and is replaced as Liberal PM by Lord Rosebery.'
        type: additional_items
        enabled: true
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: '“To the workingmen and other electors of England and Scotland.” Irish Unionist Alliance Pamphlets (exact publication date unknown).'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1895'
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: "“The new predominant partner” & “Pat, ‘predominant partner’,” Irish Unionist Alliance Pamphlets (exact date unknown).\_"
        type: additional_items
        enabled: true
      -
        year: July
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'General election in UK; conservative ministry formed under Lord Salisbury.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1896'
    content:
      -
        type: heading
        attrs:
          level: 3
        content:
          -
            type: text
            text: 'George Macloskie, “The changes of a century, or, Ulster as it was and as it is,” in The Scotch-Irish in America. Proceedings and Addresses of the Eighth Congress, at Harrisburg, PA., June 4-7, 1896.'
    type: new_item
    enabled: true
unit_number: '02'
ulster-scots_perspective:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'As always, the Unit focuses on the way unionists consciously use Scots or Ulster-Scots references to “sell” their arguments to an audience across the United Kingdom or in North America.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Once again, as we shall see, material was custom-built for different categories of the target audience.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'When someone like the Liberal Unionist intellectual, Isabella Tod, looks at issues such as nationality and allegiance, she will do so with specific reference to the history of the relationship between Ulster and Scotland. It is this connection which she uses in order to defend her fundamental '
      -
        type: text
        marks:
          -
            type: bold
        text: 'conception of Britishness.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'When a Scottish businessman, speaking on behalf of the “Belfast Scotch,” addresses his compatriots in Scotland, he reminds them of “their obligations to the Irish Presbyterians,” and calls on them to put solidarity with their “'
      -
        type: text
        marks:
          -
            type: bold
        text: 'own flesh and blood” '
      -
        type: text
        text: 'before their loyalty to the Liberal Party.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Similarly, as we shall see, other structures such as the '
      -
        type: text
        marks:
          -
            type: bold
        text: 'Presbyterian Church in Ireland, '
      -
        type: text
        text: 'or cultural organisations like the '
      -
        type: text
        marks:
          -
            type: bold
        text: 'Scotch-Irish Society of America'
      -
        type: text
        text: ' begin to take a deeper interest in the Home Rule issue, adding their particular perspectives to the widening debate around Ulster-Scots identity.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'However, references in the Ulster unionist campaign to links with Scotland have to be seen within the broader context of the material that was being published at the time featuring Ulster-Scots or exploring Ulster-Scots history. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'One of the most important of these documents was The Scot in Ulster (1888) by John Harrison. Harrison, himself a Scot, explicitly places the book within the frame of resistance to Home Rule. In it, the Ulster-Scots community is identified as the author''s “kith and kin,” and Ulster as “a branch of the Scottish nation.” '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'At the same time as this explicitly polemical text, people like W.G. Lyttle were publishing novels such as Betsy Gray; or, Hearts of Down (1885-1886), set in the Ulster-Scots community during the United Irishmen’s Rising in 1798, or Daft Eddie (1889), a book about piracy and smuggling in the Ards Peninsula. Both of these novels were initially published in instalments in the North Down Herald before being released in book form. Much of the dialogue in these books, aimed at a wide, popular audience, is in Ulster-Scots.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The period also saw the creation of the Scotch-Irish Society of America which held its first congress in Columbia, Tennessee in May 1889. The ten annual congresses which met between 1889 and 1901, were to provide a framework for the exploration of the key role played by the Ulster Scots in the creation of America.'
---
