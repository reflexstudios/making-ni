---
id: 9c23ffe6-99e2-43e8-9a25-f9d49845ff22
blueprint: modules_index
module_number: '03'
title: 'Third Home Rule Bill'
module_intro_content:
  -
    type: set
    attrs:
      values:
        type: accordian
        replicator:
          -
            title: 'The third Home Rule Bill'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The campaigns against the first two Home Rule bills were quite short-lived. The campaign against the third Home Rule Bill was going to be much more prolonged and intense.'
            type: add_set
            enabled: true
          -
            title: 'A constitutional crisis'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Although the bill was not introduced into the Commons until 11 April 1912, it was clear that such a measure had been on the cards for some time. A major constitutional crisis had erupted between the Lords and the Commons over the Lords’ rejection of the so-called “People’s Budget” in November 1909. This rejection broke a fundamental constitutional convention – that the Lords should not use their power of veto to block financial legislation adopted by the Commons. The budget had included measures to tax land in order to finance such social welfare measures as the Old Age Pensions Act adopted the year before. As expected, the Lords, many of whom were big landowners, vetoed the text. This sparked off a clash with the Liberals who had been determined to break the power of the Lords for some time.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'After two general elections in 1910 on the issue, the Liberals under their leader, H. H. Asquith, emerged as the largest party on both occasions but were unable to form a government without the help of the Irish Nationalists. The price of Irish Nationalist support would be a new Home Rule Bill.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Parliament Act '
                  -
                    type: text
                    text: 'which became law in August 1911, after a bitter struggle, removed the Lords’ veto and replaced it with a “delaying power.” From then on, bills that had gone through the Commons in three consecutive parliamentary sessions were to become law, even without the consent of the Lords. Concretely, that meant the Lords could now only delay a Bill for a period of around two years. It was not hard to see that this would affect the course of any proposed Home Rule legislation. A Home Rule Bill had the time to go through Parliament and become law without the Liberal Government needing to go the country on the issue in a general election.'
            type: add_set
            enabled: true
          -
            title: 'Unionist preparations'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The unionists knew that, once a new Home Rule Bill was introduced, they would only have a two-year slot in which to organise their resistance to Home Rule and possibly force an alternative arrangement for Ulster.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'This explains why, when the Government finally introduced the third Home Rule bill in April 1912, the unionists had already been campaigning against it for some time. Take a look at the time-line and you will see several aspects of their campaign.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The unionists already had a much improved organisational structure centred on Ulster, the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Unionist Council (UUC)'
                  -
                    type: text
                    text: ', formally launched in March 1905, the same year as Sinn Féin was created. This body, the core of the future '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Unionist Party'
                  -
                    type: text
                    text: ', was made up of representatives from local unionist associations, Ulster MPs and the Orange Order. It was designed to coordinate activity between the various strands of unionism inside and outside Parliament.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Two years later, a Joint Committee of the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Unionist Associations of Ireland '
                  -
                    type: text
                    text: "was set up. This structure was designed to allow the UUC (the northern organisation) and the Irish\_ Unionist Alliance (southern organisation) to cooperate in their anti-Home Rule campaign in England and Scotland. They were to obtain some very positive results, especially in the period between 1912 and 1914. An obvious example is the Leith Burghs by-election in Edinburgh in February 1914. After a concerted campaign (more than 70 meetings organised), the unionist candidate took the seat which had been held by a pro-Home Rule Liberal since 1886."
            type: add_set
            enabled: true
          -
            title: 'Demonstrations of support'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'As with earlier attempts at Home Rule, apart from the dogged resistance to the progress of the bill through Parliament, the unionists organised a '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'series of spectacular events '
                  -
                    type: text
                    text: 'designed to capture the imagination and influence public opinion across the United Kingdom.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Thus, in September 1911, a major rally was organised at'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' Craigavon House,'
                  -
                    type: text
                    text: ' the home of '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'William Craig'
                  -
                    type: text
                    text: ', the key organiser of the unionist campaign in Ulster. He was accompanied by'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' Edward Carson K.C'
                  -
                    type: text
                    text: '. who had been elected leader of the Irish unionists in February 1910, replacing Walter Long who had led the Irish Unionists in Westminster since 1906. At that Craigavon demonstration, Carson declared that, because the Prime Minister refused to call an election on the issue of Home Rule, “we must be prepared… the morning Home Rule passes, ourselves to become responsible for the government of the Protestant Province of Ulster.” This was the first official declaration of an intention to set up a Provisional Government in Ulster with authority over “those districts of which [unionists] had control.”'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Although some 50,000 people attended that rally, it was a small affair in comparison with the estimated 200,000 who attended the meeting organised on Easter Tuesday 1912 in the Show Ground at\_"
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: Balmoral
                  -
                    type: text
                    text: ". This meeting was addressed by\_"
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Andrew Bonar Law'
                  -
                    type: text
                    text: ", who had become the leader of the British Conservative Party the previous year.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Andrew Bonar Law was a British conservative politician who was of Ulster-Scots origin. His father, Reverend James Law, originally from Maddybenny, near Coleraine, was a Presbyterian minister in New Brunswick (Canada) where his son, Andrew, was born. A few years after his mother’s death, Andrew accompanied his aunt to her home in Glasgow where he was educated before beginning work as a bank clerk at the age of 16. His father returned to Ireland in 1877, and Andrew made frequent trips to Maddybenny from Glasgow to visit him up until his death in 1882. Initially elected as Conservative MP for a constituency in Glasgow in 1900, Andrew Bonar Law later went on to become leader of the party in 1911.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "These biographical details are important because they explain why, unlike most British politicians, Bonar Law had an intimate, first-hand knowledge of the situation on the ground in unionist Ulster. It was perhaps these strong personal and family connections that explained the nature of the positions Bonar Law was to take during the Home Rule crisis.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Indeed, his personal actions and statements at several key points in the anti-Home Rule campaign reflect his commitment to making sure that Ulster would remain within the Union. Thus, his presence as Conservative leader at Balmoral on Easter Tuesday 1912, accompanied by 70 English and Scottish MPs, represented an important statement of solidarity with the Ulster unionist campaign. A few months later, at the important unionist meeting held at Blenheim Palace, Oxfordshire, on 29 July 1912, he went as far as to state: ‘‘I can imagine no length of resistance to which Ulster can go in which I would not be prepared to support them, and in which, in my belief, they would not be supported by the overwhelming majority of the British people.’’'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Although very impressive, the Craigavon and Balmoral demonstrations were to be overshadowed by what was to be by far the most spectacular of this series of highly publicised and brilliantly organised events: the signing of the\_"
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "Ulster Covenant\_"
                  -
                    type: text
                    text: 'on Ulster Day, 28th September 1912. The document was signed by approximately half of the Protestant population of the Province of Ulster. It committed those (men) who signed it – women signed a separate declaration - to using “'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "all means which may be found necessary\_"
                  -
                    type: text
                    text: 'to defeat the present conspiracy to set up a Home Rule parliament in Ireland.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Opposition to Home Rule was also channelled through the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Women’s Unionist Council'
                  -
                    type: text
                    text: ' (set up in January 1911) and the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Presbyterian Church in Ireland'
                  -
                    type: text
                    text: ", which organised an Anti-Home Rule Convention in February 1912.\_The Convention, organised by the Liberal Unionist, Thomas Sinclair, was a huge event that took place in several venues across Belfast and, according to some accounts, brought together an estimated 40,000 participants. It was important to demonstrate to public opinion - especially non-conformist opinion - in Scotland and England that Ulster’s hostility to Home Rule was not confined to the members of the Church of Ireland and Orangemen as nationalist supporters alleged. Indeed, the very numbers attending the Convention proved that the traditionally Liberal Presbyterians were clearly hostile to Home Rule. Many of their arguments centred on what they saw as the threat to their “civil and religious liberties” under any Catholic-dominated Parliament in Dublin."
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In spite of all this resistance, Asquith’s Bill went through the Commons before being immediately rejected by the Lords on 30th January 1913.'
            type: add_set
            enabled: true
          -
            title: 'Coercing the “loyalist rebel”'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'January 1913 also saw the formation of a paramilitary force, the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Volunteer Force'
                  -
                    type: text
                    text: ', to be made up of 100,000 men who had signed the Ulster Covenant. These volunteers were to receive military training to resist the “imposition” of Home Rule. This strategy of open defiance was reinforced by the announcement in September of the creation of a '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Provisional Government '
                  -
                    type: text
                    text: 'for Ulster. This body, under Carson’s leadership, would take over the running of the province the day Home Rule went through.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "This period of ever-increasing tension demonstrated the remarkable level of unity that existed within the Protestant and unionist camp. A meeting of pro-Home Rule Protestants that was organised in Ballymoney Town Hall on 24th\_October 1913 by the outspoken Presbyterian minister, Reverend J.B. Armour, is therefore all the more interesting because it is one of the rare examples of opposition to what had become a standard position among northern Protestants. The meeting, attended by some 400 people, was addressed by such well-known figures as Roger Casement and Jack White, son of Sir George White of Broughshane, the “hero of Ladysmith,” whom we came across in Module 2. Speakers expressed their hostility to Carson’s strategy and tried to convince their audience that Home Rule did not represent any threat to Protestants. "
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "However, everyone knew that such views only commanded marginal support within a Protestant community which remained overwhelmingly opposed to Home Rule.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Everything seemed to be on course for a head-on clash between the Ulster unionists and the British government.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'It was clear however that the Government still did not take the unionist threat of armed resistance to the British state seriously. They were determined to force the unionists to submit, if necessary by using the armed forces to cow them.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The situation changed dramatically in March 1914 when a large contingent of officers stationed at the British army base of the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Curragh '
                  -
                    type: text
                    text: '(Kildare) stated they would prefer to resign their commissions rather than obey Government orders to “coerce” Ulster. Fault lines were opening up within the British establishment that went beyond normal party divisions. It was becoming increasingly clear that, now that things were coming to a head, Ulster’s “loyalist rebels” could count on the support of influential networks within the Conservative Party, the peerage, the armed forces and across the Empire.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Tensions increased when the UVF managed to pull off a spectacular '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'gun-running '
                  -
                    type: text
                    text: 'operation in April 1914, when around 25,000 rifles and several million rounds of ammunition were smuggled into Larne, Donaghadee and Bangor and distributed to the volunteers throughout the province overnight.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In the months leading up to the end of the parliamentary process, the Government began making signs that it was prepared to compromise by allowing parts of Ulster to remain outside the terms of the Bill, at least for a temporary period. Nevertheless, some vitally important questions remained. How much territory? For how long? Would the nationalists agree? Equally importantly – would the unionists agree?'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'However, it was above all the outbreak of the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'First World War '
                  -
                    type: text
                    text: 'in August 1914 that was to change everything.'
            type: add_set
            enabled: true
          -
            title: 'The war changes everything - utterly'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'When the war broke out at the beginning of August 1914, Carson, despite worries that the Government might take advantage of the situation to impose Home Rule as it stood, said that Ulster unionism’s first duty was the defence of the kingdom in time of need and offered the services of the UVF to the British army. A proportion of the UVF was duly absorbed into the army as the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '36th (Ulster) Division '
                  -
                    type: text
                    text: 'which was to see action at the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Battle of the Somme '
                  -
                    type: text
                    text: '(July 1916).'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'John Redmond, leader of the Home Rule Party, speaking in Woodenbridge in September 1914, advised the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Irish Volunteers'
                  -
                    type: text
                    text: ', a nationalist paramilitary force set up in imitation of the UVF, to follow a similar path and to volunteer for the British army. This caused a split in the nationalist movement. Whereas many Irish Volunteers did in fact join one of the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'two Irish Divisions '
                  -
                    type: text
                    text: "in the British Army - the 10th and the 16th – the more radical members were not prepared to fight for Britain.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The third Home Rule Bill was adopted (18 September 1914) just after the start of the war, but its operation was suspended until the end of the war. As it turned out, the text was never put into effect.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: '1916 saw two major events that were to define Irish history to the present day – the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Easter Rising '
                  -
                    type: text
                    text: 'and the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Battle of the Somme'
                  -
                    type: text
                    text: '. Whereas the nationalist and republican traditions see the Rising as the defining event of modern Irish history, unionists and loyalists focus on the “sacrifice” of the Somme as the defining statement of their commitment to a British identity. The heavy losses suffered by the Ulster Division on that occasion were to be an important factor in the subsequent decision to “repay” the unionist community in Ulster by partitioning Ireland and creating Northern Ireland.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In the elections that followed the end of the war, opinion in Ireland was more deeply divided than ever. The intense emotion generated by the Rising had led to a profound shift in opinion within the nationalist camp. The moderate '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Irish Parliamentary Party '
                  -
                    type: text
                    text: 'which had fought for Home Rule within the British system for fifty years was decimated at the polls. The winners were '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Sinn Féin'
                  -
                    type: text
                    text: ', a republican party that sought to break the British connection altogether.'
            type: add_set
            enabled: true
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655661401
related_units:
  - e11c1a0d-6179-4dfc-bd6e-195adfb7c32a
  - cb400513-7769-4b80-b94e-570f58b56f40
  - cc5acc9d-2305-4260-a3eb-d5e7872378a9
  - 0d0e5029-5ee9-4754-a4ee-e43e317f5050
  - ee26172c-a5d4-45bf-bf74-ec62f4bfec5e
  - e022fb3a-92f4-488d-b4b0-2ce96571045b
timeline_items:
  -
    year: '1900'
    additional_items:
      -
        year: 'February 6th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'John Redmond elected leader of the Irish Parliamentary Party (IPP).'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1903'
    additional_items:
      -
        year: 'August 14th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Land Act helps Irish tenants acquire land on generous terms.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1904'
    additional_items:
      -
        year: December
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Abbey Theatre opens in Dublin.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1905'
    additional_items:
      -
        year: 'March 3rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Ulster Unionist Council (UUC) holds its first official meeting in the Ulster Hall; the UUC will become the nucleus of the Ulster Unionist Party.'
        type: additional_items
        enabled: true
      -
        year: 'November 28th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: '“Sinn Féin” policy launched.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1906'
    additional_items:
      -
        year: January
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Liberals win general elections; they do not need IPP support to form a government.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1907'
    additional_items:
      -
        year: 'December 19th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Joint Committee of the Unionist Associations of Ireland set up to facilitate cooperation between members of the Irish Unionist Alliance (southern organisation) and the UUC (the northern organisation) in their campaign, especially in Scotland and England.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1908'
    additional_items:
      -
        year: 'August 1st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Old Age Pensions Act provides basic pension for over 70s.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1909'
    additional_items:
      -
        year: 'November 30th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The House of Lords rejects the “People’s Budget”; this sparks the crisis that was to lead to the Parliament Act.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1910'
    additional_items:
      -
        year: January
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'General election in UK; Asquith’s Liberals are the largest party; but the Irish nationalists hold balance of power.'
        type: additional_items
        enabled: true
      -
        year: 'February 21st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Sir Edward Carson chosen as leader of the Irish unionists at Westminster.'
        type: additional_items
        enabled: true
      -
        year: December
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'General election in UK; once again, Asquith’s Liberals are the largest party; once again, the Irish nationalists hold the balance of power.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1911'
    additional_items:
      -
        year: 'January 23rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Women’s Unionist Council set up; 40,000 members enrolled in the first year.'
        type: additional_items
        enabled: true
      -
        year: January
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Unionist Clubs movement revived; rapid expansion over the coming months.'
        type: additional_items
        enabled: true
      -
        year: 'August 18th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Parliament Act abolishes Lords’ veto on bills passed in the Commons.'
        type: additional_items
        enabled: true
      -
        year: 'September 23rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: '50,000 unionists march to a rally at Craigavon House, Graig’s private residence; contingents from the Orange Order and Unionists Clubs.'
        type: additional_items
        enabled: true
      -
        year: 'November 13th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Andrew Bonar Law, of Ulster descent, succeeds Balfour as leader of the Conservative Party.'
        type: additional_items
        enabled: true
      -
        year: 'December 16th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'National Insurance Act gives workers protection in case of accident or illness and provides for unemployment and sickness benefit.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1912'
    content:
      -
        type: heading
        attrs:
          level: 3
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Anti-Home Rule volunteers begin military training; small amounts of arms and ammunition continue to be smuggled into Ulster.'
        type: additional_items
        enabled: true
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Thomas Sinclair, “The Position of Ulster,” in S. Rosenberg (ed.), Against Home Rule, London & New York, published in 1912.'
        type: additional_items
        enabled: true
      -
        year: 'February 1st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Presbyterian Anti-Home Rule Convention.'
        type: additional_items
        enabled: true
      -
        year: 'February 8th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Winston Churchill addresses Home Rule meeting in Celtic Park, Belfast. Unionists protest at his visit.'
        type: additional_items
        enabled: true
      -
        year: 'April 9th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Huge meeting at the Agricultural Society’s show grounds in Balmoral; 200,000 unionists present; 70 English and Scottish MPs attend; the new Conservative leader, Andrew Bonar Law, addresses the meeting.'
        type: additional_items
        enabled: true
      -
        year: 'April 11th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Liberal PM, Asquith, introduces Third Home Rule Bill in the Commons.'
        type: additional_items
        enabled: true
      -
        year: May
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Liberal Unionists merge officially with the Conservatives.'
        type: additional_items
        enabled: true
      -
        year: 'June 11th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Agar-Robartes, MP, moves an amendment to Third Home Rule Bill suggesting the “exclusion” of the four counties with Protestant and unionist majorities: Antrim, Armagh, Down and Londonderry.'
        type: additional_items
        enabled: true
      -
        year: 'June 13th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Carson delivers speech on Agar-Robartes amendment in the House of Commons. (Amendment defeated, June 18th 320 against / 251 for.)'
        type: additional_items
        enabled: true
      -
        year: 'Jun - Sept.'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Sectarian clashes in Belfast.'
        type: additional_items
        enabled: true
      -
        year: 'September 10th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Launch of the Young Citizen Volunteers in the Ulster Hall, Belfast.'
        type: additional_items
        enabled: true
      -
        year: 'September 18th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Enniskillen: first of several meetings across Ulster to prepare for Ulster Day slogan: “We will not have Home Rule!”.'
        type: additional_items
        enabled: true
      -
        year: 'September 28th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Day; Solemn League and Covenant signed across the province of Ulster; altogether, 471,414 people signed the Covenant.'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: '“The Blue Banner,” written by William Forbes Marshall appears in The Northern Whig.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1913'
    additional_items:
      -
        year: 'January 16th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Third Reading of Third Home Rule Bill in Commons (367 for/257 against).'
        type: additional_items
        enabled: true
      -
        year: 'January 30th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Home Rule Bill defeated in Lords (326 against/69 for).'
          -
            type: paragraph
        type: additional_items
        enabled: true
      -
        year: 'January 31st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Unionist Council decides the formation of the Ulster Volunteer Force (UVF); the aim was to recruit, train and arm 100,000 men who had signed the Covenant.'
        type: additional_items
        enabled: true
      -
        year: 'March 27th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'British League for the Support of Ulster and the Union formed in England; membership included MPs and peers.'
        type: additional_items
        enabled: true
      -
        year: June
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Seizures of arms destined for UVF in Belfast and London.'
        type: additional_items
        enabled: true
      -
        year: 'September 24th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Unionist Council approves the setting up of a Provisional Government in Ulster if Home Rule became law; Carson was to be Chairman.'
        type: additional_items
        enabled: true
      -
        year: 'October 24th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Meeting of pro-Home Rule Protestants in Ballymoney.'
        type: additional_items
        enabled: true
      -
        year: 'November 19th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Citizen Army (ICA) formed out of the trade union movement in Dublin.'
          -
            type: paragraph
        type: additional_items
        enabled: true
      -
        year: 'November 25th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Volunteers (a nationalist organisation) launched at a meeting in Dublin; it soon had 180,000 men enrolled.'
        type: additional_items
        enabled: true
      -
        year: 'December 4th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ban on the importation of weapons into Ireland introduced.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1914'
    additional_items:
      -
        year: February
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Victory of unionist candidate at Leith Burghs (Edinburgh) by-election.'
        type: additional_items
        enabled: true
      -
        year: 'March 4th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'British Covenant launched in the press; it stated that the signatories were “justified in taking or supporting any action […] to prevent [Home Rule] being put into operation”; two million people had signed it by the end of July.'
        type: additional_items
        enabled: true
      -
        year: 'March 20th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: '“Curragh incident”; 57 army officers, led by Brigadier General Gough, stationed at the Curragh threaten to resign if ordered north to force unionists to accept Home Rule.'
          -
            type: heading
            attrs:
              level: 3
        type: additional_items
        enabled: true
      -
        year: 'March 27th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Bab M’Keen, “Amang oorsel’s”, Ballymena Observer.'
        type: additional_items
        enabled: true
      -
        year: 'April 24th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Colonel Frederick Crawford organises the UVF gun-running; 25,000 rifles and several million rounds of ammunition landed in Larne, Donaghadee and Bangor.'
        type: additional_items
        enabled: true
      -
        year: 'May 2nd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: '“Amazing night in Larne. Wholesale gun-running. Thousands of rifles landed,” Ballymena Weekly Telegraph.'
        type: additional_items
        enabled: true
      -
        year: 'May 25th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Home Rule Bill passes Commons for the third time.'
        type: additional_items
        enabled: true
      -
        year: 'June 23rd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Government of Ireland (Amendment) Bill presented to Lords provides for “temporary exclusion” (six years) of those Ulster counties that want to opt out of Home Rule.'
        type: additional_items
        enabled: true
      -
        year: 'July 8th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Government of Ireland (Amendment) Bill amended in Lords to provide for “permanent exclusion” of all of Ulster: unacceptable to the Commons. '
        type: additional_items
        enabled: true
      -
        year: 'July 10th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First official meeting of the Ulster Provisional Government.'
        type: additional_items
        enabled: true
      -
        year: 'July 21st-24th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'The Buckingham Palace Conference fails to produce a compromise between nationalists and unionists on Ulster.'
        type: additional_items
        enabled: true
      -
        year: 'July 26th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Howth gun-running: Irish Volunteers land 1500 guns and ammunition.'
        type: additional_items
        enabled: true
      -
        year: 'August 4th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Britain declares war on Germany; First World War begins.'
        type: additional_items
        enabled: true
      -
        year: August/September
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Recruitment to the 10th and 16th (Irish) Divisions.'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Recruitment to the 36th (Ulster) Division.'
          -
            type: paragraph
        type: additional_items
        enabled: true
      -
        year: 'September 18th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: "Government of Ireland Act, 1914 passes; its operation is immediately suspended.\_"
          -
            type: paragraph
        type: additional_items
        enabled: true
      -
        year: 'September 20th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Redmond delivers speech at Woodenbridge (Wicklow) inviting the Irish Volunteers to join the British war effort.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1915'
    additional_items:
      -
        year: 'May 25th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Carson becomes Attorney General for England in Asquith’s coalition war cabinet.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1916'
    additional_items:
      -
        year: 'April 24th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: "The “Easter Rising” in Dublin involving a section of the Irish Volunteers and the ICA; Proclamation of the Republic.\_"
        type: additional_items
        enabled: true
      -
        year: 'June 12th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'UUC accepts government proposal for Home Rule with exclusion of the 6 north-eastern counties; the plan, however, was not implemented.'
        type: additional_items
        enabled: true
      -
        year: 'July 1st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Battle of the Somme.'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First day of the Somme Offensive; heavy losses to the 36th (Ulster) Division during their attack on German trenches at Thiepval, northern France.'
        type: additional_items
        enabled: true
      -
        year: 'September 21st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Article on Private Quigg V.C.: “Rescued seven wounded comrades. Thrilling Story of Bushmills Soldier''s Heroism, Rescued seven wounded comrades. Thrilling Story of Bushmills Soldier''s Heroism,” in Ballymoney Free Press and Northern Counties Advertiser.'
        type: additional_items
        enabled: true
      -
        year: 'December 7th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Lloyd George replaces Asquith as PM.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1917'
    additional_items:
      -
        year: 'January 25th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Article on Private Quigg V.C. in Ballymoney Free Press.'
        type: additional_items
        enabled: true
      -
        year: 'April 6th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'USA enters the war alongside UK and France against Germany.'
        type: additional_items
        enabled: true
      -
        year: 'July 25th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Convention meets in Dublin; sits until April 1918; no compromise reached.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1918'
    additional_items:
      -
        year: 'February 6th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Representation of the People Act gives the vote to all men over 21 and most women over 30.'
        type: additional_items
        enabled: true
      -
        year: 'March 6th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Redmond dies and is succeeded as leader of the nationalists by John Dillon.'
        type: additional_items
        enabled: true
      -
        year: 'November 11th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First World War ends.'
        type: additional_items
        enabled: true
      -
        year: November
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Decision to construct a monument in northern France to commemorate “the gallant deeds of the Ulster Division”.'
        type: additional_items
        enabled: true
      -
        year: December
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'General election in UK; coalition government formed; in Ireland, Sinn Féin becomes dominant party, effectively eliminating the nationalists.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1919'
    additional_items:
      -
        year: 'January 18th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Paris Peace Conference inaugural meeting.'
        type: additional_items
        enabled: true
      -
        year: 'January 21st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Two policemen are shot in Co. Tipperary; this is seen as the start of the War of Independence between the IRA and the British forces.'
        type: additional_items
        enabled: true
      -
        year: February
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Letters from both Edward Carson and Rev. Park published in The report of the 30th Annual Meeting and Banquet of the Pennsylvania Scotch-Irish Society.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
unit_number: '03'
ulster-scots_perspective:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'As before, the third Unit focuses on material that illustrates the way in which Scots and Ulster-Scots connections were incorporated into the arguments in support of the anti-Home Rule campaign.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Some of the documents try to project a particular image of “the Ulster Scot” to an audience in Scotland and England. This is the case, for example, in a key speech by the unionist leader, Edward Carson, to the House of Commons in 1912, or again, in the essay by Thomas Sinclair, a leading unionist intellectual, in a collection entitled Against Home Rule, published in London and New York in 1912.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Several of the documents are written by people who explicitly state that they see themselves as belonging to the Ulster-Scots tradition. This is the case, for example, for Fred Crawford, who was the mastermind behind the major UVF smuggling operation in 1914.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Some of the documents are written in Ulster-Scots, for example, the texts by Bab M’Keen that appeared in the “Amang oorsel’s” column in the Ballymena Observer. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Others, such as the poem, “The blue banner,” written especially for Ulster Day, recall the cultural and historical connections with the Scottish Covenanters of the 17th century.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Other documents illustrate how the Ulster-Scots connection was presented in North America by people belonging to the Pennsylvania Scotch-Irish Society. These perspectives are important as they illustrate the extent of the networks that existed between unionist Ulster and North America.'
  -
    type: paragraph
    content:
      -
        type: text
        text: "Once again, this has to be seen in relation to material on the Ulster Scot that was being produced at the time and that did not have any explicit political purpose. The period between the second and third Home Rule Bills is indeed particularly rich in terms of writing in Ulster-Scots and about the Ulster-Scots community.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "We think, for example, of the publication of John Stevenson’s, Pat M’Carty, Farmer of Antrim, in 1905. This collection contains a series of poems, many of which use Ulster-Scots, supposedly written by M’Carty – which is actually Stevenson’s pen-name - with connecting prose passages in English. One of the things to emerge strongly in the book is Pat’s relationship with Scotland, which he can see from the window of his cottage. This notion of proximity, and the resulting intensity of exchanges between Scotland and this part of Ulster, is a recurring theme in Ulster-Scots writing.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Apart from poetry, the period sees the publication of a considerable body of fictional material in Ulster-Scots. We think, for example of the work of people like Archibald McIloy, who died returning from Canada in the infamous German submarine attack on the Lusitania in May 1915. McIlroy wrote a number of collections of short stories in Ulster-Scots, exploring the life of the Ulster-Scots community, especially in the villages and small towns of the province. These stories were very popular, not only at home, but also with the large expatriate community in North America. Thus, a text like The Auld Meetin’-hoose Green was published not only in Belfast (1898), but also in Toronto (1899).'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'And then, there was, of course, an entire body of non-fictional material that was designed to piece together the history of the Ulster Scot, from the time of the initial plantation through the migration to America and on to the experience of the frontier and involvement in the construction of the United States. These histories were being produced simultaneously on both sides of the Atlantic, by individual historians such as Rev. James B. Woodburn who wrote the classic, The Ulster Scot: His History and Religion, published in 1914, or James Henry Ford, The Scotch-Irish in America, published the following year by Princeton University Press.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Besides this, at the level of the local community, there were the articles in Ulster-Scots that continued to appear in the newspapers in areas with a strong Ulster-Scots presence. This was the case for example in Mid Antrim with the Ballymena Observer, or in North Antrim with a paper like the Northern Constitution, published in Coleraine.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'All of this activity ensured that the figure of the Ulster Scot was taking on a series of features that were increasingly clear in people’s minds. This included people outside the Ulster-Scots community who now had a much sharper idea of who this Ulster Scot was and what he stood for (see: "Key Questions" explored at the  beginning of Module 2). This material provided the necessary framework that ensured that Ulster-Scots references could be used in an increasingly broad context when it came to putting forward political arguments in defence of the Union.'
---
