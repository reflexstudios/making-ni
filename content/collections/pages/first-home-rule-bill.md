---
id: 6492b8fc-ba6c-4bb4-a8d1-dcd14b6d92c3
blueprint: modules_index
title: 'First Home Rule Bill'
parent: 367fd442-4bbc-4b43-baab-c9908bf4eb31
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655475785
module_number: '03'
related_units:
  - 6d7a8ca9-2d03-49aa-8137-bb65796e8ad4
  - 81dd9d9f-0636-4304-9d59-c516f6624738
  - a4d0b21a-7c5e-46e0-890c-2f40f74d7b13
timeline_items:
  -
    year: '1869'
    additional_items:
      -
        year: 'July 26th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Church of Ireland disestablished and disendowed under the terms of the Irish Church Act.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1870'
    additional_items:
      -
        year: May
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Isaac Butt launches the Home Government movement in Dublin.'
        type: additional_items
        enabled: true
      -
        year: 'September 1st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'First meeting of Butt’s Home Government Association (reformed as the Home Rule League in 1873).'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1874'
    additional_items:
      -
        year: February
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'UK General Election; Conservative victory; Home Rule candidates win 60 of the 103 Irish seats.'
        type: additional_items
        enabled: true
      -
        year: March
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Home Rule MPs form a separate group in Westminster under Butt.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1877'
    additional_items:
      -
        year: August
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Charles Stewart Parnell replaces Butt as president of the Home Rule Confederation of Great Britain.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1879'
    content:
      -
        type: heading
        attrs:
          level: 3
    additional_items:
      -
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Growing land agitation in Ireland; the Land War begins (continues until 1882).'
        type: additional_items
        enabled: true
      -
        year: October
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Creation of the Irish National Land League; Parnell elected president.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1881'
    type: new_item
    enabled: true
    additional_items:
      -
        year: August
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Land Law (Ireland) Act gave increased protection to tenants.'
        type: additional_items
        enabled: true
  -
    year: '1884'
    additional_items:
      -
        year: November
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gaelic Athletic Association (GAA) formed.'
        type: additional_items
        enabled: true
      -
        year: December
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Representation of the People Act extends franchise throughout the UK, Irish electorate tripled.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1885'
    additional_items:
      -
        year: May
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Loyal and Patriotic Union (ILPU) founded to defend unionist interests in the three southern provinces (becomes the Irish Unionist Alliance in 1891).'
        type: additional_items
        enabled: true
      -
        year: 'Nov - Dec'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'UK general election results: the 86 Irish Parliamentary Party (pro-Home Rule) MPs hold the balance of power between the Conservatives and the Liberals.'
        type: additional_items
        enabled: true
      -
        year: 'December 17th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone’s “conversion” to Home Rule announced in the British press.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    year: '1886'
    additional_items:
      -
        year: 'January 8th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Loyalist Anti-Repeal Committee formed; renamed Ulster Loyalist Anti-Repeal Union in April.'
        type: additional_items
        enabled: true
      -
        year: 'February 1st'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone becomes PM for the third time.'
        type: additional_items
        enabled: true
      -
        year: 'February 20th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'James Henderson’s speech reported in News Letter in an article entitled “The Loyalist Campaign, Great Meeting in Newry.”'
        type: additional_items
        enabled: true
      -
        year: 'February 22nd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Unionist rally in Ulster Hall; addressed by Lord Randolph Churchill.'
        type: additional_items
        enabled: true
      -
        year: March
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Loyalist Anti-Repeal Committee launches a series of meetings across Ulster, many of which are held in Orange Halls.'
        type: additional_items
        enabled: true
      -
        year: 'April 8th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Gladstone’s, Government of Ireland Bill introduced in Commons.'
        type: additional_items
        enabled: true
      -
        year: 'April 13th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Liberal Unionists join Conservatives in anti-Home Rule meeting in Belfast in Ulster Hall.'
        type: additional_items
        enabled: true
      -
        year: 'May 22nd'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Irish Protestant Home Rule Association founded in Belfast; represents minority of Protestants in favour of Home Rule.'
        type: additional_items
        enabled: true
      -
        year: 'May 18th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'T. W. Russell delivers anti-Home Rule speech in Grangemouth, Stirlingshire. Later published by ILPU as “The Case for Irish Loyalists.”'
        type: additional_items
        enabled: true
      -
        year: 'June 4th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Liberal Unionist Committee formed.'
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Severe rioting begins in Belfast; rioting flares up again in July and September.'
        type: additional_items
        enabled: true
      -
        year: 'June 8th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Home Rule Bill defeated in House of Commons; 93 Liberals vote against.'
        type: additional_items
        enabled: true
      -
        year: 'June 19th'
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'Ulster Scot Jnr.’s “Letter to his friends at home and abroad” published in the Belfast Weekly News.'
        type: additional_items
        enabled: true
      -
        year: July
        content:
          -
            type: heading
            attrs:
              level: 3
            content:
              -
                type: text
                text: 'UK General Election: Conservatives become largest party in the Commons.'
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
  -
    additional_items:
      -
        type: additional_items
        enabled: true
    type: new_item
    enabled: true
module_intro_content:
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'Unit 1: The First Home Rule Bill '
  -
    type: set
    attrs:
      values:
        type: accordian
        replicator:
          -
            title: 'The idea of Home Rule'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'The idea of “Home Government” for Ireland first emerged in 1870, the year after the Liberal Prime Minister, William Ewart Gladstone, had disestablished the Church of Ireland (Irish Church Act, 1869).'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "The idea came from Isaac Butt who, earlier in his career, had been a Conservative MP and a strong defender of the Protestant ascendancy. He suggested that an Irish Parliament should be restored in Dublin, but with limited powers. This “Home Rule” Parliament would have “control over Irish resources and revenues” and would have the power to legislate for Ireland’s “internal affairs.” However, it would have no say over matters such as the armed forces, important taxation, foreign affairs, the colonies, etc. Those things would be left to the Imperial Parliament in London.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Unlike Daniel O’Connell, who had dominated Irish politics in the pre-Famine period, Butt was not calling for a repeal of the Act of Union (1800). This act had abolished the Irish Parliament, transferred Irish representation to Westminster and created the United Kingdom. Butt insisted that his system, which he described as a “federal arrangement,” would not threaten the existence of the Union or the “unity of the Empire.” On the contrary, he argued, because “self-government” would operate “in accordance with the wants and wishes of the people,” it would remove the causes of “discontent” in Ireland. By improving what he saw as a partnership between Ireland and England, both the Union and the Empire would be strengthened.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Many, especially in Ulster, did not believe him. They thought that Butt’s scheme was just “Repeal” by another name, and that it would indeed lead to the break-up of both the United Kingdom and the Empire.\_"
            type: add_set
            enabled: true
          -
            title: 'The makings of a future nationalist party'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'In 1870, Butt created the Home Government Association (renamed the Home Rule League'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' '
                  -
                    type: text
                    text: 'in 1873). This was not a political party as such, but more of a pressure group.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The idea of Home Rule quickly caught on and, as early as 1870, candidates at by-elections in Ireland began standing as Home Rulers. In the general election in 1874, Home Rule candidates won a large majority of the Irish seats in Westminster.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'This represented a radical shift in the political landscape. Up until then, people had voted for Liberal, Conservative or independent candidates. Now a majority wanted Home Rule.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The newly-elected Home Rule MPs formed themselves into a separate group in Westminster. Charles Stewart Parnell took over from Butt as the effective leader of the movement in 1877. Parnell enforced greater discipline among the Home Rule MPs and adopted a more aggressive strategy on Irish issues in Westminster. The Home Rule MPs became known as the Irish Parliamentary Party (IPP).'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Over the next ten years, the question of Home Rule became tied in to the land question.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'A Land League was formed (1879) to help tenants defend their interests against the landlords, and Parnell was elected as its President. Tensions over things like high rents, unstable tenancies, arrears and evictions developed into the so-called Land War (1879-1882) which saw high levels of violence in rural districts. The Liberal Prime Minister, Gladstone, tried to defuse the situation and passed an important Land Act for Ireland (1881) which gave increased protection to tenants in the form of the “three F’s” (Fair rents, Free sale and Fixed tenancies). Nevertheless, the violence in the countryside continued.'
            type: add_set
            enabled: true
          -
            title: 'Home Rule – the dominant issue in British politics'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Following the general elections at the end of that year, the issue of Home Rule returned to the forefront of politics in 1885, not only in Ireland but right across the United Kingdom. Gladstone’s Liberal Party won the largest number of seats, but was far from commanding a majority in Parliament. Indeed, he needed the support of the 86 Home Rule MPs if he was to be in a position to form a new government. He therefore promised the Irish leader, Parnell, that if he would give him his support, the Liberal Party would introduce a Home Rule Bill for Ireland. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'This was a veritable bombshell in British politics. The first victim was Gladstone’s own Liberal Party which tore itself apart on this issue.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'On the one hand, there was a majority of Liberals who remained loyal to Gladstone. They were convinced that his unexpected “conversion” to Home Rule was not just about hanging on to power. They saw it rather as a courageous attempt to find a solution for what they saw as legitimate Irish grievances. They thought that if Ireland got a limited measure of self-government, the Irish would be satisfied and sign up to the status quo within the Union.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'On the other hand, a strong minority within the party thought that Home Rule would necessarily weaken the Union and would be used by the more radical elements within Irish nationalism to push for total independence. This group considered that loyalty to the Union of Great Britain and Ireland should take precedence over loyalty to the Liberal Party. They were to call themselves Liberal Unionists. In order to try to prevent Home Rule, they formed a wary alliance with the Conservatives, the main rivals of the Liberal Party.'
            type: add_set
            enabled: true
          -
            title: 'The situation in Ireland'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Overall, the Liberal Party lost the support of just under a third of its MPs in Parliament over this issue. But the situation of the party in Ireland was even worse. The Liberals were popular in Ulster, notably in Presbyterian areas. However, the issue of Home Rule raised fundamental questions of identity that had to be addressed.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "The results of the 1885 elections, the first under the new, much wider franchise, had shown that in Ulster issues of political preference were once again being tied in to religious affiliation and pushing people into more polarised positions as either pro- or anti-Home Rule. Although a tiny minority of Protestants was prepared to support Home Rule (an Irish Protestant Home Rule Association was created in May 1886), the overwhelming majority was fiercely opposed to it. Those hostile to Home Rule – the unionists - argued that, because of its inevitable Catholic majority, a Dublin parliament would come under the influence of the very conservative Catholic hierarchy which would try to influence legislation.\_When it became clear that the Liberals were going to introduce Home Rule, the Orange Order saw a considerable influx of new recruits from all classes of the (Protestant) population. The Order’s system of local Lodges, linked in to a Grand Lodge in each County, was to provide the anti-Home Rule movement with a highly efficient ready-made organisational structure.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'However, there was also an economic dimension to these identity issues. Those opposed to Home Rule were afraid that Ulster’s industries would be threatened because a future Irish Parliament would be dominated by the interests of an overwhelmingly agricultural electorate. They were particularly worried that Home Rule might turn out to be only the first stage towards total independence. This raised the issue of Ulster’s industries being cut off from both their sources of raw materials and their markets in the Empire.'
            type: add_set
            enabled: true
          -
            title: 'The First Home Rule Bill – a short, sharp campaign'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'As soon as Gladstone’s “treachery” had become clear, the various strands of unionism were quick to organise their opposition to the measure.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The structures reflected the varying strengths of unionism across the island of Ireland.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Irish Loyal and Patriotic Union'
                  -
                    type: text
                    text: ' had been constituted in May 1885 in an effort to consolidate support for the Union, especially in the southern provinces of Munster, Leinster and Connacht. It reflected the sources of unionist support that existed in the landed aristocracy and the business and commercial interests in the large cities of Dublin and Cork. It benefitted from extensive networks in Parliament and in other key institutions such as the army.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'An alternative structure, the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Loyalist Anti-Repeal Committee'
                  -
                    type: text
                    text: ', was set up in January 1886 (renamed '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Loyalist Anti-Repeal Union '
                  -
                    type: text
                    text: 'a few months later'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ")\_"
                  -
                    type: text
                    text: 'to provide a structure focused on Ulster. Although it cooperated actively with the broader Irish organisation, it was clear from the outset that it was determined to push Ulster’s interests to the fore.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Similarly, an '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'Ulster Liberal Unionist Committee '
                  -
                    type: text
                    text: 'was created in Belfast at the beginning of June 1886. This was a sign that former Liberals wanted to maintain a voice in Ulster that was distinct from the predominantly Conservative position of the Ulster Loyalist Anti-Repeal Union'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'These structures were very active in Ireland, organising such highly publicised events as the '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'rally at the Ulster Hall'
                  -
                    type: text
                    text: ' on 22 February 1886. The fact that this rally was addressed by one of the most important British Conservative figures, Randolph Churchill, showed that the battle lines were being drawn on Home Rule, not only in Ireland, but across the United Kingdom.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Although it was vital to campaign in Ireland, especially in Ulster where Conservative and Liberal Unionist candidates had a better chance of winning marginal seats, it was also important to carry the campaign into Scotland and England. Liberal and Home Rule arguments had to be challenged throughout the United Kingdom if there was to be a chance of changing the majority in Parliament and thereby blocking Home Rule.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'This was all the more important since the Representation of the People Act, 1884, had transformed the franchise across the UK. In Ireland, for example, it multiplied the numbers of people who could vote by three overnight. Even though women still did not have the right to vote, the Act extended the franchise to new categories of voters such as farm labourers and factory workers. Parties had to develop arguments that would convince this new electorate.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'For this reason, in the months leading up to the decisive vote in the House of Commons and the subsequent elections in July, speakers were sent to Britain to put forward the Irish – and Ulster – unionist case. '
            type: add_set
            enabled: true
          -
            title: 'Home Rule defeated… at least for now'
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Gladstone introduced his first Home Rule on April 8th 1886. Two months later, on 8th June, it was defeated in the House of Commons by 341 votes to 311. The 93 Liberal Unionists who voted against it ensured its defeat. As a result of the vote, the Gladstone ministry collapsed. At the elections in July the Conservatives were returned to power.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Home Rule was, however, far from dead.'
            type: add_set
            enabled: true
unit_number: '01'
ulster-scots_perspective:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The main focus of this Unit is to look in detail at examples of the material that was being used in this campaign to further the anti-Home Rule cause using Scots and Ulster-Scots references.'
  -
    type: paragraph
    content:
      -
        type: text
        text: ' We will see that, right from the outset, there was a decision to “stir up the feeling of Scotland in favour of this movement.” This involved using the various frames that we looked at in Module 2 in order to explain why many in the Ulster-Scots, and, by extension, the broader unionist community, were opposed to the proposed legislation.'
  -
    type: paragraph
    content:
      -
        type: text
        text: ' Arguments were therefore put forward underlining the close historical ties, the family and commercial links, or the Presbyterian connections that existed between Ulster and Scotland.'
  -
    type: paragraph
    content:
      -
        type: text
        text: ' The following Unit provides concrete examples of how these links to Scotland were exploited in the unionist campaign not only at home in Ulster but also in Scotland.'
  -
    type: paragraph
    content:
      -
        type: text
        text: ' This material cannot be seen as having been conjured up out of nowhere. On the contrary, it corresponded to a lived reality for large sections of the population in certain parts of Ulster. The political use to which Ulster-Scots references were being put merely reflected this underlying social reality.'
  -
    type: paragraph
    content:
      -
        type: text
        text: ' Thus, despite the fact that Ulster-Scots was primarily an oral culture, the 1880s saw the publication of a number of writings which reflected various aspects of life in the Ulster-Scots community. We think, for example, of novels such as Maria Crommelin’s, Orange Lily, published in London in 1880, or W.G. Lyttle’s, Sons of the Sod, published in Belfast in 1886, the year the Home Rule Bill came before Parliament. 1883 saw the publication of an important posthumous collection of poetry by David Herbison, the “Bard of Dunclug” - The Select Works of David Herbison, published in Belfast, Ballymena and Londonderry. And then in 1886, we have the publication in Edinburgh of History of the Irish Presbyterian Church, by Rev. Thomas Hamilton, one of a number of Presbyterian church histories to appear during the Home Rule period. Besides this, there were the articles in Ulster-Scots that appeared regularly in several local newspapers serving areas with large Ulster-Scots communities. We think for example, of the Ballymena Observer, which had a regular column in Ulster-Scots by Bab M’Keen, the pen-name of the paper’s editor, John Wier. '
  -
    type: paragraph
    content:
      -
        type: text
        text: 'All of this material was going to contribute – even if only indirectly - to the on-going “construction” of a vision of the “Ulster Scot” in the popular imagination.'
---
