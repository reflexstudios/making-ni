---
id: e0355eb6-2adc-49fd-b3c0-26c0f5ac5291
blueprint: module_3
title: 'W. Benet'
intro_text: '“You attend to your own affairs and we will attend to ours.”'
parent_unit: government_of_ireland_act_1920
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654278599
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "As we saw in Unit 3, the war, which had seen increased cooperation between the United States and Britain, had altered the perceptions of some in America towards the Irish situation. However, the overwhelming victory of Sinn Féin in the December 1918 elections and the escalating violence in Ireland led to renewed pro-Independence efforts by supporters in the States.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The unionist line on this “agitation” was clear – the “Irish question” was a matter for the British Government alone to resolve. The British weekly magazine, The Spectator, described by  H.S. Morrison (Modern Ulster) as \"That powerful friend of Ulster and ablest of British Weeklies,\" put forward this position in an article published on 19th July 1919.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Sir Edward Carson went on to warn his hearers that the Roman Catholic Church in America, with large funds at its disposal, was working with the Irish-Americans and the Germans to create an anti British feeling. But he would not yield to the agitation, as some faint hearts proposed, for fear of losing the goodwill of America. “The whole future of the world probably depends on the relations between the United States and ourselves [UK], but I am not going to submit to this kind of campaign, whether for that friendship or for any other purpose. I to-day seriously say to America, ‘You attend to your own affairs and we will attend to ours.’”'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Interestingly, there were echoes of this in America. It is perhaps not surprising to see that this position was particularly strong among people committed to promoting America’s links with Scotland and Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "William Christie Benet was born in Scotland in 1846 and migrated to South Carolina in 1868 where he became a circuit court judge. In 1920, he was invited to publish a long two-part article, entitled “Scots and Ulster-Scots in the Southern States,” in The Caledonian, a magazine published in New York aimed at the Scots community in North America.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The second part of Benet’s article, published in February 1920 '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ", looks at what it calls “the great invasion of the Southern States by the Presbyterian Scots who came from Ulster,” starting with the arrival in Boston of the “five small ships in 1718.” Benet goes on to detail the contribution of what he systematically refers to as “the Scots and the Ulster Scots” to the construction of America in the various fields of education, the army, the Church and the administration.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, as far as we are concerned, it is the way he ends the article that is of the greatest interest. Although he makes no direct reference to the on-going situation in Ireland, he makes it clear that he is hostile to any interference by America in what he sees as a purely domestic British issue.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: "Judge W.C. Benet, “Scots and Ulster-Scots in the Southern States,” Part II, The Caledonian, Vol. XIX, N°. 10, Feb. 1920, pp. 442-450.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' The Spectator, July 19th, 1919, front page, showing the prominence of “the Ulster question” at the time.'
    uuid: a6919bb4-be3e-4d98-9bcc-6daa3329de6d
    type: content
    enabled: true
    image: Screenshot-2022-05-24-at-15.39.07.png
    allow_zoom: false
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'A few months before this article was published, The Caledonian had announced its decision to open a new section, entitled “The Allies Magazine,” designed to attract contributions from all the English-speaking countries that had been allied to America during the war: Canada, Newfoundland, Great Britain, South Africa, Australia, Tasmania, New Zealand and India – i.e., the countries of the British Empire. It is clear that what Benet has to say in his – ostensibly historical – article, fits in with the new editorial line.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'In finishing this outline sketch, it gives me peculiar pleasure to note that the Scot and the Ulster-Scot in America still possess in undiminished strength one excellent characteristic inherited from their forefathers - namely, unswerving loyalty to the land of their adoption. Reviewing their history for two hundred years, since the five small ships arrived in Boston Harbor, the historian is bound to say they have borne their part well in peace and in war. They have not formed themselves into a separate semi-American group as some others of foreign blood have done. We hear of the German vote and the Irish vote. No one speaks of the Scottish vote, or asks how it will be cast in an election. When a Scot votes, he votes as a man and as a loyal American, and not as the member of a group. He may be a Democrat, a Republican, a Progressive, or an Independent, but he is always a Scot – his own master in politics. […] He casts his ballot as a loyal pro-American citizen. Nor does he use America as a safe vantage ground from which to meddle with the affairs of other countries. He does not ask this Government to advise any foreign government as to its duty. […] The Scot in America is no hyphenate; he is an American citizen through and through; and yet he has his St. Andrews Societies, his Burns Clubs, his Scottish song concerts, his Caledonian games, with all the gaiety of Highland dances, and tartan kilts, and glittering cairngorms, and skirl '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (1)
              -
                type: text
                text: ' of bagpipes. For with all his sincere, unalloyed Americanism, his “heart warms to the tartan”; he still loves Auld Scotland. If he did not, he would not be the good American he is […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'This sketch, necessarily brief, is necessarily imperfect. Its main purpose will be accomplished, however, if it will induce my readers to pursue the subject further. The history of the Scottish race in Scotland, in Ulster, and in America is a goodly heritage. There is in it much more to admire than to condemn. It is the record of a steady, hardy, thrifty, independent, law-abiding and God-fearing people. Tyrants have always found them to be “kittle cattle to deal wi’.” '
              -
                type: text
                marks:
                  -
                    type: bold
                text: '(2) '
              -
                type: text
                text: 'Conservative by nature, holding fast to that which is good, they are not given to radical changes, and prefer to “ca’ canny.”'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' (3) '
              -
                type: text
                text: 'The thistle is their fit emblem, and the motto - Nemo me impune lacessit'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' (4) '
              -
                type: text
                text: '- is the very voice of independence, almost as good as their own “braid Scots” - Wha daur meddle wi’ me? (Benet, pp. 449-450)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' The “skirl” of bagpipes is the shrill, piercing noise of the instrument.'
                  -
                    type: hard_break
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(2) '
                  -
                    type: text
                    text: "\_“Kittle cattle to deal wi”: here, people who are hard to handle, difficult to deal with."
                  -
                    type: hard_break
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (3)
                  -
                    type: text
                    text: ' The expression, “ca’ canny,” means to proceed with caution.'
                  -
                    type: hard_break
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (4)
                  -
                    type: text
                    text: " Latin motto of Scotland: No-one challenges (or, provokes) me with impunity.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Front Cover of The Caledonian, Published in New York, January 1920.'
    uuid: c87d5f05-9f26-4733-9884-8c46351f00a0
    type: content
    enabled: true
    image: The-Caledonian.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: "In this conclusion to his essay, Benet focuses on what he sees as one of the characteristics of “the Scot and the Ulster Scot,” i.e. “unswerving loyalty to the land of their adoption.” He claims that the history of these communities in America demonstrates their refusal to place the interests of any other country before those of their new homeland – America. He contrasts this stance to the attitudes of what he calls “hyphenated” Americans, who, although living in America, “formed themselves into a separate semi-American group.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The notion of “hyphenated Americans” – a derogatory term - was a frequent source of debate at the period. We think of the well-known Puck cartoon (August 9, 1899) where Uncle Sam, the personification of the USA, aggressively asks: “Why should I let these freaks cast whole ballots when they are only half Americans?” First in the line of voters comes the Irish-American…\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "American presidents such as Theodore Roosevelt and Woodrow Wilson, were very outspoken in their views on “hyphenates” whose first political loyalty they suspected as lying outside the United States.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "When it comes to giving examples of this controversial notion of the hyphenated American, it is interesting to see that Benet limits himself pointedly to the Irish and the Germans. Pro-unionist authors were keen to underline the close ties that had existed between the Irish republicans and the Germans during the First World War. People like Sir Roger Casement had negotiated German military assistance for the Rising in Dublin in 1916. Those supporting Irish independence had to explain this to an American audience, especially after America joined the war as an ally of the United Kingdom.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Meanwhile, Crawford''s contacts with arms-dealers in Hamburg to supply the U.V.F. with weapons in 1914 were conveniently forgotten. '
      -
        type: paragraph
        content:
          -
            type: text
            text: "Benet then moves on from domestic American politics to international affairs. Just as the Scot and the Ulster Scot supposedly refuse to allow their vote to be influenced by outside forces, so they refuse to “meddle with the affairs of other countries.” Thus, they do not lobby the American Government to tell other Governments what they should do: “He does not ask this Government to advise any foreign government as to its duty.” In an obvious swipe at the Irish-American lobby, he says: “No pro–Scottish Senator offers a resolution that the British Government should treat Scotland as well as it does Ireland in the matter of representation in Parliament.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Benet goes on to explain what he sees as the reasons for this attitude among those of Scots or Ulster-Scots descent: “The Scot in America is no hyphenate; he is an American citizen through and through,” casting his vote, “as a loyal pro-American citizen.” He says that this is due to the fact that the Scot acts and thinks as an individual – he is “his own master in politics” - and is not controlled by any group.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "His “unalloyed Americanism” does not prevent him exploring his Scottishness through “his St. Andrews Societies, his Burns Clubs, his Scottish song concerts, his Caledonian games […]” But that sense of attachment to Scottish identity is purely cultural; it does not, he claims, have a political dimension.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Interestingly, however, the final paragraph calls on his listeners and readers to look more closely at the “history” and “heritage” of the Scot “in Scotland, in Ulster, and in America.” Benet sees this history as necessarily a connected history, one that reflects the linkages and networks that exist between people with a common origin (See: '
          -
            type: text
            marks:
              -
                type: bold
            text: 'The Ulster Scot and Kith and Kin'
          -
            type: text
            text: ") and a shared world view. That history, characterised, he says, by endurance and independence, has produced what he calls a “law-abiding and God-fearing people.” He ends by identifying their fundamental orientation as “conservative […] not given to radical changes.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'When he concludes his essay, quoting the Latin motto, Nemo me impune lacessit '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ', or its variant in “‘braid Scots’ - Wha daur meddle wi’ me?” - he is saying that none of the component parts of this “people” - whether “in Scotland, in Ulster, [or] in America” – has any need of outside help. Each one is perfectly able to look after itself. (See: '
          -
            type: text
            marks:
              -
                type: bold
            text: 'The Ulster Scot as Fighter'
          -
            type: text
            text: )
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Carson and the northern unionists undoubtedly agreed.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' No-one challenges me with impunity. In other words, nobody provokes me and gets away with it.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Cartoon by J. S. Pughe published in Puck magazine, 9 August 1899, Public Domain.'
    uuid: d4a57fb5-b16f-4f9f-b23f-1d669ff7509c
    type: content
    enabled: true
    image: MOD3-UN4-EX1-min.png
    allow_zoom: true
book_title: 'W. Benet'
parent_module: b3bc48fa-27a8-467f-86cb-366253976041
example_number: '01'
---
