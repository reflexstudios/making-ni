---
id: 386c1c88-f2c7-43d5-a6ae-487a779df46b
blueprint: module_3
title: McNeill
intro_text: '“A profoundly moving occasion in Irish history.”'
parent_unit: government_of_ireland_act_1920
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654278991
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following extracts giving an account of the opening of the Northern Ireland Parliament are taken from Ronald McNeill’s, Ulster’s Stand for Union, completed in February 1922'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1)'
          -
            type: text
            text: ", only a few months after the events described. The passage comes from the concluding chapter of the book charting the history of “the Ulster Movement” and its resistance to Home Rule. It is written from “an Ulster standpoint” - i.e. unionist - and gives us an insider’s perspective of the campaign. The author was a member of the Standing Committee of the Ulster Unionist Council and was, in his own words, “closely associated with the leaders of the movement.” The book is dedicated to “the memory of the Unionist Party.” It can be seen as an “official” account of the unionist campaign.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "It is no coincidence that the text should appear so soon after the creation of Northern Ireland. It was important for the Unionist authorities and their sympathisers to make sure that “their” version of events appeared quickly in the public domain. If Morrison’s book, Modern Ulster, is aimed first and foremost at a regional audience, McNeill’s target audience is national, indeed, international. Ulster’s Stand for Union was published not only in London, but also, simultaneously, in America. The aim was therefore to target opinion outside Northern Ireland so as to try and counter republican arguments being put forward on both sides of the Atlantic.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'His Majesty the King had decided to open the Ulster Parliament in person on the 22nd of June, 1921, [despite] the anarchical condition of the country […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: "The scene in the streets when the King and Queen drove from the quay, on the arrival of the royal yacht, to the City Hall, was [such that] persons […] in attendance on the Royal Family [said] that they had never before seen so impressive a display of public devotion to the person of the Sovereign.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Two buildings in Belfast inseparably associated with Ulster''s stand for union, the City Hall and the Ulster Hall, were the scenes of the chief events of the King''s visit. The former, described by one of the English correspondents as “easily the most magnificent municipal building in the three Kingdoms,” was placed at the disposal of the Ulster Government by the Corporation for temporary use as a Parliament House. The Council Chamber, a fine hall of dignified proportions with a dais and canopied chair at the upper end, made an appropriate frame for the ceremony of opening Parliament, and the arrangement […] of the Chamber itself […] made it a simple matter to model the procedure as closely as possible on that followed at Westminster. […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: '[Without doubt], the moment of all others on that memorable day […] was when the King formally opened the first Parliament of Northern Ireland in the same building that had witnessed the signing of the Ulster Covenant. Without the earlier event the later could not have been. If 1921 could have been fully foreseen in 1912 it might have appeared to many Covenanters as the disappointment of a cherished ideal. But those who lived to listen to the King''s Speech in the City Hall realised that […] it was, as King George himself pronounced, “a profoundly moving occasion in Irish history.” The Speech from the Throne […] made a deep impression all over the world, and nowhere more than in Ulster itself. No people more ardently shared the […] desire of the King that his coming to Ireland might “prove to be the first step towards an end of strife amongst her people, whatever their race or creed.” So, too, when His Majesty told the Ulster Parliament that he “felt assured they would do their utmost to make it an instrument of happiness and good government for all parts of the community which they represented,” the Ulster people believed that the King''s confidence in them would not prove to have been misplaced.'
          -
            type: paragraph
            content:
              -
                type: text
                text: '[…] They accepted responsibility for the efficient working of institutions thus placed in their keeping by the highest constitutional Authority in the British Empire, although they had never asked for them, and still believed that the system they had been driven to abandon was better than the new; and they opened this fresh chapter in their history in firm faith that what had received so striking a token of the Sovereign''s sympathy and approval would never be taken from them except with their own consent. (McNeill, pp. 282-286).'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " \_Ronald McNeill, Ulster’s Stand for Union, New York, E.P. Dutton and Company, 1922."
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: " Visit of King George and Queen Mary to open in State the Northern Parliament of Ireland, 22nd\_June, 1921. From a photograph by A.R. Hogg, Belfast. In Edwin Darley Hill, Northern Banking Co. Limited, An Historical Sketch, Belfast, M’Caw, Stevenson & Orr, Limited, 1924 (opposite p. 184)."
    uuid: a12a0f42-264b-46ba-b859-487c4bcc231f
    type: content
    enabled: true
    image: GeorgeV-at-the-Albert-Clock.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: "McNeill’s account of the opening of the Northern Ireland Parliament insists on a number of themes.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The first is the intensity of the emotion with which the (unionist) public welcomed the King and Queen: “never before [had anyone seen] so impressive a display of public devotion to the person of the Sovereign.” This popular fervour, made evident in the crowd scenes photographed and filmed during the visit, confirmed the exceptional nature of the sentiment of '
          -
            type: text
            marks:
              -
                type: bold
            text: loyalty
          -
            type: text
            text: " to the Crown which McNeill says characterises unionist Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The\_“demonstrations of devotion” were, however, particularly intense because the King and Queen decided to come to Belfast in person in spite of “the anarchical condition of the country.” The increasing violence of the War of Independence between the IRA and the British forces did not deter George V from coming to Belfast to open Parliament. Unionists were particularly sensitive to this gesture which, according to them, showed that the new institutions were being endorsed by “the highest constitutional Authority in the British Empire.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Throughout the text McNeill insists on how closely the Belfast parliament resembles the Westminster “model.” This is true not only for the inside layout of the buildings, but also for the ceremonial that takes place in them.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Although there is as yet no building set aside for parliament in Belfast, McNeill is eager to show how the Belfast City Hall, “easily the most magnificent municipal building in the three Kingdoms” – i.e. England, Scotland and Ireland - reflects the outstanding '
          -
            type: text
            marks:
              -
                type: bold
            text: 'material prosperity '
          -
            type: text
            text: "of the city. As a result it was seen as an ideal substitute, “for temporary use as a Parliament House.” The City Hall had been built in 1905, replacing a new Town Hall that had been opened in Victoria Street only a few years before in 1871. Apart from the “magnificence” of the exterior, he explains that the interior lay-out of the building, with its “Council Chamber, a fine hall of dignified proportions,” equipped with “a dais and canopied chair,” made it “an appropriate frame” for “one of the most ancient and splendid of ceremonial pageants illustrating the history of British institutions.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The ceremony in Belfast was carefully constructed in order that the procedure followed should be “[modelled] as closely as possible on that followed at Westminster.” The Belfast parliament is therefore presented as a perfect copy, a reduced replica of the Westminster Parliament and the “time-honoured ceremonial” it had evolved over the centuries.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: "'
                  -
                    type: text
                    text: 'Opening of Northern Ireland Parliament 1921"'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' '
                  -
                    type: text
                    text: 'PRONI-REf-No-D3480_22~005. Courtesy of ‘The Deputy Keeper of the Records, Public Record Office of Northern Ireland’.'
    uuid: 16789037-13ec-45bb-8762-36aedaf61900
    type: content
    enabled: true
    image: PRONI-REf-No-D3480_22~005-copy.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, the building chosen for the opening ceremony had other, more local and more recent associations. It was, McNeill reminds us, “the same building that had witnessed the signing of the Ulster Covenant” a few years before. Earlier in the book, he had explicitly linked this event in to Scotland’s influence on Ulster. He saw the Ulster unionists’ decision to “bind […] themselves together by a solemn League and Covenant” as illustrating “the kinship of this tough people with the Lowlanders of Scotland, in character as in blood.” Like their “Scottish forefathers,” '
          -
            type: text
            marks:
              -
                type: bold
            text: '(Kith and kin) '
          -
            type: text
            text: 'they were determined to “resist what they deemed to be a tyrannical encroachment on their liberties and rights.” In other words, McNeill is saying that unionism’s concerted campaign of resistance embodied in the Covenant finds it roots in Scottish history. This includes its commitment to the possible use of physical force. Indeed, the official ceremony in 1921, which is an elaborate display of constitutional continuity and '
          -
            type: text
            marks:
              -
                type: bold
            text: stability
          -
            type: text
            text: ', would not have been possible without the threat of '
          -
            type: text
            marks:
              -
                type: bold
            text: 'armed rebellion'
          -
            type: text
            text: " that had been staged in the same building by what he pointedly calls “the Covenanters” nine years before.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "McNeill cannot help but evoke the regret of many unionists that it had not been possible to include the whole of Ulster in the new Northern Ireland. What he refers to as “the disappointment of a cherished ideal” is an allusion to the decision, reluctantly taken, to opt for a six-county Northern Ireland, despite the Covenant being signed by unionists across Ulster’s nine counties.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "He also underlines a central irony: although unionism was celebrating the opening of these new institutions, “they had never asked for them, and still believed that the system they had been driven to abandon was better than the new.” In other words, unionists would have preferred the status quo ante, i.e. the situation in which the whole of Ulster, indeed the whole of Ireland, remained within the United Kingdom under a single Parliament in Westminster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Despite this, and however paradoxical this may seem, unionists were now determined to hold on to what they had been given. The text reflects this determination when it says the opening of parliament by the King was seen as holding a “promise of permanence” for the new institutions. McNeill concludes the passage saying that these institutions “would never be taken from them except with their own consent.” These remarks are important as they illustrate how unionists now believed that the very existence of these institutions gave them a greater security within the United Kingdom. The Parliament would allow them to run their regional affairs. But it would also give them a platform from which to defend their position not only with regard to London but also to whatever regime would emerge in Dublin.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: "'
                  -
                    type: text
                    text: 'Opening of Northern Ireland Parliament by King George V at Belfast City Hall 1921"'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' '
                  -
                    type: text
                    text: PRONI-Ref-SO
                  -
                    type: text
                    marks:
                      -
                        type: italic
                    text: '1'
                  -
                    type: text
                    text: 'Z_4. Courtesy of ‘The Deputy Keeper of the Records, Public Record Office of Northern Ireland’.'
    uuid: add67b68-c2a3-40ea-bc97-1621381037e3
    type: content
    enabled: true
    image: PRONI-Ref-SO_1_Z_4-copy.jpg
    allow_zoom: true
book_title: McNeill
parent_module: b3bc48fa-27a8-467f-86cb-366253976041
example_number: '03'
---
