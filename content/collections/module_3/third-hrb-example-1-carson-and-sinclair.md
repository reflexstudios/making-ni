---
id: e11c1a0d-6179-4dfc-bd6e-195adfb7c32a
blueprint: module_3
title: 'Carson & Sinclair'
parent_unit: third_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655629477
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following extracts present two very different arguments by two of the most influential unionist politicians of the period, the Ulster Unionist leader, Edward Carson, and Thomas Sinclair, Ulster’s leading Liberal Unionist. Both of the arguments are centred on the figure of the Ulster Scot. '
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '“Do you really think that the Ulster Scot is the kind of man you can trample underfoot?”'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The first passage is from the second volume of Ian Colvin’s, The Life of Lord Carson, published in 1934.\_"
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Edward Carson, a Dublin barrister and former Solicitor-General for both Ireland and England, became leader of the Irish Unionists in Westminster in February 1910. His powerful oratory and undoubted charisma ensured his popularity among the Ulster unionists whom he led with great skill during the unprecedented political crisis caused by the third Home Rule bill.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "In the following extract, Colvin gives a summary of a speech delivered by Carson on 13th June 1912 in the House of Commons during a debate on the Government of Ireland Bill. The debate was on an amendment that had been moved by the Liberal MP, Agar-Robartes. It proposed that four counties, Antrim, Down, Armagh and Londonderry, each of which had a clear unionist majority, should be excluded from the provisions of the future Act. The proposed Amendment presented unionists with \"a horrid dilemma\". On one hand, it offered a section of the northern unionists the possibility of remaining outside the terms of the Home Rule Act. On the other hand, the plan raised worrying questions not only about the unionists in the South, but also about the unionists in the other five counties of Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Before going on to deal with Agar-Robartes'' amendment, Carson talks about the dangers of trying to force Ulster into accepting Home Rule. Colvin''s account moves constantly between direct quotation and résumé.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'When [...] Carson rose to speak on the Amendment, it was [...] to state the case of the Ulster Unionists. These people were not asking for anything — “they are only asking to stay where they are,” to enjoy “the rights under which they were born, and under which they have lived and flourished.” [...] It was to Ulster “not a political matter,” but “a matter affecting their lives, their liberties, their employment and everything that goes to make up what man holds dear in life.” [...] '
              -
                type: hard_break
              -
                type: text
                text: "Belfast was a great city, which had prospered under the Union; her citizens were free, they were loyal, they were satisfied. He pointed to her trade and to her industries, which enjoyed the markets not of Ireland alone but of the British Empire. Her people attributed their prosperity to their connection with Great Britain: “Do you not think that they are right? Do you not think that these are the very kind of things that men will struggle to the very end to maintain rather than run the risk of what is at best a gamble on the future?” And Carson proceeded, as his custom was, to reduce the position to its elemental simplicity:\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "“They say, ‘We want to remain in this position.’\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "“They say, ‘We have never yet been told what benefit we can get by being driven out of this position.’\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "“In the circumstances, is the statesman going to say, ‘My business is to coerce you against your will?”\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "He went on to lay before Ministers the consequences of that policy. “I do not want to say one word by way of threat. [...] I am using no threat of any kind whatsoever. I shall assume for the purpose of my argument that you coerce Ulster [...] I shall assume that the Navy and the Army are going to put Ulster down. What then?\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "“Do you think that people with this burning passion in relation to the Government under which they are to live will be good citizens?\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '“Do you really think that the Ulster Scot is the kind of man you can trample underfoot?” (Colvin, pp. 120-121).'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "(1) \_"
                  -
                    type: text
                    text: 'Ian Colvin, The Life of Lord Carson, Vol. II, London, Victor Gollancz, 1934.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: '  Edward Carson addressing Parliament: Vanity Fair, 9 November 1893. '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://upload.wikimedia.org/wikipedia/commons/2/23/Edward_Carson_Vanity_Fair_9_November_1893.jpg'
                          rel: null
                          target: null
                          title: null
                    text: 'https://upload.wikimedia.org/wikipedia/commons/  2/23/Edward_Carson_Vanity_Fair_9_November_1893.jpg'
    uuid: 6a357037-8eb4-4c34-987f-d0e20875fdd1
    type: content
    enabled: true
    image: 3-Carson-adressing-Parliament-19-Nov-893-Vanity-Fair.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Carson, who is addressing the House of Commons, is in fact warning the members of Parliament that if they allow Ulster to be “coerced,” i.e. forced into a Home Rule Ireland against their will, they will be responsible for any violence that might take place as a result. He is saying that, although the issue of Home Rule may be a political or constitutional abstraction for many of the British MPs present, for the unionist community in Ulster it is a question that will affect every aspect of their everyday lives. In other words, it is not an intellectual question for them, but rather something which has an immediate, concrete significance. It is this “passion” that explains that it will be difficult, if not impossible to control them.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Significantly, it is the figure of the “Ulster Scot” that Carson refers to when he wants to conjure up in the imagination of his listeners someone who is the most capable of resisting the coercion he fears is being planned. As we saw in Module 2 (see: "Key Questions" explored at the beginning of Module 2), the body of history and literature that had been building up over the years on both sides of the Atlantic around the figure of the Ulster Scot had produced an increasingly sharp image of his identity. For those involved in the construction of that image, this Ulster Scot has a series of – invariably positive – characteristics that were built in to the narrative of the Ulster-Scots experience. Thus, as we have seen, the Ulster Scot was represented as a fearless '
          -
            type: text
            marks:
              -
                type: bold
            text: pioneer
          -
            type: text
            text: ', capable, through his energy and '
          -
            type: text
            marks:
              -
                type: bold
            text: industry
          -
            type: text
            text: ', of turning a “wilderness” into a “garden”; he is a determined '
          -
            type: text
            marks:
              -
                type: bold
            text: fighter
          -
            type: text
            text: ', ready to defend himself, even against apparently overwhelming odds; lastly, and perhaps most importantly, he has a '
          -
            type: text
            marks:
              -
                type: bold
            text: 'religious conviction '
          -
            type: text
            text: 'and a '
          -
            type: text
            marks:
              -
                type: bold
            text: 'confidence in Providence '
          -
            type: text
            text: "that makes him hard to turn.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In any case, it is striking that when Carson refers to the Ulster Scot, he makes no attempt to explain what he means. It is clear that, by 1912, the “idea” of the Ulster Scot had become sufficiently clear in the popular imagination for him to be able to call on this figure without fear of ambiguity. When he asks his rhetorical question - “Do you really think that the Ulster Scot is the kind of man you can trample underfoot?” - Carson knows that the MPs will understand that the Ulster Scot he is referring to is a by-word for dogged resistance. As the unionist postcard, “The Ulster Scot,” puts it: “This land oor heritage by richt [...] Three hunner years we hae been here, An deil th’ fit they’ll budge us.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Although he is careful to wrap up what he says in a series of formal denials - “I do not want to say one word by way of threat. [...] I am using no threat of any kind whatsoever” - it is clear that Carson is indeed making a threat. Carson is warning Parliament that, if the British Government tries to use the army and the navy to intimidate Ulster into submission, there will be a reaction. As Randolph Churchill had said in 1886 at the very beginning of the Home Rule crisis, “Ulster will fight!”\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'An Anti-Home Rule Postcard. It is interesting to note that the postcard represents the Ulster Scot as a Highlander, wearing a kilt and carrying a claymore. This is odd as the Ulster Scots were of Lowland origin. It is as if those producing the postcard had decided to use the most distinctive image of "Scottishness" they could find because they knew it would be more easily recognisable to the wider British public.'
    uuid: 9a8991ea-945d-48dc-8ee4-435ce561f8ce
    type: content
    enabled: true
    image: MOD3-UN3-EX1-min.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '"The Ulster Scot is not in Ireland to-day [...] an ordinary immigrant"'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Thomas Sinclair is one of the most important figures in Ulster unionist history. He was a businessman and a prominent member of the Presbyterian Church. In politics, he was a Liberal, but, when Gladstone “converted” to Home Rule, he left the party, becoming the first president of the Ulster Liberal Unionist Association. He was the main organiser behind the Ulster Unionist Convention in Belfast in 1892. He was also the person who framed the Ulster Covenant (1912) and wrote the Constitution for the Ulster Provisional Government.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In this extract from his essay that appeared in the influential collection, Against Home Rule, published in London and New York'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1), '
          -
            type: text
            text: "Sinclair is addressing “English and Scotch electors.”\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "By Ulster, I mean the six counties, Antrim, Down, Londonderry, Armagh, Tyrone, Fermanagh, with the important adjacent Unionist sections of Monaghan, Cavan, and Donegal, in all of which taken together the Unionist population is in an unmistakable majority, and in which the commercial and manufacturing prosperity of the province is maintained by Unionist energy, enterprise, and industry.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '[...] The Ulster Scot is not in Ireland to-day [...] an ordinary immigrant. His forefathers were “planted” in Ulster in the troublous times of the seventeenth century. [...] This scheme was devised in the hope that through the industry, character, and loyalty of the new population, the Northern province [...] should enjoy peace and prosperity, and become an attached portion of the King''s dominions; and that eventually its influence would be usefully felt throughout the rest of Ireland. '
          -
            type: paragraph
            content:
              -
                type: text
                text: "[...] We are in Ireland as [the] trustees [of our English and Scottish fellow-citizens], having had committed to us, through their and our forefathers, the development of the material resources of Ulster, the preservation of its loyalty [...]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "It cannot be denied, on an examination of the history of the last three centuries, and especially of that of the one hundred and ten years since the establishment of the Legislative Union, that [...] we have not unsuccessfully fulfilled our trust. Our forefathers found a province, the least favoured by nature of the four of which Ireland consists, and it is to-day the stronghold of Irish industry and commerce. [...]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "Ulster Unionists, therefore, having conspicuously succeeded in maintaining the trust committed to their forefathers, and constituting as they do a community intensely loyal to the British connection [...] cannot believe that their British fellow-citizens will, at this crisis, turn a deaf ear to this claim. Three or four decades after the Ulster plantation, when, in the midst of the horrors of 1641, the Scotch colony in Ulster was threatened with extermination, it appealed for help to its motherland. It did not appeal in vain. A collection for its benefit was made in the Scottish churches, supplies of food and several regiments of Scottish soldiers were sent to its aid, and its position was saved. We are confident that the descendants of these generous helpers will be no less true to their Ulster kith and kin to-day.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: 'Thomas Sinclair, “The Position of Ulster,” in S. Rosenbaum (ed.), Against Home Rule, London, New York, Frederick Warne & Co., 1912, pp. 170-172.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Thomas Sinclair. Source  '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://discoverulsterscots.com/history-culture/ulster-covenant-belfast'
                          rel: null
                          target: null
                          title: null
                    text: 'https://discoverulsterscots.com/history-culture/ulster-covenant-belfast'
    uuid: 78d302fd-0bf6-4e08-a849-b2f1745a7784
    type: content
    enabled: true
    image: Thomas-Sinclair.PNG
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "He begins by stating what he understands by “Ulster.” For him, Ulster means the territory in which “the Unionist population is in an unmistakable majority.” He identifies this territory as corresponding to “the six counties, Antrim, Down, Londonderry, Armagh, Tyrone, Fermanagh, with the important adjacent Unionist sections of Monaghan, Cavan, and Donegal.” In other words, “Ulster” no longer means the province of Ulster, but rather a nucleus within that province, centred on “six counties,” to which are attached a series of unspecified zones in Monaghan, Cavan, and Donegal. The “mind map” of Ulster is therefore more about politics – and religion - than geography.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is interesting to note that when addressing these “English and Scotch electors,” Sinclair takes the “Ulster Scot” as the representative of this “Unionist population.” Going back to the history of the Plantation, he explains how the settlers - "the majority Scotch” - were sent over to Ulster “in the troublous times of the Seventeenth century” to ensure that the territory would prosper through “the industry, character, and loyalty of the new population.” Sinclair is here calling on the frames that associate the settler population with '
          -
            type: text
            marks:
              -
                type: bold
            text: stability
          -
            type: text
            text: ', “peace and '
          -
            type: text
            marks:
              -
                type: bold
            text: prosperity
          -
            type: text
            text: ".” He even says that the parts of Ulster settled with this “new population” became “an attached portion of the King's dominions,” suggesting that, rather than belonging to Ireland, they should be seen as a projection of the neighbouring island into Ireland. In earlier Modules, we have already seen a similar idea in Harrison’s, The Scot in Ulster, and in T.C.'s essay, \"Ulster and its people,\" published in Fraser's Magazine in August 1876.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Sinclair then moves on to his main argument. He says that “the Ulster Scot is not in Ireland to-day [...] an ordinary immigrant.” He explains that the Ulster Scot, who forms the backbone of the unionist population in Ulster, was there to fulfil the terms of a “trust” that was confided to them at the time of the Plantation'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1)'
          -
            type: text
            text: '. This “trust,” he says, involves “the development of the material resources of Ulster” and “the preservation of its loyalty.” He claims that the unionists have fulfilled their side of the bargain: they have made Ulster the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'most prosperous province in Ireland '
          -
            type: text
            text: 'and they have remained '
          -
            type: text
            marks:
              -
                type: bold
            text: 'loyal to the Crown'
          -
            type: text
            text: ", defending its interests on the ground in Ireland. He tells his “British fellow-citizens” that the unionists expect them to respect their side of the bargain, and stand by them at his time of crisis.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In conclusion, he returns to history, citing as his example the help given by Scotland to the fledgling Scottish colony in Ulster at the time of the 1641 rebellion (Cf. '
          -
            type: text
            marks:
              -
                type: bold
            text: 'the Ulster Scot and sectarian violence'
          -
            type: text
            text: '). He does not hesitate to use emotive language, referring to “the horrors of 1641” and the danger of “extermination.” Once again, he airbrushes the English settlers out of the equation and, focusing on the Scottish narrative of material and military help, he concludes by saying: ‘We are confident that the descendants of these generous helpers will be no less true to their Ulster '
          -
            type: text
            marks:
              -
                type: bold
            text: 'kith and kin '
          -
            type: text
            text: to-day.”
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: 'A trust is a relationship in which property is held by one person for the benefit of another. Sinclair is saying that, at the time of the Plantation, the Crown gave Ulster (the property) to the Ulster Scots to hold in trust for the benefit of the Crown and the British people as a whole.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Title Page of S. Rosenbaum (ed.), Against Home Rule, The Case For the Union, London and New York, Frederick Warne & Co., 1912.'
    uuid: 5d0934a1-b7a9-4bd9-9435-d49f01da7f86
    type: content
    enabled: true
    image: 14-Against-Home-Rule.png
    allow_zoom: true
book_title: 'Carson & Sinclair'
parent_module: 9c23ffe6-99e2-43e8-9a25-f9d49845ff22
example_number: '01'
---
