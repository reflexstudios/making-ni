---
id: 0d0e5029-5ee9-4754-a4ee-e43e317f5050
blueprint: module_3
title: Crawford
intro_text: '“A land where war and strife have been constant for almost three hundred years”'
parent_unit: third_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655131815
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Published in 1947, just after the end of the Second World War, Fred Crawford’s, Guns for Ulster, gives his personal account of how he organised the smuggling and distribution of a large shipment of arms for the Ulster Volunteer Force in 1914. ('
          -
            type: text
            marks:
              -
                type: bold
            text: '1) '
          -
            type: text
            text: "The success of this operation sent an electric shock though Ireland and indeed the whole of the United Kingdom.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "It was first and foremost a message of defiance addressed to the Liberal Government. The gun-running strengthened the hand of the unionist leaders in Ulster, turning them overnight into a much more credible threat. They were now on the way to being able to put up effective resistance if the Government decided to attempt to “coerce” Ulster. However, since the incident at the Curragh a few days before (See Bab M’Keen article, “Things in General. Hame Rule in Parteeclar”), the idea that the Government might move against Ulster had become increasingly unlikely.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is interesting to see how Crawford goes out of his way in the Introduction to his book to underline his Ulster-Scots credentials. After giving a brief outline of the Plantation, he says:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'From these settlers sprang a people, the Ulster-Scot, who have made themselves felt in the history of the British Empire and, in no small measure, in that of the United States of America. (Crawford, p5).'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Indeed, it is as if he sees his activities as a gun-runner as being directly related to the experience of the Ulster-Scots community in Ireland, “liv[ing] and surviv[ing] for generation after generation in a land where war and strife have been constant for almost three hundred years.” (Crawford, p. 8) He underlines how his family has been rooted in Ulster since the earliest days of the Plantation and ties himself in to the Presbyterian tradition that is at the heart of so much Ulster-Scots experience:\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'One of my ancestors, the Rev. Thomas Crawford, came from Kilbirnie in Scotland, and was buried at Donegore in County Antrim, in 1670. He married Janet, daughter of the Rev. Andrew Stewart, whose account of the early settlers has already been quoted. (Crawford, p. 8)'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Their experience on this new “frontier” is clearly an on-going part of the way Crawford imagines the experience of his community over the years.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " \_Fred H. Crawford, Guns for Ulster, Belfast, 1947."
      -
        type: paragraph
    uuid: 1d295eae-6027-4e37-8fed-b773f74b1af4
    type: content
    enabled: true
    image: images.jpg
    allow_zoom: false
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is this self-styled Ulster Scot who was the mastermind behind the secret purchase of the weapons for the UVF and their transport by ship from Germany to Larne. In his book, Crawford says it had been obvious to him from as early as 1892 that “our [i.e. unionist] resistance, to be successful, must eventually come to armed resistance.” He explains that he had been in contact with arms dealers in Germany and in London since 1906, organising small shipments of rifles, machine guns and ammunition on a regular basis to prepare the way for a major operation. What Crawford says about his commitment to physical force places him within a tradition going back to the Apprentice Boys in Londonderry, and, arguably, the United Irishmen who, in the words of the Ulster-Scots poet, James Campbell, were prepared to rebel against the governments of the day “to right some things that we thought wrang.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: "However, it would be a mistake to think that Crawford was “just” a soldier. He explains how he had been actively involved in the unionist campaign in Britain as a speaker during the second Home Rule crisis, just after his return from Australia. It is obvious therefore that he had a very clear idea of the impact that his military preparations would have on public opinion.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The newspapers in Ulster and across the United Kingdom were immediately filled with reports of the dramatic events at Larne, Donaghadee and Bangor on the night of the 24th April 1914. The following extract'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1)'
          -
            type: text
            text: " is typical of the impact the event had not only on the political level but also in terms of what it shows about the way the unionist campaign was being projected into the popular imagination, at home, but especially among the broader British public.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "The night of Friday, 24th April, 1914, is a date that will find a permanent place on the page of history. On it there were enacted happenings for which Great Britain’s long and chequered story affords no parallel. However prosaically the record may be set down, it will send a thrill of amazement through every man and woman who reads the simple matter of fact account of what actually took place, and which the writer personally saw.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "On Friday night there were landed at Larne within a few hours 40.000 rifles and nearly three and half million rounds of ammunition. There was no rush or bustle in the doing of it. It was accomplished with celerity, yet without fuss or splutter, because it was done in pursuance of a well-formed plan, executed as perfectly as it had been preconceived. Thousands unconsciously played a part in it, though only a few hundreds were directly and immediately concerned in the actual work, or were cognisant of what was in progress. All the arms were landed at Larne Harbour, and a vast transport of hundreds of motor cars, lorries, and waggons drawn from their various centres came to the town. So exactly had this mobilisation been arranged that these hundreds of motors reached the assembly point at an identical moment. It was an amazing sight to see this huge procession of cars nearly three miles in length descending upon the town with all their headlights ablaze.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The people flocked to their doors as the seemingly endless procession filed past in the direction of the harbour. […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: "THE “MOUNTJOY” ARRIVES.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'In the neighbourhood of Larne Harbour and throughout the streets of the town strong bodies of men wearing armlets stood in line silent as soldiers on parade, while officers moved about and conversed in low tones. At nine o’clock the throb of an approaching steamer’s engines could be heard coming up the Lough; then masthead lights were discernible, and presently the grey, gaunt outline of the “mystery ship” took definite shape. In a few minutes she was alongside the landing stage and made fast to her moorings […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: "SOME STRENUOUS WORK.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "The “mystery ship,” it was noticed bore on her bows the name Mountjoy—no doubt readers of Derry’s history will draw their own parallel. As she came alongside the lines of men in waiting on shore were divided up, part being assigned sentry duty at the gate approaches; while others were quickly aboard […] Hardly had the hatches been removed before bands of great sturdy fellows stripped to their shirts and pants plunged into the vitals of the ship to join the crew in getting her cargo ashore […]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'As each car received its complement, the driver accelerated his engine, let in his clutch, and slipped away in a cloud of smoke; while another moved into the vacant space and thus the work went hour after hour without pause. […]'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: 'Ballymena Weekly Telegraph, May 2, 1914. Reprinted from Belfast Evening Telegraph, April 25, 1914. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: '"The Remarkable Gun-running by the Ulster Volunteers Landing Arms and Ammunition for Anti-Home Rulers", Illustrated London News, May 2nd, 1914.'
    uuid: 24c32d93-9f9c-4e6c-b450-eac150703ed4
    type: content
    enabled: true
    image: Larne.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The preceding account is designed to be as dramatic as possible. The author, working for a Belfast newspaper, says that he was an eye-witness to the events described. He does not hesitate to say that “the night of Friday, 24th April, 1914” saw “happenings” that were without parallel in “Great Britain’s long and chequered story.” He talks of the “thrill of amazement” which he is sure his account will produce in his (presumably unionist) readers.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The author begins by claiming that “40.000 rifles '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
          -
            type: text
            text: 'and nearly three and half million rounds of ammunition” were landed “within a few hours.” Much of the remainder of the account is designed to get over the idea of the extraordinary organisation involved in the operation. This was “a well-formed plan, executed as perfectly it had been preconceived.” The “mobilisation” had been arranged “exactly.” The timing of every stage of the operation was perfect: “hundreds of motors reached the assembly point at an identical moment.” What is being described is a mechanism which has been conceived and executed with mechanical precision. The description, with its emphasis on solid preparation and military discipline, taps in to the images of the Ulster Scot not only as '
          -
            type: text
            marks:
              -
                type: bold
            text: 'self-reliant and energetic organiser'
          -
            type: text
            text: ' but also as determined '
          -
            type: text
            marks:
              -
                type: bold
            text: fighter
          -
            type: text
            text: ' that are emerging in the literature of the period. When we read: “strong bodies of men wearing armlets stood in line silent as soldiers on parade, while officers moved about and conversed in low tones,” the only conclusion we can draw is that the unionist leadership - people like Crawford himself - are capable of mounting a major military operation and that they can clearly count on the discipline and obedience of their followers. Once again, the narrative is illustrating an idea that we noticed in Module 2 – the supposed '
          -
            type: text
            marks:
              -
                type: bold
            text: 'social cohesion'
          -
            type: text
            text: ' of the Ulster-Scots community that unites around a particular political project.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This has to be seen against the mockery to which the UVF had been exposed during the year or so that it had been in existence. The nationalist press and indeed the British Government had poked fun at these inexperienced, would-be soldiers parading through the streets with wooden rifles. The author, clearly in sympathy with the unionist cause, is keen to dispel this image of an inexperienced, undisciplined rabble. In its place, he paints a picture of a well-ordered body of men working together with clockwork precision in an open act of '
          -
            type: text
            marks:
              -
                type: bold
            text: rebellion
          -
            type: text
            text: " against the Government.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "This may explain why the author insists on the calm with which the operation is effected: “There was no rush or bustle in the doing of it. It was accomplished with celerity, yet without fuss or splutter.” Once again, these comments reinforce the idea that this is a disciplined, structured force, one that could be counted on to react well under pressure. \_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' The figure was closer to 25,000 rifles. Reports at the time did not hesitate to exaggerate the numbers involved.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'An artist’s impressions of the gun-running. Source: The Illustrated London News, May 2nd, 1914.'
    uuid: 2b96c65d-765c-4b5a-a81e-bfa5a81cbcbc
    type: content
    enabled: true
    image: MOD3-UN3-EX4-SCR2-min.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The other aspect of the account has to do with the notion of scale. Numbers are very important. We have already seen that the operation allegedly landed “40.000 rifles and nearly three and half million rounds of ammunition.” If “thousands unconsciously played a part in it, […] a few hundreds were directly and immediately concerned in the actual work.” Once again, “[h]undreds of motor cars, lorries, and waggons” were involved, and the “huge procession of cars,” described at one point as “seemingly endless,” was “nearly three miles in length.” Everything in the account is designed to impress the reader with the extraordinary nature of the facts and figures involved. “It was an amazing sight to see” and indeed “the people flocked to their doors” to witness this spectacle. This is an essential part of the message of the event. The UVF and, behind them, the unionist authorities, were generating images which were going to mark the popular perception of the movement. The event was clearly as much a publicity stunt as it was a military operation. As such, it fitted in perfectly with their well-run unionist campaign.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The final part of the extract focuses on the historical references which underpin the operation. When the account talks of the “grey, gaunt outline of the ‘mystery ship’,” the tone comes close to that of a Richard Hannay adventure story by John Buchan. However, there is another, more important reference, located deep within a unionist reading of history. We discover that the ship transporting the arms into Ulster, The Clydevalley, had been renamed the Mountjoy II as it approached land. As the author points out, this is a reference to the ship that broke the boom on the Foyle, ensuring the relief of '
          -
            type: text
            marks:
              -
                type: bold
            text: Londonderry
          -
            type: text
            text: ' after its long siege in 1689. The action of the UVF is therefore set alongside one of the key events in the Ulster Protestant and unionist imaginary. The arrival of the arms is being read as a form of liberation, and like the lifting of the siege, the first step towards what is clearly seen as a final victory.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The unionists had shown that they could mount a major operation involving secret international negotiations, complex smuggling arrangements and the organisation of an impressive clandestine distribution network. However, the operation was also about provocation and, above all, intimidation. In many ways, it was answering the Government’s threats of coercion in kind. The situation that the Government was faced with on the morning of the 25th April had changed radically. Coming so shortly after the incident at the Curragh, the publicity generated around the events at Larne, Donaghadee and Bangor was a serious setback to its plans.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'The Mountjoy sailing up the Foyle to relieve Londonderry, ending the Siege (July 28th, 1689).'
      -
        type: paragraph
    uuid: e540a345-5452-48e0-8267-36f560af5de9
    type: content
    enabled: true
    image: MOD2-FRA9-SCR1.jpg
    allow_zoom: true
book_title: Crawford
parent_module: 9c23ffe6-99e2-43e8-9a25-f9d49845ff22
example_number: '04'
---
