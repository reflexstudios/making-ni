---
id: ee26172c-a5d4-45bf-bf74-ec62f4bfec5e
blueprint: module_3
title: 'The Somme'
intro_text: '“Every man… covered himself with glory on that memorable morning of the 1st July”'
parent_unit: third_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654277821
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The Battle of the Somme began on Saturday 1st July 1916 and was to drag on until the following November. The two armies, the French and the British on one side, and the Germans on the other, had “dug in,” constructing complex lines of defensive trenches that faced each other over “No Man’s Land.” The attack on 1st July was an attempt by the British to break the stalemate and disrupt the German line. The men of the 36th (Ulster) Division, based in Thiepval Wood, were given the job of attacking the Schwaben Redoubt, a heavily fortified position on rising ground opposite what was left of the wood. The painting by Beadle which hangs in Belfast City Hall, shows the Ulster Division “going over the top,” i.e. leaving the trenches and advancing over No Man’s Land towards the enemy.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The troops immediately came under intense machine gun fire from the underground German defences that had survived a previous bombardment. This resulted in terrible casualties. Despite this, they managed to attain their objectives, but were soon forced to retreat. The losses on the day were enormous, with some 5500 members of the Division, officers and men, killed, wounded or missing. The battle had a devastating effect on the 36th Division. It also had a devastating effect on the community back at home in Ulster. As one contemporary commentator wrote: “The province of Ulster was thrown into mourning by the awful death roll. Several families lost two and three sons.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the course of the Great War, some nine men serving with the Ulster Division were to receive a Victoria Cross - the highest military honour in the British Army – for acts of heroism. Perhaps the best-known of them is the Lurgan-born Private William (Billy) McFadzean who had been a member of the Young Citizen Volunteers before joining up when the war broke out. McFadzean won his V.C. “for most conspicuous bravery near Thiepval Wood” on the first morning of the Battle of the Somme, sacrificing his life to save his comrades by deliberately throwing himself on two bombs that were about to detonate accidentally in the trenches prior to the initial attack. McFadzean has become a folk hero in contemporary loyalist circles, with a ballad composed in his honour. All in all, during the First World War, 23 VCs were given to men with a connection to Ulster.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' "Attack of the Ulster Division, 1 July 1916" by James Prisep Beadle (1863-1947). This painting is reproduced by the kind permission of Belfast City Council. The original can be viewed at City Hall, Belfast.'
    uuid: 99b05f1f-86dd-4233-a387-e61f22e584f4
    type: content
    enabled: true
    image: beadle.jpeg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This section will focus on another young soldier, Robert Quigg (1885-1955), a former member of the UVF from Bushmills, who, like many of his fellow Ulster Volunteers, joined the Royal Irish Rifles at the beginning of the war. '
      -
        type: paragraph
        content:
          -
            type: text
            text: "The following extracts from an article in the Ballymoney Free Press and Northern Counties Advertiser, Thursday 21 September 1916, describe Quigg’s actions on 1st-2nd\_ July 1916. The official “citation” for bravery (“He advanced […] he had to give it up”) is built in to the article but was not marked by quotation marks.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'RESCUED SEVEN WOUNDED COMRADES.'
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'Thrilling Story of Bushmills Soldier''s Heroism.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The honour accorded to Private Quigg was awarded for most conspicuous bravery. He advanced to the assault with his platoon three times. Early next morning, hearing a rumour that his platoon officer was lying out wounded, he went out seven times to look for him under heavy shell and machine-gun fire, each time bringing back a wounded man. The last man he dragged in on a waterproof sheet from within a few yards of the enemy''s wire. He was seven hours engaged in this most gallant work, and finally was so exhausted that he had to give it up. Private Quigg is the eldest son of Mr. and Mrs. Robert Quigg, of Carnkirk, a townland between Bushmills and Ballintoy, near the Antrim Coast. In July, shortly after the Ulster Division''s great attack, a telegram in the following terms was sent to Private Quigg''s mother by the Honourable Lady Macnaghten, of Dundarave, who was then at her London house:- “I hear from Major Hamilton that your son. Private Robert Quigg, has behaved magnificently, going out alone in the face of danger seven times to try and find Sir Harry (Lady Macnaghten''s son). He brought back a wounded man each time. All are very proud of him." It will be remembered that after the “big push,” Sir Harry Macnaghten was reported missing and believed killed. […] The new V.C. was deeply attached to Sir Harry, and did his best to discover and bring him in to his own lines on 1st July. Private Quigg is a native of Ardihennon (Giant''s Causeway), where he was born on 1st March, 1885; was educated at the local national school under Mr. David M''Conaghy, by whom he is described as a diligent and reliable pupil. After his school days he worked for neighbouring farmers, and was eight years in the employment of Mr. David Forsythe, Turfahun. Private Quigg is about six feet in height, and of powerful build. On the formation of the Ulster Volunteer Force he joined the Bushmills Company, and was one of the first batch of recruits who volunteered in September, 1914, for active service. He has participated with great credit in the Ulster Division engagements on the Western war area […] The new V.C. is a member of Aird L.0.L., No. 1195, and a member of the flute band associated with that lodge. His family and he are connected with Billy Parish Church. His father is a well-known guide and boatman at the Giant’s Causeway […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: "Congratulations have been showered upon Mr. and Mrs. Quigg on their son's noble achievement. […] It is told of Private Quigg, by one who knows him, that when he first offered himself to the recruiting officer he was refused. So keen was his disappointment that on returning home he wept. Not to be daunted, however, he again offered himself, and was successful. “The recruiting officer will be gay proud of him now,\" added our informant. None can read the story of Private Quigg's act without feeling thrilled: proud of him as an Ulsterman, and as a unit of the gallant Ulster Division, every man of which, we have been repeatedly told, covered himself with glory on that memorable morning of the Ist July. To say that the people of Bushmills and district are jubilant over the great honour so nobly won by Private Quigg is putting it mildly.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Portrait of Robert Quigg in uniform. Source: ‘For valour’: Ulster VCs of the Great War, Ulster-Scots Community Network, p.11.'
    uuid: 27975a0f-4910-438e-a32d-89d898a71292
    type: content
    enabled: true
    image: Portrait-of-Robert-Quigg-VC.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: "It is interesting, to note how the above account reflects several of the “frames” we noted in Module 2. Private Quigg takes his place as an example of\_"
          -
            type: text
            marks:
              -
                type: bold
            text: the
          -
            type: text
            text: "\_"
          -
            type: text
            marks:
              -
                type: bold
            text: "Ulster Scot as Fighter.\_"
          -
            type: text
            text: 'Like Willie Gilliland or the apprentices in Derry, the narrative of his actions underlines his determination to take individual action against seemingly impossible odds'
          -
            type: text
            marks:
              -
                type: bold
            text: '. '
          -
            type: text
            text: "His physical courage earns him the highest award for bravery in the British army. Importantly, the story shows how the ordinary person can perform extraordinary actions. No-one had ever heard of Private Quigg before the events described. He is projected on to the public stage through the heroism of his actions on that day that saved the lives of several of his comrades. Similarly, Quigg’s story can be seen as illustrating the frame which associates\_the\_Ulster Scot with\_"
          -
            type: text
            marks:
              -
                type: bold
            text: 'loyalty to the British Crown'
          -
            type: text
            text: .
          -
            type: text
            marks:
              -
                type: bold
            text: "\_"
          -
            type: text
            text: 'The brief account of his life story shows how this ordinary farm hand from North Antrim did not hesitate a moment to volunteer to join the army at the very beginning of the war. The reader is to understand that his readiness to defend the country represents the attitudes of the entire community from which he comes. Lastly, the story fits in with what we had said regarding'
          -
            type: text
            marks:
              -
                type: bold
            text: "\_"
          -
            type: text
            text: "the fact that the broader Ulster-Scots narrative of the period tried to underline the high level of cohesion that existed between the different social classes that made up Ulster-Scots society. In the present case, Private Quigg wins his VC while he was engaged in an attempt to save his officer, who was also the son of the local land-owner. When they went to the Front, Quigg acted as his orderly. When Macnaghten went missing after the attack, Quigg went searching for him. Although Macnaghten's body was never found, Quigg managed to rescue seven other wounded soldiers who had been left stranded in No Man's Land after the attack on the 1st July, trailing them back to the British lines on a tarpaulin, under continuous fire from the enemy. His story is therefore a perfect illustration for the image of\_"
          -
            type: text
            marks:
              -
                type: bold
            text: 'cross-class solidarity'
          -
            type: text
            text: "\_that Unionism sought to project into the public imagination."
      -
        type: paragraph
        content:
          -
            type: text
            text: "Quigg is a typical example of the men who spontaneously joined the 36th (Ulster) Division at the outbreak of the First World War. Coming from a rural background, he left school at an early age (probably when he was around fourteen) before going on to work as a farm hand. A member of the Orange Order (LOL 1195), he joined the UVF in Bushmills as soon as it was formed in January 1913. The military training he received in the UVF prepared him for his experience in the war when, along with many of his comrades, he followed Carson’s recommendations and volunteered for the army, joining the 12th Battalion The Royal Irish Rifles.\_Quigg received his VC “for the most conspicuous bravery” from the King at a ceremony at Sandringham on 8th January 1917."
      -
        type: paragraph
        content:
          -
            type: text
            text: "Given the prestige of the Victoria Cross, accounts of Quigg’s actions appear throughout the press at the time. However, unsurprisingly, we get the best idea about his background from the local newspapers. It is here, for example, that we pick up a few valuable clues as to things like language use in the local community. We catch a glimpse of this in the quotation from “our informant,” when he – or she – says: “The recruiting officer will be gay proud of him now,\" a version of what was probably said in the original Ulster-Scots: “gye an proud o im noo.”\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Victoria Cross Medal. By Arghya1999 - Own work, CC BY-SA 3.0: '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=80374505'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=80374505'
    uuid: 50bf9d54-92cf-4e9d-b755-d586556380f3
    type: content
    enabled: true
    image: VC.PNG
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: 'More evidence that Ulster-Scots is there… in the background'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Evidence of Ulster-Scots influence in the local community comes up again in a later article which contains a report of a “reception given by the residents of Bushmills and district to Private Quigg, V.C. in the Hamill Hall” on his return to the area in January 1917 '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ". At this event, an “original poem,” written specially for the occasion, was recited by Mrs King, Principal of Eagry National School. This poem, in honour of the returned hero, describes his actions at the Battle of the Somme, and expresses delight at his return.\_ What is interesting is that the author does not hesitate to reflect the realities of the local community by inserting a short passage in Ulster-Scots. She does so, calling on the (questionable) humour of the period!"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'And here he stands before us all,'
              -
                type: hard_break
              -
                type: text
                text: 'With honour, wealth, and fame,'
              -
                type: hard_break
              -
                type: text
                text: 'Enough to make the proudest girl'
              -
                type: hard_break
              -
                type: text
                text: 'Consent to change her name.'
              -
                type: hard_break
              -
                type: text
                text: 'Small wonder an old woman said;'
              -
                type: hard_break
              -
                type: text
                text: 'When watching him come in,'
              -
                type: hard_break
              -
                type: text
                text: '“If I had ten dochters noo'
              -
                type: hard_break
              -
                type: text
                text: 'He’d get them ivery yin.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: "References like these remind us that Ulster-Scots remained the vernacular for a considerable proportion of the population in the area, as it did in other parts of Ulster. It is interesting to see that, although Ulster-Scots was the target of sustained attack through the national school system, it is precisely Mrs King, Principal of Eagry National School, who recites the poem that contains the above extract in Ulster-Scots. This tells us a lot about the ambiguous attitude of the community towards language use. The teacher, who will of course have the required training in elocution in order to do justice to the English of the poem, will also, hopefully, be able to give an authentic rendering of the Ulster-Scots. The point is that, although key institutions in the community like the school and the Presbyterian Church went out of their way to discourage the use of Ulster-Scots, it proved impossible to eradicate. Both of the extracts show us that, although Ulster-Scots is almost invisible – it represents such a small proportion on the printed page – it is nonetheless still present. What we see, or, rather, what we hear – in both cases, this is the spoken word - is only the tip of the iceberg.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The scale of the tragedy that cut down so many young lives meant that Ulster’s involvement in the Battle of the Somme was to leave an indelible mark on the folk memory of Ulster. In Unit 4, we will see how the unionist authorities were quick to recognise the impact of the Somme when they chose to construct a memorial to Ulster’s dead in the immediate aftermath of the war.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "\_(1)"
                  -
                    type: text
                    text: ' “Private Quigg, VC,” Ballymoney Free Press, January 25, 1917, p.4.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' British Army Graveyard, Somme, July 2019 (Wesley Hutchinson).'
    uuid: d02b5ac0-8dfa-492a-972b-2abe58725ac0
    type: content
    enabled: true
    image: War-Graves.png
    allow_zoom: true
book_title: 'The Somme'
parent_module: 9c23ffe6-99e2-43e8-9a25-f9d49845ff22
example_number: '05'
---
