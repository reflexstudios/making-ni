---
id: 81dd9d9f-0636-4304-9d59-c516f6624738
blueprint: module_3
title: 'T.W. Russell'
parent_unit: first_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654272996
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The Irish Loyal and Patriotic Union (ILPU) was formed in May 1885 to unite the unionist vote in the southern provinces. Although centred on the south, it helped with the campaign in Ulster. It became the basis for the Irish Unionist Alliance in 1891 in time for the campaign against the Second Home Rule Bill. The ILPU was very active during the initial campaign against Gladstone’s first Home Rule Bill. As we saw in example 1 in this Unit, it sent unionist speakers to address audiences in Scotland and England and published a large number of pamphlets containing copies of speeches or other propaganda material. One of these was a speech by T. W. Russell in Grangemouth, Scotland, on 18th May 1886.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Although he had been living in Ireland for some thirty years, Russell had been born in Scotland, in the town of Cupar, just over 40 miles from Grangemouth, where he delivers this speech. His Scottish background and the fact that he had lived in Ulster so long meant that he seemed ideally placed to present the reasons for pro-Union opposition to Home Rule to a Scottish audience. When Russell makes this speech, the Home Rule Bill was still going through Parliament. The meeting was part of the on-going agitation outside Parliament to discredit Gladstone’s policy. As we saw in the Henderson extract, it was particularly important to do this in Scotland as it was one of the strongholds of Gladstone’s Liberal party.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Near the end of this long speech (the original pamphlet is some 31 pages long!) '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ", he reminds his audience of the numerous connections that tie many in Ulster in to a Scottish past. The following extract is taken from that closing section.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Before closing, may I make a final appeal to this audience on behalf of the Loyalist minority? What, I ask, have they done that they are to be deprived of their Imperial inheritance [...]? Three hundred years ago Ulster was peopled by Scotch settlers for State reasons. You are bound to remember this. The men there are bone of your bone, flesh of your flesh. The blood of the Covenanters courses through their veins; they read the same Bible; they sing the same psalms; they have the same Church polity. Nor have they proved altogether unworthy of their ancestry. Two hundred years ago, when the Empire was in peril, the descendants of these Scotch settlers, hunted from post to pillar, remembering that they belonged to an Imperial race, “turned desperately to bay,” '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (2)
              -
                type: text
                text: ' under the walls of Derry, and left a by no means dishonourable record of their prowess for the historian. The descendants of these men have made Ulster what it is. They have turned the most sterile province of Ireland into the most fertile; they have planted industries, and established commerce; the shipyards of Belfast vie with those of the Clyde; the linen trade of Ulster takes its place amongst the great industries of the land. Wherever we find these Loyalists, as Mr. Chamberlain '
              -
                type: text
                marks:
                  -
                    type: bold
                text: '(3) '
              -
                type: text
                text: "has said, “there we find the nucleus of prosperity, order, and industry.” It is the same in Dublin, in Cork, wherever you go Scotchmen and Englishmen are to be found at the head of great business affairs in which capital has been sunk. Their hands are unstained with crime or outrage; they are not the moonlighters or cattle maimers [...]\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' T.W. Russell, “The case for the Irish Loyalists: Being the substance of an address delivered at Grangemouth, Stirlingshire, on Tuesday evening, May 18," Dublin, London, The Irish Loyal And Patriotic Union, 1886. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (2)
                  -
                    type: text
                    text: ' “the imperial race turned desperately to bay” is a reference to the Protestants inside the walls of Derry. It is a quotation from, Thomas Babington Macaulay, The History of England from the Accession of James II, Vol.3.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (3)
                  -
                    type: text
                    text: ' Joseph Chamberlain, former Radical, who became one of the leaders of the Liberal Unionists.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Front cover of the published version of Russell’s speech.'
    uuid: 6f4a2974-8037-446f-870c-219629f002d4
    type: content
    enabled: true
    image: THE-CASE.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The first frame that Russell goes for is '
          -
            type: text
            marks:
              -
                type: bold
            text: 'loyalty '
          -
            type: text
            text: 'to '
          -
            type: text
            marks:
              -
                type: bold
            text: Empire
          -
            type: text
            text: ": “What, I ask, have [the Irish Loyalists] done that they are to be deprived of their Imperial inheritance [...]?” The passage is designed to prove how unwavering their support for the British interest in Ireland and across the Empire has been.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "He begins by reminding his listeners of the Ulster Plantation and states that, if the “Scotch settlers” had moved there, it was “for State reasons.” According to Russell, since it was the State that sent the settlers over in the first place, the State has no choice today but to respect its responsibilities towards them. When he addresses his audience, saying that they “are bound to remember this,” he means that, collectively, British society has a binding duty, a moral obligation to remember the circumstances of the initial settlement and to stand by the descendants of those first settlers.\_Notice that, like Henderson in the previous passage, he makes no mention of the English settlers."
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This focus on the Scottish settlers brings him on naturally to the theme of “'
          -
            type: text
            marks:
              -
                type: bold
            text: 'kith and kin'
          -
            type: text
            text: ",” stating that “[t]he men there [i.e., in Ulster] are bone of your bone, flesh of your flesh.” He can be certain that his listeners will recognise the passage from Genesis in the King James Version of the Bible (Gen. 2, 23). It refers to the creation of Eve from Adam’s rib: “And Adam said, This is now bone of my bones, and flesh of my flesh: she shall be called Woman, because she was taken out of Man.” Just as the Bible portrays Woman as having been “taken out of Man,” so Russell suggests that the Ulster-Scots community has a similarly intimate, physical connection with the Scottish people.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Continuing on the theme of the genealogical bonds that link the settlers in to the Scottish people, Russell says: “The blood of the Covenanters courses through their veins.” In choosing to focus specifically on Ulster’s links to the Covenanters, he is tuning in to the notion of '
          -
            type: text
            marks:
              -
                type: bold
            text: 'persecution '
          -
            type: text
            text: 'that surrounds much of their history. However, rather than develop this theme of repression, he prefers to underline how the descendants of the Scottish settlers in Ulster continue to be motivated by the same religious faith as the people of Scotland: “they read the same Bible; they sing the same psalms; they have the same Church polity.” The focus therefore is on a common '
          -
            type: text
            marks:
              -
                type: bold
            text: Presbyterianism
          -
            type: text
            text: ", one whose preference for “psalms” rather than “hymns” is shared across the North Channel.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He then moves on to another key frame – the idea of '
          -
            type: text
            marks:
              -
                type: bold
            text: 'Presbyterian loyalty'
          -
            type: text
            text: '. His reference to an “imperial race turned desperately to bay” is a quotation from Macaulay’s, History of England [1855]. The historical event referred to is of course the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'siege of Derry'
          -
            type: text
            text: ". Following the usual Presbyterian line that we saw in Module 2, Russell reiterates the idea that it was the Scots – up to this point he has still not mentioned the English settlers – who bore the brunt of the assault on the city. Although he does not say so explicitly, his listeners will know that they are to understand this as clear proof of Presbyterian loyalty to the British Crown.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'The creation of Eve. Woodcut by Hans Brosamer, 1550. Public Domain. Source: The New York Public Library Digital Collections. '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://digitalcollections.nypl.org/items/1299f6c0-63ac-0133-393b-00505686a51c'
                          rel: null
                          target: null
                          title: null
                    text: 'https://digitalcollections.nypl.org/items/1299f6c0-63ac-0133-393b-00505686a51c'
    uuid: 46e409b7-1bc9-4e1c-bb04-0613e45ae194
    type: content
    enabled: true
    image: 1.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The defence of Londonderry leads him on to the recurrent association between the Ulster Scot and '
          -
            type: text
            marks:
              -
                type: bold
            text: 'material prosperity'
          -
            type: text
            text: '. However, unlike what we see, for example, in Hamilton’s, History of the Irish Presbyterian Church, published the same year this speech was made, reference to Londonderry does not lead the speaker on to lament the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'discrimination '
          -
            type: text
            text: "suffered by its former defenders. Instead, we are immediately reminded of the industry and energy of the settlers who “turned the most sterile province of Ireland into the most fertile.” Indeed, there is no mention whatsoever of the Test Act or of any of the legislation passed to restrict Presbyterian rights. Thus we are told that the descendants of the Scotch settlers: “have planted industries, and established commerce; the shipyards of Belfast vie with those of the Clyde; the linen trade of Ulster takes its place amongst the great industries of the land.” It is clear that, as with his reference to the Covenanters, Russell prefers to pass quickly over anything that might interfere with the central line of his argument - the idea of a community totally in tune with its imperial mission and committed to its British identity.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The remainder of the extract moves the focus away from Ulster Scottishness towards the idea of the broader “Loyalist” community in Ireland. It does so quite imperceptibly. Suddenly, there is no more specific reference to Scottish culture, no more mention of “Scotch settlers” – only “Loyalists.” However, thanks to the groundwork he has done, Russell can be sure that many of the essential features of the “Scotch settler” will be extended to the “Irish Loyalist” in the minds of his listeners.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Indeed, the characteristics identified in Module 2 as belonging to the Ulster Scot – “'
          -
            type: text
            marks:
              -
                type: bold
            text: 'prosperity, order, and industry'
          -
            type: text
            text: "” – are, as it were, transferred to the broader loyalist community, not only in Ulster, but throughout Ireland. Thus, we are told: “It is the same in Dublin, in Cork, wherever you go Scotchmen and Englishmen are to be found at the head of great business affairs in which capital has been sunk.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "If Russell associates the “Irish Loyalist” with prosperity and industry, he is also a symbol of an ordered, law-abiding society: “Their hands are unstained with crime or outrage.” The idea is here quite clear: Russell is claiming that whereas “prosperity” and “order” characterise the loyalists, their political opponents, the Home Rulers, are characterised by “poverty” and “disorder.” The references to “moonlighting” and “cattle maiming” are obvious allusions to the activities of the Land War activists (1879-1882) and their campaign of agrarian violence. As we shall see in later speeches and writings, such a black and white reading on the issue of law and order will be a recurring feature of anti-Home Rule material.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "When the Home Rule Bill was defeated in the Commons (June 8) and Gladstone was forced to call a general election, Russell was selected as the unionist candidate in South Tyrone, standing as a Liberal Unionist. He defeated the well-known Nationalist candidate, William O’Brien, who had been elected to the seat the previous December.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In the following extract, by Ulster Scot Jnr., we will see the reasoning behind his selection as a candidate.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ' IMAGE'
                  -
                    type: text
                    text: ': Caricature of T. W. Russell in the magazine, Vanity Fair, March 1888.'
    uuid: 84dc7fbc-aaac-463c-a42d-cd8325ce8061
    type: content
    enabled: true
    image: T-W-RUSSELL.png
    allow_zoom: true
book_title: 'T.W. Russell'
parent_module: 6492b8fc-ba6c-4bb4-a8d1-dcd14b6d92c3
example_number: '02'
intro_text: '“The men [of Ulster] are bone of your bone, flesh of your flesh.”'
---
