---
id: 1da42e59-4f61-43dd-8177-86f0bc026b5a
blueprint: module_3
title: 'A Belfast Scotchman''s Opinion'
parent_unit: second_home_rule_bill
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1655481431
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The constant movement of people in both directions between Belfast and Lowland Scotland over the centuries meant that, as with other areas of rural Ulster, many people in Belfast had strong family and occupational connections on both sides of the North Channel. The lives of the wealthier citizens, much better documented than those of the bulk of the population, illustrate this quite clearly. We think, for example, of people like James Mackie, whose father was from Dumfries, who owned an important engineering firm in West Belfast that produced machinery for the textile industry; or Thomas Workman, whose father was from Ayrshire, who was one of the founders of the major shipbuilding concern, Workman, Clarke and Co., a rival of Harland and Wolff; or again, Sir John Milne Barbour, whose mother was from Edinburgh and who was chairman and managing director of the Linen Thread Company, at one stage one of the world leaders in its field.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "It was therefore not surprising that the energy of this fast-growing Victorian city should attract ever-growing numbers of Scots. The Scottish community in Belfast increased considerably between the middle of the 19th\_century and the period just before the outbreak of the First World War when numbers stood at around 12,500. They were represented in every facet of the commercial and industrial life of the city, from factory workers to factory owners. They worked in sectors like printing, textile production, ship-building, engineering, distilling and trade. They also contributed to the civic life of the city in bodies such as the Chamber of Commerce, not to mention their involvement in the churches, especially the various Presbyterian denominations. Indeed, most Belfast Scots belonged to one of the Protestant churches.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In order to cater for the needs of this growing community, a network of “Scottish” societies was set up in areas such as culture, sports and charity. Thus, we see the creation of the first Burns Club in Belfast in 1872. Its activities were to be widely publicised in the local press over the course of the following decades. As regards sport, apart from golf and bowls, we note that there were at one point no less than three curling clubs in the city. In the area of charity, the Belfast Benevolent Society of St Andrew, set up in November 1867, was created to “alleviate distress” among the poorest of the city’s Scottish-born population. The Society would, for example, pay for funeral expenses or for travel so that Scots who had fallen on hard times could return home to Scotland. Another structure, the Belfast Scottish Association, was set up in 1888 “principally for the benefit of the working classes,” as a “means of forming a closer union and bond of sympathy between the various classes of Scotchmen in Belfast.” Other structures, such as the Scottish Unionist Club referred to in the text in this Example reflected how people of Scots origin were increasingly being encouraged to take a clear position on the key political issue of Home Rule.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Northern Whig, 3th April 1893 – Front-page advertisement addressed to the Scottish people living in Ulster concerning the unionist demonstration planned for the following day.'
      -
        type: paragraph
    uuid: 4b0a8d35-1a37-40e7-bdf9-d87f93c4638f
    type: content
    enabled: true
    image: All-Scotsmen.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The following document is of particular interest because it functions at a number of different levels.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The document begins its life as a business letter written by a Scottish businessman living in Belfast and addressed to what he describes as “a large manufacturer in Scotland” that supplied him with tweed, presumably for a clothing firm.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "However, the author decides to send a copy to the News-Letter, the leading unionist newspaper published in Belfast, because, he claims, he thought the contents might be of interest to the general public. Whether or not he had intended to publish the letter from the start is hard to say.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In any case, once the letter is published in April 1893, it is picked up by the Irish Unionist Alliance, the body that had replaced the Irish Loyal and Patriotic Union in 1891, which in turn decides to use it as part of its propaganda campaign. Such leaflets were circulated at political meetings or handed out by so-called “colporteurs” who distributed election material from door to door in England and Scotland.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The text is therefore of interest as it is an example of the kind of material that was identified as having potential by the people behind the anti-Home Rule campaign '
          -
            type: text
            marks:
              -
                type: bold
            text: "(1).\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'LEAFLET No. 46.                                                                                                SEVENTH SERIES'
              -
                type: text
                marks:
                  -
                    type: bold
                text: '. '
              -
                type: hard_break
          -
            type: blockquote
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'A BELFAST SCOTCHMAN''S OPINION. '
                  -
                    type: hard_break
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'TO THE EDITOR OF THE BELFAST NEWS-LETTER. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'SIR,-The following is a copy of a letter sent by a Belfast firm to a large manufacturer in Scotland, which probably you will consider worthy of a corner in your paper:— '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'DEAR SIRs, I have returned your samples of tweed per Glasgow boat to-night, and in doing so I regret to say I cannot meantime place any orders. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The unsettled condition of Ireland, with the prospects of very serious trouble over this wretched Home Rule Bill is a sufficient reason for coming to this decision. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'I have now been over twenty-four years in Ireland, and up till 1886 was one of Mr. Gladstone''s warmest admirers, and so great was my attachment to him in consequence of the many useful acts he has succeeded in passing for the good of these countries that, if this Home Rule Bill was thrown overboard, very probably I should be disposed to again give him my allegiance. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The Belfast Scotch are almost to a man against the Bill, and on Tuesday last 1,800 of them took part in the great Unionist procession. Now, sir, surely the Belfast Scotch are quite as able to judge as to the results of the passing of this Home Rule measure as their countrymen in Scotland; in fact, it is only natural to suppose that they should (and, I affirm, do) know the Irish question infinitely better. However, all through this momentous struggle no sympathy, no support, has been offered or given by the Gladstonian Scotch to their fellow-countrymen here. Blinded by a glamour of selfish party spirit thrown over them by the evil genius of a great chieftain, they are evidently forgetful of all past history and their obligations to the Irish Presbyterians, and to the very existence of the Protestant faith of the land. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "When one is deserted by his own flesh and blood, and looked upon with contempt for having found it necessary to become a Liberal Unionist, as the writer unfortunately experienced in a recent visit to his native place, it is almost beyond endurance, and this, taken in conjunction with one's Irish friends almost crying out in despair at the unjust and unchristian conduct of the Scotch people, has, indeed, made the past few years rather a dark page in my history.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "I do sincerely hope that there may be brighter days in store for us all, and that soon—very soon–Scotland may become true to itself, and stand firm in the cause of truth and right, and thereby maintain its grand old historic reputation of the past.—Yours truly,\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "A PRESBYTERIAN SCOTCHMAN.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Belfast News-Letter, April 12th, 1893.\_\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "Leaflet No 46, Seventh Series, “A Belfast Scotchman’s opinion,” in\_Irish Unionist Alliance, Publications. Vol. II, Dublin, London and Belfast, The Irish Unionist Alliance,\_[1893], pp. 337-338.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Ad for “Irish and Scotch tweed” in Belfast Evening Telegraph, 22 February 1898.'
    uuid: 13aa2050-0b5d-4a21-bf0d-810e5118f499
    type: content
    enabled: true
    image: 13-Scotch-Tweed-ad.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The author of the letter says he is returning samples to a manufacturing firm in Scotland. He alleges that the political uncertainty of the times means that he is unable to place an order with them. Indeed, he is afraid of widespread political violence – “the prospects of very serious trouble over this wretched Home Rule Bill" - which, he suggests, is likely to undermine the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'material prosperity'
          -
            type: text
            text: " of Ulster. The idea is that business and in particular the trade between the north of Ireland and Scotland is being adversely affected by the renewed prospect of Home Rule.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "However, when one reads the letter, one cannot help but think that there may be more to it than that and that the author is cancelling the order almost as a protest at the attitude of what he calls the “Gladstonian Scotch” to the plight of the unionists in Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The author goes out of his way to give the manufacturer an idea of his background. The detail he gives is surprising in the context of a purely business letter.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "He says that, up until 1886, the year the first Home Rule Bill was introduced, he had been a supporter of Gladstone and the Liberal Party. He alludes to “the many useful acts he has succeeded in passing,” a reference to Gladstone’s record of social and political reform. He identifies the Home Rule issue as the only thing standing between him and renewed support for the Liberal Party.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "He goes on to say that the “Belfast Scotch” are “almost to a man against the Bill.” 1800 of them had shown their disapproval by taking part in a “grand unionist demonstration” in Belfast the previous week attended by Balfour, a leading Conservative politician, former Chief Secretary for Ireland and future Prime Minister. The author’s statement is confirmed by the following extract from the Northern Whig (5th April 1893), which gives an account of the Scottish participation in this spectacular demonstration of the previous day:\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "But perhaps the most striking display was that of the Scottish Unionist Club, the members of which, to the number of about 2,000, marched to the place of meeting from their gathering point in Corporation Square, preceded by full corps of pipers playing “Wha dare meddle wi’ me?” The appearance of this fine contingent, composed alike of classes and masses and swinging along to the stirring strain of the national music [...] created [great enthusiasm] among the crowd [...], and the warlike “skirl” from a couple of dozen sheepskins—powerful and strident though it was - was almost drowned by the storm of cheers which greeted the passage of this noble accession to the local Unionist ranks.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The author does not hesitate to underline the extent to which the position of these expatriate Scots differs from that of the Scots at home. Indeed, he says that “all through this momentous struggle no sympathy, no support, has been offered or given by the Gladstonian Scotch to their fellow-countrymen here.” He states categorically that the “Gladstonian Scotch” - i.e. those who had remained faithful to the Liberal Party line - have allowed loyalty to leader and to party to go before loyalty to their '
          -
            type: text
            marks:
              -
                type: bold
            text: 'fellow Presbyterians'
          -
            type: text
            text: ', of Scottish descent, in Ireland. He sees this as an act of “desertion by '
          -
            type: text
            marks:
              -
                type: bold
            text: 'his own flesh and blood'
          -
            type: text
            text: ",” and talks of the “contempt” shown him in Scotland because he had left the party to become a Liberal Unionist. To make matters worse, his “Irish friends” cannot understand what they see as “the unjust and unchristian conduct of the Scotch people” towards them.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "By signing himself, “A Presbyterian Scotchman,” he is underlining the two primary handles the unionists were trying to use in their “appeal” to Scotland – a shared religion and a common ancestry.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The virulence of language he chooses to use is indeed surprising. It suggests something of the incomprehension felt by many in the Ulster-Scots community at the failure of the Scots, as a nation, to come to their aid. This situation ran contrary to everything that their history had told them about the religious solidarity between Presbyterians on either side of the North Channel.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "By choosing to reproduce the letter and distribute it for use in political campaigning, the Irish Unionist Alliance are clearly trying to shame the Scots into changing their attitudes.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In any case, the technique is of interest, in that it is firmly focused, not on abstract principles or in philosophical debate, but rather on direct, personal experience. This is one man talking about how the experience of living in Ulster has changed his political opinions. He states clearly that he, and “the Belfast Scotch [...] know the Irish question infinitely better” and that their Scots fellow-countrymen should bow to that superior knowledge.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Arthur Balfour. Bain News Service, publisher, Public domain, via Wikimedia Commons '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/wiki/File:A.J._Balfour_LCCN2014682753_'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/wiki/File:A.J._Balfour_LCCN2014682753_'
    uuid: e8915fcf-917c-41d6-8fdf-daf582f3ab21
    type: content
    enabled: true
    image: 2-Balfour.png
    allow_zoom: true
book_title: 'A Belfast Scotchman''s Opinion'
parent_module: 2b84ddf4-6861-4fdd-a014-1a8e9aba79c4
example_number: '04'
---
