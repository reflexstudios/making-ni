---
id: 0fb0ef24-dac4-4223-912a-be192041f609
blueprint: module_3
title: 'The General Assembly'
parent_unit: second_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654872445
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The unionist campaign against Home Rule was keen to use the networks that existed between the Protestant churches across the United Kingdom. Religion was seen as one of the sources most likely to generate sympathy. From the outset, Home Rule, which would have meant a Parliament in Dublin with an overwhelming Catholic majority, was denounced by many as “Rome Rule.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'If it seemed obvious to Irish Protestants of all denominations that they would be assured of support from the overwhelmingly Protestant mainland, this conviction was even stronger with regard to the Scots, whose Presbyterianism'
          -
            type: text
            marks:
              -
                type: bold
            text: ' '
          -
            type: text
            text: 'was shared by a large number of people in Ireland, mainly in Ulster. '
          -
            type: text
            marks:
              -
                type: bold
            text: "(1)\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, things were not always quite so simple. There were many factors that led to the Scots being less open in their support of the Protestant minority in Ireland than unionists might have wished. Although there was always considerable support from individual ministers and congregations, the different Churches often hesitated to become involved. Things were particularly complex in Scotland where, as we have already seen, Gladstone and the Liberals enjoyed high levels of support in the population. There was also a fear that the sectarian tensions that existed in Ulster might be imported into Scottish politics. And then there were issues of internal Scottish politics – for example, the possible disestablishment of the (Presbyterian) Church of Scotland - that complicated even further the relationship between the Churches and the Liberal Government in London.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " According to the 1881 Census, Presbyterians numbered 470,734 people, or 9.10% of the population. They were concentrated in Ulster.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Title page of The Constitution and Government of the Presbyterian Church in Ireland, with a Directory for the Administration of Ordinances, Belfast, 1887. '
    uuid: a4a70882-26e9-4cd0-8148-04b3ad08a385
    type: content
    enabled: true
    image: 1.JPG
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Those organising the unionist campaign were to put a lot of effort into trying to win Protestant opinion in England and Scotland over to their side. The following extract from “Delegation from the Free Church,” published in The Belfast News Letter, on Saturday, June 10, 1893, '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: " - i.e. right in the middle of the increasingly tense debate on the second Home Rule Bill - gives an account of a visit from members of the Free Church of Scotland to the General Assembly of the Presbyterian Church In Ireland.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The General Assembly is the governing body of the Presbyterian Church. It is made up of the minister and an “elder” (elected by the members of the congregation) from each congregation. It meets once a year to discuss business and to elect the Moderator, considered to be primus inter pares, i.e. first among equals. The appointment is for one year. '
      -
        type: paragraph
        content:
          -
            type: text
            text: "The Free Church of Scotland was formed by evangelicals who broke away from the Established [Presbyterian] Church of Scotland in 1843. This so-called “Disruption” was caused by what was seen as excessive State interference in the internal functioning of the Established Presbyterian Church. The Free Church had close ties with the Presbyterian Church in Ireland.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: '“Delegation from the Free Church” contained in the report, “The General Assembly of the Presbyterian Church in Ireland: Fifth Day,” in Belfast Newsletter, June 10., 1893, p. 6.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Burning Bush emblem featured in Sermons by the Ministers of the General Assembly of the Presbyterian Church in Ireland, Belfast, Religious Tracts and Book Depot, 1888.'
    uuid: db4302d9-6b9c-4345-ada4-8c7bd809d7da
    type: content
    enabled: true
    image: LOGO.png
    allow_zoom: false
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Although the Free Church as a whole “had expressed no opinion on the Home Rule Bill,” the following text highlights the fact that divisions clearly existed within the Free Church on the issue and suggests that many of its ministers were prepared to come out in public to declare their support for the Ulster unionist position. The Rev. Kelman, was particularly active in drumming up support, writing letters to The Scotsman and organising what one speaker calls “agitation” on the issue within the Free Church. Kelman has come to Belfast to read an address signed by a number of ministers of the Free Church declaring their “heartfelt sympathy” for the unionists, recognising the “very formidable danger [that] threatened them” and affirming their admiration for “the noble stand that they were making in opposition to that bill.”\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Assembly then received a deputation, consisting of Rev. John Kelman, Leith, and Mr. John M’Candlish [...] Edinburgh, on behalf of the ministers and elders of the Free Church opposed to Home Rule [...] '
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Rev. John Kelman, who was warmly greeted on rising to present the address from the ministers, elders and deacons of the Free Church, said that they did not profess to represent the Free Church of Scotland. The Free Church, as a Church, had expressed no opinion on the Home Rule Bill. But they represented, on the one hand, a number of ministers of the Free Church throughout Scotland; and, on the other hand, a number of elders and deacons of the Free Church in the Presbytery of Edinburgh, who wished to express their hearty sympathy with them in the circumstances in which they were at present placed. He would confine himself to the address by the ministers. [...] '
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Signatures have been furnished to the address from every part of the country, from Shetland to Solway, and from the Hebrides to Peterhead. There were in Scotland seventy-four Presbyteries in the Free Church. From sixty-eight of these signatures have been received. (Applause.) There were only six Presbyteries – six small Presbyteries [...] from which no signatures had come [...] The number of Free Church ministers who had signed the address is 285, and the number of office-bearers in the Presbytery of Edinburgh was 386. But it must not by any means be supposed that that number indicated the whole amount of feeling among the ministers of the Free Church in opposition to the Home Rule Bill, or of sympathy with the Protestants of Ireland in their present struggles. To his certain knowledge there were a considerable number more who were strongly opposed to Home Rule, and whose sympathies were warmly with them [...] He knew of more ministers now who were opposed to Home Rule, but who had not signed the address, than he knew of altogether before the movement they represented began. (Hear. Hear.) '
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Engraving of Rev. James Seaton Reid, in Robert Allen, James Seaton Reid, A Centenary Biography, Belfast, William Mullan & Sons (Publishers) Ltd., 1951. Reid was the author of The History of the Presbyterian Church in Ireland, in three volumes, 1834-1853.'
    uuid: 615793e7-27c4-4fad-8dff-0bba68f2b08e
    type: content
    enabled: true
    image: Rev-James-Seaton-Reid.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Kelman is keen to underline that “signatures have been furnished to the address from every part of the country, from Shetland to Solway, and from the Hebrides to Peterhead.” He confirms this geographical spread by stating that the signatures he had collected came from sixty-eight of the seventy-four Presbyteries in the Free Church. '
          -
            type: hard_break
          -
            type: hard_break
          -
            type: text
            text: 'He shows that support came not only from the clergy but also from the laymen who held office within the Church and cites the example of the Presbytery of Edinburgh where 386 “office-bearers” have put their signatures to the message of support.'
          -
            type: hard_break
          -
            type: hard_break
          -
            type: text
            text: "Kelman points out to his audience that the signatures collected did not give an accurate picture of feeling within the Church on the issue of Home Rule and that the figures he quotes should therefore be seen as only part of the much more widespread sympathy for the unionist cause within the Church.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "However, although he says that only “six small Presbyteries” did not contribute signatures to the address, he does not explain why only the Presbytery of Edinburgh was referred to regarding support among lay Presbyterians. Equally importantly, although he mentions the number of ministers who signed the address (285), we do not know exactly what that figure represents with regard to the number of ministers overall.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Statistics provided by the Free Church in 1888 show that the Church had around 1100 ministers at the time. That means that just over a quarter had come out openly against the official neutrality of the church and stated their support for Irish unionists.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Kelman suggests that this is only the tip of the iceberg and that the level of sympathy within the Church is much stronger than the figures suggest. His reference to “the movement” inside the Free Church of Scotland demonstrates that the unionist case was being actively promoted by a dedicated group – humorously referred to by one speaker as the “Kelmanites” - within the Church. Kelman claims that they are continuing to gain ground among the ministers since there are “more ministers now who were opposed to Home Rule, but who had not signed the address, than he knew of altogether before the movement they represented began.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: "This sympathy for the unionist cause was to continue to grow, especially in the context of the crisis surrounding the third Home Rule Bill.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' The illustration is taken from Rev. Thomas  Brown, Annals of The Disruption; With Extracts  From the Narratives of Ministers  who  Left  The  Scottish Establishment in 1843, Edinburgh, Magniven & Wallace, 1892. opposite p. 242. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'After "The Disruption," those congregations who had joined the Free Church had to build new churches. The Image shows a minister of  the Free Church preaching in the open air to his congregation assembled on the shore.  Brown, commenting on the difficulties of the Free Church, clearly seeks to tie its circumstances in to the history of the early Christian Church and, in so doing, recalls the persecution of the Scottish Covenanters. He says: “Many  a  time  under  those lowly  roofs,  or  out  on  those  bare  hillsides,  men''s  thoughts went  back  to  the  days  of  persecution  when  our  covenanting forefathers  met  for  the  worship  of  God  amid  the  glens  and moors  of  our  native  land,  or  to  scenes  associated  with  memories more  sacred  still — the  river-side  at  Philippi,  where  prayer  was wont  to  be  made — the  boat  floating  on  the  Sea  of  Galilee,  out of  which  One  spake  as  man  never  spake — or  the  lonely  desert which  the  presence  of  God  turned  into  a  Bethel,  the  very  gate of  heaven.”'
    uuid: ae7382a5-2468-494e-a7d3-84e538e0a601
    type: content
    enabled: true
    image: Preachng-at-the-seaside.png
    allow_zoom: true
book_title: 'General Assembly'
parent_module: 2b84ddf4-6861-4fdd-a014-1a8e9aba79c4
example_number: '02'
---
