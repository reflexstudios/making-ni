---
id: 4c069e3a-2e33-4e16-9b49-0764790bc3ec
blueprint: module_3
title: 'Starting The Settlement'
parent_unit: government_of_ireland_act_1920
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1655481591
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Leonard Raven-Hill (1867-1942) was a highly successful cartoonist and illustrator. His work appeared in a number of magazines, most notably in the satirical magazine, Punch, to which he contributed for some forty years. Raven-Hill was conservative in his politics. Well known for his work encouraging men to enlist during the First World War, he produced a number of cartoons on Ireland, especially in the period between the end of the war and the early 1920s.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Raven-Hill’s cartoon, “Starting the settlement,” appeared in Punch on June 8, 1921. The first election to the Parliament of Northern Ireland had taken place some two weeks earlier, on 24th May. As predicted, the Unionists won an overwhelming majority, taking 40 seats, while the Nationalist Party and Sinn Féin won six seats each.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Northern Ireland Parliament was opened on June 22. As we saw in Example 3, the ceremony, attended by King George V in person, was held at the Belfast City Hall. It has to be remembered that when the Northern Ireland Parliament was created under the terms of the Government of Ireland Act (1920), there was no building to house it. Initial work on the Stormont site did not begin until 1923. During the early years of the existence of Northern Ireland, Parliament met at the Assembly’s College (now Union Theological College) in Botanic Avenue, Belfast. Parliament Buildings was officially opened on November 16 1932 by the Prince of Wales, Edward Windsor.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Cartoon by Leonard Raven-Hill which appeared in Punch on June 8, 1921.'
    uuid: 0d3e6d79-b8bc-41d6-90b8-4f55ef713c57
    type: content
    enabled: true
    image: Starting-The-Settlement.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Start by looking at the various parts of the image. In the foreground, we see a man, energetically sawing wood at a sawing easel. We know that this is supposed to represent the new Prime Minister of Northern Ireland, James Craig, because lying on the ground, among a pile of tools to the right is a large leather work bag with “J. Craig” written on it. Craig is not dressed as you might imagine a Prime Minister of the period to be dressed. He is wearing rough working clothes. His sleeves are rolled up, and he has a hat on his head. He is clearly hard at work, totally absorbed in the task of sawing a branch of wood.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He has already cut down a number of trees. There are a number of tree stumps in the ground. So, we can assume that he has been working here for some time. Indeed, he has succeeded in making a clearing in what is obviously very dense forest. In the middle ground, we see a simple log cabin. We can assume that he has built this himself. It is a very rudimentary construction, very much like the log cabins that people would have imagined from the stories they read about the American '
          -
            type: text
            marks:
              -
                type: bold
            text: frontier
          -
            type: text
            text: ". We are to understand that this is meant to represent the Northern Ireland Parliament because nailed on to a tree in the right foreground we see a sign with the words “Ulster Parliament House,” above what is clearly a Red Hand of Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Everything that we have described is bathed in light. This is in sharp contrast with the background which is much darker in overall tone. The house has been built immediately on the edge of rising ground that slopes off to the right. The ground is covered in huge trees with their roots showing. There are heavy shadows everywhere. We can see the sky in the distance. It is full of dark, menacing clouds. There is even a dramatic flash of lightning in the far left of the image. We can see that a number of trees have fallen in the forest. However, they do not look as if they have been felled by the lumberjack. Rather, they look as if they might have been struck by lightning.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The title is an obvious play on the word “settlement” which recalls the settler origins of many of those Ulster Unionists who had fought to oppose Home Rule for Ireland and whose resistance had resulted in the creation of a separate parliament for Northern Ireland.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The immediate reference would appear to be to their ancestors, the first Scottish and English settlers in Ulster, at the time of the Plantation of Ulster. The image recalls the passage we read in Module 2 ('
          -
            type: text
            marks:
              -
                type: bold
            text: 'See: The Ulster Scot and Material Prosperity'
          -
            type: text
            text: "), in which Rev. Hamilton, referring to the arrival of the Scots at the Plantation period, says: “Soon the sound of the axes of many settlers rang through the forests of Ulster, and of their crowbars and hammers in the quarries, as they felled trees and hewed out stone for the erection of homesteads. Castles, houses, and bawns rose quickly all over the country.” This is exactly what we see in the cartoon. Craig is the settler who has cut down the trees, clearing the forest for the construction of this new Parliament, or perhaps this new political entity, Northern Ireland.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "The Cabinet of Northern Ireland in 1921. From left to right, Dawson Bates, Marquess of Londonderry, James Craig, H. M. Pollock, E. M. Archdale, J. M. Andrews. Public domain.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                    text: 'https://en.wikipedia.org/wiki/James_Craig,'
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                      -
                        type: italic
                    text: 1st
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                    text: Viscount
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                      -
                        type: italic
                    text: 'Craigavon#'
                  -
                    type: hard_break
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                      -
                        type: italic
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                      -
                        type: italic
                    text: '/media/File:Northern'
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                    text: Ireland
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                      -
                        type: italic
                    text: Cabinet
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://en.wikipedia.org/wiki/James_Craig,_1st_Viscount_Craigavon#/media/File:Northern_Ireland_Cabinet_1921.jpg'
                          rel: null
                          target: null
                          title: null
                    text: 1921.jpg
    uuid: 5874daf6-b611-4824-80bc-f593b4436c37
    type: content
    enabled: true
    image: Northern_Ireland_Cabinet_1921.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It has to be remembered that Craig is himself of Scots origin. Patrick Buckland, his biographer, reminds us that his “Scottish ancestors had settled in Ulster probably in the seventeenth century but had lived in County Down since the early eighteenth century.” It is interesting to note that Punch alludes to Craig’s Scottish origins, saying that, in political terms, he is “as inflexible as granite,” and comparing him to “his rocky namesake, Ailsa Craig,” (Punch, Nov. 16 1921), the name of the island in the Firth of Clyde off the coast of South Ayrshire opposite County Antrim.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, there may be a second suggested layer of meaning – also linked in to the figure of the Ulster Scot. A contemporary audience might also have seen a reference to Scotch-Irish '
          -
            type: text
            marks:
              -
                type: bold
            text: emigration
          -
            type: text
            text: ' and to their experience on the American '
          -
            type: text
            marks:
              -
                type: bold
            text: frontier
          -
            type: text
            text: ". In his representation of the log cabin and the menacing, virgin forest, Raven-Hill seems to be playing with the Ulster Scot’s links to America and the latter’s role opening up the frontier, constantly pushing deeper and deeper into the forest. As John Gamble said in A View of Society and Manners, in the North of Ireland (See: Module 1), “[The Scotch Presbyterian] has little sentiment of locality, and, therefore, emigrates with an indifference only inferior to that of an American planter, who, having created a beautiful spot in the wilderness, disposes of it, and removes some hundreds of miles to create and abandon in the same manner, another.” The idea would appear to be that these “settlers” are used to life on the frontier and do not hesitate to push deeper and deeper into “the wilderness.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The overall impression does not seem in any way hostile to Craig and the new Unionist project. On the contrary, Craig is shown to be busy with the work at hand and fully concentrated on the immediate problem. The strong shadowing in the background and the storm that is clearly building in the distance suggest that the settlement may be in for some rough times. However, this does not seem to worry Craig. He is so busy that he doesn’t even bother to look over his shoulder at the storm that seems to be brewing in the background.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Are we to read this as Craig being unconscious of the dangers, or somehow indifferent to the threat? Given his reputation as a methodical organiser capable of producing a perfectly oiled political machine, and given Craig’s legendary energy, that would be surprising.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In any case, however a contemporary Englishman might read the cartoon, an Ulster Scot would most likely look at the image very positively. He would notice that, unlike the Plantation period, there is neither “wolf” nor “woodkern” to be seen in this cartoon forest. Similarly, his reading of the ever-growing body of material on the history of the Ulster Scot would invite him to conclude that, as was the case in Ulster and on the American frontier, the “settler” in the image would soon have created a '
          -
            type: text
            marks:
              -
                type: bold
            text: "prosperous\_environmen"
          -
            type: text
            text: 't. So, the suggestion is that, given Craig’s dogged determination and single-minded focus, the cabin will be solid enough to withstand whatever the storm may throw at it. '
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Opening of the new Parliament Buildings, of Northern Ireland, at Stormont, 1932, Commemorative Medal.'
      -
        type: paragraph
    uuid: 7d7831b7-9594-4096-a12b-38a8e6ce1724
    type: content
    enabled: true
    image: IMG_3832.JPG
    allow_zoom: true
book_title: 'Starting The Settlement'
parent_module: b3bc48fa-27a8-467f-86cb-366253976041
example_number: '04'
---
