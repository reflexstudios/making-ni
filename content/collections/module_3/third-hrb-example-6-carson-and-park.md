---
id: e022fb3a-92f4-488d-b4b0-2ce96571045b
blueprint: module_3
title: 'Carson & Park'
intro_text: '“Since the last annual meeting, two other notable names have been added to our roll of Honorary Members…”'
parent_unit: third_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654278276
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Certain sections of the Irish-American community had been very active in their support for the demands of Irish nationalism over the years. They had organised tours of the main American cities for key figures like the nationalist leader Charles Stewart Parnell (1880), generating considerable public interest and sympathy and collecting huge sums of money for the campaign back in Ireland. They had lobbied politicians and church leaders and had ensured the Irish Question attracted the attention of the American press.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'When the United States entered the war as an ally of the British in April 1917, support for Irish independence, in whatever form, had to be weighed against America’s support for the allied war effort. Irish republicans like Sir Roger Casement had tried to get German help for the Rising in Dublin in 1916. However, a year later, men from the Irish-American community were being asked to join the American army and fight alongside the British against the Germans. After the war, solidarity between allies was a key factor in the decision to keep Irish representatives out of the Peace Conference that met in Paris in January 1919 to discuss reparations and lay down the new post-war order.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Unsurprisingly, events in Ireland were also reported on in the Scotch-Irish community in America. Although they often claim to be keen to avoid political controversy, when you read between the lines, it is clear that their sympathies often lie with the unionists. This impression is confirmed in the volume containing the report of the 26th Annual Meeting and Dinner of the Pennsylvania Scotch-Irish Society (1915) which carries as its frontispiece a photograph of “The Right Honourable Sir Edward Carson, M.P.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In the introduction to this volume, we also learn that Carson had just been elected as an honorary member of the Society. “Honorary membership” is offered to someone because a Society admires this person’s achievements or position on a given issue. The public nature of the choice reflects on the Society as a whole. The remarks on this new member, described as a “great British statesman, whom Ulster follows with such dauntless courage and unfaltering trust and devotion,” leave no-one in doubt as to the warmth of their feelings towards him.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The same year, the Society also elected Rev. William Park, Minister of the Rosemary Street Congregation, described as “the oldest Presbyterian Church in Belfast.” Once again, we remark this close association between Scotch-Irish and Presbyterian identity.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The author of the introduction concludes: “The Pennsylvania Scotch-Irish Society is gratified and honoured to have these [two] great Ulstermen on its roll of members.”\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Portrait of Edward Carson published by the Scotch-Irish Society of Pennsylvania in the report on their 26th Annual Meeting and Dinner (1915).'
    uuid: b9854e0a-0178-4b3e-9a9f-0357254342d0
    type: content
    enabled: true
    image: MOD3-UN3-EX6.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Several years later, the two names crop up again together in connection with the activities of the Society. The report of the 30th Annual Meeting and Banquet of the Society, published in 1919, '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
          -
            type: text
            text: 'features messages from both Carson and Park, dated February 1919. Both men refer to the end of the war and focus on the cooperation between Britain and America. Carson salutes, “[t]he joint efforts of America and the British Empire in fighting the battle of freedom for an ideal which we all equally love,” while, according to Park: “The United States and Great Britain have never been brought so close together in friendly co-operation as during the past year or two,” i.e. since America joined the war alongside Britain.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Park does not hesitate to remind his American audience of the nature of Ulster’s contribution to this combined effort, and sends the Society a detailed account of Ulster’s involvement in the war. The following extracts are taken from this paper, dated February 5th, 1919, and later published by the Society. Park takes a pedagogical line and explains to his Scotch-Irish audience the circumstances in which “Ulster” – clearly he means unionist Ulster – moved from threatened rebellion to being one of the principal sources of support for the British – and allied – war effort.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Dear Sir: — As you are holding your Annual Dinner on the 21st of this month, may I be permitted, as one of your honorary members, to send you a word of greeting? \_ The United States and Great Britain have never been brought so close together in friendly co-operation as during the past year or two. The presence of your President in our country, and now in France at the Peace Congress, is an outstanding event, and his wise words as to a League of Nations for preventing war will not soon be forgotten.\_ \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "Ulster, as I am sure you know, has done her part nobly in the great conflict now happily ended. […] \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "When the European war broke out Ulster was in the position of a community which was threatened with the forcible deprivation of what it considered its most precious rights. The armed forces of the British Empire were being mobilized in order to compel the Ulster people to acquiesce in what Protestant Ulster thought (and still thinks) would be separation from Great Britain. In defence of its rights and to protect the Union which the British Parliament (but not the British people) seemed bent on dissolving, Ulster had been forced, reluctantly, but as the result of intense conviction, to take steps to maintain its rights. […]\_ Nobody could have been surprised if under the circumstances when the war broke out Ulster had stood aloof or sulked. But there was no such feeling in the loyal province. The moment the German danger was made manifest the entire population of Unionist Ulster rallied to the cause of the Empire and of liberty with an overwhelming enthusiasm.\_ \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "Our trusted leader, Sir Edward Carson, issued his call to arms, and in a few weeks the splendid Ulster Division was enlisted. […] \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "They received their baptism of fire on […] the opening day of the gigantic battle of the Somme. They went over the top before Thiepval with the old cry on their lips that was heard two centuries ago on the walls of Derry, the cry of \"No Surrender.\" […]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "The contribution of Ulster to the British Army was not, however, limited to the Ulster Division.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The Belfast [ship]yards were in the forefront of the anti-submarine war. They built many of the "Q" boats, which proved so successful in combating the under-water pirates. […] '
          -
            type: paragraph
            content:
              -
                type: text
                text: "The Handley-Page aeroplanes which proved the decisive weapon in the air warfare were manufactured by Messrs. Harland & Wolff […] It is a great feather in Ulster's cap that what were […] the most effective military aeroplanes used by the British Army were built in a city and in a yard that had no experience whatever of aeroplane manufacture until the third year of the war. […]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "All the linen used in the making of aeroplane wings for the British Army was manufactured in Ulster. […]\_ \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "It is not too much to say that it was the linen industry of Ulster which proved the decisive factor in the Allied victory in the air. Without the North of Ireland linen, our fighters and our aeroplane manufacturers would have been helpless. There would have been no aeroplanes for the Allied armies or too few to be of service. And without sufficient aeroplanes the Allied victory would have been impossible.\_ \_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: 'Thirtieth Annual Meeting & Dinner of the Pennsylvania Scotch-Irish Society, at The Bellevue-Stratford, Philadelphia, February 21, 1919, Philadelphia, Allen, Lane & Scott, 1919, pp.44-50. '
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' "Peace Celebrations in Belfast,” The Weekly Telegraph, 16th August, 1919.'
    uuid: e5f65eb0-7bd8-4b0e-8cf1-ee2a14cfab52
    type: content
    enabled: true
    image: Screenshot-2022-06-03-at-18.41.10.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Park begins his account by recalling the increasingly tense stand-off between the unionists and the British Government over the third Home Rule Bill. He points to the fundamental reason for unionist resistance – their refusal to accept “separation from Great Britain.” Park is clearly eager to have his audience understand that unionists believed Home Rule was, in reality, only the first step towards complete Irish independence.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The objective was therefore “to protect the Union” against the actions of the British Parliament which appeared determined to dissolve the Union against the wishes of “the British people.” Park insists on the paradox of the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'loyalist rebel'
          -
            type: text
            text: ": “Our people were being forced into revolt in order to prevent the British Empire from taking steps for its own dissolution.” \_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, when the war broke out, the UVF that had been formed and armed to resist “the armed forces of the British Empire,” obeyed Carson’s call and “rallied to the cause of '
          -
            type: text
            marks:
              -
                type: bold
            text: 'the Empire'
          -
            type: text
            text: ' and of liberty” by joining the British army. (Cf. '
          -
            type: text
            marks:
              -
                type: bold
            text: 'The Ulster Scot as fighter'
          -
            type: text
            text: )
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Park’s account of the 36th (Ulster) Division insists on their “baptism of fire” at the Somme. When he says the men “went over the top before Thiepval with the old cry on their lips that was heard two centuries ago on the walls of'
          -
            type: text
            marks:
              -
                type: bold
            text: ' Derry'
          -
            type: text
            text: ', the cry of ‘No Surrender,’” he is sure his audience will immediately recognise the historical reference. The state of Pennsylvania, where this particular Scotch-Irish society is based, has a town called Londonderry'
          -
            type: text
            marks:
              -
                type: bold
            text: ','
          -
            type: text
            text: " named after the city in Ulster by Ulster-Scots migrants recently arrived in the new world.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'References such as these make it clear that Park is eager to emphasise the historical and ethnic links that exist between the “Ulstermen” and the Scotch-Irish diaspora (Cf. the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'Kith and kin'
          -
            type: text
            text: " frame). He is equally keen to suggest how that shared past continues to manifest itself in choices made in the present day, such as the decision to join the war against Germany.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Later in the text, Park goes on to record the key role played by Belfast’s industries, especially its ship-building and aircraft industries and its linen production (linen being used to make airplane wings at the time). He is doing this for several reasons. By emphasising Ulster’s industrial base, he is pointing to the fundamental difference that exists between this province and the rest of – a predominantly agricultural - Ireland. But, above all, by showing the extent of Ulster’s contribution to the “Allied armies” and the “Allied victory,” he is reminding his audience how Ulster helped contribute to a war in which American troops were directly involved. The message is not hard to see – it is thanks to the efficiency of Ulster’s ship-building and aeronautical industries that many American lives were saved.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: 'IMAGE:'
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "\_"
                  -
                    type: text
                    text: "Photo of Harland and Wolff shipyards taken in 1911.\_Public Domain:"
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "\_"
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=7416860'
                          rel: null
                          target: null
                          title: null
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=7416860'
    uuid: 80376d47-732a-4622-9c90-26ba51d41f29
    type: content
    enabled: true
    image: 'Knocking_off_at_Harland_&_Wolff,_Belfast.jpg'
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Park never launches into a direct attack against the nationalists or their powerful Irish-American supporters. Just as was the case for many Irish nationalists, who had volunteered to join the British Army during the First World War, many Irish-Americans had fought in the American armed forces. It was therefore important not to attack Irish America as a whole. However, the “elephant in the room” is clearly Irish republicanism’s hostility to the allied war effort and republican links with Germany during the war. Thus, Park congratulates the members of the Pennsylvania Scotch-Irish Society, saying, “the presence of your President [Woodrow Wilson] in our country, and now in France at the Peace Congress, is an outstanding event, and his wise words as to a League of Nations for preventing war will not soon be forgotten.” In doing so, Park is reminding them that, despite pressure from the American Senate, Wilson did not challenge his British ally’s decision to prevent Irish representatives from attending the Peace Conference where they intended\_ to call for “the establishment of Ireland as an Independent Nation.” Clearly, Wilson had accepted London’s position that Ireland was a matter of internal British politics.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "What Rev. Park is doing here is extremely interesting at a number of levels.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In Module 2, we saw how the Scotch-Irish community in America was increasingly involved in putting together a “connected history” of their community. Organisations such as the Scotch-Irish Society of America and individual authors such as Charles A. Hanna, author of The Scotch-Irish or The Scot in North Britain, North Ireland and in North America (1902), or C.K. Bolton, author of Scotch-Irish Pioneers in Ulster and America (1910), traced their roots back to Scotland and Ulster, and celebrated the contribution of the Ulster Scot in the construction of America. This work was by no means confined to America. In 1912, the American ambassador to the UK, Whitelaw Reid, published The Scot in America and The Ulster Scot. This book, published in London, was based on two highly successful lectures he had given in Edinburgh (November 1911) and Belfast (March 1912), in which he summed up current American readings of Scotch-Irish history. \_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In many ways, Rev. Park is reversing the direction in which this Scotch-Irish history was being written. Rather than looking at Ulster and the history of the Ulster Scot from an American standpoint and within an essentially historical perspective, he is consciously putting forward a history of Ulster’s war record from a unionist perspective and aiming it explicitly at the Scotch-Irish community in America. Park does not insist on the specifically Ulster-Scots contribution to the war effort. Rather, he writes from the broader perspective of “Unionist Ulster.” What he is doing is to take advantage of what unionists clearly see as a receptive audience in the Scotch-Irish community in order, as it were, to “bring the Scotch-Irish story up to date.” Park is trying to make sure that the members of the Pennsylvania Scotch-Irish Society will be able to “apply” their history to the contemporary situation. His message is very simple: the Ulster Scot is now a unionist and Scotch-Irish sympathies should be firmly unionist as well.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "As will be confirmed in the next section, unionism sought to exploit the Scotch-Irish networks such as these to encourage Americans to look at Ireland from the altered perspective of post-war alliances.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Whitelaw Reid (in top hat), as American Ambassador, attending a ceremony in the presence of King George V. The Sphere, 20th July 1912.'
    uuid: 3a34b2b8-8cce-40f4-9eb9-3c08bfc1641e
    type: content
    enabled: true
    image: 12-American-HAC-1912.png
    allow_zoom: true
book_title: 'Carson & Park'
parent_module: 9c23ffe6-99e2-43e8-9a25-f9d49845ff22
example_number: '06'
---
