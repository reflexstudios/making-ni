---
id: cc5acc9d-2305-4260-a3eb-d5e7872378a9
blueprint: module_3
title: 'Bab M''Keen'
intro_text: '“Things in General. Hame Rule in Parteeclar”'
parent_unit: third_home_rule_bill
updated_by: 73b51ea8-5c46-4529-be4c-3ec8796f6b5f
updated_at: 1655739820
audio: Mod-3-HRB3-ExBab--1638558883.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: italic
            text: 'The Ballymena Observer'
          -
            type: text
            text: ", which began publication in 1855, served the mid-Antrim area centred on the market town of Ballymena. The area had a large Ulster-Scots population. Starting in the late 1870s, the paper featured a regular column in Ulster-Scots by a certain Bab M’Keen which continued over several decades. At this period, Bab M’Keen was John Wier, the paper’s editor and owner,\_whose fame - and readership - stretched as far as the United States and Canada."
      -
        type: paragraph
        content:
          -
            type: text
            text: "\_Frequently, the subjects Wier chose were humorous, but, as this text shows, Ulster-Scots was also used in the local press of the day for serious political comment. The column is entitled, “Amang oorsel’s.” This suggests that the Ulster-Scots that would have been spoken by many of his readers was being used to create a greater sense of intimacy between the editor and his public.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The dear, but am thankfu’ am here this nicht tae throw anither shot when I micht been amang the majority, an’ the hale o'' you weedows an’ orphans. I little thoucht the ither day when I said ’at Ulster was in a bleeze ’at the fire was sae near us. I kent an’ sae did the maist o’ you ’at the lowe was close, but I thoucht naebody wud a said as a prophet ’at last Seturday wud seen a fire lichted ’at wudna maybe been put oot in oor time. But accordin'' tae a’ accoonts, that wae sae, an'' am thankfu’ we hae escaped for sae far. It''s easy seein’ noo what was the meanin’ o’ the speeches o’ Winston Churchill an’ a‘ the ither scoondrels ’at wud let war loose on this country. They kent what only some were aware o’ ’at a’ the arrangements had been made an’ wur giein’ due warnin’. But they didna coont on what happened at the Curragh; that was ayont their ken. There was a Providence workin’ there they hadna calculated on. Anither auld sayin’ broucht in true: the wisest schemes—an’ sae on. […] '
              -
                type: text
                marks:
                  -
                    type: bold
                text: '(1) '
              -
                type: text
                text: 'It’s aye the darkest ’oor afore the dawn, an’ although the licht lucked lang o’ comin’, it was pleesant for some folk tae see it at last. I b’lieve we’ll aye be in the toon for a wee langer.'
              -
                type: hard_break
          -
            type: paragraph
            content:
              -
                type: text
                text: "I hae been readin’ ower the excuses the Government’s makin’ aboot sendin’ in these sodgers intae Ulster, an’ am juist fair scunnered wi’ them. They’re a’ a pack o' leears an’ prevaricators, Guid forgie me for callin’ them names, but I canna keep mysel’ in seein’ hoo near we wur a’ o’ bein’ sent oot o’ the worl’ withoot iver maybe haein’ time tae bliss oorsel’s. There was the twa gunboats in Belfast Lough an’ aboot a dizzen ithers ordered in, an’ sodgers iverywhaur, an’ talk o’ a big ragement o’ Irish Volunteers in Derry: that’s no’ the Ulster yins; an’ us here juist in the middle an’ atween. Wasn’t it weel we had a wheen Volunteers o’ oor ain tae fa’ back on; am thinkin’ sae. The drillin’ these last six months hasna a’ been lost. There wud been some shootin’ afore a wud been a ower, an’ through it a’ naebody has been doon hearted. An’ it’s a’ ower noo for a wheen days tae somethin’ else turns up. But I foresee an election afore lang ’at’ll settle metters. It’s peace we’re in need o’. Some day you’ll a’ be in my wye o’ thinkin’.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "\_(1)"
                  -
                    type: text
                    text: ' The complete saying is: “the wisest schemes are broken by unexpected accidents.”'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Between Oorsel’s, Ballymena Observer, 27 April 1914.'
    uuid: 8c581ecc-752c-4e23-ba8b-100e8852d4c3
    type: content
    image: 'between-oorsel''s.png'
    audio_file:
      - audio/6.-Third-Module-Passage-1.wav
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The extracts'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1) '
          -
            type: text
            text: "concern an exceptionally tense period in the “Ulster Crisis.” They refer to a series of inter-related incidents in March 1914 sparked by the decision of the Asquith government to put on a show of force to try to cow Ulster unionists into accepting his Home Rule legislation then going through Parliament.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "M’Keen refers to the dramatic appearance in Belfast Lough on the morning of Saturday 21st March of two British cruisers, Attentive and Pathfinder. The ships went to Carrickfergus where they dropped off a company of the Yorkshire Light Infantry. The Attentive then crossed the Lough to anchor in Bangor.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This was part of a much larger operation which envisaged moving large numbers of troops towards strategic sites in Ulster from bases in the south of Ireland and Scotland. The army was to be supported by a number of navy vessels waiting off the coast of Ireland. M’Keen clearly sees this strategy as being totally irresponsible. He suggests that, had things gone according to plan, there would have been a clash with the Ulster Volunteers, with serious loss of life as a result. He says that sending troops into Ulster would have started a fire “’at wudna maybe been put oot in oor time.” As editor of an important local newspaper, Wier was of course following events with particular attention. This explains his caustic remark regarding Winston Churchill, First Lord of the Admiralty. The previous week in Bradford, Churchill had made a speech, quoted in The Times, in which he had said that force might have to be used against the Ulster unionists to make them accept Home Rule, but that, in any case, there were "worse things than bloodshed even on an extended scale." Wier alleges that comments such as these show that this operation had been on the cards for some time.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'However, things had not gone according to plan. “But they didna coont on what happened at the Curragh; that was ayont their ken.” This so-called “Curragh incident” had taken place the day before, March 20th.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Commander-in-Chief of the army in Ireland, Lieutenant-General Paget, had received orders from London to move troops north to Ulster. However, when these orders were communicated to officers at the Curragh (Co. Kildare), one of the most important army bases in Ireland, they understood that the operation might not be limited to maintaining order. Indeed, it seemed to some that the sudden arrival in Ulster of troops in such numbers might provoke the unionists into active resistance. This would lead to a clash with the army. Whether or not the Government had foreseen such a scenario, the entire operation soon came to a grinding halt because an overwhelming majority of the officers, led by Brigadier-General Hubert de la Poer Gough, immediately indicated that they would rather resign their commissions than move against the Ulster unionists. A telegram sent to the War Office on the evening of Friday 20th March read: “Officer commanding 5th Lancers states that all officers except two, and one doubtful, are resigning their commissions today. I much fear same conditions in the 16th Lancers. Fear men will refuse to move.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This was particularly unwelcome news to the Government as it suggested that it could no longer count on the loyalty of the military. Although the dispute was quickly papered over, with none of the officers actually resigning, the question of Home Rule was clearly driving a wedge through British society.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "\_(1) "
                  -
                    type: text
                    text: 'Ballymena Observer, Friday, March 27, 1914, p. 7.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: "\_Photo of General Hubert de la Poer Gough, circa 1916, by official British military photographer. Public Domain: \_"
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=7526837'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=7526837'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Gough had fought in India and in the Boer War, where he led the force that relieved the town of Ladysmith which had been under siege by the Boers for months (See Module 2, Empire).\_"
    uuid: 8e826740-ad9f-474d-908b-d529905201c5
    type: content
    image: 18-LtGenHubert_de_la_Poer_Gough.jpg
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: "M’Keen interprets the cause behind this unexpected reversal to “Providence.”\_ He says: “There was a Providence workin’ there they hadna calculated on.” This tunes in to a theme running through Ulster-Scots culture that suggests the community is somehow placed under a form of divine protection. As with the "
          -
            type: text
            marks:
              -
                type: bold
            text: 'siege of Londonderry'
          -
            type: text
            text: ", where the besieged were relieved in extremis, so here, when it seemed their “the darkest ’oor” had come, they were saved from the threatened confrontation by the unexpected refusal of the officers to obey orders.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'M’Keen’s column goes on to refer to the UVF. He says that, but for what happened at the Curragh, the unionists would have found themselves cornered.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Indeed, M’Keen identifies a double threat. On the one hand, there is the Government, whom he describes as a “pack leears an’ prevaricators” and whom he accuses of being prepared to unlease war on the unionists in order to force them to accept Home Rule. On the other, there is the “big ragement o’ Irish Volunteers,” i.e. the nationalist paramilitary force that had been formed (November 1913) in response to the Ulster Volunteer Force (created January 1913). In these circumstances, M’Keen asks his readers: “Wasn’t it weel we had a wheen Volunteers o’ oor ain tae fa’ back on?” and goes on to say: “The drillin’ these last six months hasna a’ been lost.” M’Keen seems to think the UVF is a last line of defence in what he suggests could turn into a three-cornered fight between the UVF, the army and the nationalist volunteers: “There wud been some shootin’ afore a wud be a ower.” (Cf. the frame of '
          -
            type: text
            marks:
              -
                type: bold
            text: the
          -
            type: text
            text: ' '
          -
            type: text
            marks:
              -
                type: bold
            text: 'Ulster-Scot as rebel '
          -
            type: text
            text: and
          -
            type: text
            marks:
              -
                type: bold
            text: ' the'
          -
            type: text
            text: ' '
          -
            type: text
            marks:
              -
                type: bold
            text: 'Ulster-Scot as fighter'
          -
            type: text
            text: )
      -
        type: paragraph
        content:
          -
            type: text
            text: 'M’Keen’s position reflects the relief of many in the Ulster-Scots community and indeed across the unionist population. The Curragh proved that there was support for the unionist position at the very heart of the British army. This reflected support for the unionist cause running right through the British establishment, in the Conservative Party, the Protestant Churches and the'
          -
            type: text
            marks:
              -
                type: bold
            text: ' Empire'
          -
            type: text
            text: '. His remark, “I b’lieve we’ll aye be in the toon for a wee langer,” suggests that Home Rule will prove to be more difficult to implement than many in the Government believed.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Winston Churchill leaving Admiralty House (1912). Public Domain:  '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=684509'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=684509'
    uuid: 8164ff43-e884-4138-8e18-a56e2d37b187
    type: content
    image: Winston_Churchill_verl_sst_das_Geb_ude_der_Admiralt_t_(1912).jpg
    enabled: true
    allow_zoom: true
book_title: 'Bab M''Keen'
parent_module: 9c23ffe6-99e2-43e8-9a25-f9d49845ff22
example_number: '03'
---
