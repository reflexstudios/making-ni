---
id: 4c01053c-0a47-4346-ac09-4bd4d0ed79ae
blueprint: module_3
title: 'The Predominant Partner'
parent_unit: second_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1655630440
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "In the nineteenth century it was common for political cartoonists to represent the various nations of the United Kingdom as male characters: John Bull (the Englishman), Pat, or Paddy (the Irishman) and Sandy, or Jock (the Scotsman).\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The figure of John Bull was originally created by John Arbuthnott at the beginning of the 18th century. In most British cartoons of the period, John Bull is represented as a well-off, well dressed, friendly farmer who is fond of his beer and his beef. Unsurprisingly, when he appears in Irish nationalist or republican cartoons the image is much less affectionate. Pat, the Irishman, has an even more chequered history. At times, he is represented as quick-witted and charming; at others, he is given a more sinister, threatening appearance deliberately intended to make him resemble an ape. In such explicitly hostile, racist representations, he is more often referred to as Paddy, rather than Pat. When he talks, the spelling often makes fun of his pronunciation and grammar. Of all the national personifications, Paddy is the only one to have such an obviously negative variant. Sandy or, sometimes, Jock, is the Scot. Wearing tartan, Sandy is often represented as a canny, elderly man who pays great attention to what he spends.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The unionists used variations of these figures to project an idealised image of the harmony and unity that they saw existing inside the Union. The cartoon, opposite, entitled \"No Home Rule,\" printed in Belfast, by J. Cleland and Son Ltd., is a classic example of this approach, showing the four national stereotypes – Taffy is included to represent Wales - linked arm in arm and smiling warmly as they say in unison: “United we stand. Divided we fall.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Several of these figures feature in the two cartoons on the following screens, produced by the Irish Unionist Alliance for use in its campaign in Britain. They are of interest to us because they illustrate very clearly the efforts that unionists were making to design material that was “custom-built” for a Scottish audience. The cartoons were produced during 1894 or the early part of 1895. '
    uuid: 0ba9884b-76a1-4cb6-8dc3-a96495aa7ece
    type: content
    enabled: true
    image: MOD3-UN2-EX6-min.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The first of our cartoons is directed at people across England and Scotland. It shows two characters, John Bull and Pat. Pat is sitting on a chair with “Dublin” written on it. He has his feet on another chair, with “Westminster” written on it. His attitude is clearly off-hand, if not actually ill-mannered. He seems to be smoking a small pipe and does not offer to take his feet off the chair on John Bull’s approach. When asked if one seat isn’t enough for him, he answers that he will \"make a bit of room” but that John Bull should not be “after expecting a chair to [him]self.”\_ Pat is showing that, in the words of the cartoon, he has become “the new predominant partner.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The notion of the “predominant partner” is a reference to a controversial speech made by Lord Rosebery, the Liberal leader who succeeded Gladstone as Prime Minister in March 1894. In it, he said that, in terms of its electoral weight, England should be recognised as the “predominant partner” in the United Kingdom, and that the Liberals’ main objective should be to ensure that English opinion, with its Conservative majority, would be won over to the idea of Home Rule. The cartoon clearly plays with this idea and says that, thanks to the Liberals’ alliance with the Irish Nationalists, it is no longer England, but Pat (i.e. Ireland) that has become “the predominant partner.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The “points” underneath make it clear that the cartoon concerns the provision that Ireland should be allowed to send 80 MPs to Westminster - a reduction from the 103 seats Ireland currently held - even though there would be a Parliament for Irish affairs in Dublin. This was a major change from the First Home Rule Bill which had foreseen excluding Irish representation from Westminster. Initially, under the 2nd Home Rule bill, Irish MPs were to be limited to participating only on Irish and imperial affairs. As a Liberal Party pamphlet, ‘An Outline of the Home Rule Bill,’ explained (January 1893): “inasmuch as they obtain a Legislature with full powers over Irish affairs, they will not be permitted to speak or vote on English and Scotch affairs.” However, that restriction was lifted in the course of the bill’s passage through Parliament. The unionist pamphlet looks at this change very negatively, foreseeing that Irish involvement in Westminster will consist solely in “obstructing” British reforms, “turning out” British ministries and “spending” British money obtained from English and Scottish tax-payers.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "Leaflet No. 6, Eighth series, “The New Predominant Partner,” in Irish Unionist Alliance, Publications, Vol. IV, Dublin, London and Belfast, Irish Unionist Alliance; Dublin, Hodges, Figgis, and Company, Limited,\_[1895], p. 111."
    uuid: a3530d22-02bd-4286-8dbb-564728edf7a9
    type: content
    enabled: true
    image: Predominant-Partner-1638618819.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The second cartoon is a re-cycled version of the former. What is new here is that “Sandy” - Sandy is an affectionate term for Alexander - the Scot, appears in the picture alongside John Bull. In this version, the two of them arrive together and try to sit down on a “Westminster” bench marked “England and Scotland.” When they see Pat insolently spread out on the two seats, they ask in unison if he cannot be satisfied with one chair and not take theirs as well. Once again, he says that he’ll make room for them, without offering to take his feet off the chair.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Apart from this new addition to the visuals, the leaflet makes another important change to the text it includes underneath the cartoon drawing. Rather than listing the “Two main points in the Home Rule Bill,” the alternative, Scottish version includes a quotation from R. Wallace, described as a “Gladstonian MP.” The text is important because it reinforces the change introduced into the drawing, by focusing people’s attention more particularly on Scotland.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Wallace was a Liberal MP for a Scottish constituency (East Edinburgh), and in the speech, delivered in the Commons during the third reading, he draws the attention of Scots to the way the Bill would affect them, if it were to be voted into law. He focuses on the fact that, under the new terms of the Bill, the 80 Irish members who would be able to sit and vote on British affairs in Westminster would outnumber the 72 MPs representing Scotland.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The unionists picked up on this argument, delighted to be able to have a “Gladstonian MP” recognise that, when Westminster came to discuss Scottish issues, the Irish would have a greater say in the outcome than the Scots themselves! On the other hand, the Scots would have no say whatsoever in the internal running of an Irish legislature. This was accompanied by an entire battery of jokes about the Irish having two votes while the Scots – and the English – would only have one: "One man one vote, one Irishman two votes." It was not hard to see how this could be turned into excellent propaganda material.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'A third significant change has found its way into the second version. The first cartoon has one misspelling – “shure” – and, quite unnecessarily, introduces an Irish structure, translated as “after +ing,” to suggest Pat’s origins. The second cartoon, aimed at Scotland, insists much more heavily on these features. On top of the elements already mentioned, it includes the words “ye” and “yez” and inserts “intirely” (again purposely misspelt) at the end of the sentence. The result reinforces the caricatural nature of Irishness, a mocking representation that corresponds closely to the “stage Irishman” who was a popular standard in the English theatre of the period.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The cartoons point to what many people considered to be a very serious problem – the fact that the Irish would be able to run their own affairs – “separately and securely transacted by the Irish themselves in Ireland” – while at the same time having a say in the way the other parts of the United Kingdom were run. Many thought that this would give the Irish leverage in the future by negotiating even more independence in return for support on this or that issue before Parliament.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In the end, the bill was, as expected, defeated in the Lords.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "When it came to the election in the summer of 1895, although the Liberal Party remained the largest party in Scotland, the number of seats it held fell from 51 to 39, all 12 seats being won by Conservatives and Liberal Unionists.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "It looked as if Unionist arguments were having an effect on Scotland after all.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "Leaflet No. 14, Eighth series, “Pat, ‘Predominant Partner’,” in Irish Unionist Alliance, Publications, Vol. IV, Dublin, London and Belfast, Irish Unionist Alliance; Dublin, Hodges, Figgis, and Company, Limited,\_[1895], p. 149."
    uuid: f723a643-dfa6-4337-b65b-d9b1f627431d
    type: content
    enabled: true
    image: 'Pat,-Predominant-Partner-min-1638618901.png'
    allow_zoom: true
book_title: 'The Predominant Partner'
parent_module: 2b84ddf4-6861-4fdd-a014-1a8e9aba79c4
example_number: '06'
---
