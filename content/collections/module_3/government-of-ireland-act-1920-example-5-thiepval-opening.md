---
id: 64c90d38-7625-4341-bdd7-b35e6a288b24
blueprint: module_3
title: 'Thiepval Opening'
intro_text: '“This little bit of transplanted Ulster […] under the shadow of Helen’s Tower”'
parent_unit: government_of_ireland_act_1920
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654279494
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The 36th (Ulster) Division that had been created from the ranks of the UVF suffered heavy losses at the Battle of the Somme on July 1st 1916. Contemporary reports speak of some 5,500 officers and men killed, wounded or missing on the first day of the attack. This event had a devastating effect on families throughout the province of Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Four days after the Armistice was signed on 11th November 1918, Carson wrote the following letter to the local press:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Sir,—The end of the war immediately recalls to our minds the gallant deeds of the Ulster Division, and particularly those who have lost their lives in Flanders.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "I suggest that a monument be erected on a suitable site on the battlefield to commemorate for all time the heroic deeds of those men, and to that end that a Fund be started in which all sympathisers may participate, any balance to be devoted to the U.V.F. Patriotic Fund.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I shall be glad if any readers who desire to be associated with this memorial will forward their contributions to Lieut.-Colonel Sir Craig, Bart., D.L., M.P., at the Old Town Hall, having kindly consented to act as Honorary Treasurer.—Yours truly. EDWARD CARSON, Old Town Hall, Belfast, 15th November, 1918.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The 36th was at the very heart of unionism’s conception of loyalty to their British identity. Given the circumstances in which this letter was written, it is quite normal Carson should focus on “the gallant deeds of the Ulster Division” and the particularly heavy losses that had been suffered on the western front.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "However, it is important to remember that Ulster’s contribution to the British war effort went well beyond the iconic sacrifice at the Somme. Many Ulstermen, from both traditions, joined regiments other than those that made up the 36th. This was notably the case for Scottish regiments such as the Argyll and Sutherland Highlanders and 4th Seaforth Highlanders, who had a recruiting office in Belfast. The decision to join a Scottish regiment reflected the importance of on-going family links with Scotland. Many men from the nationalist community also joined the ranks of 10th and 16th (Irish) Divisions formed at the beginning of the war.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Ulster Tower in Thiepval has today become a key site commemorating all of Ulster’s war dead in the First World War.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Ulster Tower, Thiepval, June 2019 (Wesley Hutchinson).'
    uuid: c662f1b5-027b-42f5-907a-6441e67bc334
    type: content
    enabled: true
    image: Thiepval-Tower---Wesley.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "In October 1919, Craig attended an exhibition of models for designs for war memorials organised by the Royal Academy in London. Disappointed by what he saw, he suggested that, rather than using any of the government-approved models, the committee should come up with some Ulster landmark that would give the project a more recognisably Ulster identity.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "One of the possible models was Helen’s Tower at Clandeboye in County Down. Helen’s Tower, designed by the Scottish architect, William Burn, had been built in 1861 by the future first Marquis of Dufferin and Ava (see Module 2) as a tribute to his mother, Lady Dufferin, some six years before her death. Many of the men of the 36th (Ulster) Division, especially those from Belfast and neighbouring areas, had done their basic training in a camp that had been opened at the outbreak of the war at Clandeboye in the grounds of the Dufferin estate.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Such connections were important when it came to making the final choice for the monument. However, it is likely that there was more to the choice than this. It is no coincidence that people like Craig, himself an Ulster Scot (see Example 3 in this Module), should have chosen as the preferred model a building that was built in what is known as the Scottish baronial style. This style took its inspiration from the Scottish tower houses of the late Middle Ages. Victorian architects played with features like battlements, gables, turrets, conical towers and steep roofs that conjured up ideas of rugged independence and a romantic past. The style was very much in fashion from the mid nineteenth century across the United Kingdom. Balmoral Castle in Scotland, built for Queen Victoria, or Stormont Castle in Dundonald, Co. Down, are well-known examples of this architectural style.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "But again there is more to it than just a question of which style happens to be in or out of fashion. Opting for Helen’s Tower as a model for the Ulster Tower in Thiepval reflects fundamental cultural choices that run deep into the history of the region. The dominant culture of the region in which the original Helen’s Tower was built is unmistakably Scots. As we have seen, the north of the Ards Peninsula was one of the first parts of Ulster to be settled by Scots at the very beginning of the 17th century. It is at the heart of what is often seen as a “cultural province” that extends from Lowland Scotland into the north-eastern counties of Ulster. These settlements formed the nucleus and defined the characteristics of what is imagined as a distinct community. It is precisely this space that is seen as being behind the identity from which the new Northern Ireland emerged. Deciding to build a replica of this particular tower in France to the memory of Ulster’s war dead is a way to underline the importance of this heritage. The tower can therefore be seen as a fundamental, conscious statement of cultural identity.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Helen''s Tower By Ross, CC BY-SA 2.0: '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=9154608.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=9154608.'
      -
        type: paragraph
    uuid: e953e505-63ae-4715-88c5-74b078deb762
    type: content
    enabled: true
    image: 8-Helens-Tower-1644248235.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "This is all the more important in that the Ulster Tower was to be the first permanent image that the new Northern Ireland projected on to the international stage (see black & white photo opposite). That image of a stark Scottish tower house, perfectly designed for self-defence, emerging out of a desolate landscape and surmounted by the Union Jack, is in many ways a perfect image of the embattled state of Ulster unionism in November 1921 when Northern Ireland’s very existence was still hanging in the balance.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Ulster Tower was officially opened on 19th November 1921 by Field Marshall Sir Henry Wilson, Chief of the Imperial General Staff. Although they had been the prime movers behind the project, neither Carson nor Craig were present at the ceremony, both claiming to have been indisposed at the last minute. The opening came as the British Government was in talks with Sinn Féin following the War of Independence. These talks were to lead, only a few weeks later, to the signing of the Anglo-Irish Treaty which created the Free State, while allowing Northern Ireland to retain its present status. However, many in the unionist camp were unsure of this outcome. They did not trust the Government to have unionist Ulster’s best interests at heart. In the words of the Belfast Telegraph, 21 November 1921:'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Today the British Cabinet is closeted with some of the men who took part in that shocking betrayal of their country [the Easter Rising of 1916], and are bartering the rights of those who fought for them to those who fought against them. They seem to think that by so doing peace can be brought to Ireland […] We wish the Prime Minister better company.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Such mistrust might explain why Carson and Craig preferred to stay in Belfast instead of going over to France to be present at the official opening of the tower.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Meanwhile, the ceremony went ahead in the presence of relatives of the dead being commemorated by the monument. The regional and national press were there in force to record the events of the day. The following extract comes from an article in the Northern Whig, entitled, “The Ulster War Memorial,” published on 21 November 1921.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'But doubtless the last thoughts of many of the heroes who fell that day were of the quiet home scenes, such as that which is commanded by the Clandeboye Monument, reproduced on the ‘famed Picard field’ where they fell. To many of them indeed “Helen’s Tower” had been a familiar landmark from boyhood. It met the gaze of the Belfast boys on their frequent train trips to Bangor. Not a man from County Down and few from Antrim or Derry or Armagh but had seen the tower and heard or read of its story.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'So this little bit of Ulster in Picardy seems particularly appropriate as a monument to the fallen – far more appropriate than would a conventional ‘victory’ trophy with sculptured battle scenes upon its faces and groups of dead and engines of death clustered around its base.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'To many of the pilgrims of the party the familiar outline of the tower seemed as grateful as is the first glimpse of M’Arts’s Fort to the Belfastian returning to his native city after a long sojourn abroad.'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'From the summit a wide tract of country is commanded, and some of the travellers fancied that they could recognise in parts of the area features reminding them of Down and Antrim and Derry and Armagh […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Much as we sympathised with them we could not of course enter into the hearts of the bereaved widows and parents and friends among our pilgrims who looked from the tower at the cemeteries dotting the slopes and tried to fix the spots where their loved ones lay. But it seemed to us that it must be soothing to them to know that, though buried on the field of glory where they fell, their soldier heroes are sleeping in this little bit of transplanted Ulster and under the shadow of Helen’s Tower awaiting the great Reveille.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Opening of The Ulster Tower 19th November 1921. Belfast Telegraph, November 22nd, 1921.'
    uuid: b36630f9-f653-42d6-b37f-22c6adb0cd5a
    type: content
    enabled: true
    image: Thiepval-Opening-min.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The passage is unusual for a newspaper report supposedly designed to inform its readers about events at the official opening of the tower. It operates at a radically different level, focusing on a fundamental aspect of the reality behind the Ulster Tower – the way in which the tower replicates Ulster in France.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The whole passage is about seeing and imagining.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "It talks about the way Helen’s Tower had been “a familiar landmark” that had “met the gaze of the Belfast boys on their frequent train trips to Bangor.” When the author says, “Not a man from County Down and few from Antrim or Derry or Armagh but had seen the tower and heard or read of its story,” he is saying that this is an instantly recognizable icon to everyone in what happens to be the four predominantly unionist counties that make up the core of Protestant, unionist Northern Ireland.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "When these people arrive in France, they see this familiar tower “reproduced on the ‘famed Picard field’.” These “pilgrims” – for this is a holy site – are “grateful” to see “the familiar outline of the tower.” They find this reassuring; it makes them feel at home in what for most will be a strange environment. The reason for this is that the tower is a “little bit of Ulster in Picardy.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'But this feeling of familiarity is not confined to the tower itself. When the people climb the tower and look out “from the summit” over the landscape “some of the travellers fancied that they could recognize in parts of the area features reminding them of Down and Antrim and Derry and Armagh.” What is happening here is of considerable interest. The Ulster landscape emerges out of the battlefields of Picardy. But, once again, it is a specific Ulster landscape. By reiterating the four counties already mentioned, the author, without actually saying it, leads the reader to understand that this is a resolutely unionist space.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'What they see, or rather what they imagine they see, reflects what those Ulstermen who died here imagined they saw the day of the Battle of the Somme - the “last thoughts of many of the heroes […] were of the quiet home scenes, such as that which is commanded by the Clandeboye Monument.” Just like the “pilgrims,” the young men who died on this devastated landscape imagine for a second that they are back at home in their familiar surroundings. The author is saying that they are indeed at home since they are now “sleeping in this little bit of transplanted Ulster and under the shadow of Helen’s Tower awaiting the great Reveille.”'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'PHOTO: '
                  -
                    type: text
                    text: 'Plaque in the Memorial Chamber, at the base of the Ulster Tower, Thiepval, June 2019 (Wesley Hutchinson).'
    uuid: 4c7ea588-5605-4378-873e-65ce723f5696
    type: content
    enabled: true
    image: Plaque.png
    allow_zoom: true
book_title: 'Thiepval Opening'
parent_module: b3bc48fa-27a8-467f-86cb-366253976041
example_number: '05'
---
