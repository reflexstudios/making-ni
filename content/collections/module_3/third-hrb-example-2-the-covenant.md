---
id: cb400513-7769-4b80-b94e-570f58b56f40
blueprint: module_3
title: 'The Covenant'
intro_text: '“God grant we be as leal to these as were the men of old.”'
parent_unit: third_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654277546
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "“Ulster Day,” organised on 28th September 1912, was designed to convince public opinion in the United Kingdom and throughout the Empire that Ulster Unionism was determined to resist Home Rule, at whatever cost. The events of the day were built around the signing of Ulster’s Solemn League and Covenant, a condensed, indeed terse statement of the reasons for Ulster’s rejection of Home Rule. The text committed those signing it to using “all means that may be found necessary” to resist the implementation of Home Rule. It was interpreted as a formal announcement by the Ulster Unionist leadership that they were prepared to enter into armed resistance against the British Government.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The decision to focus resistance to Home Rule on a “covenant” was a clear cultural statement. The idea, found throughout the Bible, of a solemn binding agreement between God and “his” people, had a major impact on the reformed tradition. As a means of galvanising popular commitment to a political and religious cause, the covenant became a hallmark of Scots Presbyterianism. Thus, on three occasions, in 1581, 1638 and 1643, the Scots entered into a covenant to defend their system of Church government against papal authority and rule by bishops. Two of these covenants, the National Covenant, 1638, and the Solemn League and Covenant, 1643, were administered to the Presbyterian settlers in Ulster. Those who committed themselves to these Covenants were known as Covenanters. As we saw in Modules 1 and 2, the influence of the Covenanters extended deep into Ulster Presbyterian society.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "When it was decided to attempt to produce a document that would encapsulate unionist resistance to Home Rule, the leaders tapped into this cultural foundation. Thomas Sinclair, whom we came across in the preceding Example in this Unit, reworked the archaic wording of the original documents and produced a dense, clear text which reflected the efficiency and urgency that characterised unionist material at the time.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Unsigned copy of the Ulster Covenant Public Domain: '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=7418230.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=7418230.'
    uuid: 77c22944-152d-4767-9d6c-06fbaf5c0318
    type: content
    enabled: true
    image: Ulster_Covenant.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The signing of the Solemn League and Covenant was an immense strategic success. Carefully managed signings were organised all over Ulster, most notably in Belfast City Hall where Carson, the unionist leader, was the first to sign in the glare of the national media. Ulstermen who lived outside Ulster were also invited to sign the Covenant on Ulster Day. This was notably the case in Scotland where the local Unionist associations organised signings in several locations, notably in Lanarkshire and around Glasgow where over 7000 people signed. All told, the Ulster Covenant was signed by 218,206 men, while some 228,991 women signed a parallel “Ulster Women’s Declaration.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The following poem, “The Blue Banner,” written by William Forbes Marshall (1888- 1959) was published in The Northern Whig, Belfast’s Liberal Unionist paper, to coincide with the signing of the Covenant on Ulster Day. It plays on the associations with the Scottish Covenants and, in so doing, underlines the deep cultural and historical connections between the Ulster Scot and “the land from whence [he] came.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Marshall, who was 24 at the time, was to become a well-known author, publishing collections of poems such as Verses from Tyrone, a novel, Planted by a River (1948), and an important booklet, Ulster Sails West (1943), outlining “the part played by Ulstermen in building the United States.” '
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'The Blue Banner'
          -
            type: paragraph
            content:
              -
                type: hard_break
              -
                type: text
                text: 'Firm-leagued we face the future, tho'' the road be dark and steep; '
              -
                type: hard_break
              -
                type: text
                text: 'The road that leads to honour is the lonely road we keep.'
              -
                type: hard_break
              -
                type: text
                text: 'And, though all the world forsake us, this is the course we hold. '
              -
                type: hard_break
              -
                type: text
                text: "The course our fathers followed in the Cov’nant days of old.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We fain'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' (1)'
              -
                type: text
                text: ' would look for comfort to the land from whence we came.'
              -
                type: hard_break
              -
                type: text
                text: 'Where still abide our kith and kin and clansmen of our name, '
              -
                type: hard_break
              -
                type: text
                text: 'Where lives were deemed of small account by valiant men and true. '
              -
                type: hard_break
              -
                type: text
                text: "For Christ, His Crown, and Cov’nant and the war-worn folds of blue.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Long years have been and faded since the old-time banner waved. '
              -
                type: hard_break
              -
                type: text
                text: 'See! how it flashes once again ere dangers must be braved!'
              -
                type: hard_break
              -
                type: text
                text: '“The Cov’nant oath we now will swear that Britain may be told'
              -
                type: hard_break
              -
                type: text
                text: "We stand for faith and freedom and the memories of old.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '“For all they died for gladly in the homeland o’er the sea,'
              -
                type: hard_break
              -
                type: text
                text: 'For blood-won rights that still are ours as Ulsterborn and free, '
              -
                type: hard_break
              -
                type: text
                text: 'For the land we came to dwell in, and the martyrs’ faith we hold - '
              -
                type: hard_break
              -
                type: text
                text: "God grant we be as leal to these as were the men of old.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: '                                                                                          W.F.M.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Gladly, willingly.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "\_"
                  -
                    type: text
                    text: 'W. F. Marshall. Source: USCN booklet, The Life, Work and Legacy of Rev. W. F. Marshall, available at: '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "\_"
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'http://www.ulster-scots.com/uploads/USCNWFMarshall.pdf'
                          rel: null
                          target: null
                          title: null
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: 'http://www.ulsterscots.com/uploads/USCNWFMarshall.pdf'
    uuid: 5866156f-40ae-40af-ba44-13294072fb6d
    type: content
    enabled: true
    image: Frame-182.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As often in political material of this sort, the poem, published in the Northern Whig on Saturday 28th September 1912 (Ulster Day), draws a parallel between a dramatic event or period in history - “the Cov’nant days of old” - and the contemporary situation. The reference is to seventeenth-century Scotland and to the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'religious persecution '
          -
            type: text
            text: "of the Presbyterians who had signed the Covenant in defence of the Presbyterian Church and its form of church government.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Throughout the poem, the focus is very much on the Scots origins of the contemporary unionists who are represented as modern-day Covenanters. Marshall returns to this theme at several points throughout the poem. Scotland is identified as “the land from whence we came,” or, “the homeland o’er the sea.” Once again, as we have seen elsewhere, the author insists on the fact that this historical connection is still as valid as ever: Scotland is the place “where still abide our kith and kin and clansmen of our name.” This emphasis on Scotland as country of origin ('
          -
            type: text
            marks:
              -
                type: bold
            text: 'kith and kin'
          -
            type: text
            text: ") is a clear cultural choice on the part of the poet. Whereas much unionist material will at least pay lip-service to parallel English roots, the author does not hesitate to go all-out for Scotland as the exclusive frame of his origin myth.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Marshall, who had trained at the Presbyterian College in Belfast, and who was to be ordained into the Presbyterian Church the following year (1913), sees this Scottish identity as being inseparable from '
          -
            type: text
            marks:
              -
                type: bold
            text: Presbyterianism
          -
            type: text
            text: ". However, he specifically chooses the Covenanters - often portrayed as among the most radical, most uncompromising elements in the Presbyterian family - as his models for the contemporary Ulster unionists. The fact that Ulster unionism should have chosen to produce its own Covenant, inspired from the earlier Scottish model, is of course in the minds of everyone reading the poem in the Northern Whig on Ulster Day. But, as we had already seen in Module 2, this choice merely reflects the close ties that existed historically between the Covenanters and the Presbyterian communities in Ulster. People like the visionary 17th century preacher, Alexander Peden, frequently visited Ulster to escape government persecution in Scotland.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'The Blue Banner. Image Courtesy of Ulster-Scots Community Network. '
    uuid: f4bcd078-e672-4df7-9250-177e236ea667
    type: content
    enabled: true
    image: Covenants-RGB-HR.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Besides these religious or historical considerations, another reason why Marshall chooses to focus on the Covenanter is that it allows him, and, by extension, the broader unionist campaign, to tap into the pathos surrounding the Covenanter as '
          -
            type: text
            marks:
              -
                type: bold
            text: victim
          -
            type: text
            text: ".\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Many of the stories in popular representations of the Covenanters focus on their sufferings at the hands of a cruel government. Everyone in Ulster would have heard accounts of “the Killing Times” that stirred up empathy for the victims and outrage at the perpetrators of what was seen as unjustified State oppression. The familiar folklore of the Covenanters therefore had the advantage of providing sharp images of “goodies” and “baddies” that could be easily transposed to the contemporary situation. In this scenario, the authorities in London did not come off well. Tuning in to this broad story and the emotions that it generated allowed the unionists to take on the role of victims, as it were, by proxy.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Other aspects of the Covenanter narrative make it ideal as a model for contemporary unionism. For example, when Marshall refers to “the lonely road we keep” or when he says, “though all the world forsake us,” he is tuning in to another central element of the Covenanter imaginary, i.e. the idea that they are a tiny minority – the Covenanters frequently used the biblical notion of “the remnant” – with few to support them but themselves, but who, nonetheless, are sure of ultimate victory because of their commitment to “Christ, His Crown, and Cov’nant.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Finally, the choice of the word “leal” in the final line of the poem requires a brief comment. Firstly, the word is Scots, an adjective meaning “loyal” or “faithful” in terms of allegiance or duties. The fact that Marshall chooses this, the only word of Scots in the poem, suggests that he means a particularly Scots brand of '
          -
            type: text
            marks:
              -
                type: bold
            text: loyalty
          -
            type: text
            text: ". Given the context, the reader is to understand that the loyalty involved is to the Covenant - here, Ulster’s Solemn League and Covenant. Like their ancestors, the unionists are represented as “firm-leagued,” meaning that they have chosen to bind themselves by a solemn oath to resist Home Rule. Their loyalty therefore is specifically towards their own political project which, like “the men of old,” they see as being placed under divine protection.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As so often before, this reading of loyalty places them at odds with the Government of the day. As we shall see, the notion of the “'
          -
            type: text
            marks:
              -
                type: bold
            text: 'loyalist rebel'
          -
            type: text
            text: "” was to take on increasing importance in the following months with the creation of the Ulster Volunteer Force.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Opening section of the Solemn League and Covenant, 1643. '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://earlofmanchesters.co.uk/the-treaty-against-a-king-the-1643-solemn-league-and-covenant/'
                          rel: null
                          target: null
                          title: null
                    text: 'https://earlofmanchesters.co.uk/the-treaty-against-a-king-the-1643-solemn-league-and-covenant/'
                  -
                    type: text
                    text: ' '
    uuid: c9b0fde9-08b1-400f-a015-a0bc147bc153
    type: content
    enabled: true
    image: 5-Solemn-League-and-Covenant-1643.png
    allow_zoom: true
book_title: 'The Covenant'
parent_module: 9c23ffe6-99e2-43e8-9a25-f9d49845ff22
example_number: '02'
---
