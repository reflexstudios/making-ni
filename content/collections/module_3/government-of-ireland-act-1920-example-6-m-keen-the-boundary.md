---
id: ccc69764-e80e-4004-8167-e22478c22bf6
blueprint: module_3
title: 'The Boundary Question'
parent_unit: government_of_ireland_act_1920
updated_by: 9cc8b8c2-b549-440c-97b4-781eeb9f3623
updated_at: 1655740275
audio: MOD3-UN4-EX6-SCR3-1638559102.mp3
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Boundary Commission was set up under the terms of the Anglo-Irish Treaty signed in London between the British Government and the representatives of the Sinn Féin leadership on 6th December 1921. According to section 12 of the document, the Commission’s function would be to "determine in accordance with the wishes of the inhabitants, so far as may be compatible with economic and geographic conditions, the boundaries between Northern Ireland and the rest of Ireland."'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Unionists were bitterly hostile to this provision as they said the issue of the border had already been settled by the 1920 Government of Ireland Act that had set up Northern Ireland. In order to get the work of the Commission off the ground, the British Government ended up appointing a representative on their behalf, J.R. Fisher, a unionist intellectual and former editor of the Northern Whig. Nationalists, on the other hand, were much more positive. They were convinced that the criterion of the “wishes of the inhabitants” would lead to areas with a Catholic nationalist majority being transferred to Dublin. Thus, according to the Londonderry Sentinel (June 11, 1925), “Nationalists [claimed] the counties of Tyrone and Fermanagh, the city of Derry, and portions of the counties of Londonderry, Armagh and Down.” They hoped that the loss of such large areas would weaken Northern Ireland to such an extent that it would soon have no choice but to become part of the Free State or, perhaps, a future Republic.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The Commission finally began its work in 1924 under the chairmanship of Mr Justice Richard Feetham, a judge of the Supreme Court of the Union of South Africa. One of the biggest problems it faced was how it should interpret Article 12 of the Treaty.  Could the Commission \"re-draw\" the border from scratch? Or should it  limit itself to merely \"adjusting\" the existing boundary? In the end it was decided that the Commission did not have a mandate to introduce fundamental changes to the 1920 settlement and could only envisage marginal transfers of land on either side of the existing border.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The Commission divided its time between London and travelling on the ground in areas on both sides of the existing border. It took evidence in the form of written and oral statements from public bodies such as urban and rural district councils, professional associations, and political registration associations, as well as from private individuals such as those with commercial or business interests. Before it was ready to publish its report, its main findings, recommending limited transfers of territory in both directions, were leaked to a British newspaper, the conservative Morning Post, which published a proposed map of the changes on 7th November 1925.\_The resulting scandal led to the resignation of the Free State representative, Eoin MacNeill, and the stalling of the entire process.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In order to break the deadlock, tripartite negotiations then began between London, Dublin and Belfast and the result, announced on 3rd December 1925, was that the existing border was to be confirmed exactly as it stood, the Free State being released from the payment of certain financial obligations by way of compensation.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'This map appeared in the Northern Whig and Belfast Post, on the 9th November, 1925. It is a copy of the original map of the Boundary Settlement, as leaked by the Morning Post, on 7th November, 1925.  '
    uuid: 1535775e-9b5d-41be-9ffd-7c33aa99959d
    type: content
    image: Screenshot-2022-05-24-at-16.22.25.png
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'How were these events perceived by the unionist population of Northern Ireland?'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Hugh Shearman was a unionist author who produced a number of important texts putting forward the position of the unionist authorities in Northern Ireland. These included Not an Inch (1942), focused on the period leading up to the creation of Northern Ireland, and Modern Ireland (1952), “written for young people in the senior classes of our schools,” which provided a history of the country from a unionist perspective. The following extracts are taken from Anglo-Irish Relations (1948) (1),\_ in which Shearman deals with the work of the Commission."
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'There was very intense feeling over the frontier question. Craig made it clear that he and his colleagues would tolerate no revision of what had been offered and accepted in 1920 […] In December 1925, therefore, [Stanley] Baldwin [Conservative British Prime Minister] held a conference of the heads of the Irish governments, and a tripartite agreement was signed, amending […] the agreement of 1921 […]'
          -
            type: paragraph
            content:
              -
                type: text
                text: "The new agreement was made in an atmosphere of great cordiality, and it was recorded in the text that the three governments were ‘resolved mutually to aid one another in a spirit of neighbourly comradeship’. The main provisions were (1) that the boundary was to remain unaltered; (2) the Free State [would no longer have to] undertake a share of the public debt of the United Kingdom\_ […]; and (3) the Council of Ireland was abolished […]"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The close of 1925 thus saw a stabilization in Ireland. The two new governments had been set up and were functioning normally. Their areas of jurisdiction had been settled and their constitutions and relations had been defined. The Free State government ruled over twenty-six counties and about three million people, and the government of Northern Ireland exercised a limited rule under the United Kingdom government, over six counties and about one and a quarter million people. (Shearman, pp. 204-206)'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Although the inhabitants of several Protestant enclaves along the border, especially in Donegal and Monaghan, were bitterly disappointed by this result (See the short Northern Whig article opposite), the majority of the unionist population was clearly relieved by the confirmation of the status quo.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Equally disappointed were the large numbers of Catholic nationalists that found that their areas were not, as they had hoped, going to be transferred to the Free State. Far from leading to the collapse of Northern Ireland as a separate political entity, the result of the negotiations, confirming the existing border, actually strengthened the position of the Unionist authorities.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Hugh Shearman, Anglo-Irish Relations, London, Faber and Faber, 1948.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Northern Whig, 5th December, 1925. '
      -
        type: paragraph
    uuid: fdb81779-6f41-449a-afa8-a2fdeadb8219
    type: content
    image: Screenshot-2022-05-24-at-16.14.16.png
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The sense of relief referred to in Shearman''s text comes out strongly in the following extracts from articles in Ulster-Scots that appeared in the Ballymena Observer at the end of 1925. As we saw in an earlier section of this Module (see "Things in General. Hame Rule in Parteeclar"), the Ballymena Observer, which served the mid-Antrim area, featured a regular column in Ulster-Scots by a certain Bab M''Keen, a pen-name for the owner and editor of the paper, John Wier.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Although he often took a light-hearted tone, Bab did not hesitate to comment on more serious matters that affected the life of the local community. The column was often made up of a number of short bite-size snippets commenting on this or that event. Sometimes the Ulster-Scots material comments on the same subjects that appeared in English in the same newspaper. This is the case, for example, with \"Message from Sir James Craig: Statesmanship Has Succeeded,\" which appeared in December 1925. (See opposite.) Wier devotes a number of these short passages in Ulster-Scots to the question of the Boundary Commission.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the following extract'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1), '
          -
            type: text
            text: 'Bab discusses the outcome of the negotiations on the Boundary question as seen from the perspective of the Ulster-Scots community in County Antrim.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Daur I say word or twa aboot the boundary? It micht be as much as my heid wud be worth, but I think I’ll risk it. Weans, it's comin' near the end, an’ them 'at’s no’ content may juist chew the cud, an’ be gled they hae onythin’ ava tae masticate. Naebody need iver a had ony expectation o’ gettin’ iverythin’ they wanted. Alang the boundary it has been kin’ o’ a fricht; farther in, it’s juist a feelin’; but a wheen o’ days noo ’ll show daylight on the lie o’ the lan’. It’s a peety there iver had been ony difference, but in twa’r three years there’ll niver be anither word, I trust, amang us. That’ll be the Millenium for poor oul’ Ireland. Och aye, we need a rest sometime, tae get red o’ a’ oor tribulations.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Och hone, och hone!'
              -
                type: text
                marks:
                  -
                    type: bold
                text: ' (2)'
              -
                type: text
                text: " We’ll be a’ left alone,\_"
              -
                type: hard_break
              -
                type: text
                text: "Wi’ no’ excuse for a grumble,\_"
              -
                type: hard_break
              -
                type: text
                text: "Oul Erin at peace, a’ troubles tae cease,\_"
              -
                type: hard_break
              -
                type: text
                text: "The dear keep us patient, an’ ’umble.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He returns to this issue in the same column a few weeks later'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (3)'
          -
            type: text
            text: '. '
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "There's nae use in me sayin’ onythin' mair aboot this boundary business. Except maybe a wheen ’at cud be pleased wi’ naethan’, nearly iverybody’s clappin’ hisel’ on the back, ’at oor troubles ir ower, at ony rate for the present. Great Britain has come oot weel, am tellt in money metters. ’Deed, I weel b’lieve John Bull wud be gled tae dip intae his troosers pockets tae his knees, tae get red o’ the Irish bother. It was nearly a peety tae let him aff sae chape, but it’s dane noo. Some o’ his needs though it wud been difficult tae get. It’s no’ an easy jab, oot o’ empty breeks takin’ a big sum o’ money. I wud say noo, the less talk aboot the metter the better, either in or oot o’ Parliament. The origin o’ the cause o’ the square up was like a “bolt frae the blue.” Let naethin’ be dane noo tae mak’ the \"turn-oot” onythin but a success. Withoot ony o’ us cryin’ oot for ower much, we’re a’ cryin’ oot for peace onywye.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: 'Bab M’Keen, “The Boundary,” in The Ballymena Observer, November 20, 1925, p. 3.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: "(2)\_"
                  -
                    type: text
                    text: "From the Irish, “ochón,”\_meaning “alas.” It is interesting to note that someone using Ulster-Scots does not hesitate to use expressions in Gaelic.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (3)
                  -
                    type: text
                    text: " “The Boundary Buried,” in The Ballymena Observer, December 18, 1925, p. 3.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' "Message from Sir James Craig: Statesmanship Has Succeeded," Ballymena Observer, 11th December, 1925. '
    uuid: 28f37528-b27c-4aad-b7de-a5f3f9495456
    type: content
    image: Ballymena-Observer.png
    audio_file:
      - audio/MOD3-UN4-EX6-SCR3B.mp3
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Despite their many differences in style and tone, these texts by Shearman, Craig and M'Keen  give us a good idea of unionist reaction to the agreement reached around the Boundary issue. Shearman, writing more than twenty years after the event, puts things in a nutshell when he says that “the close of 1925 […] saw a stabilization in Ireland.” Craig, in his official declaration immediately after the negotiations, claims that, “the prolonged period of anxiety and stress” is over and is pleased to announce a satisfactory conclusion to the negotiations around the border issue. M’Keen, reacting spontaneously to the developing situation, clearly agrees with him: “Waens, it's comin' near the end.” and, “except maybe a wheen ’at cud be pleased wi’ naethan’,” – he presumably means the hard-line republicans, and perhaps, some hard-line unionists - the majority are content – “nearly iverybody’s clappin’ hisel’ on the back.” He goes as far as to represent the agreement reached as paving the way toward “the Millenium for poor oul’ Ireland,” a humorous reference to the biblical prediction (Revelation 20) of a thousand years of peace on earth.\_ "
      -
        type: paragraph
        content:
          -
            type: text
            text: "The authors refer to the conditions arrived at between the three parties to the negotiation.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Clearly, the most important thing for all three authors is that “the boundary was to remain unaltered.” This in itself was a victory for the unionists who had built their opposition to the Boundary Commission on slogans such as, “Not an inch” - the title of another of Shearman’s books - suggesting that they would not give up an inch of territory to Dublin.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Whereas Shearman explains the financial aspects of the agreement in a matter-of-fact way, M’Keen takes a more humorous approach saying that, whatever it cost, “I weel b’lieve John Bull wud be gled tae dip intae his troosers pockets tae his knees, tae get red o’ the Irish bother.” He even suggests that the Northern Ireland Government might have tried to get a bit more money out of London while it was at it: “It was nearly a peety tae let him aff sae chape, but it’s dane noo.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The last aspect, concerning the Council of Ireland, is only mentioned by Shearman. This refers to an institution foreseen under the terms of the Government of Ireland Act, 1920.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The Council was to have brought together representatives from both parliaments in Ireland and would have been a forum for discussion and action on matters of common concern. The Act foresaw that, at some point in the future, the two parliaments might choose to turn the Council into a single parliament for the whole of Ireland. However, the Council of Ireland never met because the Southern Parliament, boycotted by Sinn Féin, was never operational. The agreement reached on the border in 1925 abolishes the Council altogether. This was another source of satisfaction for the unionists.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "M’Keen, with typical frankness, deals with an important aspect of the question that neither Craig nor Shearman mentions. He shows us that it was not only a person's politics that affected the way they saw the implications of the Boundary Comission: where they lived also played a key role in their perception of the outcome. Thus, he says: “Alang the boundary it has been kin’ o’ a fricht; farther in, it’s juist a feelin’.” Like Craig and Shearman, M’Keen is clearly speaking from a unionist perspective. When he talks about those, “alang the boundary,” he is presumably referring to the unionists living near the border on the Northern Ireland side. He says that while “it has been kin’ o’ a fricht” for them, not knowing whether or not their area was going to be allowed to remain in Northern Ireland, for those “farther in,” i.e. unionists living away from the border in places like Ballymena, there was less cause for concern: “it’s juist a feelin.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "However, he does not mention the unionists living near the southern side of the border who had hoped that their areas might end up being attached to Northern Ireland. The situation of the unionist minorities in the Free State was indeed a sensitive issue which none of our authors seem prepared to deal with.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Nor does M’Keen mention the large number of nationalists who had hoped that the Commission would have placed their area in the Free State.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Clearly, the dominant emotion that emerges from all three authors is that of relief not only that an agreement has been reached but also that it has emerged in what is presented as such a cordial environment. Craig, who is “fully satisfied with the outcome of the negotiations,” underlines how the “signatories to the agreement separate with a cordiality which […] may result in more friendly relations being permanently maintained throughout Ireland.” Similarly, Shearman’s analysis underlines the “atmosphere of great cordiality” of the negotiations and records that the three governments “resolved mutually to aid one another in a spirit of neighbourly comradeship.” M’Keen picks up on this optimism when he pleads: “Let naethin’ be dane noo tae mak’ the ‘turn-oot’ onythin but a success.”'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The “stabilization” that Shearman announces in his text confirms the existence of Northern Ireland in the same six-county format as had initially been laid down in the Government of Ireland Act which became the basis of the Constitution of Northern Ireland (see opposite).\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'First page of the Constitution of Northern Ireland, in Sir Arthur S. Quekett, LL.D., The Constitution of Northern Ireland, Part II, the Government of Ireland Act, 1920 and Subsequent Enactments, Belfast, His Majesty’s Stationary Office, 1933, p. 1.'
    uuid: 8213886f-2e8a-4816-87b5-274b30d7fa9e
    type: content
    image: constitution.png
    enabled: true
    allow_zoom: true
book_title: 'The Boundary Question'
example_number: '06'
---
