---
id: b17a70a9-c081-4c9d-b39c-b8cfb71b3448
blueprint: module_3
title: 'Myth & Fact'
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654273919
parent_unit: second_home_rule_bill
audio: Never-Gonna-Give-You-Up-Original.mp3
unit_content:
  -
    image: Isabella-Tod.png
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Isabella Tod was born in Edinburgh on May 18, 1836. Her father, James Banks Tod, a Scot, was a merchant in the city. Her mother, Maria Isabella Waddell, belonged to a well-known Presbyterian family in County Monaghan. When her father died, her mother decided to return to Ireland and she and her daughter settled in Belfast. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'An obituary in The Northern Whig (9 December 1896) underlines her strong attachment to both her Irish and Scottish roots: “It was with pride she recalled how one of her maternal ancestors signed at Holywood in 1646 the copy of the Solemn League and Covenant sent from Scotland to those who, for conscience sake, had fled to Ireland and she would point out his signature, bold and purposelike, amongst those historic names.” '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ' As we shall see, these links between Ulster and Scotland form the basis of her conception of Britishness'
          -
            type: text
            marks:
              -
                type: bold
            text: '. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Tod was a life-long campaigner, speaking on platforms throughout the United Kingdom, publishing articles in newspapers and magazines and lobbying Parliament on a wide range of issues, especially those affecting women. Her influence, both intellectual and practical, went well beyond the province of Ulster. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'She was deeply involved in the Temperance movement which sought not only to discourage the consumption of alcohol but also to deal with the problems of poverty and domestic violence that it produced. She was also committed to women’s education, publishing On the Education of Girls of the Middle Classes in 1874. She was behind the creation of a number of schools for girls across Ireland and fought to ensure that women would be able to compete as equals with men in both secondary and higher education. However, she is perhaps best known for her involvement in the movement for women’s suffrage, helping to found the North of Ireland Women''s Suffrage Association. As a result of her campaigning, women living in Belfast obtained the municipal franchise in 1887, eleven years before other towns in Ireland. '
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Tod was a committed unionist. Although she had been a Liberal all her life, like the majority of Ulster Liberals, she was horrified by Gladstone’s decision to support Home Rule. She spent the last ten years of her life defending the unionist cause, founding the Liberal Women''s Unionist Association in Belfast in 1886, writing essays and letters to the press and addressing meetings not only in Ireland but throughout the United Kingdom. Like many others in the Liberal tradition, she was convinced that belonging to the United Kingdom was the best way to ensure the continued social progress that she had fought for all her life. She was equally convinced that a Home Rule Parliament in Dublin would lead to a narrower, more restrictive society, under increasing clerical influence.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' “Death of Miss Isabella M.S. Tod,” The Northern Whig, 9 December 1896.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Portrait of Isabella Tod by unknown photographer - '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://recollections.biz/blog/10-interesting-insights-isabella-tod/'
                          rel: null
                          target: null
                          title: null
                    text: 'https://recollections.biz/blog/10-interesting-insights-isabella-tod/'
                  -
                    type: text
                    text: ', Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=69924019.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://commons.wikimedia.org/w/index.php?curid=69924019.'
    uuid: ab8328d6-11b5-4215-be4b-16ef27311373
    type: content
    enabled: true
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following extracts are taken from an essay written for The Liberal Unionist, a periodical published by the Liberal Unionist Association. '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: " Although the first Home Rule Bill had been defeated in June the previous year, it was clear that the issue of Home Rule was not going to go away. It was therefore important to keep up the pressure on the unionist side by contributing to the debate.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'England and all its Colonies are full of Irish men and women; and Ireland has almost as many people of English and Scotch extraction deep-rooted in her soil. Irish Liberals feel that all the arguments in the world [...] could not make the Celts and Catholics of Ireland any more their countrymen than the Saxons and Protestants of Great Britain are; or enable them to form an arbitrary partnership with the former separate from the latter. Yet the presence of the mixed population of Ireland is not due only or chiefly to any wholesale modern migration and displacement of the original inhabitants. At the time of the invasion of Henry the Second [1169] the island was not half peopled; and yet, even then, it was not purely Celtic, but mixed. The early history of every country is one of successive colonisations; but too much attention has been paid to that which was led by a king and blessed by a pope '
              -
                type: text
                marks:
                  -
                    type: bold
                text: (2);
              -
                type: text
                text: ' and too little to the far larger, but more gradual interfusion of races, which went on both before and after that event. In particular, the intercourse between the north of Ireland and the south and west of Scotland and north of England has been great and constant, not for two hundred years, but two thousand. In the dawn of history, in the glimpses we can get through the poems which have survived from pagan times, Scottish immigration and emigration are perpetual. Feuds and alliances are as near and vivid with the Scottish chiefs as with any Irish and with no more recognition of a supreme claim on the one side than the other [...] '
          -
            type: paragraph
            content:
              -
                type: text
                text: "It is impossible to disentangle Celts from Saxons in Ireland as in England or America [...] The dangers of merely Celtic life are an intensifying and stereotyping of [...] adherence to tradition, and indifference to the rest of the world [...]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "Irish Liberals [...] have never given up their hold either of Ireland or England [...] They will still spend their warmest affection [...] upon the land and the people that are nearest to them; but none the less they will keep their birthright in the Imperial Parliament; they will claim to live under laws made by the representatives of a great mixed nation, open to the vast tidal waves of the world’s thought, and not by a permanent little provincial minority; they will continue to share the heritage of empire which they have helped to gain; and they will go on [...] to weave a thousand ties [...] of friendship and co-operation [...] between the two islands, whose history [...] has been far too closely interwoven by Providence for the wilfulness of man to tear them asunder now.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Isabella M. S. Tod, “Myth and fact,” The Liberal Unionist, June 1st 1887, pp. 146-147.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(2) '
                  -
                    type: text
                    text: 'Tod is referring to the initial Anglo-Norman invasion of Ireland in the 12th century. The King was of course Henry II of England, and the Pope was Adrian IV, the only Englishman to have occupied the papal office.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Isabella M. S. Tod (1836–1896) by Marguerita Rosalie Rothwell, owned by NMNI. Item Ref: NI'
                  -
                    type: text
                    marks:
                      -
                        type: italic
                    text: NMNI
                  -
                    type: text
                    text: BELUM_U4493-001.
    uuid: 8d8d4165-2cdf-4e9c-8c08-f9b6da9c31db
    type: content
    enabled: true
    image: NI_NMNI_BELUM_U4493-001.jpeg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Tod’s main idea in this extract is that the populations of Great Britain and Ireland have been inextricably linked together for centuries. This interaction spills over into the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'British Empire '
          -
            type: text
            text: "which, she says, was built by the combined efforts of the peoples of these islands. She goes on to say that, as an Irish Liberal, and a unionist, she could never choose between “the Celts and Catholics of Ireland” and “the Saxons and Protestants of Great Britain”; she considers both, equally, as her “countrymen.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The article is therefore designed to challenge what she sees as the arbitrary choices the “separatism” of Irish Home Rule is trying to impose.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "She claims that this “mixed population” is not a recent phenomenon. On the contrary, its roots go back to “the dawn of history.” She challenges nationalist representations of history by claiming that the Anglo-Norman invasion of Ireland in the late 12th century did not produce a “wholesale [...] displacement of the original inhabitants,” and that in any case, the existing (reduced) population – “the island was not half peopled” – was not exclusively Celtic, as is often claimed, but was already “mixed.” She claims that too much attention has been paid to the 12th century invasion by Henry II, and not enough to the “more gradual interfusion of races, which went on both before and after that event.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Here, she focuses particular attention on the “intercourse between the north of Ireland and the south and west of Scotland and north of England.” Interestingly, Tod does not focus on the more usual frame of '
          -
            type: text
            marks:
              -
                type: bold
            text: 'Presbyterianism '
          -
            type: text
            text: 'associated with the Plantation period. Rather, she sees the specific relationship between Ulster and Scotland that had been going on “not for two hundred years, but two thousand,” as being at the very heart of her sense of Britishness. She refers to the early legends of the Ulster Cycle – she is thinking of figures like Cuchulainn, Scáthach, Deirdre, etc. – which, she says, illustrate the “perpetual” nature of “Scottish immigration and '
          -
            type: text
            marks:
              -
                type: bold
            text: emigration
          -
            type: text
            text: "” between Ulster and Scotland. She sees this literary production as reflecting a political reality in which “feuds and alliances are as near and vivid with the Scottish chiefs as with any Irish.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The basic aim of this passage is to question representations of Ireland as being a nation whose Celtic past makes it entirely separate from an allegedly “Saxon” Britain. Indeed, Tod sees such separatist thinking as both divisive and dangerous. Like many others in the unionist camp, she is afraid that a Celtic Ireland would result in an inward-looking and reactionary country, “indifferen[t] to the rest of the world,” and hostile to the social progress that Tod sees as the fruit of Ireland’s place within the United Kingdom.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In the final paragraph, Tod returns to the fact that she refuses to make choices between the different strands of her identity. Although she will continue to look with “warmest affection” on Ireland, she is determined to maintain her “birthright in the Imperial Parliament.” She sees herself as belonging to what she calls “a great mixed nation,” and states that she prefers to live in a country whose laws are made by people “open to the vast tidal waves of the world’s thought,” rather than by what she calls “a permanent little provincial minority.” This barbed comment reflects her conviction that Home Rule will produce a self-perpetuating nationalist élite that will impose a narrow vision of Irishness. Clearly, she is afraid that the progressive stance that she has defended all her life in areas like education and women’s rights would be under threat in what she fears will be the narrower mind-set of an Irish parliament.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Her concluding remark to the effect that the historic interdependence between Britain and Ireland is due to “Providence” suggests that this same Providence will ensure that they will not be “torn asunder now.”\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: "Deirdre's Lament, from the painting by J. H. Bacon A.R.A., in Charles Squire, Celtic Myth and Legend, Poetry and Romance, London, The Gresham Publishing Company, n.d., opposite\_p. 198."
    uuid: 2df2d1f7-099f-46c8-ae3e-b6d9d3357031
    type: content
    enabled: true
    image: '4-Deirdre''s-Lament-by-J-H-Bacon.png'
    allow_zoom: true
book_title: 'Myth & Fact'
parent_module: 2b84ddf4-6861-4fdd-a014-1a8e9aba79c4
example_number: '01'
---
