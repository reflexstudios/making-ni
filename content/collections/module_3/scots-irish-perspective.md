---
id: e61ff15e-62dc-425e-8510-112ba4d25c77
blueprint: module_3
title: 'The Scotch-Irish Perspective'
parent_unit: second_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654872503
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As we saw in Modules 1 and 2, the creation of such structures as the Scotch-Irish Society of America reflected a growing interest in the history of the Ulster Scot in North America. The annual meetings of this Society, the first of which was held in Columbia, Tennessee, in May 1889'
          -
            type: text
            marks:
              -
                type: bold
            text: ', '
          -
            type: text
            text: "attracted interest across the United States and Canada, with speakers delivering papers on the history of Scots-Irish immigration in the various States as well as on their contribution to the construction of the American republic. Given the political tensions in Ireland around the question of Home Rule, these meetings saw a growing number of references to the contemporary situation in Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Rev. John MacIntosh, whom we have already come across in Module 2 (see: Victim of Discrimination), was born into an emigrant Ulster-Scots family in Philadelphia. The family returned to Ulster where he was ordained into the Presbyterian ministry. After serving as minister in May Street Presbyterian Church, he later returned to Philadelphia where he became a prominent member of the Scotch-Irish Society of America.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following extract is taken from a speech entitled “The Making of the Ulsterman,” which he delivered in Pittsburg, Pennsylvania, in 1890, during the second congress of the Scotch-Irish Society of America. '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: " The text is of interest in that it provides us with a new take on the notion of kith and kin, which was such an important element in the arguments used by the anti-Home Rule campaign not only in Scotland but also in North America. Here, he is addressing an American audience, many of whom would have been born in America and, unlike himself, might not have had any direct experience of the “old countries.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "MacIntosh sets the scene for his talk by painting a picture of a conversation with two friends, Presbyterian ministers like himself, one of whom is a Scot and the other an Ulsterman. Together they represent the “three lands, three houses, three histories” of the Scotch-Irish community. The fact that the conversation should take place “in a county Down churchyard” is of interest in that it reminds us of the way John Harrison, in his book, The Scot in Ulster, published two years earlier in 1888 (See Module 2, Example 1, Kith and Kin), used a similar site to focus his reader’s attention on the globalised Ulster-Scots network that emerged out of emigration.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' Rev. John S. MacIntosh, “The Making of the Ulsterman,” in, The Scotch-Irish in America, Proceedings and Addresses of the Second Congress, Pittsburg, Pennsylvania, May 29-June 1, 1890, Cincinnati, Robert Clarke & Co., 1890, pp. 85-106.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: 'Rev. John MacIntosh. Frontispiece to the Fourth Congress of the Scotch-Irish in America, Proceedings and Addresses, Nashville, 1892.'
    uuid: 0ef5773a-6198-419a-91af-8eb8a03d9103
    type: content
    enabled: true
    image: Rev-John-MacIntosh-min.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: 'MacIntosh on the three cousins/brothers '
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'In a county Down churchyard, where the plain forefathers of the hamlet [were buried], we sat, three of us, all clergymen, one bright forenoon in July some fifteen years ago. '
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Bright - bright is indeed a word most poor and wholly dishonest to portray one of those all-rare, ideal days that now and again visit [...] the majestic coast of Northern Ireland; days when the [...] unique beauties of the Antrim shores are dazzlingly unveiled by a very flood-tide of brilliancy [...] '
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We looked across the silver streak of the shimmering sea that lay between the fatherland of the Scottish "Lowlands" and the "school-lands" of Ulster; we saw the fishing boats on the Galloway shores; we occasionally caught a flash of light from some window-pane, and we saw whence the "planters" came to Ulster, and how. '
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We ourselves were the very living story; we three told the three lands of the Scotch-Irish, the three lives, the three tales; for one of us was a Lowlander from old Dumfriesshire, Scotland, and the second was a typical Ulsterman from the egg-like hills of Down in Ireland, and the third was a Scotch-Irishman born on the banks of our own Schuylkill, cousins all by race, so near in likeness and yet so far off and distinct that each was a type of his own branch of the common stock. '
          -
            type: paragraph
            content:
              -
                type: text
                text: 'There we were, a very evolution in history. '
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We thought, and talked; we fought good-naturedly, each for the superiority of his own branch, and laughed with kindly merriment at our own and our brothers'' follies. But the great racial facts were unquestionable; of one blood were we, yet of three lands, three houses, three histories. The Ulsterman is Scot, and yet by no means just the Scot of the Lowlands: the Ulster-man is Irish, and yet wholly other than the Celt of Connaught; new and fresh is he, and newer and fresher still the Scotch-Irishman of this land. […] (MacIntosh, p. 85).'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': : Title page of The Scotch-Irish Society of America, Proceedings of the Scotch-Irish Congress at Columbia, Tennessee, May 8-11, 1889, Cincinnati, Robert Clarke & Co., 1889.'
    uuid: bf3840fe-a7af-490e-b027-adfc59080841
    type: content
    enabled: true
    image: Congress.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "MacIntosh insists on the fact that he can “see” Scotland clearly from where he is in North Down. He mentions the “fishing boats on the Galloway shores” and the fact that “we occasionally caught a flash of light from some window-pane.” This is a living landscape, one in which the everyday activities of the Scottish “neighbours” are clearly visible. What he refers to as “the fatherland of the Scottish ‘Lowlands’” is a reality that you can almost touch. Sitting in the churchyard and looking out over the sea is in itself a history lesson. It shows him how easy it was for the “planters” to come to Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'MacIntosh is eager to get across the idea that, although the Lowlander, the Ulsterman and the Scotch-Irishman represent “the three lands of the Scotch-Irish, the three lives, the three tales,” they were all “of one blood” and belonged to a “common stock.” The fundamental message he is trying to get across is therefore that of a common heritage. This idea of a shared genealogy – he says, “of one blood were we” and refers to his friends and colleagues as “cousins” and indeed “brothers” – is of primary importance to MacIntosh’s representation. We can imagine that it is particularly important for an American audience. MacIntosh is saying that although they are far apart geographically from both Scotland and Ulster, and although they are strikingly different because of that distance, they are nonetheless tied together inseparably by the ties of blood. They are “'
          -
            type: text
            marks:
              -
                type: bold
            text: 'kith and kin'
          -
            type: text
            text: ".”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Texts such as this were of vital importance in creating a shared imaginary between the scattered representatives of the Ulster-Scots diaspora. Just as it was important for those unionists addressing audiences in Scotland to encourage them to remember long-standing connections, so it was equally important for Scotch-Irish activists in America to underline the common history and identity shared across the diaspora that emerged out of '
          -
            type: text
            marks:
              -
                type: bold
            text: emigration
          -
            type: text
            text: ". It was also of vital importance that MacIntosh’s audience should understand that although the various elements of the Scotch-Irish network are closely linked together, they are “wholly other than the Celt of Connaught.” In other words, the Scotch-Irishman is not to be thought of as an “Irish-American.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "This focus is of particular importance when it comes to putting forward the unionist position to what was considered to be one of the most potentially favourable audiences in America, the Scotch-Irish Society.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Advertisement for ships to America, and elsewhere, that appeared in the Business Directory of Belfast, 1865-66.'
    uuid: c335e70c-bc02-4e61-91c1-a57c71c10e9a
    type: content
    enabled: true
    image: Emigration.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "A Scotch-Irishman is first and foremost an American\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "People clearly felt that it was important to underline that identifying oneself as Scotch-Irish did not in any way throw into question a person's wholehearted commitment to America.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'This issue comes up in the following passage from a speech entitled, “The changes of a century, or, Ulster as it was and as it is,” by Professor George Macloskie, delivered a few years later at the Eighth Congress of the Scotch-Irish Society of America, at Harrisburg, Pennsylvania, in June 1896. '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
          -
            type: text
            text: "We have already come across Professor Macloskie in Module 1. Indeed, you might wish to take another look at what he had to say in the speech he delivered at the first Congress of the Scotch-Irish Society in Columbia, Tennessee, May 1889.\_(See Module 1, The Scotch-Irish Congress)."
      -
        type: paragraph
        content:
          -
            type: text
            text: "Unlike MacIntosh, Macloskie had been born in Ireland and, after a period as minister there, had come to America to teach natural history, later biology, at Princeton in 1875. His analysis, like that of MacIntosh, has all the greater weight in America because he has direct knowledge of the situation on the ground in Ireland.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Our Scotch-Irish Society in the United States bears its only allegiance, under God, to the star-spangled banner, and we want not to be half British, but entirely American; but we dearly love the friends and scenes that we have left behind us; we love the old flag of England all the more because it has ceased to be a symbol of civil and religious injustice, and is now known to symbolize civil equality, complete civil reform, and political liberty everywhere over the whole world [...]'
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The determined attitude taken by Ulster in opposition to Home Rule, as contrasted with their participation in the rebellion of 1798, finds its explanation in the conviction that the attitude of the British Government has changed for the better, while any independent government which the majority of the Irish voters would be able to establish would be a change greatly for the worse. [...] They [i.e. the ‘Ulster people’] have, by long and peaceful struggles, shaken off the tyranny of landlordism over their votes as well as their property, and the more galling tyranny of ecclesiasticism over their consciences, they are determined not to permit the reestablishment of a worse ascendancy by a majority who have shown no sympathy with the spirit or prosperity of the northern province. Their intense loyalty to England’s crown and constitution, as now represented, is the strongest argument for this determined stand. (Macloskie, pp. 101-103)'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' George Macloskie, “The changes of a century, or, Ulster as it was and as it is,” The Scotch-Irish in America. Proceedings and Addresses of the Eighth Congress, at Harrisburg, PA., June 4-7, 1896, Nashville, Tenn., 1896, pp. 95-109.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    text: "George Macloskie, ca. 1870s. Historical Photograph Collection, Faculty Photographs Series (AC058), Box FAC63. Source\_: "
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://blogs.princeton.edu/mudd/2019/01/this-week-in-princeton-history-for-january-28-february-3/.'
                          rel: null
                          target: null
                          title: null
                    text: 'https://blogs.princeton.edu/mudd/2019/01/this-week-in-princeton-history-for-january-28-february-3/.'
    uuid: 76889cee-bd2b-4fe5-9260-f7691ee1e9df
    type: content
    enabled: true
    image: George-Macloskie.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In the opening part of the extract, Macloskie goes out of his way to emphasise the allegiance of the Scotch-Irish Society to “the star-spangled banner.” Having established the basic premise that the Society is “entirely American” in its political sympathies, he goes on to say that they nevertheless “love the old flag of England.” This declaration contrasts with the formal declarations of political neutrality that we often find in the publications of the Society. Of even greater interest perhaps is the fact that Macloskie explains that his attachment to England lies in the fact that “it has ceased to be a symbol of civil and religious injustice, and is now known to symbolize civil equality, complete civil reform, and political liberty everywhere over the whole world.” This sweeping endorsement of England runs against much of what was said by speakers at earlier Scotch-Irish conferences, many of whom did not hesitate to denounce the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'anti-Presbyterian discrimination '
          -
            type: text
            text: "of the English and especially Anglican authorities as the primary “push factor” in the mass migration of Ulster Scots to America from the early 18th century on. Here, we find one of the key figures of the Society going out of his way to place this in historical perspective and to suggest that it would be a mistake to apply historical frames on to the contemporary situation. The message is: England has changed – “for the better.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'He develops this idea by drawing a parallel with the situation back in Ulster. He contrasts the radically different political choices of the Ulstermen of today and at the time of the “'
          -
            type: text
            marks:
              -
                type: bold
            text: 'rebellion of 1798'
          -
            type: text
            text: '.” Here again, the explanation, Macloskie argues, lies in the fact that “the attitude of the British Government has changed for the better.” The “Ulster people” – read Ulster Scots - have succeeded in “shak[ing] off the tyranny of landlordism over their votes as well as their property, and the more galling tyranny of ecclesiasticism over their consciences.” Their opposition to Home Rule can be explained by their determination to avoid coming under “a worse ascendancy by a majority who have shown no sympathy with the spirit or prosperity of the northern province.” Although Macloskie never mentions the threat of '
          -
            type: text
            marks:
              -
                type: bold
            text: 'religious discrimination '
          -
            type: text
            text: 'directly, the choice of the word “ascendancy” makes it clear that, like Isabella Tod, he sees Home Rule as leading inevitably to the “ascendancy” of the Catholic Church through the influence it has over the majority of the Irish population. According to Macloskie, this “ascendancy” would be worse than that of the Anglican Church. He also refers to another familiar frame – that of '
          -
            type: text
            marks:
              -
                type: bold
            text: 'the prosperity of the northern province'
          -
            type: text
            text: ", suggesting that it is a source of jealousy elsewhere in Ireland and that Home Rule would result in the economic decline of Ulster.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Macloskie concludes this short passage by recalling Ulster’s “intense '
          -
            type: text
            marks:
              -
                type: bold
            text: loyalty
          -
            type: text
            text: '” to England’s crown and constitution. It is clearly important for him to spell things out as the contemporary choices contrast so sharply with much of what the Society’s historians and enthusiasts have to say about the 17th and 18th centuries, the main focus of attention for many of the members of the Society.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: " The Star Spangled Banner,  [Smith, John Stafford] - 1750-1836 (composer) [Key, Francis Scott] -- 1779-1843 (lyricist);\_Library of Congress, Music Division. "
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://www.loc.gov/resource/ihas.100000006.0/?sp=1'
                          rel: null
                          target: null
                          title: null
                    text: 'https://www.loc.gov/resource/ihas.100000006.0/?sp=1'
    uuid: 6f0c88e2-f853-4a8b-9e5b-7a90db26bd96
    type: content
    enabled: true
    image: Banner.jpg
    allow_zoom: true
book_title: 'Scotch-Irish Perspective'
parent_module: 2b84ddf4-6861-4fdd-a014-1a8e9aba79c4
example_number: '03'
---
