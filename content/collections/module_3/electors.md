---
id: 14844afd-88a2-4492-ae39-fb9244ed015c
blueprint: module_3
title: 'The Electors of England & Scotland'
parent_unit: second_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654872601
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "The battle for Irish self-government was to be determined at the polls across the United Kingdom, not in Ireland where the nationalist/unionist split was very largely a foregone conclusion.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The following text published “during the last eight months of 1893,” is an example of unionist material designed for distribution in Great Britain. It is addressed “To the workingmen and other electors of England and Scotland.” '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
          -
            type: text
            text: "The document does not therefore target Scotland in particular. It is nevertheless of interest to us as it is an example of how the campaign in Scotland fitted in to a broader campaign in Britain.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The unionist campaign in mainland Britain had to satisfy two main criteria.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "First of all, advocates of the unionist position had to overcome what they called the “misrepresentations” of the unionist position that had been successfully promoted by the Home Rulers, in particular that the unionists were all hard-line Orangemen and that they were defending the interests of the well-to-do, especially the landed aristocracy.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Secondly, it was clearly not enough to put forward the same arguments as in Ulster. The “electors of England and Scotland” did not necessarily have the same cultural and historical references as the electorate on the other side of the Irish Sea.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'As far as the Irish unionists were concerned, the greatest danger was that the electorate in Scotland and England should think that Home Rule was a purely “Irish” affair, i.e. that it did not affect them directly. Unionists could call on history and on shared religious positions. These themes were important, and had to be underlined. But in the end, factors such as these were not necessarily going to be enough in themselves to swing opinion in their favour. Hence the need to find arguments that were in tune with the everyday preoccupations of the British electorate on the ground. Irish unionists had to find themes that would show that electorate that Home Rule was not something that only concerned the Irish, but that the outcome of the debate was going to affect the way they lived their lives... in Fife... or in Kent.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: ' '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: 'Leaflet No. 99, Seventh series'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: ",\_"
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: “
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: 'To the Workingmen and Other Electors of England and Wales” (1893), in Irish Unionist Alliance, Publications, Volume III, Dublin, London and Belfast, The Irish Unionist Alliance; Dublin, Hodges Figgis & Co, Ltd, [1894?], pp. 233-234.'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Home Rule Map of Ireland, 1894, Public Domain, The Norman B. Leventhal Collection, Boston Public Library, accessed via '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://picryl.com/media/home-rule-map-of-ireland-1e0578'
                          rel: null
                          target: null
                          title: null
                    text: 'https://picryl.com/media/home-rule-map-of-ireland-1e0578'
    uuid: 35bddda8-a8de-4737-a5d6-188d5d72842f
    type: content
    enabled: true
    image: home-rule-map-of-ireland.jpg
    allow_zoom: true
  -
    content:
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'To the workingmen and other electors of England and Scotland'
              -
                type: text
                marks:
                  -
                    type: bold
                text: "\_ "
          -
            type: paragraph
            content:
              -
                type: text
                text: 'THE question that will be placed before you at the next election is the most important that has ever been submitted to the electors of this country. It is probable that the House of Lords will decline to give its consent to the Home Rule Bill until you have had an opportunity of considering this question. You will then be called upon to decide once for all: '
              -
                type: text
                marks:
                  -
                    type: bold
                text: "will you or will you not break up the United Kingdom of England, Scotland, and Ireland by setting up an independent parliament in Ireland?\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "[...]\_"
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: "The people of Ireland are divided on this question.\_"
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'The Irish Home Rulers '
              -
                type: text
                text: "who ask for an independent parliament are the same people who have always taken the side of England's enemies, and who have carried on the system of lawlessness which caused all the murders and outrages in Ireland in years gone by.\_"
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'The Irish Loyalists'
              -
                type: text
                text: ", who entreat you not to withdraw the protection of British laws from Ireland, are the men who have always stood shoulder to shoulder with you against the enemies of Great Britain, and, while living quietly and making no noise, have created Irish industry and Irish prosperity.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "[...]\_"
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: "Will you break up this United Parliament, and hand over Ireland to the men who hate you and all who have stood by you?\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "If you do this you will, without a shadow of doubt, bring about the following state of affairs:–\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "1. You will create an Irish difficulty far exceeding in magnitude any with which you have had to deal before.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "2. You will destroy Irish trade by handing over the industrious to the lawless portion of the people, and you will thus flood England and Scotland with Irish labourers and artisans looking for employment.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "3. You will leave Ireland open to any foreign foe with whom we may some day be at war, as the government of Ireland will then be in the hands of men who have openly avowed their hatred and hostility to England, and who still maintain these feelings.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "4. You will hand over the peaceable and industrious inhabitants of Ireland to the mercy of those men who, when murder and outrage stalked through the land, never gave heed to the bitter cry of man or woman in their hour of deepest distress. This would be a gross breach of national honour.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "[...]\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "Consider this question carefully. The prosperity of England and Scotland, as well as of Ireland, and the lives and liberties of hundreds of thousands of your loyal brethren in Ireland depend upon your votes at the next election.\_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "Act like men, therefore, and vote for the Unionist or Conservative candidates in your constituencies, and defeat the Home Rule Bill for ever.\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Front page of the two page leaflet.'
    uuid: dba89036-7d44-4b4c-9c44-914241e092fb
    type: content
    enabled: true
    image: Ex.-5-To-the-workingmen-and-other-electors-of-England-and-Scotland.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The leaflet is addressed “To the workingmen and other electors of England and Scotland.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Gladstone’s Representation of the People Act (1884) created a uniform franchise throughout the UK for adult male householders and lodgers paying £10 a year. Although women still did not have the vote, the act extended the franchise to around 60% of the adult male population. It was vitally important for parties to produce material aimed specifically at this new electorate. It is interesting to see how the primary target of the leaflet is clearly “workingmen,” the vague “other electors” only coming in second place.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The leaflet is produced by an Irish Unionist organisation, the Irish Unionist Alliance. '
          -
            type: text
            marks:
              -
                type: bold
            text: '(1) '
          -
            type: text
            text: "Also, it deals exclusively with the issue of Irish Home Rule. And yet, it is designed to be distributed to the “electors of England and Scotland.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "We will see how the leaflet tries to tie the issue of Home Rule in to the everyday political preoccupations of this target group. There was little point using the rhetoric used at home. Unionists had to re-think and re-package their arguments for an English or Scottish audience in order to show how this issue was going to affect them in their everyday lives.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The initial line in the leaflet shifts the emphasis away from “the Irish question” towards the much broader issue of the break-up of the United Kingdom. This is of interest since, in theory, Home Rule did not involve Ireland leaving the UK – Home Rule meant a limited measure of legislative autonomy within the UK and the Empire. What unionists are doing here is to project the idea that Home Rule is really only a half-way house to what they see as the inevitable independence of Ireland. They are telling the English and Scottish electorate that the issue is not really about Ireland at all; it is rather about the continued existence of the United Kingdom.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is vital for Irish unionists to get across the message that Home Rule is not merely a “local” or at best a “regional” issue. They are saying that it threatens the very existence of the United Kingdom, and, by extension, that of the '
          -
            type: text
            marks:
              -
                type: bold
            text: Empire
          -
            type: text
            text: ".\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The leaflet goes on to push a theme that is a standard of all the anti-Home Rule campaigns: the idea that '
          -
            type: text
            marks:
              -
                type: bold
            text: 'Home Rule is linked to violence and disorder'
          -
            type: text
            text: ".\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In order to get its message across, the pamphlet takes a straightforward binary approach. On the one hand, we have the Home Rulers who are “the enemies of Great Britain,” and who have been responsible for “all the murders and outrages in Ireland in years gone by.” On the other hand, those opposed to Home Rule, the Irish Loyalists, “have always stood shoulder to shoulder with you” and have been responsible for the creation of “Irish industry and Irish '
          -
            type: text
            marks:
              -
                type: bold
            text: prosperity
          -
            type: text
            text: ".”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The terms of the binary framing are clear. Home Rule is synonymous with those hostile to British interests; their agenda involves the use of systematic violence and generates disorder. Unionism, on the other hand, is synonymous with '
          -
            type: text
            marks:
              -
                type: bold
            text: 'loyalty '
          -
            type: text
            text: 'and with forces that reinforce '
          -
            type: text
            marks:
              -
                type: bold
            text: 'social stability'
          -
            type: text
            text: .
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " The ILPU was renamed IUA in 1891.\_"
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: "Slogan on the verso side of\_Leaflet No. 5, Eighth series, “Bad look out for English Working Man,” in Irish Unionist Alliance, Publications, Vol. IV, Dublin, London and Belfast, The Irish Unionist Alliance; Dublin, Hodges, Figgis, and Company, Limited, [1895], pp. 109-110."
    uuid: c17d2372-a8f3-4e0d-91be-4b62841447b0
    type: content
    enabled: true
    image: electors.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The following section moves on to explore the consequences of an election that would give victory to those in favour of Home Rule. There are a number of key themes.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The first is that such a decision would actually damage the interests of the Irish themselves.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The leaflet says that the result would be the collapse of the Irish economy. But again, it is the knock-on effect of that that interests them most of all: they argue that the collapse of the Irish economy would mean that England and Scotland would be “flood[ed] with Irish labourers and artisans looking for employment.” This is a recurring theme in political debate in Britain, and one that is by no means limited to a conservative reading of social history. Frederick Engels, co-author of The Communist Manifesto, in his History of the Working Class in England, published in 1844 just before the Great Famine, clearly identified the large numbers of poor Irish migrants in English and Scottish cities as being responsible for lowering the wages and working conditions of the British working class.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The strategy here is to suggest that this is not an abstract, philosophical debate about constitutional independence. Their vote will, the pamphlet says, have a direct effect on their lives on the ground in Scotland and England.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The leaflet moves on to suggest that Home Rule would represent a security threat to Britain by handing over power to those hostile to Britain’s interests. Ireland, they say, would be “open to any foreign foe with whom we may some day be at war.” This of course was a theme that was to take on increasing importance in the course of the Home Rule debate. The outbreak of war in South Africa (1899) saw many nationalists give their support to the Boers who were opposing further British expansion in South Africa. The issue of “aiding and abetting foreign powers” becomes a predominant concern during the First World War when Irish republicans were to negotiate with Germany in an attempt to ensure its support for the Rising in 1916.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'A final argument centres on the moral obligation that people in Britain have towards “'
          -
            type: text
            marks:
              -
                type: bold
            text: 'the peaceable and industrious inhabitants of Ireland.'
          -
            type: text
            text: '” Once again, the leaflet alludes to the recent '
          -
            type: text
            marks:
              -
                type: bold
            text: violence
          -
            type: text
            text: " in Ireland, linked especially to the Land War. It suggests that this would become the norm in an independent Ireland.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The leaflet ends by calling on electors to vote Unionist or Conservative to ensure that the Liberals will no longer be in a position to push Home Rule on to the statute book.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Interestingly, the pamphlet never refers to the issue of religion. The focus throughout remains firmly on issues of allegiance, the economy and security. \_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE: '
                  -
                    type: text
                    marks:
                      -
                        type: bts_span
                        attrs:
                          class: null
                    text: 'Leaflet No. 5, Eighth series, “Bad look out for English Working Man,” in Irish Unionist Alliance, Publications, Vol. IV, Dublin, London and Belfast, The Irish Unionist Alliance; Dublin, Hodges, Figgis, and Company, Limited, [1895], pp. 109-110.'
    uuid: 60528916-8dbf-4618-b03c-d10cfa4d8192
    type: content
    enabled: true
    image: Britannia.png
    allow_zoom: true
book_title: Electors
parent_module: 2b84ddf4-6861-4fdd-a014-1a8e9aba79c4
example_number: '05'
---
