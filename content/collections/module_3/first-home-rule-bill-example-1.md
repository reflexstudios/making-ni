---
id: 6d7a8ca9-2d03-49aa-8137-bb65796e8ad4
blueprint: module_3
title: Henderson
parent_unit: first_home_rule_bill
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654163544
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Our first example of the way the Scottish frame was used in anti-Home Rule material is taken from a report of a speech by James Henderson that featured in an article in The Belfast News-Letter on Saturday, 20 February 1886, entitled “The Loyalist Campaign, Great Meeting in Newry.”\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "James Henderson was the owner of the Belfast News-Letter, one of the main unionist daily newspapers in Ulster. He also owned an associated weekly paper, the Belfast Weekly News (see Example 3 in this Unit). Henderson was the treasurer of the predominantly conservative Ulster Loyalist Anti-Repeal Committee, which had been set up a few weeks before at the beginning of January 1886. The organisation, soon to be re-named the Ulster Loyalist Anti-Repeal Union, was designed for the purpose of influencing public opinion by meetings in England and Scotland. It provided a unionist structure focussed specifically on Ulster, distinct from the southern-controlled Irish Loyal and Patriotic Union. The Ulster-based Union, closely associated with the Orange Order and the Protestant churches in Ulster, quickly became known for the forthright nature of its speakers such as Dr. Kane and Rev. Hanna referred to in the speech. Dr R. R. Kane, an Irish speaker, was a Church of Ireland rector and a Grand Master of the Orange Order in Belfast who was well known for his fiery anti-Home Rule speeches. Rev. Hugh Hanna was a Presbyterian minister noted for his radical hostility to Home Rule. His nickname, “Roaring Hugh,” said a lot about his confrontational style. He was one of the founders of the Ulster Loyalist Anti-Repeal Union and, like Kane, was in great demand as a speaker.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The extracts from Henderson’s speech look at two issues which, although separate, are clearly linked in his mind.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Rev. Hugh Hanna, “Roaring Hugh.” Source: '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://www.culturenorthernireland.org/article/544/roaringhugh-hanna'
                          rel: null
                          target: null
                          title: null
                    text: 'https://www.culturenorthernireland.org/article/544/roaringhugh-hanna'
    uuid: 3d1aa521-eca9-4a32-9f07-71be8b8d1795
    type: content
    enabled: true
    image: Roaring-Hugh-min.png
    allow_zoom: true
  -
    content:
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The CHAIRMAN, Who was received with great cheering, briefly thanked the meeting, and called upon Mr. James Henderson, M.A., to move the first resolution. (Cheers.) [...] '
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Mr HENDERSON said - [...] Some of those at the meeting had come there expecting to hear their good friends, Dr. Kane and Dr. Hanna. I exceedingly regret to say that neither of them were able to attend, and in a certain sense I appear to represent them. Dr. Hanna was asked by the Irish Loyal and Patriotic Union - a different organisation to that to which we belong, and which was started in Ulster - to address a meeting in Scotland. It is greatly to be desired that we should stir up the feeling of Scotland in favour of this movement, which we are all now so anxious to promote. (Cheers.) I believe myself that if we can stir up the religious feeling in Scotland we have won the battle. (Cheers.) In the past Scotland sent over a large proportion of the hard-headed, hard-working people whose descendants now peopled Ulster. I attribute principally the comparative prosperity of our province to the hard toil and the great intelligence of the Scotch settlers. Scotland, too, is the stronghold of Mr. Gladstone; and if we excite this feeling among the Scotch, that they ought not to leave us to be destroyed, it will be one of the most important points in our favour. (Cheers.) [...] '
              -
                type: hard_break
          -
            type: paragraph
            content:
              -
                type: text
                text: "But more fatal than any consideration to which attention has yet been called is the firm belief, so often and eloquently expressed at these Ulster Loyalist meetings, that the loyal people of this country would never recognise the authority of the Parliament which is demanded by Mr. Parnell and his most obedient followers. (Great cheering.) It is not for me at present to offer any suggestions as to the propriety or otherwise of any portions of the country assuming that attitude, but this much I will say, without fear of contradiction, that all history proves that what the Protestants of Ireland say they mean, and what they mean they say. (Renewed cheering.) Civil war would follow if such reasoning bore its legitimate fruit; therefore, I think it is hardly necessary for me to state that such a result would be hostile to the best interests of Ireland and I hope it will not become necessary. [...]\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ": Rev. Thomas Hamilton, History of the Irish Presbyterian Church, Edinburgh, 1887, indicating that it is part of a series of \"handbooks\" aimed at “Bible classes and private students.” The first edition of this text had come out at the end of 1886, the year Gladstone’s Bill went through the Commons. That edition sold out in three months. Two other editions appeared in 1887, including this cheap “special edition” designed to “be circulated by the thousand in Ireland.”\_"
    uuid: 70400c8f-3fc6-436e-a285-d99f2172e4da
    type: content
    enabled: true
    allow_zoom: true
    image: Screenshot-2022-05-09-at-13.32.32.png
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: "[NB: Frames noted in Module 2 are indicated in heavy print.]\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "In this short passage – almost an aside at the beginning of his largely improvised speech – Henderson focuses on the importance of “stir[ring] up the feeling of Scotland in favour of this movement.” Clearly, he considers that Ulster’s links with Scotland mean that unionist arguments will be sure to have a particular resonance there.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "He targets three ideas in quick succession.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Significantly, the first association he makes is with religion. “Stirring up” religious feeling in Scotland, he says, will be enough to win the day for unionism. By this he is clearly referring to a shared Protestantism - indeed, he probably means a '
          -
            type: text
            marks:
              -
                type: bold
            text: 'shared Presbyterianism'
          -
            type: text
            text: ' - which, he is convinced, will take precedence over political sympathies. The fact that the crowd should react most enthusiastically to this reference – see “(Cheers.)” – suggests that Henderson is expressing a commonly held conviction that this is indeed the key factor in the equation. His second argument tunes in to the idea of “'
          -
            type: text
            marks:
              -
                type: bold
            text: 'kith and kin'
          -
            type: text
            text: '.” Ulster, he says, is “peopled” with the descendants of the “hard-headed, hard-working” settlers that Scotland had “sent over” in the past. He immediately follows this up with the idea that it is specifically these Scotch settlers who are responsible for creating “'
          -
            type: text
            marks:
              -
                type: bold
            text: 'the prosperity of our province.'
          -
            type: text
            text: "” Henderson therefore carefully avoids any reference to the English component in this settlement and suggests that Ulster as it is today is essentially a Scottish creation.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is striking how Henderson can go through these allusions in such quick succession. It is clear that he feels no need to develop any of these points. On the contrary, he takes it for granted that his listeners in Newry – described as “a large and enthusiastic meeting of the Loyalists of this neighbourhood” - will be familiar with all the necessary historical and cultural references. He only has to allude to the “settlers” and to the “prosperity” of the province'
          -
            type: text
            marks:
              -
                type: bold
            text: ' '
          -
            type: text
            text: 'and his audience will be sure to understand a reference to the Plantation as it is represented in the various church histories of the period. Similarly, when he says, “they ought not to leave us to be destroyed,” he can assume that they will recognise a reference to events such as 1641 when the Scots sent military and financial help to their fellow Presbyterians in Ulster who were faced with '
          -
            type: text
            marks:
              -
                type: bold
            text: 'sectarian violence'
          -
            type: text
            text: ".\_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: IMAGE
                  -
                    type: text
                    text: ': Merchant''s Quay, Newry Co. Down, 1870. '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/wiki/File:Merchants_Quay'
                          rel: null
                          target: null
                          title: null
                      -
                        type: bold
                    text: 'https://commons.wikimedia.org/wiki/File:Merchants_Quay'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ',_Newry.jpg'
    uuid: 0ee5ff7a-592d-4014-ba0f-315ec73c864c
    type: content
    enabled: true
    image: audio/16-Merchants-quay-1870-.jpg
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Henderson goes on to point to what is going to be one of the central problems in the unionist campaign in Scotland – “Scotland [...] is the stronghold of Mr. Gladstone.” Indeed, in the most recent general election (1885), Gladstone’s Liberals had won 51 of the 70 Scottish seats, while the Conservatives, the allies of the unionists, had only won 8. Despite this stark electoral reality, Henderson confidently puts forward the idea that calling on a shared Protestantism and recalling a common heritage between Ulster and Scotland will be enough to swing the Scots against Gladstone and in favour of the unionist position.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Unfortunately, Henderson underestimates the hold Gladstone and his Liberal Party have over Scotland.\_As we shall see, right from the start, unionists would have to work hard to convince the Scottish electorate to reconsider their support for the Liberal Party."
      -
        type: paragraph
        content:
          -
            type: text
            text: 'At the end of the extract, Henderson moves on to an even more sensitive subject. He is quite categorical when he states that “the loyal people of this country” would “never recognise the authority” of [a Home Rule] Parliament.” Although he talks about “the Protestants of Ireland,” when he refers to the possibility of certain “portions of the country assuming that attitude,” he clearly suggests that resistance will be centred on Ulster. These remarks are important as they confirm the feeling that Ulster is a separate political reality. This was already evident in the way the anti-Home Rule campaign there was being structured around the Ulster Loyal Anti-Repeal Union.'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Significantly, he goes as far as to state that “civil war would follow if such reasoning bore its legitimate fruit.” Although the idea of armed unionist resistance to Home Rule is associated chiefly with the third Home Rule Bill – cf. the creation of the UVF and the Larne gun-running later in this Module – it is important to note that the threat of violence was a feature of the campaign from the very beginning. Here, Henderson is tuning in to another frame we noticed in Module 2 – the idea that the Ulster Scot will not hesitate to enter into '
          -
            type: text
            marks:
              -
                type: bold
            text: 'rebellion '
          -
            type: text
            text: 'against the government, “to right some things that [they] thought wrang.”'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' William Ewart Gladstone, 1892. '
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'By Samuel Alexander Walker - '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://www.flickr.com/photos/photohistorytimeline/29865879296'
                          rel: null
                          target: null
                          title: null
                      -
                        type: bold
                    text: 'https://www.flickr.com/photos/photohistorytimeline/29865879296'
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: ', Public Domain, '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'https://commons.wikimedia.org/w/index.php?curid=80163795'
                          rel: null
                          target: null
                          title: null
                      -
                        type: bold
                    text: 'https://commons.wikimedia.org/w/index.php?curid=80163795'
    uuid: 58892a73-4e7d-4af2-a816-7c526b8f985e
    type: content
    enabled: true
    image: 'audio/17-William_Ewart_Gladstone,_1892_(cropped).jpg'
    allow_zoom: true
book_title: Henderson
parent_module: 6492b8fc-ba6c-4bb4-a8d1-dcd14b6d92c3
example_number: '01'
intro_text: '“We should stir up the feeling of Scotland in favour of this movement.”'
---
