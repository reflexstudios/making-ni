---
id: c6762377-24ab-49e1-89ab-80770884a84b
blueprint: module_3
title: Morrison
intro_text: '“A beautiful landscape, bulging with agricultural wealth”'
parent_unit: government_of_ireland_act_1920
updated_by: 7811d47e-7e55-4ac8-831c-7d8bfec14d7a
updated_at: 1654278728
unit_content:
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: 'H.S. Morrison’s book, Modern Ulster: Its Character, Customs, Politics and Industries, was published at the beginning of 1920 '
          -
            type: text
            marks:
              -
                type: bold
            text: (1)
          -
            type: text
            text: ", at a period of growing political violence across Ireland. Despite this increasingly unstable context, the narrative that Morrison provides in his book projects a highly positive image of contemporary Ulster. The picture to emerge is one of progress in every single field of social life – health, housing conditions, safety in the workplace, pensions and education. He constantly compares the situation of the present day with the situation thirty or even sixty years before, underlining the advances that have been made.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Morrison, who was a local doctor, was active in the unionist movement and, like Ronald McNeill, author of another text in this Unit, he was a Member of the Standing Committee of the Ulster Unionist Council. Needless to say, he puts the progress he describes in the book down to the Union. He is quite open about his commitment to structures like the Orange Order and the Ulster Volunteer Force.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "Morrison was a doctor in Aghadowey, a rural area near Coleraine. But, as the Introduction by Rev. D.B. Knox, the Editor of The Irish Presbyterian, makes clear, this was only the tip of the iceberg. He was, at various times, and amongst other things, chairman of Aghadowey Co-operative Creamery, Coroner, elder, Director of the Old Age Fund of the Irish Presbyterian Church and Battalion Surgeon of the Ulster Volunteers of County Derry. All of this meant that he had direct first-hand experience of a wide number of concrete issues affecting local people. \_"
      -
        type: paragraph
        content:
          -
            type: text
            text: "The book is an example of the material that was being produced at the time by people actively involved in the promotion of the unionist cause. Although, when the book was published, Northern Ireland had not yet emerged from the Government of Ireland Act (December 1920), we already have the feeling that people are convinced that things will soon work themselves out. The self-assured tone of the book suggests a confident, self-reliant political culture that is not particularly worried by the growing violence throughout the island.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Once again, the message is that Ulster is different.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: '(1) '
                  -
                    type: text
                    text: "\_H.S. Morrison, Modern Ulster: Its Character, Customs, Politics and Industries, London, H.R. Allenson Ltd., 1920."
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' H.S. Morrison, M.D., used as the frontispiece of Modern Ulster.'
    uuid: d71cc515-f2ed-46fd-bf7b-86de6cb53979
    type: content
    enabled: true
    image: H.S.-MORRISON-1638623081.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            text: "Or, perhaps, are we to understand rather that it is Ulster-Scots territory in Ulster that is different?\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'If we ask the question in these terms it is because the opening chapter, entitled “The Ulster Scot,” would appear to place the entire book under the umbrella of this particular tradition. We are to understand that although the Union may have benefitted Ireland as a whole, the particular success of “modern Ulster” is due to the presence – the predominance? - of this tradition. Throughout, Morrison focuses on the area round Coleraine that he knows intimately, an area with a strong Ulster-Scots influence. However, it is clear that the reader is expected to apply what he says to “Ulster” as a whole. In Morrison’s view, therefore, the Ulster Scot is at the heart of this “modern Ulster.” He makes this clear in sections like “Humorous sayings,” or“Ulster pastimes,” many of which are Ulster-Scots in origin. Take the following humorous passage as an example.'
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'In olden times it was the custom at tea parties to turn your cup upside down when you had enough, as a hint to your hostess that you would not take any more.'
          -
            type: paragraph
            content:
              -
                type: text
                text: "\_“Whammel” is another word conveying the same meaning, upside down, as applied to anything. “Rive” is another word for vomit. At soirees people used to, and still do, drink cup after cup of tea.\_ \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'A lad sat beside his mother at one of these functions and as she finished her tenth cup of tea called to her with evident anxiety, “Whammel, mother, or you''ll rive.” (Morrison, p. 37)'
      -
        type: paragraph
        content:
          -
            type: text
            text: "The following extracts are from the opening section of the book, entitled, “The Ulster Scot.”\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "The Ulster Scot is a Scotchman improved by three hundred years’ residence in Ulster.\_ \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "The Rev. J. S. M’Intosh, [MacIntosh] M.A., at the second annual session of the Scottish Irish Congress at Pittsburg, Pa., May 1890, said: “One of the greatest facts in history is the Plantation of Ulster [which] start[ed] a movement the end of which the world sees not yet. […] \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "“We Americans journey to Plymouth Rock to tell of the landing of the Puritans […]; but let us not forget that the Ulster man has his, and America has a right to know and keep the day of the Ulster landing; by that landing the seat of a new empire has been formed; for imperial […] was that race, that came to Ulster to change it from savage wilds to smiling fields and busy towns.”\_ \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: "In 1912 Sir Edward Carson and his trusty followers in the Imperial Parliament were organising an Ulster Day that will have for this province as much significance as Independence Day has for the United States; and as […] the Scottish emigrants embarked for their hazardous enterprise in the North of Ireland, and in the face of many difficulties made the wilderness blossom as a garden; so to-day when every outlook is gloomy their descendants face the future with an easy mind, confident and self-reliant; seeking no ascendancy, resolved that none shall come, and with a courage that is amazing, trusting in themselves alone, if it should come to that pass, are as ready now as three hundred years ago, to lift to another stage that movement of which […]\_ the world had not seen the end. (Morrison, pp. 17-18). \_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' Drawing of “Donegall Place, Belfast 1870.” In Edwin Darley Hill, Northern Banking Co. Limited, An Historical Sketch, Belfast, M’Caw, Stevenson & Orr, Limited, 1924 (opposite p. 134).'
    uuid: 83cfaed0-54a6-4a2a-b088-e01d0c35c1fd
    type: content
    enabled: true
    image: dp.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: 'It is interesting to note how Morrison begins by referring to the work of the Scotch-Irish Society of America, and in particular to Rev. MacIntosh, whom we came across in Module 2. (See: '
          -
            type: text
            marks:
              -
                type: bold
            text: 'The Ulster Scot: A Victim of Discrimination'
          -
            type: text
            text: ') The fact that Morrison should quote passages from MacIntosh’s speech shows the interconnectedness between the Ulster-Scots and Scotch-Irish communities at the time and how ideas and themes moved across the network from one side of the Atlantic to the other. MacIntosh is looking back to the arrival of the first Scottish settlers in Ulster. He says that Americans celebrate Plymouth Rock, i.e. the site in Massachusetts where the Puritans from The Mayflower first set foot on American soil in December 1620. However, he says that America should equally celebrate the arrival of the Scottish settlers in Ulster because they were to contribute as much to American history as the Puritans. (See: '
          -
            type: text
            marks:
              -
                type: bold
            text: 'The Ulster Scot and Emigration'
          -
            type: text
            text: )
      -
        type: paragraph
        content:
          -
            type: text
            text: 'Morrison picks up on this idea and draws what he sees as a further possible parallel between American and Ulster-Scots history. He claims that Ulster Day, the day of the signing of the Ulster Covenant, will have for Ulster “as much significance as Independence Day has for the United States.”'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (1) '
          -
            type: text
            text: 'He seems to be suggesting that Ulster unionists are so confident in themselves – “fac[ing] the future with an easy mind, confident and self-reliant” – that, rather than agree to be ruled by Dublin, they are prepared to “trust in themselves alone,” an ironic reference to the Irish expression, “sinn féin.”'
          -
            type: text
            marks:
              -
                type: bold
            text: ' (2) '
          -
            type: text
            text: 'In other words, he is suggesting that, just like the American revolutionaries of the 1770s, many of whom were Scotch-Irish, unionists too could opt for independence – for Ulster. Returning to the theme of the Plantation, he goes on to say that, just as the settlers “made the wilderness blossom as a garden,” so their descendants in present-day Ulster are confident that they can do the same. (See: '
          -
            type: text
            marks:
              -
                type: bold
            text: 'The Ulster Scot and Material Prosperity'
          -
            type: text
            text: )
      -
        type: paragraph
        content:
          -
            type: text
            text: "Throughout the book, Morrison returns to the idea that the lives of the ordinary people have been improved thanks to the legislative Union with Britain. The following passage is typical of the material that appears throughout the book. The text is accompanied by a number of photographs taken by a local photographer specially for the publication.\_"
      -
        type: blockquote
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: "In illustration No. 3, page 64, you see the double cottages built by the Rural Council, Coleraine Union. We have ninety of them in the district, and only for the War we would have had more. The cottages have half an acre of ground. A pair of cottages cost in all £340, and are let at 1s. 3d. per week,\_ plus 11s. for taxes. Properly cultivated, as nearly all are, the ground would produce oats and potatoes value for the rent two or three times over at present prices. The pictures show the\_ modern rambler roses and flower beds, and turf-stacks and pump. Just across the road are four more cottages, built on land taken from my property, and these “Aghadowey Cottages,” neat and trim, with plots growing luxuriant crops of oats, potatoes, onions, and other vegetables, the homes of a healthy, virile people, content, prosperous, and happy, form a very pleasing picture in a beautiful landscape, bulging with agricultural wealth, embroidered and beautified by the bright green of the\_quickly growing grass of the hay fields, by the potato crop, as fresh and free from disease as in July, and above all by the waving fields of golden grain and ripening flax and the commencing autumnal tints on the foliage of trees and hedgerows.\_ (Morrison, pp. 109-110) \_"
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (1)
                  -
                    type: text
                    text: " \_Independence Day, 4th July, is the day on which the 13 American colonies adopted Jefferson’s, Declaration of Independence (1776)."
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: (2)
                  -
                    type: text
                    text: ' Sinn féin means “ourselves,” but, when used to designate the political movement, is often mistranslated as “ourselves alone.”'
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' "New or District Council cottage," in H.S. Morrison, Modern Ulster, opposite p.64.'
    uuid: 4a8caabc-173d-4031-9d65-900f13335292
    type: content
    enabled: true
    image: image1.png
    allow_zoom: true
  -
    content:
      -
        type: paragraph
        content:
          -
            type: text
            marks:
              -
                type: bold
            text: '[NB: Frames noted in Module 2 are indicated in heavy print.]'
      -
        type: paragraph
        content:
          -
            type: text
            text: "This passage is typical of Morrison’s idealised representation of the progress he associates with belonging to the Union.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'An essential part of his strategy consists in providing visual evidence to back up what he says. However, he also incorporates highly detailed information into his description and commentary. Thus, he tells us exactly how many cottages have been built, the amount of public money invested in their construction, the price of the rent and the amount of taxes due per annum. He goes on to say that “properly cultivated, as nearly all are, the ground would produce oats and potatoes value for the rent two or three times over at present prices.” In other words, he is saying that the local Council is providing what amounts to free accommodation to its tenants. If the tenant exploits the land attached to the cottage to the full, he can pay his rent and make a surplus into the bargain. His remark that “nearly all” the plots are “properly cultivated” confirms the image of self-help and '
          -
            type: text
            marks:
              -
                type: bold
            text: 'material prosperity'
          -
            type: text
            text: " that he sees as characteristic of the Ulster Scot.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'His commentary also tunes in to another frame that we remarked in Module 2 – the notion of the '
          -
            type: text
            marks:
              -
                type: bold
            text: 'cross-class solidarity'
          -
            type: text
            text: " that supposedly exists within Ulster-Scots society. The cottages show how the farm labourer’s life is being improved by increasing prosperity and better living conditions. When he says that, “just across the road are four more cottages, built on land taken from my property,” he is pointing to the fact that those in the community like himself who are better-off contribute – apparently willingly – to this improvement.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'The final sentences take on a radically different tone. We move away from hard facts and statistics and into an impassioned description of an idyllic rural landscape. This landscape is “beautiful […] bulging with agricultural wealth, embroidered and beautified by the bright green of the quickly growing grass of the hay fields.” The potato crop is “fresh and free from disease,” the plots produce “luxuriant crops of oats, potatoes, onions, and other vegetables,” and the fields are full of “golden grain and ripening flax.” It is no coincidence that Morrison should have chosen to draw parallels in the earlier passage between the Plantation period and the contemporary situation in Ulster. What he says is almost a replication of the descriptions of the early Plantations that we saw in Module 2. What he is saying is that, “as three hundred years ago,” the Ulster Scot is custom-built to produce social progress and material prosperity. Whatever happens politically.'
      -
        type: paragraph
        content:
          -
            type: text
            text: "Publications such as Morrison’s text, that came out several months before the Government of Ireland Act, make it clear that those behind the unionist resistance to a Dublin parliament were by no means intimidated by the challenges they faced, however the political game worked itself out in London.\_"
      -
        type: paragraph
        content:
          -
            type: text
            text: 'In many ways, his text confirms the confident assertions in the previous example by Benet. As he had suggested, the Ulster Scot is indeed capable of standing on his own two feet.'
      -
        type: set
        attrs:
          values:
            type: notes
            notes:
              -
                type: paragraph
                content:
                  -
                    type: text
                    marks:
                      -
                        type: bold
                    text: 'IMAGE:'
                  -
                    type: text
                    text: ' “Vessels alongside Donegall Quay, 1880” and “Cross Channel Steamers alongside Donegall Quay, Present Day,” in D.J. Owen, A Short History of the Port of Belfast, Belfast, Mayne, Boyd & Son. Limited, 1917 (opposite p. 72).'
    uuid: 0bd8b364-dae8-4dbc-841e-a46df5adaa12
    type: content
    enabled: true
    allow_zoom: true
    image: vessels.png
book_title: Morrison
parent_module: b3bc48fa-27a8-467f-86cb-366253976041
example_number: '02'
---
