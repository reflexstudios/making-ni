# Making NI 
DEV: https://makingni.reflex-dev.com/

username: hello@reflex-studios.com
password: reflexstudios

## Setup
- git clone git clone git@bitbucket.org:reflexstudios/jnr-clothing.git
- cd /sitename
- composer install
- cp .env.example .env && php artisan key:generate
- If using Tailwind JIT mode make sure package.json has - "watch": "TAILWIND_MODE=watch mix watch",

## Valet:
- valet link
- valet park
- valet open
## Dev:
- npm install
- npm run dev
- npm run watch - If using Tailwind JIT mode make sure package.json has - "watch": "TAILWIND_MODE=watch mix watch",

## Dev Site 
makingni.reflex-dev.com
username: devmakingni
password: =2aWkKKZxEvr
ssh -p 4835 devmakingni@185.211.22.175
## Important Links

- [Statamic Main Site](https://statamic.com)
- [Statamic 3 Documentation][docs]
- [Statamic 3 Core Package Repo][cms-repo]
- [Statamic 3 Migrator](https://github.com/statamic/migrator)
- [Statamic Discord][discord]

[docs]: https://statamic.dev/
[discord]: https://statamic.com/discord
[contribution]: https://github.com/statamic/cms/blob/master/CONTRIBUTING.md
[cms-repo]: https://github.com/statamic/cms
